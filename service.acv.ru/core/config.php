<?php
/**
 * Конфигурация сайта
 */
class Config
{
    /**
     * Подключение к БД.
     */
    public static $db_dsn    = 'mysql:dbname=serviceacv;host=localhost';
    public static $db_user   = 'serviceacv';
    public static $db_pass   = '6I8x081pzOzJmggShjsm';
    public static $db_prefix = 'z_';

    /**
     * Режим отладки (0 или 1).
     * - вывод ошибок PHP в браузер;
     */
    public static $debug  = 1;

    /**
     * Кэширование.
     */
    public static $cache = false;

    /**
     * 404-я страниц если в URL есть GET параметры (кроме админки).
     * Чтобы разрешить параметры, нужно их указать в экшене модуля: Router::allowGET(array('search'));
     */
    public static $allow_get = false;

    /**
     * Основной домен (без слеша на конце).
     */
    public static $url_base = 'http://service.acv.ru';

    /**
     * Расширение страниц ('/', '.html'...).
     */
//    public static $url_ext = '/';
    public static $url_ext = '';

    /**
     * Адрес админки.
     */
    public static $url_admin = 'admin_acv';


    /**
     * Уровни доступа пользователей.
     */
    public static $level = array(
        3 => 'СЦ',
        4 => 'Администратор'
    );

    // Цвета для админки
    public static $level_color = array(
        3 => '#2cff75',
        4 => '#aa60ff'
    );

    public static $stage = array(
        1 => 'новый отчет',
        2 => 'отчет в работе',
        3 => 'отчет закрыт'
    );

    public static $stage_color = array(
        1 => 'white',
        2 => 'yellow',
        3 => 'green'
    );

    public static $step = array(
        1 => 'Обработка отчета',
        2 => 'Недостаточно информации',
        4 => 'Гарантийный случай',
        3 => 'Не гарантийный случай',
        5 => 'Компенсация выезда',
        6 => 'Коммерческая замена',
        7 => 'Не гарантийный случай с компенсацией выезда',
    );

    public static $questions = array(
        1 => 'Выполнено ли заземление внутреннего бака за патрубок?',
        4 => 'Установлено ли устройство стабилизации электросигнала?',
        2 => 'Предохранительные устройства установлены в соответсвии с инструкцией?',
        5 => 'К предохранительному клапану подключена дренажная линия?',
        3 => 'В качестве теплоносителя используется антифриз?',
        6 => 'Является ли данный антифриз сертифицированным  ACV?',
    );

    /**
     * Мета-тег "robots"
     */
    public static $robots = array(
        ''         => '',
        'noindex'  => 'noindex — не индексировать текст',
        'nofollow' => 'nofollow — не переходить по ссылкам',
        'none'     => 'none — не индексировать текст и не переходить по ссылкам'
    );

    public static $types = array(
        0 => '',
        1  => 'Бойлеры',
        2  => 'Котлы',
        3  => 'Горелки'
    );

    public static $types_letter = array(
        0 => '',
        1  => 'Бойлера',
        2  => 'Котла',
        3  => 'Горелки'
    );

    /**
     * Изображение - заглушка
     */
    public static $caps = '/themes/site/img/caps.png';

    /**
     * Размеры для генирации превьюшек.
     */
    public static $thumbs = array(
        'works' => array(
        ),

    );
}