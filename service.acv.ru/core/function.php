<?php
/**
 * Автозагрузка классов.
 */
spl_autoload_register('autoload');
 
function autoload($name)
{
	if (file_exists(CORE_DIR . '/classes/' . mb_strtolower($name) . '.php')) {
		include_once CORE_DIR . '/classes/' . mb_strtolower($name) . '.php';
	}
}

function dd($var)
{
	var_dump($var);
	exit();
}

/**
 * Редирект, установка сообщения в сессию.
 * 
 * @param string $url
 * @param string $message_type
 * @param string $message_text
 * @return void
 */
function redirect($url, $query = '') 
{
	header('Location: ' . get_url($url) . $query, true, 301);
	exit();
}

/**
 * Получение не пустого значения из $value1 или $value2.
 * 
 * @param string $value1
 * @param string $value2
 * @return string
 */
function no_empty($value1, $value2)
{
	return (empty($value1)) ? $value2 : $value1;
}

/**
 * Очистка текста от тегов и мусора.
 * 
 * @param string $value
 * @return string
 */
function strip_text($value)
{
	$value = stripslashes($value);		
	$value = htmlspecialchars_decode($value, ENT_QUOTES);
	$value = str_replace(array('<br>', '<br />', '<br/>', '&nbsp;', '&ensp;', '&emsp;'), ' ', $value);
	$value = strip_tags($value);

	return trim($value);
}

/**
 * Обрезка текста для превью.
 * 
 * @param string $value
 * @param int $limit
 * @return string
 */
function crop_text($value, $limit = 300)
{
	$value = strip_text($value);	

	if (mb_strlen($value) < $limit) {
		return $value;
	} else {
		$value   = mb_substr($value, 0, $limit);
		$length  = mb_strripos($value, ' ');
		$end     = $value{$length - 1};

		if (empty($length)) {
			return $value;
		} elseif ($end == '.' || $end == '!' || $end == '?') {
			return mb_substr($value, 0, $length);
		} elseif ($end == ',') {
			return mb_substr($value, 0, $length - 1) . '...';
		} else {
			return mb_substr($value, 0, $length) . '...';
		}
	}
}

function prepare_text($value)
{
	$value = stripslashes($value);
	$value = str_replace('&shy;', '', $value);
	$value = str_replace('dir=""', '', $value);
	$value = str_replace('id=""', '', $value);
	$value = str_replace('scope=""', '', $value);
	return $value;
}

function preview($preview, $text = '', $limit = 300) 
{
	return no_empty(prepare_text($preview), crop_text(prepare_text($text), $limit));
}

/**
 * Склонение числительных.
 * 
 * @param string $value Значение
 * @param array $words  Массив вариантов, например: array('товар', 'товара', 'товаров')
 * @return string
 */
function num_word($value, $words) 
{
	$num = $value % 100;
	if ($num > 19) { 
		$num = $num % 10; 
	}
	
	switch ($num) {
		case 1:  return $value . ' ' . $words[0];
		case 2: 
		case 3: 
		case 4:  return $value . ' ' . $words[1];
		default: return $value . ' ' . $words[2];
	}
}

/**
 * Конвертирование размера файла.
 * 
 * @param int $size
 * @return string
 */
function convert_bytes($size)
{
	$i = 0;
	while (floor($size / 1024) > 0) {
		++$i;
		$size /= 1024;
	}

	$size = str_replace('.', ',', round($size, 1));
	switch ($i) {
		case 0: return $size .= ' байт';
		case 1: return $size .= ' КБ';
		case 2: return $size .= ' МБ';
	}
}

/**
 * Возвращает список файлов в дериктории в виде массива.
 * 
 * @param string $path
 * @param bool $set_keys
 * @return array
 */
function list_files($path, $set_keys = false)
{  
	if ($path[mb_strlen($path) - 1] != '/') {
		$path .= '/';  
	}	
	
	$files = array();
	$dh = opendir($path);  
	while (false !== ($file = readdir($dh))) {  
		if ($file != '.' && $file != '..' && !is_dir($path.$file) && $file[0] != '.') {
			$files[] = $file;    
		}  
	}

	closedir($dh);   

	if ($set_keys) {
		return array_combine($files, $files);	
	} else {
		return $files; 
	}
} 
 
/**
 * Удаление каталога со всем содержимым.
 * 
 * @param string $dir
 * @return void
 */
function remove_dir($dir) {
	if ($objs = glob($dir . '/*')) {
		foreach($objs as $obj) {
			is_dir($obj) ? remove_dir($obj) : unlink($obj);
		}
	}
	rmdir($dir);
}

/**
 * Форматирование timestamp в человеческий вид.
 * 
 * @param int $timestamp
 * @param bool $show_time вывод времени
 * @return string
 */
function date_human($timestamp, $show_time = false)
{
	if (empty($timestamp)) {
		return 'никогда';
	} else {
		$now   = explode(' ', date('Y n j H i'));
		$value = explode(' ', date('Y n j H i', $timestamp));

		if ($now[0] == $value[0] && $now[1] == $value[1] && $now[2] == $value[2]) {
			return 'сегодня в ' . $value[3] . ':' . $value[4];
		} else {
			$month_names = array('', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
			$out = $value[2] . ' ' . $month_names[$value[1]] . ' ' . $value[0];
			if ($show_time) {
				$out .= ' <span class="nobr">в ' . $value[3] . ':' . $value[4] . '</span>';
			}
			return $out;
		}
	}
}

/**
 * Форматирование timestamp.
 * 
 * @param int $timestamp
 * @param bool $show_time вывод времени
 * @return string
 */
function date_short($timestamp, $show_time = true)
{
	if (empty($timestamp)) {
		return '—';
	} elseif ($show_time) {
		return date('d.m.y H:i', $timestamp);
	} else {
		return date('d.m.y', $timestamp);	
	}
}

function get_month($timestamp)
{
	if (empty($timestamp)) {
		return 'никогда';
	} else {
		$now   = explode(' ', date('Y n j H i'));
		$value = explode(' ', date('Y n j H i', $timestamp));
		$month_names = array('', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
		$out = $month_names[$value[1]];
		return $out;
	}
}

/**
 * Проверка валидности E-mail.
 * 
 * @param string $email
 * @return bool
 */
function check_email($email)
{
	if (empty($email) || !mb_eregi("^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,4}$", $email)) {
		return false;
	} else {
		return true;
	}
}

/**
 * Делает выделенным пункт выподающего списка.
 * 
 * @param mixed $var
 * @param string $value
 * @return string
 */
function selected($var, $value) 
{
	if (!is_array($var)) {
		$var = explode(',', $var);
	}

	return (in_array($value, $var)) ? ' selected' : '';
}

/**
 * Отмечает чекбокс.
 * 
 * @param mixed $var
 * @param string $value
 * @return string
 */
function checked($var, $value = null) 
{
	if (is_null($value)) {
		return ($var) ? ' checked="checked"' : '';
	} else {
		if (!is_array($var)) {
			$var = explode(',', $var);
		}

		return (in_array($value, $var)) ? ' checked="checked"' : '';
	}
}

/**
 * Делает заблокированным пункт выподающего списка.
 * 
 * @param mixed $var
 * @param string $value
 * @return string
 */
function disabled($var, $value) 
{
	if (!is_array($var)) {
		$var = explode(',', $var);
	}

	return (in_array($value, $var)) ? ' disabled' : '';
}

/**
 * Выборка значений по ключу из ассоциативного массива.
 * 
 * @param array $array 
 * @param string $key
 * @return array
 */
function extract_values($array, $key)
{
	$res = array();
	foreach ($array as $row) {
		$res[] = $row[$key];
	}

	return $res;
}

/**
 * Возвращает массим всех родителей.
 *
 * @param string $table  Название таблицы
 * @param int $parent    ID родительской записи
 * @param bool $approve  Проверят установлен ли флаг "approve"
 * @param array $res     Вспомогательная переменная (используется для рекурсии)
 * @return array
 */
function get_parents($table, $parent, $approve, $res = array())
{
    $sql_approve = ($approve) ? 'AND `approve` = 1' : '';
    if (!empty($parent) && $row = DB::getRow("SELECT * FROM `{$table}` WHERE `id` = ? {$sql_approve} LIMIT 1", $parent)) {
        array_unshift($res, $row);
        $res = get_parents($table, $row['parent'], $approve, $res);
    }

    return $res;
}

/**
 * Возвращает массим всех потомков.
 * 
 * @param string $table  Имя таблицы
 * @param int $parent    ID записи
 * @param bool $approve  Проверят установлен ли флаг approve
 * @param array $res     Вспомогательная переменная (используется для рекурсии)
 * @return array
 */
function get_childs($table, $id, $approve, $res = array(), $sort = 'sort')
{
	$sql_approve = ($approve) ? 'AND `approve` = 1' : '';
	if ($items = DB::getAll("SELECT * FROM `{$table}` WHERE `parent` = ? {$sql_approve} ORDER BY `{$sort}`", $id)) {
		foreach ($items as $row) {
			$res[] = $row;
			$res = get_childs($table, $row['id'], $approve, $res);
		}
	}

	return $res;
}

/**
 * Форматирование URL.
 * 
 * @param string $url
 * @return string
 */
function get_url($url = '')
{
	$url = trim($url, ' /');
	if (empty($url)) {
		return Config::$url_base;
	} else {
		return Config::$url_base . '/' . $url . Config::$url_ext;
	}
}

/**
 * Формирование URL для админки.
 * 
 * @param string $url
 * @return string
 */
function get_url_admin($url = '')
{
	$url = trim($url, ' /');
	if (empty($url)) {
		return Config::$url_base . '/' . Config::$url_admin . Config::$url_ext;
	} else {
		return Config::$url_base . '/' . Config::$url_admin . '/' . $url . Config::$url_ext;
	}
}

/**
 * Превью текста.
 * 
 * @param string $value
 * @param int $limit
 * @return string
 */
function preview_text($value, $limit = 255)
{
	if (strlen($value) < $limit) {
		return $value;
	} else {
		$value   = substr($value, 0, $limit);
		$length = strripos($value, ' ');
		$end = $value{$length - 1};

		if (empty($length)) {
			return $value;
		} elseif ($end == '.' || $end == '!' || $end == '?') {
			return substr($value, 0, $length);
		} elseif ($end == ',') {
			return substr($value, 0, $length - 1) . '...';
		} else {
			return substr($value, 0, $length) . '...';
		}
	}
}

function remove_css_attrs($value)
{
	$value = preg_replace('/(width: )+[0-9]+(px;)/', '', $value);
	$value = preg_replace('/(height: )+[0-9]+(px;)/', '', $value);
	return $value;
}

function get_blocks($row) 
{
	?>
	<div class="blocks blocks_<?php echo $row['color']; ?>">
		<?php echo prepare_text($row['text']); ?>
	</div>	
	<?php
}

/**
 * Вывод баннерной плитки.
 */
function get_banners($banner_id)
{
	if (!empty($banner_id) && $rows = DB::getAll("SELECT * FROM `#__banners_items` WHERE `benner_id` = ? ORDER BY `sort`", $banner_id)) {
		?>
		<div id="fixel-grid" class="fixel-grid">
			<?php foreach ($rows as $row): ?>	
			<?php if ($row['efect'] == 1): ?>
			<!-- Смещение вверх -->
			<div class="items none image grid-<?php echo $row['size']; ?>">
				<article>
					<div class="item-image-wrap flipper">
						<?php echo remove_css_attrs(stripslashes($row['text_1'])); ?>
						<div class="item-desc">
							<?php echo stripslashes($row['text_2']); ?>
						</div>
					</div>	
				</article>
			</div>	
			<?php elseif ($row['efect'] == 2): ?>
			<!-- Переворачивание по горизонтали -->	
			<div class="items rotateY image grid-<?php echo $row['size']; ?>">
				<article>
					<div class="item-image-wrap flipper">
						<div class="item-image front">
							<?php echo remove_css_attrs(stripslashes($row['text_1'])); ?>
						</div>
						<div class="item-desc back"><?php echo stripslashes($row['text_2']); ?></div>
					</div>	
				</article>	
			</div>
			<?php elseif ($row['efect'] == 3): ?>
			<!-- Переворачивание по вертикали -->					
			<div class="items rotateX image grid-<?php echo $row['size']; ?>">
				<article>
					<div class="item-image-wrap flipper">
						<div class="item-image front">
							<?php echo remove_css_attrs(stripslashes($row['text_1'])); ?>
						</div>
						<div class="item-desc back"><?php echo stripslashes($row['text_2']); ?></div>
					</div>	
				</article>
			</div>	
			<?php elseif ($row['efect'] == 4): ?>
			<!-- Подсказка снизу -->
			<div class="items print-design none video grid-<?php echo $row['size']; ?>">
				<article>
					<div class="item-video-wrap">
						<div class="item-image">
							<?php echo remove_css_attrs(stripslashes($row['text_1'])); ?>
						</div>
						<div class="item-desc">
							<?php echo stripslashes($row['text_2']); ?>
						</div>
					</div>	
				</article>
			</div>	
			<?php elseif ($row['efect'] == 5): ?>
			<!-- Слайдер -->
			<div class="items print-design none gallery bsl grid-<?php echo $row['size']; ?>">
				<article>
					<div class="item-gallery-wrap">
						<?php echo remove_css_attrs(stripslashes($row['text_1'])); ?>
					</div>	
				</article>
			</div>
			<?php else: ?>			
			<!-- Контент -->
			<div class="items identity-design none text grid-<?php echo $row['size']; ?>">
				<article><?php echo remove_css_attrs(stripslashes($row['text_1'])); ?></article>
			</div>	
			<?php endif; ?>
			<?php endforeach; ?>
		</div>
		<?php
	}
}

/**
 * Вывод текстов.
 */
function get_text($module, $table, $item)
{
	?>
	<div class="pages_text_row">
		<div class="pages_text text">
			<h1><?php echo no_empty($item['h1'], $item['name']); ?></h1>
			<div class="pages_text-first">
				<?php 
				// Фотки
				$images = Files::get($module, $item['id'], 'thumb');
				if (count($images) == 1) {
					?>
					<div class="pages_text-img">
						<a class="fancybox" href="/uploads/<?php echo $module; ?>/<?php echo $images[0]['filename']; ?>"><img src="/uploads/<?php echo $module; ?>/148x148/<?php echo $images[0]['filename']; ?>" alt="<?php echo $images[0]['alt']; ?>"></a>
					</div>
					<?php
				} elseif (count($images) > 1) {
					$prew = array_shift($images);
					?>
					<div class="pages_text-img">
						<a class="fancybox-<?php echo $prew['id']; ?>" href="/uploads/<?php echo $module; ?>/<?php echo $prew['filename']; ?>"><img src="/uploads/<?php echo $module; ?>/<?php echo $prew['filename']; ?>" alt="<?php echo $prew['alt']; ?>"></a>
					</div>

					<?php if (!empty($images)): ?>
					<script>
					$(".fancybox-<?php echo $prew['id']; ?>").click(function() {
						$.fancybox.open([
							<?php foreach ($images as $i => $row): ?>
							{
								href : '/uploads/<?php echo $module; ?>/<?php echo $row['filename']; ?>',                
								title : '<?php echo $row['alt']; ?>'
							}<?php if (count($images) - 1 != $i) echo ','; ?>							
							<?php endforeach; ?>							
						]);
						return false;
					});				
					</script>
					<?php endif; ?>
					<?php
				}
				?>					
				<?php echo prepare_text($item['text']); ?>
			</div>
		</div>	
		
		<?php foreach (DB::getAll("SELECT * FROM `{$table}_items` WHERE `page_id` = ? ORDER BY `sort`", $item['id']) as $row): ?>
		<div class="pages_text text">
			<?php if (!empty($row['h2'])): ?><h2><?php echo $row['h2']; ?></h2><?php endif; ?>
			
			<div class="pages_text-last">
				
				<?php 
				// Фотки
				$images = Files::get($module, $item['id'], 'thumb_' . $row['id']);
				if (count($images) == 1) {
					?>
					<div class="pages_text-img">
						<a class="fancybox" href="/uploads/<?php echo $module; ?>/<?php echo $images[0]['filename']; ?>"><img src="/uploads/<?php echo $module; ?>/148x148/<?php echo $images[0]['filename']; ?>" alt="<?php echo $images[0]['alt']; ?>"></a>
					</div>
					<?php
				} elseif (count($images) > 1) {
					$prew = array_shift($images);
					?>
					<div class="pages_text-img">
						<a class="fancybox-<?php echo $prew['id']; ?>"href="/uploads/<?php echo $module; ?>/<?php echo $prew['filename']; ?>"><img src="/uploads/<?php echo $module; ?>/148x148/<?php echo $prew['filename']; ?>" alt="<?php echo $prew['alt']; ?>"></a>
						<?php if (!empty($images)): ?>
						<script>
						$(".fancybox-<?php echo $prew['id']; ?>").click(function() {
							$.fancybox.open([
								<?php foreach ($images as $i => $row2): ?>
								{
									href : '/uploads/<?php echo $module; ?>/<?php echo $row2['filename']; ?>',                
									title : '<?php echo $row2['alt']; ?>'
								}<?php if (count($images) - 1 != $i) echo ','; ?>							
								<?php endforeach; ?>							
							]);
							return false;
						});				
						</script>
						<?php endif; ?>
					</div>
					<?php
				}
				?>
				<?php echo prepare_text($row['text']); ?>				
			</div>
		</div>
		<?php endforeach; ?>		
	</div>
	<?php
}

function is_admin()
{
    return (Auth::$user['level'] == 4) ? true : false;
}

function setWarrantyTimestamp($months, $import_ship_date)
{
	// забирает текущее время в массив
	$timestamp = $import_ship_date;
	$date_time_array = getdate($timestamp);

	$hours = $date_time_array['hours'];
	$minutes = $date_time_array['minutes'];
	$seconds = $date_time_array['seconds'];
	$month = $date_time_array['mon'];
	$day = $date_time_array['mday'];
	$year = $date_time_array['year'];

	// используйте mktime для обновления UNIX времени
	return $timestamp = mktime($hours,$minutes,$seconds,$month + $months,$day,$year);
}

function random_string($length, $chartypes)
{
	$chartypes_array=explode(",", $chartypes);
	// задаем строки символов. 
	//Здесь вы можете редактировать наборы символов при необходимости
	$lower = 'abcdefghijklmnopqrstuvwxyz'; // lowercase
	$upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; // uppercase
	$numbers = '1234567890'; // numbers
	$special = '^@*+-+%()!?'; //special characters
	$chars = "";
	// определяем на основе полученных параметров, 
	//из чего будет сгенерирована наша строка.
	if (in_array('all', $chartypes_array)) {
		$chars = $lower . $upper. $numbers . $special;
	} else {
		if(in_array('lower', $chartypes_array))
			$chars = $lower;
		if(in_array('upper', $chartypes_array))
			$chars .= $upper;
		if(in_array('numbers', $chartypes_array))
			$chars .= $numbers;
		if(in_array('special', $chartypes_array))
			$chars .= $special;
	}
	// длина строки с символами
	$chars_length = strlen($chars) - 1;
	// создаем нашу строку,
	//извлекаем из строки $chars символ со случайным 
	//номером от 0 до длины самой строки
	$string = $chars{rand(0, $chars_length)};
	// генерируем нашу строку
	for ($i = 1; $i < $length; $i = strlen($string)) {
		// выбираем случайный элемент из строки с допустимыми символами
		$random = $chars{rand(0, $chars_length)};
		// убеждаемся в том, что два символа не будут идти подряд
		if ($random != $string{$i - 1}) $string .= $random;
	}
	// возвращаем результат
	return $string;
}

function warranty_end($date_end)
{
    $datetime1 = new DateTime("now");
    $datetime2 = new DateTime(date('d.m.Y', $date_end));
    $interval = $datetime1->diff($datetime2);
    return [
        'year' => $interval->format('%r%y'),
        'month' => $interval->format('%r%M') + $interval->format('%r%y') * 12,
        'day' => $interval->format('%r%D')
    ];
//    echo $interval->format('%r%y лет, %r%M месяцев, %R%D дней, %R%H часов, %R%I минут, %R%S секунд');
}