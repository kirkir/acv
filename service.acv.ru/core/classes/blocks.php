<?php
/**
 * Текстовае блоки.
 */
class blocks
{
	/**
	 * Получение контента по алесу.
	 * 
	 * @param string $sef
	 * @return string
	 */
	public static function get($sef, $default = null)
	{
		if ($data = DB::getRow("SELECT * FROM `#__blocks` WHERE `approve` = 1 AND `sef` = ?", $sef)) {
			return stripslashes($data['text']);
		} else {
			return $default;		
		}
	}
}