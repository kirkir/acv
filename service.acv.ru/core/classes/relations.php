<?php

/**
 * Created by PhpStorm.
 * User: kir
 * Date: 10.08.16
 * Time: 13:48
 */
class Relations
{
    public static $RELATION_ACT = array(
        'add' => 1,
        'edit' => 2,
    );

    public static $RELATION_NAME = array(
        1 => 'Добавление отчета',
        2 => 'Редактирование отчета'
    );

    public static function setRelation($action)
    {
        if(!Auth::perm(4)) {
            DB::set("INSERT INTO `#__relations` SET `relation` = ?, `user_id` = ?, `date_add` = UNIX_TIMESTAMP()", array(self::$RELATION_ACT[$action], Auth::$user['id']));
        }
    }

    public static function executedRelation($id)
    {
        DB::set("UPDATE `#__relations` SET `executed` = 1 WHERE `id` = ?", array($id));
    }

    public static function getRelation($id)
    {
        if(Auth::perm(4)) {
            return DB::getAll("SELECT * FROM `#__relations` WHERE `executed` = 0 AND `user_id` = ? ORDER BY `date_add` DESC", $id);
        }
    }

}