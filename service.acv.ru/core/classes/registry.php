<?php
/**
 * Реестр.
 */
class Registry
{ 
	private static $_storage = array(); 

	/**
	 * Установка значения.
	 * 
	 * @ruturn mixed
	 */
	public static function set($name, $value)
	{ 
		return self::$_storage[$name] = $value;
    }

	/**
	 * Получение значения.
	 * 
	 * @param string $name
	 * @param mixed $default
	 * @ruturn string
	 */
    public static function get($name, $default = null)
	{
		return (isset(self::$_storage[$name])) ? self::$_storage[$name] : $default;
    }

	/**
	 * Очистка.
	 * 
	 * @ruturn bool
	 */
    public static function clean()
	{
		self::$_storage = array(); 

		return true;
    }
}