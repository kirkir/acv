<?php
/**
 * Формирование HTML
 */
class HTML
{
	/**
	 * Формирование OL.
	 * 
	 * @param array $items
	 * @return string
	 */
	public static function ol($items) 
	{
		$items = (array) $items;
		if (empty($items)) {
			return null;
		} else {	
			return '<ol><li>' . implode('</li><li>', $items) . '</li></ol>';
		}
	}

	/**
	 * Ссылка.
	 * 
	 * @param string $url
	 * @param string $ankor
	 * @param array $attrs
	 * @return string
	 */
	public static function a($url, $ankor = null, $attrs = array())
	{
		if (is_null($ankor)) {
			$ankor = $url;
		}

		$attrs_str = '';	
		foreach ($attrs as $key => $row) {
			$attrs_str .= $key . '="' . $row . '" ';
		}

		return '<a href="' . $url. '" ' . $attrs_str . '>' . $ankor . '</a>';
	}

	/**
     * Элементы выподающего списка из ассоциативного массива.
     * Ключ массива поподает в value.
	 * 
	 * @param array $array     Масив данных (может быть многомерным).
	 * @param mixed $selected  Масив или значение выделенных инпуов.
	 * @param string $key_text Если $array многомерный, указывает какой элемент масива выводить в качестве текста.
	 * @return void
     */
	public static function options($array, $selected, $key_text = 0) 
	{
		foreach ($array as $i => $row) {
			?><option <?php echo selected($selected, $i); ?> value="<?php echo $i; ?>"><?php echo (is_array($row)) ? $row[$key_text] : $row; ?></option><?php
		}
	}

	/**
     * Список чекбоксов из ассоциативного массива + разделение его на колонки.
     * Ключи масива используются в атрибуте value инпута.
	 * 
	 * @param string $name       Имя инпута (атрибут name).
	 * @param array $array       Масив данных (может быть многомерным).
	 * @param mixed $selected    Масив или значение выделенных инпуов.
	 * @param int $count_columns Кол-во колонок.
	 * @param string $key_text   Если $array многомерный, указывает какой элемент масива выводить в качестве текста.
	 * @return void
     */
	public static function checkboxes($name, $array, $selected = null, $count_columns = 0, $key_text = 0) 
	{
		if (empty($count_columns)) {
			foreach($array as $i => $row) {
				?>  
				<div class="checkboxes__row">
					<label><input class="checkboxes__control" <?php echo checked($selected, $i); ?> type="checkbox" name="<?php echo $name; ?>" value="<?php echo $i; ?>"> <?php echo (is_array($row)) ? $row[$key_text] : $row; ?></label>
				</div>  
				<?php
			}	
		} else {
			$array = array_chunk($array, ceil(count($array) / $cols), true);
			?>
			
			<table class="checkbox__cols">
				<tbody>
					<tr>
						<?php foreach ($array as $n => $items): ?>
						<td class="rcheckbox__column rcheckbox__column_<?php echo $n; ?>">
							<?php foreach ($items as $i => $row): ?>
							<div class="checkbox__row">
								<label><input class="checkbox__control" <?php echo checked($selected, $i); ?> type="checkbox" name="<?php echo $name; ?>[]" value="<?php echo $i; ?>"> <?php echo (is_array($row)) ? $row[$key] : $row; ?></label>
							</div>
							<?php endforeach; ?>
						</td>
						<?php endforeach; ?>
					</tr>
				</tbody>
			</table>

			<?php
		}
	}
	
	/**
     * Список радио кнопок из ассоциативного массива + разделение его на колонки.
     * Ключи масива используются в атрибуте value инпута.
	 * 
	 * @param string $name       Имя инпута (атрибут name).
	 * @param array $array       Масив данных (может быть многомерным).
	 * @param mixed $selected    Масив или значение выделенных инпуов.
	 * @param int $count_columns Кол-во колонок.
	 * @param string $key_text   Если $array многомерный, указывает какой элемент масива выводить в качестве текста.
	 * @return void
     */
	public static function radioButtons($name, $array, $selected = null, $count_columns = 0, $key_text = 0) 
	{
		if (empty($count_columns)) {
			foreach($array as $i => $row) {
				?>  
				<div class="radiobuttons__row">
					<label><input class="radiobuttons__control" <?php echo checked($selected, $i); ?> type="radio" name="<?php echo $name; ?>" value="<?php echo $i; ?>"> <?php echo (is_array($row)) ? $row[$key_text] : $row; ?></label>
				</div>  
				<?php
			}		
		} else {
			?>  
			<table class="radiobuttons">
				<tbody>
					<tr>
						<?php foreach (array_chunk($array, ceil(count($array) / $count_columns), true) as $n => $items): ?>
						<td class="radiobuttons__column radiobuttons__column_<?php echo $n; ?>">
							<?php foreach ($items as $i => $row): ?>
							<div class="radiobuttons__row">
								<label><input class="radiobuttons__control" <?php echo checked($selected, $i); ?> type="radio" name="<?php echo $name; ?>" value="<?php echo $i; ?>"> <?php echo (is_array($row)) ? $row[$key_text] : $row; ?></label>
							</div>
							<?php endforeach; ?>
						</td>
						<?php endforeach; ?>
					</tr>
				</tbody>
			</table>  
			<?php
		}
	}
	
	/**
	* Выпадающий список таблицы
	*/
	public static function optionsDB($table, $select, $i = 0)
	{
		$option = '';
		$list = DB::getAll("SELECT * FROM `$table`");
		foreach ($list as $row) {
			$select1 = ($row['id'] == $select) ? ' selected="selected"' : '';
			$option .= '<option value="' . $row['id'] . '"' . $select1  . '>';
				
			for ($m = 0; $m < $i; $m++) {
				$option .= '-';
			}
	
			$option .= $row['name'] . '</option>';
		}
	
		$i++;

		echo $option;
	}
}