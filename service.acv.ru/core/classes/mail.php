<?php
/**
 * Отправка писем
 */
class Mail
{
	public $from = '';
	public $to = array();
	
	public $subject = '';
	public $body = '';

	/**
     * От кого
     */
	public function from($email, $name = null)
	{
		$this->from = (empty($name)) ? $email : '=?UTF-8?B?' . base64_encode($name) . '?= <' . $email . '>';
	}

	/**
     * Кому
     */
	public function to($email, $name = null)
	{
		if (empty($email)) {
			$this->to[] = trim(Settings::get('email'));
		} else {
			$emails = explode(',', $email);
			foreach ($emails as $row) {
				$this->to[] = (empty($name)) ? trim($row) : '=?UTF-8?B?' . base64_encode($name) . '?= <' . trim($row) . '>';
			}
		}
	}

	/**
     * Отправка
     */
	public function send()
	{
		// Тема письма
		$subject = (empty($this->subject)) ? 'No subject' : $this->subject;		
		
		// Заголовки
		$headers = implode(
			"\r\n", 
			array(
				'Content-Type: text/html; charset=UTF-8',
				'Content-Transfer-Encoding: BASE64',
				'MIME-Version: 1.0',
				'From: ' . $this->from,
				'Reply-To: ' . $this->from,
				'Return-Path: ' . $this->from,
				'X-Mailer: ' . Settings::get('name'),
				'Date: ' . date('r')
			)
		);

		// Подпись
		$signature = Settings::get('email_signature');
		$signature = (empty($signature)) ? '' : '<div style="margin-top: 20px;">' . $signature . '</div>';		
		
		// Тело письма
		$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			</head>
			<body style="margin: 0 0 0 0; padding: 10px 10px 10px 10px; background: #ffffff; color: #000000; font-size: 14px; font-family: Tahoma, Arial, sans-serif; line-height: 1.4em;">
				' . $this->body . '
				' . $signature . '
			</body>
		</html>';
		
		$body = str_replace("\t", '', $body);
		$body = str_replace('<table>', '<table style="border: 1px solid #E4E4E4; margin: 0 0 15px 0; padding: 0 0 0 0;">', $body);
		$body = str_replace('<th>', '<th style="text-align: center; padding: 5px 10px; border: 1px solid #E4E4E4; color: #000000; font-size: 14px;">', $body);
		$body = str_replace('<th colspan="2">', '<th colspan="2" style="text-align: center; padding: 5px 10px; border: 1px solid #E4E4E4; color: #000000; font-size: 14px;">', $body);
		$body = str_replace('<td style="text-align: center;">', '<td style="text-align: center; padding: 5px 10px; border: 1px solid #E4E4E4; color: #000000; font-size: 14px;">', $body);
		$body = str_replace('<td>', '<td style="text-align: left; padding: 5px 10px; border: 1px solid #E4E4E4; color: #000000; font-size: 14px;">', $body);
		$body = str_replace('<ul>', '<ul style="margin: 0 0 15px 0;">', $body);
		$body = str_replace('<li>', '<li style="margin: 2px 0 2px 0;">', $body);
		$body = str_replace('<p>', '<p style="margin: 0 0 15px 0; padding: 0 0 0 0; color: #000000; font-size: 14px; font-family: Tahoma, Arial, sans-serif; line-height: 18px;">', $body);
		$body = str_replace('<a>', '<a style="color: #003399; text-decoration: underline; font-size: 14px; font-family: Tahoma, Arial, sans-serif; line-height: 18px;">', $body);
		$body = str_replace('<h2>', '<h2 style="margin: 0 0 15px 0; padding: 0 0 0 0; color: #000000; font-size: 16px; font-family: Tahoma, Arial, sans-serif; line-height: 20px; font-weight: bold;">', $body);

		foreach ($this->to as $to) {
			mb_send_mail($to, $subject, $body, $headers);
		}
		
		return true;
	}
}