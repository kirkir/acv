<?php
/**
 * Вспомогательный класс для страниц.
 */
class Pages
{
	/**
	 * Получение данных страницы по его ЧПУ.
	 * 
	 * @param string $sef
	 * @return array
	 */
	public static function get($sef)
	{
		$parents = array();
		$id = $num = 0;

		$route = explode('/', $sef);
		foreach ($route as $part) {
			if ($res = DB::getRow("SELECT * FROM `#__pages` WHERE `parent` = ? AND `sef` = ? AND `approve` = 1 LIMIT 1", array($id, $part))) {
				$parents[] = $res;
				$id = $res['id'];
				$num++;
			} else {
				break;
			}
		}

		if (count($route) == $num) {
			return array_pop($parents);
		} else {
			return null;
		}
	}
	
	/**
     * Cылка на стрницу.
     * 
     * @param string $sef
     * @param string $ankor
     * @return string
     */
    public function link($sef, $ankor = '')
    {
		$sef = trim($sef, '/');
		if (empty($sef)) {
			// Cылка на главную.
			return '<a href="' . get_url() . '">' . $ankor . '</a>';
		} else {
			$parent = 0;
			foreach (explode(',', $sef) as $row) {
				$data = DB::getRow("SELECT * FROM `#__pages` WHERE `approve` = 1 AND `parent` = ? AND `sef` = ? LIMIT 1", array($parent, $row));
				if (!empty($data)) {
					$parent = $row['id'];
				} else {
					$data = null;
					break;
				}
			}
	
			if (!empty($data)) {
				return '<a href="' . get_url($sef) . '">' . no_empty($ankor, $data['name']) . '</a>';
			}
		}

		return null;
	}
	
    public static function getMenu($sef)
    {
		$out = '';
		if ($item = self::get($sef)) {
			$items = DB::getAll("SELECT * FROM `#__pages` WHERE `parent` = ?", $item['id']);
			foreach ($items as $row) {
				$out .= '<li><a href="' . Tree::url('#__pages', $row['id']) . '">' . $row['name'] . '</a></li>';
			}
		}
		
		return $out;
	}
}