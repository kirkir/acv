<?php
/**
 * Древовидная херня
 */
class tree
{
	/**
	 * Преобразавание массива в древовидное представление.
	 * 
	 * @return array
	 */
	public static function convert($array, $r = 0, $c = 'children')
	{
		$i = 'id'; 
		$p = 'parent';
		if (!is_array($array)) {
			return array();
		}
	
		$ids = array();
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				if ((isset($v[$i]) || ($i === false)) && isset($v[$p])) {
					$key = ($i === false) ? $k : $v[$i];
					$parent = $v[$p];
					$ids[$parent][$key] = $v;
				}
			}
		}

		return (isset($ids[$r])) ? self::_convertNode($ids, $r, $c) : false;
	}	

	private static function _convertNode($index, $root, $cn)
	{
		$_ret = array();
		foreach ($index[$root] as $k => $v) {
			$_ret[$k] = $v;
			if (isset($index[$k])) {
				$_ret[$k][$cn] = self::_convertNode($index, $k, $cn);
			}
		}

		return $_ret;
	}
	
	/**
	 * Возвращает массим всех родителей.
	 * 
	 * @param string $table  Название таблицы
	 * @param int $parent    ID родительской записи
	 * @param array $_res    Вспомогательная переменная (используется для рекурсии)
	 * @return array
	 */
	public static function getParents($table, $parent, $_res = array())
	{
		if (!empty($parent) && $row = DB::getRow("SELECT * FROM `{$table}` WHERE `id` = ? LIMIT 1", $parent)) {
			array_unshift($_res, $row);
			$_res = self::getParents($table, $row['parent'], $_res);
		}

		return $_res;
	}

	/**
	 * Возвращает массим всех потомков.
	 * 
	 * @param string $table  Имя таблицы
	 * @param int $parent    ID записи
	 * @param array $_res    Вспомогательная переменная (используется для рекурсии)
	 * @return array
	 */
	public static function getChilds($table, $id, $_res = array())
	{
		if (!empty($id)) {
			foreach (DB::getAll("SELECT * FROM `{$table}` WHERE `parent` = ? ORDER BY `sort`", $id) as $row) {
				$_res[] = $row;
				$_res = self::getChilds($table, $row['id'], $_res);
			}
		}

		return $_res;
	}

	/**
	 * Возвращает массим ID всех потомков.
	 * 
	 * @param string $table  Имя таблицы
	 * @param int $parent    ID записи
	 * @param int $current   Включать ли ID записи в результирующий массив
	 * @param array $_res    Вспомогательная переменная (используется для рекурсии)
	 * @return array
	 */
	public static function getChildsId($table, $id, $include_id = false, $_res = array()) 
	{
		if (!empty($id)) {
			if ($include_id) {
				$_res[] = $id;
			}

			foreach (DB::getAll("SELECT `id` FROM `{$table}` WHERE `parent` = ?", $id) as $row) {
				$_res[] = $row['id'];
				$_res = self::getChildsId($table, $row['id'], false, $_res);
			}
		}

		return $_res;
	}

	
	
	
	

	
	/**
     * Древовидный выпадающий список (input select) из БД.
	 * 
	 * @param string $table   Таблица БД
	 * @param mixed $selected Массив id'шников выделенных элементов
	 * @param mixed $disabled Массив id'шников заблокированных элементов
	 * @param int $parent
	 * @param int $livel
	 * @return void
     */
	public static function options($table, $selected, $disabled = array(), $parent = 0, $livel = 0) 
	{
		$livel++;
		if ($items = DB::getAll("SELECT `id`, `name` FROM `{$table}` WHERE `parent` = ? ORDER BY `sort`", $parent)) {
			foreach ($items as $row) {
				?><option <?php echo selected($selected, $row['id']); ?> <?php echo disabled($disabled, $row['id']); ?> value="<?php echo $row['id']; ?>"><?php for ($i = 1; $i < $livel; $i++) echo '&emsp;&emsp;'; echo $row['name']; ?></option><?php
				self::options($table, $selected, $disabled, $row['id'], $livel);
			}
		}
	}
	
	/**
	 * Формирование URL.
	 * 
	 * @param string $table  Имя таблицы
	 * @param int $id        ID записи
	 * @param string $before ЧПУ который будет добавлен в начале URL
	 * @param string $after  ЧПУ который будет добавлен в конце URL
	 * @return string
	 */
	public static function url($table, $id, $before  = '', $after = '')
	{
		$items = DB::getRow("SELECT * FROM `{$table}` WHERE `id` = ?", $id);
		
		if ($table == '#__prod_category' && empty($after)) {
			if (!empty($items['url'])) {
				return get_url($items['url']);
			}
		}

		if (empty($items)) {
			return get_url();
		} else {
			$parents = array();
			if (!empty($items['sef_old'])) {
				return $items['sef_old'];
			} elseif (isset($items['parent'])) {
				$parents = get_parents($table, $items['parent'], false);
			}
			
			$parents[] = $items;
			$parents = extract_values($parents, 'sef');

			if (!empty($before )) {
				$before  .= '/'; 
			}			
			
			if (!empty($after)) {
				$after = '/' . $after; 
			}

			return get_url($before  . implode('/', $parents) . $after);			
		}
	}

	/**
     * Меню категорий.
	 * 
	 * @param string $table  Имя таблицы
	 * @param string $class  CSS класс онже ключ в Registry
	 * @param string $before ЧПУ который будет добавлен в начале URL ссылок
	 * @param bool $url_tree Режим формирования ссылок: true - ирархические, false - прямые.
	 * @return void
     */
	public static function menu($table, $class = 'menu', $before  = '', $url_tree = true)
	{
		$selected = Registry::get($class, array());
		$items = DB::getAll("SELECT * FROM `{$table}` WHERE `approve` = 1 AND `parent` = 0 ORDER BY `sort`");
		$count = count($items);
		?> 

		<ul class="<?php echo $class; ?>">
			<?php foreach ($items as $i => $row):?>
			<li class="<?php 
		
			echo $class . '_1__row';

			if ($i == 0) {
				echo ' first';
			} elseif ($i == $count - 1) {
				echo ' last';
			} else {
				echo ' mid';
			}

			echo $class_active = (in_array($row['id'], $selected)) ? ' active' : '';
	
			?>">
				<a class="<?php echo $class . '_1__link' . $class_active; ?>" id="fid_menu_<?php echo $row['id']; ?>" href="<?php 

				if (!empty($row['sef_old'])) {
					echo $row['sef_old'];
				} elseif ($url_tree) {
					echo self::url($table, $row['id'], $before);
				} else {
					echo get_url($row['sef']);
				}

				?>"><?php echo $row['name']; ?></a>
				<?php self::_menuChild($table, $class, $row['id'], $before, $url_tree); ?>
			</li>
			<?php endforeach; ?>
		</ul>

		<?php
	}

	/**
     * Вспогательный метод для menu() 
     */
	private static function _menuChild($table, $class, $id, $before, $url_tree, $livel = 1)
	{
		$livel++;
		$class_livel = $class . '_' . $livel;
		
		$selected = Registry::get($class, array());
		if ($items = DB::getAll("SELECT * FROM `{$table}` WHERE `approve` = 1 AND `parent` = ? ORDER BY `sort`", $id)) {
			$count = count($items);
			?>
			<ul class="<?php echo $class_livel; if (in_array($id, $selected)) echo ' active'; ?>">
				<?php foreach ($items as $i => $row): ?>  
				<li class="<?php 

				echo $class_livel . '__row'; 

				if ($i == 0) {
					echo ' first';
				} elseif ($i == $count - 1) {
					echo ' last';
				} else {
					echo ' mid';
				}	

				echo $class_active = (in_array($row['id'], $selected)) ? ' active' : '';
				
				?>">
					<a class="<?php echo $class_livel . '__link' . $class_active; ?>" id="fid_menu_<?php echo $row['id']; ?>" href="<?php 

					if (!empty($row['sef_old'])) {
						echo $row['sef_old'];
					} elseif ($url_tree) {
						echo self::url($table, $row['id'], $before);
					} else {
						echo get_url($row['sef']);
					}
					?>"><?php echo $row['name']; ?></a>  
					<?php self::_menuChild($table, $class, $row['id'], $before, $url_tree, $livel); ?>
				</li>
				<?php endforeach; ?>
			</ul>		
			
			<?php
		}
	}
	
}