<?php
/**
 * Системное ядро.
 */
class Core
{
	private $cache_names = array();

	/**
	 * Получение данных из кэша.
	 * 
	 * @param string $name
	 * @return string|false
	 */
	public function cacheGet($name) 
	{
		if (Config::$cache) {
			$path = (Router::isAdmin()) ? 'admin' : 'site';
			$file = CORE_DIR . '/cache/' . $path . '/' . $name . '.tmp';
			if (file_exists($file)) {
				return file_get_contents($file);
			} else {
				$this->cache_names[] = $name;
				return false;
			}				
		} else {
			return '';
		}
	}

	/**
	 * Отправка данных в кэш.
	 * 
	 * @param string $content
	 * @return string
	 */
	public function cacheSet($content) 
	{
		if (Config::$cache) {
			$name = array_pop($this->cache_names);
			$path = (Router::isAdmin()) ? 'admin' : 'site';
			$dir  = CORE_DIR . '/cache/' . $path;
			if (!is_dir($dir)) {
				mkdir($dir, 0777, true); // У дериктории /core/ должны быть права 777 иначе "Warning: mkdir() [function.mkdir]: Permission denied in".
			}
			file_put_contents($dir . '/' . $name . '.tmp', $content);
		} 

		return $content;
	}

	/**
	 * Начало кэширования фрагмента.
	 * 
	 * @param string $name
	 * @return bool
	 */
	public function cacheBegin($name) 
	{
		if ($content = $this->cacheGet($name)) {
			echo $content;
			return false;
		} else {
			ob_start();
			return true;
		}
	}
	
	/**
	 * Завершение кэширования фрагмента.
	 * 
	 * @param string $name
	 * @return void
	 */
	public function cacheEnd() 
	{
		echo $this->cacheSet(ob_get_clean());
	}
	
	/**
	 * Очистка всего кэша.
	 * 
	 * @param string $name
	 * @return void
	 */
	public function cacheClear() 
	{
		if (Config::$cache) {
			remove_dir(CORE_DIR . '/cache');
		}
	}
	
	/**
     * Рендер .
     * 
     * @param string $filename
     * @param mixed $data
     * @param string $cache_name
	 
     * @return string
     */
    public function render($filename, $data = null, $cache_name = null)
    {
		if (!empty($cache_name)) {
			return ($content = $this->cacheGet($cache_name)) ? $content : $this->cacheSet($this->renderFile($filename, $data));
		} else {
			return $this->renderFile($filename, $data);
		}
	}

	/**
     * Рендер файла-шаблона.
     * 
     * @param string $filename
     * @param mixed $data
     * @return string
     */
	public function renderFile($filename, $data = null)
    {
		if (file_exists($filename)) {
			ob_start();
			include $filename; 
			return ob_get_clean(); 
		}
		
		return 'Cannot open tpl file: ' . $filename;
	}
}