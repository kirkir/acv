<?php
/**
 * Контроль доступа.
 */
class Auth
{
	/**
	 * Кол-во попыток авторизации.
	 * 
	 * @var array
	 */
	static public $attempts = 0;

	/**
	 * Данные пользователя.
	 * 
	 * @var array
	 */
	static public $user = array();

	/**
	 * Инициализация (метод выполняется в init.php).
	 * 
	 * @return bool
	 */
 	static public function init()
	{
		if (isset($_SESSION['auth_attempts'])) {
			self::$attempts = $_SESSION['auth_attempts'];
	
			// 404-я ошибка если неудачных попыток > 30
			if (self::$attempts > 30) {
				echo Router::get('404');
				exit();
			}
		}

		$cookie = Input::getStr($_COOKIE['auth'], '');
		if (!empty($cookie) && $user = DB::getRow("SELECT * FROM `#__users` WHERE `hash` = ? AND `approve` = 1 LIMIT 1", $cookie)) {	
			self::$user = $user;
			return true;
		} else {
			return false;			
		}
	}
 
	/**
	 * Авторизация.
	 * 
	 * @param string $login
	 * @param string $password
	 * @param int $livel уровень доступа
	 * @param bool $remember запомнить меня на 14 дней
	 * @return array
	 */
	static public function login($login, $password, $livel, $remember = true)
	{
		$user = DB::getRow("SELECT * FROM `#__users` WHERE `login` = :login OR `email` = :login LIMIT 1", array('login' => $login));
		if (empty($user)) {
			$error = 'Такой пользователь не найден';
		} elseif ($user['password'] != self::getHash($password)) {
			$error = 'Неверный пароль';
		} elseif (empty($user['approve'])) {
			$error = 'Ваш аккаунт заблокирован';
		} elseif ($user['level'] < $livel) {
			$error = 'Низкий уровень доступа';
		} else {
			$hash = md5($_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'] . uniqid());
			if (DB::set("UPDATE `#__users` SET `hash` = ?, `date_visit` = UNIX_TIMESTAMP() WHERE `id` = ? LIMIT 1", array($hash, $user['id']))) {
				self::$user = $user;

				// Запомнить меня.
				if ($remember) {
					setcookie('auth', $hash, time() + 1209600, '/'); // 14 дней
				} else {
					setcookie('auth', $hash, 0, '/');
				}		

				// Обнуление неудачных попыток авторизации.
				self::$attempts = 0;
				unset($_SESSION['auth_attempts']);
	
				return array('response' => true);
			} else {
				$error = 'Произошла ошибка.';
			}
		}


		// +1 к кол-ву неудачных попыток авторизации.
		@$_SESSION['auth_attempts']++;
		self::$attempts = $_SESSION['auth_attempts'];

		return array('response' => false, 'message' => $error);
	}

    /* Экшн регистрации */
    public static function reg($data, $level)
    {

        $data['status'] = (isset($data['status'])) ? $data['status'] : 0;

        if (DB::set("INSERT INTO `#__users` SET `login` = :login, `name` = :name, `email` = :email, `phone` = :phone, `password` = :password, `level` =  :level, `date_add` = UNIX_TIMESTAMP(), `approve` = 0, `status` = :status",
            array(
                'login' => $data['login'],
                'name' => $data['name'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'password' => self::getHash($data['password']),
                'level' => $level,
                'status' => $data['status']
            ))
        ) {
            return array('response' => true);
        }
    }

	/**
	 * Вывод формы с авторизацией.
	 * 
	 * @return string
	 */
	static public function form()
	{
		if (Router::$route[0] == Config::$url_admin) {
			echo Router::get(Config::$url_admin . '/login');
		} else {
			echo Router::get('login');
		}

		exit;
	}

	/**
	 * Завершение сеанса.
	 * 
	 * @return void
	 */
	static public function logout()
	{
		self::$user = false;
		setcookie('auth', '', 0, '/');

		if (Router::$route[0] == Config::$url_admin) {
			return redirect(Config::$url_admin);
		} else {
			return redirect();
		}
	}

	/**
	 * Хеш пароля.
	 * 
	 * @param int $password
	 * @return string
	 */
	static public function getHash($password)
	{
		return md5($password . 'webstd_2013');
	}

	/**
	 * Проверка уровня доступа.
	 * 
	 * @param int $livel уровень доступа
	 * @return bool
	 */
	static public function perm($livel)
	{
		return (self::$user && self::$user['level'] >= $livel) ? true : false;
	}

	/**
	 * Проверка доступа.
	 * 
	 * @param int $livel уровень доступа
	 * @return bool
	 */
	static public function allow($level)
	{
		return (self::perm($level)) ? true : self::form();
	}

	/**
	 * Генератор паролей.
	 * 
	 * @param int $length длина
	 * @return string
	 */
	static public function genPassword($length = 6) {
		$password = '';
		$arr = array(
		  'a', 'b', 'c', 'd', 'e', 'f',
		  'g', 'h', 'i', 'j', 'k', 'l',
		  'm', 'n', 'o', 'p', 'q', 'r',
		  's', 't', 'u', 'v', 'w', 'x',
		  'y', 'z', 'A', 'B', 'C', 'D',
		  'E', 'F', 'G', 'H', 'I', 'J',
		  'K', 'L', 'M', 'N', 'O', 'P',
		  'Q', 'R', 'S', 'T', 'U', 'V',
		  'W', 'X', 'Y', 'Z', '1', '2',
		  '3', '4', '5', '6', '7', '8',
		  '9', '0'
		);
		
		for ($i = 0; $i < $length; $i++) {
			$password .= $arr[mt_rand(0, count($arr) - 1)];
		}
		
		return $password;
	}
}