<?php
/**
 * ����������� � ����� �������.
 */
class Labels
{
	/**
	 * ������ ������
	 * @var array
	 */
	private $_store = array();

	/**
	 * �����������.
	 * 
	 * @param array $store ������ ������.	 
	 * @return void
	 */
	public function __construct($store) 
	{
		$this->_store = $store;
	}

	/**
	 * ������������ HTML ����.
	 * 
	 * @param array $data ������ �� ����������.	 
	 * @return string
	 */
	private function _prepare($data) 
	{
		$res = array_sum((array) $data);
		if (!empty($res)) {
			return '<span class="label label-warning">' . $res . '</span>';
		}
		
		return null;
	}

	/**
	 * ����� ����.
	 * 
	 * @return string
	 */
	public function getAll() 
	{
		return $this->_prepare($this->_store);
	}

	/**
	 * ����� �����������.
	 * 
	 * @param array ����� ������� $this->_store
	 * @return string
	 */
	public function getSelected($list) 
	{
		$keys = array();
		foreach ($list as $row) {
			if (!empty($this->_store[$row])) {
				$keys[] = $this->_store[$row];
			}
		}
		
		return $this->_prepare($keys);
	}
	
	/**
	 * ����� �� �����.
	 * 
	 * @param string ���� ������� $this->_store
	 * @return string
	 */
	public function get($key) 
	{
		return $this->_prepare($this->_store[$key]);
	}
}