<?php
/**
 * Гениратор форм Bootstrap 3.
 */
class Forms
{
	/**
	 * Счётчик для tabindex.
	 * 
	 * @var string
     */
	private $tabindex = 1;

	/**
     * Поля формы.
     * 
     * @var array
     */
	public $form = array();		

	/**
     * Ошибки формы.
     * 
     * @var array
     */
	public $error = array();	

	public $instance = null;

	/**
	 * Конструктор.
	 * 
	 * @paran object $instance
	 * @return void
     */
	public function __construct($instance = null)
	{
		$this->instance = $instance;
		$this->error = @$this->instance->error;
		$this->field = @$this->instance->form;
	}

	/**
	 * selected="selected".
	 * 
	 * @param mixed $var
	 * @param string $value
	 * @return string
	 */
	public static function selected($var, $value) 
	{
		if (!is_array($var)) {
			$var = explode(',', $var);
		}

		return (in_array($value, $var)) ? ' selected' : '';
	}

	/**
	 * checked="checked".
	 * 
	 * @param mixed $var
	 * @param string $value
	 * @return string
	 */
	public static function checked($var, $value = null) 
	{
		if (is_null($value)) {
			return ($var) ? ' checked' : '';
		} else {
			if (!is_array($var)) {
				$var = explode(',', $var);
			}

			return (in_array($value, $var)) ? ' checked' : '';
		}
	}

	/**
	 * disabled="disabled".
	 * 
	 * @param mixed $var
	 * @param string $value
	 * @return string
	 */
	public static function disabled($var, $value) 
	{
		if (!is_array($var)) {
			$var = explode(',', $var);
		}

		return (in_array($value, $var)) ? ' disabled' : '';
	}
	
	/**
	 * class="muted".
	 * 
	 * @param mixed $var
	 * @param string $value
	 * @return string
	 */
	public static function muted($var, $value) 
	{
		if (!is_array($var)) {
			$var = explode(',', $var);
		}

		return (in_array($value, $var)) ? ' class="text-muted"' : '';
	}

	/**
	 * Формирование value.
	 * 
	 * @paran array $config
	 * @return string
     */
	private function getValue($config) 
	{
		$name = str_replace('[]', '', $config['name']);

		if (isset($this->field[$name])) {
			return $this->field[$name];
		} else {
            var_dump(2); exit;
		}
	}
	
	/**
	 * Формирование id из названия поля.
	 * 
	 * @paran array $config
	 * @return string
     */
	private function getId($config) 
	{
		$name = str_replace(array('[', ']'), '', $config['name']);
		if (empty($name)) {
			return 'fid-' . $this->tabindex;
		} else {
			return 'fid-' . $name;
		}
	}		
	
	/**
	 * Формирование class.
	 * 
	 * @paran array $config
	 * @return string
     */
	private function getClass($config, $append = array()) 
	{
		if (!is_array($config['class'])) {
			$config['class'] = explode(' ', $config['class']);
		}
		
		if (!empty($append)) {
			if (!is_array($append)) {
				$append = explode(' ', $append);
			}
			$config['class'] = array_merge($config['class'], $append);
		}

		$config['class'] = array_diff($config['class'], array());

		return implode(' ', $config['class']);
	}	

	/**
	 * Формирование заголовка поля.
	 * 
	 * @paran array $opt параметры поля
	 * @return string
     */
	private function getLabel($config) 
	{
		if (empty($config['label'])) {
			return null;
		} else {
			$res = '<label'; 
			if (!empty($config['id'])) {
				$res .= ' for="' . $config['id'] . '"'; 
			}
			if (!empty($config['required'])) {
				$res .= ' class="required"'; 
			}
			$res .= '>' . $config['label'] . ':</label> '; 

			return $res;
		}
	}

	/**
	 * Формирование подскизки и ошибки у поля формы.
	 * 
	 * @paran array $opt параметры поля
	 * @return string
     */
	private function getHelp($config) 
	{
		$res = '';
		if (!empty($config['help'])) {
			$res .= '<p class="help-block">' . $config['help'] . '</p>';
		}
		if (isset($this->error[$config['name']]) && !is_bool($this->error[$config['name']])) {
			$res .= '<p class="help-block error-block">' . $this->error[$config['name']] . '</p>';
		}
		
		return $res;
	}

	/**
	 * Формирование атрибутов.
	 * 
	 * @paran array $keys
	 * @paran array $config
	 * @return string
     */
	private function getAttrs($keys, $config) 
	{
		$res = array();
		foreach ($keys as $row) {
			if (!empty($config[$row])) {
				if (in_array($row, array('multiple', 'disabled', 'selected', 'checked'))) {
					$res[] = $row;
				} else {
					$res[] = $row . '="' . $config[$row] . '"';
				}
			}
		}
		
		if (!empty($config['attrs'])) {
			foreach ($config['attrs'] as $key => $row) {
				$res[] = $key . '="' . $row . '"';
			}
		}

		return implode(' ', $res);
	}

	
	/**
	 * Сетка.
	 * 
	 * @param array $cols
	 * @return string
     */
	public function grid($cols) 
	{
		$out = '';
		if (!empty($cols)) {
			$count = count($cols);
			$out .= '
			<table class="form-cols">
				<tbody>
					<tr>';
					foreach ($cols as $i => $items) {
						if ($i == 0) {
							$class = 'form-cols-first';
						} elseif ($i == ($count - 1)) {
							$class = 'form-cols-end';
						} else {
							$class = 'form-cols-center';
						}				
						
						$out .= '<td class="' . $class . '" style="width: ' . round(100 / $count) . '%">';
						foreach ((array) $items as $row) {
							$out .= $row;
						}
						$out .= '</td>';
					}
					$out .= '
					</tr>
				</tbody>
			</table>';		
		}

		return $out;
	}
	
	public function cols($config)
	{
		$count = ceil(count($config['items']) / $config['cols']);
		return $this->grid(array_chunk($config['items'], $count));
	}

	/**
	 * Группа полей формы.
	 * 
	 * @return string
     */
	public function group($config)
	{
		ob_start();
		?>
	
		<div class="legend-group">
			<?php if (!empty($config['label'])): ?><legend><?php echo $config['label']; ?></legend><?php endif; ?>  
			<div class="legend-body">
				<?php 
				if (!empty($config['header'])) {
					echo '<p>' . $config['header'] . '</p>';
				}

				if (!empty($config['cols'])) {
					echo $this->cols(array('cols' => $config['cols'], 'items' => $config['items']));
				} elseif (!empty($config['grid'])) {
					echo $this->grid($config['grid']);
				} else {
					foreach ($config['items'] as $row) {
						echo $row;
					}						
				}

				if (!empty($config['footer'])) {
					echo '<p>' . $config['footer'] . '</p>';
				}
				?>
			</div>			
		</div>

		<?php
		return ob_get_clean();	
	}
	
	/**
	 * Дерево радиокнопок.
	 * 
	 * @paran array $config параметры поля
	 * @return string
     */
	public function treeRadio($config) 
	{
		$this->tabindex++;

		$default = array(
			'label'            => '',                        // загаловок
			'required'         => false,                     // обезателен для заполнения
			'name'  	       => '',					     // название
			'value'            => $this->getValue($config),  // значение			
			'id'               => $this->getId($config),     // id  
			'class'            => array(),                   // class		
			'style'            => '',                        // атрибут style
			'attrs'            => array(),                   // дополнительные атрибуты
			'options_first'    => null, 			         // первый, если null - не выводится					
			'options_disabled' => array(),				     // id записей которые будут заблокированы для выбора
			'options'          => array(),                   // элементы списка.
			'help'             => '' 					     // подсказака
		);
		
		$config = array_merge($default, $config);
		
		// Преобразование масива в древовидный.
		$config['options'] = Tree::convert($config['options']);

		$out = '
		<div class="form-group form-group-tree-radio' . $this->getErrorClass($config['name']) . '">
			' . $this->getLabel($config) . '
			<ul id="' . $config['id'] . '" class="treeview">';
			if (!is_null($config['options_first'])) {
				$out .= '<li class="livel_1"><label><input type="radio" ' . $this->getAttrs(array('class', 'name', 'style'), $config) . '> ' . $config['options_first'] . '</label></li>';
			}
			foreach ($config['options'] as $row) {
				$out .= '
				<li class="livel_1"><label' . self::muted($config['options_disabled'], $row['id']) . '><input type="radio" ' . $this->getAttrs(array('class', 'name', 'style'), $config) . ' value="' . $row['id'] . '" ' . self::checked($config['value'], $row['id']) . ' ' . self::disabled($config['options_disabled'], $row['id']) . '> ' . $row['name'] . '</label>';
				if (isset($row['children'])) {
					$out .= $this->treeRadioOptions($config, $row['children'], 1);
				}
				$out .= '</li>';
			}
			$out .= '
			</ul>
			' . $this->getHelp($config) . '
		</div>
		
		<script>
		$("#' . $config['id'] . '").treeview({
			collapsed: true,
			animated:  "medium",
			unique:    true,
			persist:   "checked" 
		});
		</script>'; 
		
		return $out;
	}
	
	private function treeRadioOptions($config, $options, $livel) 
	{
		$out = '<ul>';
		$livel++;
		foreach ($options as $row) {
			$out .= '
			<li class="livel_' . $livel . '">
				<label' . self::muted($config['options_disabled'], $row['id']) . '><input type="radio" ' . $this->getAttrs(array('class', 'name', 'style'), $config) . ' value="' . $row['id'] . '" ' . self::checked($config['value'], $row['id']) . ' ' . self::disabled($config['options_disabled'], $row['id']) . '> ' . $row['name'] . '</label>';
				if (isset($row['children'])) {
					$out .= $this->treeRadioOptions($config, $row['children'], $livel);
				}
				$out .= '
			</li>';
		}
		return $out . '</ul>';
	}
	
	/**
	 * Дерево радиокнопок.
	 * 
	 * @paran array $config параметры поля
	 * @return string
     */
	public function listCheckbox($config) 
	{
		$default = array(
			'label'            => '',                        // загаловок
			'required'         => false,                     // обезателен для заполнения
			'name'  	       => '',					     // название
			'value'            => $this->getValue($config),  // значение
			'id'               => $this->getId($config),     // id       
			'class'            => array(),                   // class
			'size'             => 0,                         // кол-во строк 
			'multiple'         => false,                     // мультивыборка
			'disabled'         => false,  			         // заблокировать поле
			'tabindex'         => $this->tabindex++,         // атрибут tabindex, если не указан выводится автоматически.
			'style'            => '',                        // атрибут style
			'attrs'            => array(),                   // дополнительные атрибуты (onclick="return false" и т.д.), задаются массивом: array('onclick' => 'return false;')				
			'options_first'    => null, 			         // первый options, если null - не выводится					
			'options_disabled' => array(),				     // id записей которые будут заблокированы для выбора
			'options'          => array(),                   // элементы списка.
			'help'             => '', 					     // подсказака для поля
			'chunk'             => false
		);

		$config = array_merge($default, $config);
		$config['class'] = $this->getClass($config, 'form-control');
		ob_start();
		?>  
		
		<div class="form-group<?php if (isset($this->error[$config['name']])) echo ' has-error'; if (!empty($config['required'])) echo ' required'; ?>">
			<?php echo $this->getLabel($config); ?>
			
			<?php
			$out = '';
			$out .= '<div id="' . $config['id'] . '" class="treeview">';
			if (!is_null($config['options_first'])) {
				$out .= '<li class="livel_1"><label><input type="checkbox" ' . $this->getAttrs(array('class', 'name', 'style'), $config) . ' value="' . $row['id'] . '"> ' . $config['options_first'] . '</label></li>';
			}

			if ($config['chunk']) {
				$chunk = array_chunk($config['options'], 2, true);
			}

			if ($chunk) {
				$out .= '<table class="" style="background: #fcf8e3; width: 100%">';
				foreach ($chunk as $key => $row) {
					$out .= '<tr>';
					foreach ($row as $key2 => $checkbox) {
						$out .= '<td style="padding: 5px 10px;"><div class="checkbox"><label' . self::muted($config['options_disabled'], $key) . '><input type="checkbox" ' . $this->getAttrs(array('name', 'style'), $config) . ' value="' . $key2 . '"' . checked($config['value'], $key2) . disabled($config['options_disabled'], $key2) . '> ' . $checkbox . '</label></div></td>';
					}
					$out .= '</tr>';

				}
				$out .= '</table>';
			} else {
				foreach ($config['options'] as $key => $row) {
					if (is_array($row)) {
						echo '<div class="checkbox"><label' . self::muted($config['options_disabled'], $row['id']) . '><input type="checkbox" ' . $this->getAttrs(array('name', 'style'), $config) . ' value="' . $row['id'] . '"' . checked($config['value'], $row['id']) . disabled($config['options_disabled'], $row['id']) . '> ' . $row['name'] . '</label></div>';
					} else {
						echo '<div class="checkbox"><label' . self::muted($config['options_disabled'], $key) . '><input type="checkbox" ' . $this->getAttrs(array('name', 'style'), $config) . ' value="' . $key . '"' . checked($config['value'], $key) . disabled($config['options_disabled'], $key) . '> ' . $row . '</label></div>';
					}
				}
			}
			$out .= '</div>';
			echo $out;
			?>

			<?php echo $this->getHelp($config); ?>
		</div>  

		<?php
		return ob_get_clean();
	}
	
	
	/**
	 * Дерево радиокнопок.
	 * 
	 * @paran array $config параметры поля
	 * @return string
     */
	public function treeCheckbox($config) 
	{
		$this->tabindex++;

		$default = array(
			'label'            => '',                        // загаловок
			'required'         => false,                     // обезателен для заполнения
			'name'  	       => '',					     // название
			'value'            => $this->getValue($config),  // значение			
			'id'               => $this->getId($config),     // id  
			'class'            => array(),                   // class		
			'style'            => '',                        // атрибут style
			'attrs'            => array(),                   // дополнительные атрибуты
			'options_first'    => null, 			         // первый, если null - не выводится					
			'options_disabled' => array(),				     // id записей которые будут заблокированы для выбора
			'options'          => array(),                   // элементы списка.
			'help'             => '' 					     // подсказака
		);
		
		$config = array_merge($default, $config);
		
		// Преобразование масива в древовидный.
		$config['options'] = Tree::convert($config['options']);

		$out = '
		<div class="form-group form-group-tree-checkbox' . $this->getErrorClass($config['name']) . '">
			' . $this->getLabel($config) . '
			<ul id="' . $config['id'] . '" class="treeview">';
			if (!is_null($config['options_first'])) {
				$out .= '<li class="livel_1"><label><input type="checkbox" ' . $this->getAttrs(array('class', 'name', 'style'), $config) . ' value="' . $row['id'] . '"> ' . $config['options_first'] . '</label></li>';
			}
			foreach ($config['options'] as $row) {
				$out .= '<li class="livel_1"><label' . self::muted($config['options_disabled'], $row['id']) . '><input type="checkbox" ' . $this->getAttrs(array('class', 'name', 'style'), $config) . ' value="' . $row['id'] . '" ' . self::checked($config['value'], $row['id']) . ' ' . self::disabled($config['options_disabled'], $row['id']) . '> ' . $row['name'] . '</label>';
				if (isset($row['children'])) {
					$out .= $this->treeCheckboxOptions($config, $row['children'], 1);
				}
				$out .= '</li>';
			}
			$out .= '</ul>
			' . $this->getHelp($config) . '
		</div>

		<script>
		$("#' . $config['id'] . '").treeview({
			collapsed: true,
			animated:  "medium",
			unique:    true,
			persist:   "checked" 
		});
		</script>'; 
		
		return $out;
	}
	
	private function treeCheckboxOptions($config, $options, $livel) 
	{
		$out = '<ul>';
		$livel++;
		foreach ($options as $row) {
			$out .= '<li class="livel_' . $livel . '"><label' . self::muted($config['options_disabled'], $row['id']) . '><input type="checkbox" ' . $this->getAttrs(array('class', 'name', 'style'), $config) . ' value="' . $row['id'] . '" ' . self::checked($config['value'], $row['id']) . ' ' . self::disabled($config['options_disabled'], $row['id']) . '> ' . $row['name'] . '</label>';
			if (isset($row['children'])) {
				$out .= $this->treeCheckboxOptions($config, $row['children'], $livel);
			}
			$out .= '</li>';
		}
		return $out . '</ul>';
	}
	
	
	
	
	
	/**
	 * Выподающий список.
	 * 
	 * @paran array $config параметры поля
	 * @return string
     */
	public function select($config) 
	{
		$default = array(
			'label'            => '',                        // загаловок
			'required'         => false,                     // обезателен для заполнения
			'name'  	       => '',					     // название
			'value'            => $this->getValue($config),  // значение
			'id'               => $this->getId($config),     // id
			'class'            => array(),                   // class
			'size'             => 0,                         // кол-во строк
			'multiple'         => false,                     // мультивыборка
			'disabled'         => false,  			         // заблокировать поле
			'tabindex'         => $this->tabindex++,         // атрибут tabindex, если не указан выводится автоматически.
			'style'            => '',                        // атрибут style
			'attrs'            => array(),                   // дополнительные атрибуты (onclick="return false" и т.д.), задаются массивом: array('onclick' => 'return false;')
			'options_first'    => null, 			         // первый options, если null - не выводится					
			'options_disabled' => array(),				     // id записей которые будут заблокированы для выбора
			'options'          => array(),                   // элементы списка.
			'help'             => '' 					     // подсказака для поля
		);

		$config = array_merge($default, $config);
		$config['class'] = $this->getClass($config, 'form-control');
		ob_start();
		?>  
		
		<div class="form-group<?php if (isset($this->error[$config['name']])) echo ' has-error'; if (!empty($config['required'])) echo ' required'; ?>">
			<?php echo $this->getLabel($config); ?>
			<select <?php echo $this->getAttrs(array('class', 'id', 'size', 'name', 'multiple', 'disabled', 'tabindex', 'style'), $config); ?>>
				<?php
				if (!is_null($config['options_first'])) {
					echo '<option value="">' . $config['options_first'] . '</option>';
				}
				foreach ($config['options'] as $key => $row) {
					if (is_array($row)) {
						echo '<option value="' . $row['id'] . '"' . selected($config['value'], $row['id']) . disabled($config['options_disabled'], $row['id']) . '>' . $row['name'] . '</option>';
					} else {
						echo '<option value="' . $key . '"' . selected($config['value'], $key) . disabled($config['options_disabled'], $key) . '>' . $row . '</option>';
					}
				}
				?>
			</select>
			<?php echo $this->getHelp($config); ?>
		</div>  

		<?php
		return ob_get_clean();
	}
	
	/**
	 * Древовидный выподающий список.
	 * 
	 * @paran array $config параметры поля
	 * @return string
     */
	public function selectTree($config) 
	{
		$default = array(
			'label'            => '',                        // загаловок
			'required'         => false,                     // обезателен для заполнения
			'name'  	       => '',					     // название
			'value'            => $this->getValue($config),  // значение
			'id'               => $this->getId($config),     // id       
			'class'            => array(),                   // class
			'size'             => 0,                         // кол-во строк 
			'multiple'         => false,                     // мультивыборка
			'disabled'         => false,  			         // заблокировать поле
			'tabindex'         => $this->tabindex++,         // атрибут tabindex, если не указан выводится автоматически.
			'style'            => '',                        // атрибут style
			'attrs'            => array(),                   // дополнительные атрибуты (onclick="return false" и т.д.), задаются массивом: array('onclick' => 'return false;')				
			'options_first'    => null, 			         // первый options, если null - не выводится					
			'options_disabled' => array(),				     // id записей которые будут заблокированы для выбора
			'options'          => array(),                   // элементы списка.
			'options_bind'     => 'parent', 		         // название поля связующие родителей и потомков списка.
			'help'             => '' 					     // подсказака для поля
		);

		$config = array_merge($default, $config);
		$config['class'] = $this->getClass($config, 'form-control');
		ob_start();
		?>  
		
		<div class="form-group<?php if (isset($this->error[$config['name']])) echo ' has-error'; if (!empty($config['required'])) echo ' required'; ?>">
			<?php echo $this->getLabel($config); ?>
			<select <?php echo $this->getAttrs(array('class', 'id', 'size', 'name', 'multiple', 'disabled', 'tabindex', 'style'), $config); ?>>
				<?php 
				if (!is_null($config['options_first'])) {
					echo '<option value="">' . $config['options_first'] . '</option>';
				}
				echo $this->optionsTree($config, $config['options']);
				?>
			</select>
			<?php echo $this->getHelp($config); ?>
		</div>  

		<?php
		return ob_get_clean();
	}
	
	private function optionsTree($config, $options, $_parent = 0, $_livel = 0) 
	{
		$res = '';
		$_livel++;
		foreach ($options as $i => $row) {
			if ($row[$config['options_bind']] == $_parent) {
				array_slice($options, $i);
				$res .= '<option value="' . $row['id'] . '"' . selected($config['value'], $row['id']) . disabled($config['options_disabled'], $row['id']) . '>';
				for ($i = 1; $i < $_livel; $i++) {
					$res .= '&emsp;&emsp;';
				}
				$res .= $row['name'] . '</option>';
				$res .= $this->optionsTree($config, $options, $row['id'], $_livel);
			}
		}

		return $res;
	}

	/**
	 * Текстовое поле.
	 * 
	 * @paran array $config параметры поля
	 * @return string
     */
	public function text($config) 
	{
		$default = array(
			'label'            => '',                        // загаловок
			'required'         => false,                     // обезателен для заполнения
			'name'  	       => '',					     // название
			'value'            => $this->getValue($config),  // значение
			'id'               => $this->getId($config),     // id       
			'class'            => array(),                   // class
			'disabled'         => false,  			         // заблокировать поле
			'tabindex'         => $this->tabindex++,         // атрибут tabindex, если не указан выводится автоматически.
			'style'            => '',                        // атрибут style
			'group_style'      => '',
			'placeholder'      => '',                        // атрибут placeholder
			'type'             => 'text',                    // атрибут type
			'attrs'            => array(),                   // дополнительные атрибуты (onclick="return false" и т.д.), задаются массивом: array('onclick' => 'return false;')
			'help'             => ''       				     // подсказака для поля
		);

		$config = array_merge($default, $config);
		$config['class'] = $this->getClass($config, 'form-control form-control-text');
		ob_start();
		?>  

		<div style="<?php if($config['group_style']) echo $config['group_style']; ?>" class="form-group<?php if (isset($this->error[$config['name']])) echo ' has-error'; if (!empty($config['required'])) echo ' required'; ?>">
			<?php echo $this->getLabel($config); ?>
			<input <?php echo $this->getAttrs(array('class', 'id', 'name', 'type', 'disabled', 'value', 'tabindex', 'placeholder', 'style'), $config); ?>>
			<?php echo $this->getHelp($config); ?>
		</div>  

		<?php
		return ob_get_clean();
	}
	
	/**
	 * Текстовое поле.
	 * 
	 * @paran array $config параметры поля
	 * @return string
     */
	public function password($config) 
	{
		$default = array(
			'label'            => '',                        // загаловок
			'required'         => false,                     // обезателен для заполнения
			'name'  	       => '',					     // название
			'id'               => $this->getId($config),     // id       
			'class'            => array(),                   // class
			'disabled'         => false,  			         // заблокировать поле
			'tabindex'         => $this->tabindex++,         // атрибут tabindex, если не указан выводится автоматически.
			'style'            => '',                        // атрибут style
			'placeholder'      => '',                        // атрибут placeholder
			'type'             => 'password',                // атрибут type
			'attrs'            => array(),                   // дополнительные атрибуты (onclick="return false" и т.д.), задаются массивом: array('onclick' => 'return false;')				
			'help'             => '' 					     // подсказака для поля
		);

		$config = array_merge($default, $config);
		$config['class'] = $this->getClass($config, 'form-control form-control-text');
		ob_start();
		?>  

		<div class="form-group<?php if (isset($this->error[$config['name']])) echo ' has-error'; if (!empty($config['required'])) echo ' required'; ?>">
			<?php echo $this->getLabel($config); ?>
			<input <?php echo $this->getAttrs(array('class', 'id', 'name', 'type', 'disabled', 'tabindex', 'placeholder', 'style'), $config); ?>>
			<?php echo $this->getHelp($config); ?>
		</div>  

		<?php
		return ob_get_clean();
	}
	
	public function datetime($config) 
	{
		$default = array(
			'label'            => '',                        // загаловок
			'required'         => false,                     // обезателен для заполнения
			'name'  	       => '',					     // название
			'value'            => $this->getValue($config),  // значение
			'id'               => $this->getId($config),     // id       
			'class'            => array(),                   // class
			'disabled'         => false,  			         // заблокировать поле
			'tabindex'         => $this->tabindex++,         // атрибут tabindex, если не указан выводится автоматически.
			'style'            => '',                        // атрибут style
			'placeholder'      => '',                        // атрибут placeholder
			'type'             => 'text',                    // атрибут type
			'attrs'            => array(),                   // дополнительные атрибуты (onclick="return false" и т.д.), задаются массивом: array('onclick' => 'return false;')				
			'help'             => '' 					     // подсказака для поля
		);

		$config = array_merge($default, $config);
		$config['class'] = $this->getClass($config, 'form-control datetimepicker');
		$config['value'] = ($this->getValue($config)) ? date('d.m.Y H:s', $this->getValue($config)) : date('d.m.Y H:s'); 

		ob_start();
		?>  

		<div class="form-group<?php if (isset($this->error[$config['name']])) echo ' has-error'; ?>">
			<?php echo $this->getLabel($config); ?>
			<input <?php echo $this->getAttrs(array('class', 'id', 'name', 'type', 'disabled', 'value', 'tabindex', 'placeholder', 'style'), $config); ?>>
			<?php echo $this->getHelp($config); ?>
		</div>  
		
		<?php
		return ob_get_clean();
	}
	
	/**
	 * Текстовое поле (значение).
	 * 
	 * @paran array $config параметры поля
	 * @return string
     */
	public function value($config) 
	{
		$default = array(
			'label'            => '',                        // загаловок
			'required'         => false,                     // обезателен для заполнения
			'name'  	       => '',					     // название
			'value'            => $this->getValue($config),  // значение
			'id'               => $this->getId($config),     // id       
			'class'            => array(),                   // class
			'disabled'         => false,  			         // заблокировать поле
			'tabindex'         => $this->tabindex++,         // атрибут tabindex, если не указан выводится автоматически.
			'style'            => '',                        // атрибут style
			'placeholder'      => '',                        // атрибут placeholder
			'type'             => 'text',                    // атрибут type
			'attrs'            => array(),                   // дополнительные атрибуты (onclick="return false" и т.д.), задаются массивом: array('onclick' => 'return false;')				
			'help'             => '', 					     // подсказака для поля
			'unit'             => '' 					     // 
		);

		$config = array_merge($default, $config);
		$config['class'] = $this->getClass($config, 'form-control');
		ob_start();
		?>  

		<div class="form-group<?php if (isset($this->error[$config['name']])) echo ' has-error'; ?>">
			<?php echo $this->getLabel($config); ?>
			<div class="input-group">
				<input <?php echo $this->getAttrs(array('class', 'id', 'name', 'type', 'disabled', 'value', 'tabindex', 'placeholder', 'style'), $config); ?>>
				<span class="input-group-addon"><?php echo $config['unit']; ?></span>
			</div>
			<?php echo $this->getHelp($config); ?>
		</div>  

		<?php
		return ob_get_clean();
	}
	
	
	private function getErrorClass($name) 
	{
		return (isset($this->error[$name])) ? ' has-error' : '';
	}

	/**
	 * Textarea.
	 * http://htmlbook.ru/html/textarea
	 * http://getbootstrap.com/css/#forms 
	 * 
	 * @paran array $config параметры поля
	 * @return string
     */
	public function textarea($config) 
	{
		$default = array(
			'label'       => '',                        // Загаловок
			'required'    => false,                     // Обезателен ли для заполнения
			'name'        => '',					    // Название
			'value'       => $this->getValue($config),  // Значение      
			'class'       => array(),                   // Class
			'id'          => $this->getId($config),     // id       
			'cols'        => '',  			            // Ширина поля в символах
			'rows'        => 5,  			            // Высота поля в строках текста
			'style'       => '',                        // Атрибут style
			'placeholder' => '',                        // Атрибут placeholder
			'attrs'       => array(),                   // Дополнительные атрибуты, задаются массивом: array('onclick' => 'return false;')		
			'maxlength'   => '',  			            // Максимальное число введенных символов.
			'readonly'    => false,  			        // Устанавливает, что поле не может изменяться пользователем.
			'disabled'    => false,  			        // Заблокировать поле
			'tabindex'    => $this->tabindex++,         // Атрибут tabindex, если не указан выводится автоматически.
			'help'        => '' 					    // Подсказака для поля
		);

		$config = array_merge($default, $config);
		$config['class'] = $this->getClass($config, array('form-control form-control-textarea'));
		$required = (!empty($config['required'])) ? ' required' : ''; 
		
		return '
		<div class="form-group form-group-textarea' . $this->getErrorClass($config['name']) . $required . '">
			' . $this->getLabel($config) . '
			<textarea ' . $this->getAttrs(array('class', 'id', 'name', 'disabled', 'cols', 'rows', 'style', 'tabindex', 'placeholder', 'attrs'), $config) . '>' . prepare_text($config['value']) . '</textarea>
			' . $this->getHelp($config) . '
		</div>';
	}
	
	/**
	 * Submit.
	 * 
	 * @paran array $config параметры поля
	 * @return string
     */
	public function submit($config) 
	{
		$default = array(
			'label'       => '',                        // Загаловок
			'name'        => '',					    // Название
			'value'       => '',                        // Значение      
			'class'       => array(),                   // Class
			'id'          => $this->getId($config),     // id   
			'type'       => 'submit',                   
			'disabled'    => false,  			        // Заблокировать поле
			'tabindex'    => $this->tabindex++,         // Атрибут tabindex, если не указан выводится автоматически.
			'style'       => '',                        // Атрибут style
			'attrs'       => array()                   // Дополнительные атрибуты, задаются массивом: array('onclick' => 'return false;')		
		);

		$config = array_merge($default, $config);
		$config['class'] = $this->getClass($config, array('form-control-submit'));

		return '
		<div class="form-group' . $this->getErrorClass($config['name']) . '">
			<button ' . $this->getAttrs(array('name', 'value', 'class', 'id', 'type', 'disabled', 'tabindex', 'style', 'attrs'), $config) . '>' . $config['label'] . '</button>
		</div>';
	}
	
	public function file($config) 
	{
		$default = array(
			'label'       => '',                        // Загаловок
			'name'        => '',					    // Название
			'class'       => array(),                   // Class
			'id'          => $this->getId($config),     // id   
			'disabled'    => false,  			        // Заблокировать поле
			'tabindex'    => $this->tabindex++,         // Атрибут tabindex, если не указан выводится автоматически.
			'style'       => '',                        // Атрибут style
			'attrs'       => array()                   // Дополнительные атрибуты, задаются массивом: array('onclick' => 'return false;')		
		);

		$config = array_merge($default, $config);
		$config['class'] = $this->getClass($config, array('form-control-submit'));

		return '
		<div class="form-group' . $this->getErrorClass($config['name']) . '">
			' . $this->getLabel($config) . '
			<input type="file" ' . $this->getAttrs(array('name', 'class', 'id', 'disabled', 'tabindex', 'style', 'attrs'), $config) . '>
			' . $this->getHelp($config) . '
		</div>';
	}

	/**
	 * Визуальный редактор.
	 * 
	 * @paran array $config параметры поля
	 * @return string
     */
	public function html($config) 
	{
		$default = array(
			'label'            => '',                        // загаловок
			'required'         => false,                     // обезателен для заполнения
			'name'  	       => '',					     // название
			'value'            => $this->getValue($config),  // значение      
			'class'            => array(),                   // class
			'id'               => $this->getId($config),
			'disabled'         => false,  			         // заблокировать поле
			'tabindex'         => $this->tabindex++,         // атрибут tabindex, если не указан выводится автоматически.
			'help'             => '' 					     // подсказака для поля
		);

		$config = array_merge($default, $config);
		$config['class'] = $this->getClass($config, array('ckeditor', 'form-control'));
		ob_start();
		?>  

		<div class="form-group<?php if (isset($this->error[$config['name']])) echo ' has-error'; ?>">
			<?php echo $this->getLabel($config); ?>
			<textarea <?php echo $this->getAttrs(array('class', 'id', 'name', 'disabled', 'tabindex'), $config); ?>><?php echo prepare_text($config['value']); ?></textarea>
			<?php echo $this->getHelp($config); ?>
		</div>

		<?php
		return ob_get_clean();
	}
	
	/**
	 * Чекбокс.
	 * 
	 * @paran array $config параметры поля
	 * @return string
     */
	public function checkbox($config) 
	{
		$default = array(
			'label'            => '',                        // загаловок
			'name'  	       => '',					     // название
			'value'            => $this->getValue($config),  // значение
			'id'               => $this->getId($config),     // id       
			'class'            => array(),                   // class
			'disabled'         => false,  			         // заблокировать поле
			'tabindex'         => $this->tabindex++,         // атрибут tabindex, если не указан выводится автоматически.
			'style'            => '',                        // атрибут style
			'attrs'            => array(),                   // дополнительные атрибуты (onclick="return false" и т.д.), задаются массивом: array('onclick' => 'return false;')				
			'help'             => '' 					     // подсказака для поля
		);

		$config = array_merge($default, $config);
		$config['class'] = $this->getClass($config);
		ob_start();
		?>  

		<div class="form-group checkbox<?php if (isset($this->error[$config['name']])) echo ' has-error'; ?>">
			<label>
				<input type="checkbox" <?php echo checked($config['value']); ?> <?php echo $this->getAttrs(array('class', 'id', 'name', 'disabled', 'tabindex', 'style'), $config); ?>> <?php echo $config['label']; ?>
			</label>
			<?php echo $this->getHelp($config); ?>
		</div>

		<?php
		return ob_get_clean();
	}
	
	
	/**
     * Загрузка файлов.
     */
	public function files($config)
	{
		$file = $this->instance->files[$config['using']];
		$config['name'] = 'files_' . $config['using'];
	
		$default = array(
			'label'            => '',                          // загаловок
			'name'  	       => '',	                       // название
			'using'            => '',
			'required'         => false,                       // обезателен для заполнения  
			'id'               => $this->getId($config),                                // id     			
			'class'            => array(),                                              // class
			'disabled'         => false,  			                                    // заблокировать поле
			'tabindex'         => $this->tabindex++,                                    // атрибут tabindex, если не указан выводится автоматически.
			'help'             => 'Формат файла: ' . implode(', ', Files::$default_ext), // подсказака для поля
			'size'             => '155x130',
			'control-label'    => 'true'
		);

		$config = array_merge($default, $config);	
		$config['class'] = $this->getClass($config);
		ob_start();

		if (empty($file['type'])) {
			// Изображения
			if (empty($file['multiple'])) {
				?>
				<div class="form-group">
					<?php echo $this->getLabel($config); ?>
					<?php if (!empty($file['data'])): ?>
					<ul class="thumbnail-images">
						<?php foreach($file['data'] as $row): ?>
						<li class="thumbnail">
							<a target="_blank" href="/uploads/<?php echo $row['module'] . '/' . $row['filename']; ?>">
								<img src="/uploads/<?php echo $row['module'] . '/'.$config['size'].'/' . $row['filename']; ?>" />
							</a>
							<div class="caption">
								<p class="form-group">
									<input class="form-control" type="text" name="files_<?php echo $config['using']; ?>_alt[<?php echo $row['id']; ?>]" value="<?php echo $row['alt']; ?>" placeholder="Описание">
								</p>
								<p class="form-inline">
									<span class="text-muted pull-right"><?php echo convert_bytes($row['size']); ?></span>
									<div class="checkbox">
										<label class="checkbox text-danger">
											<input type="checkbox" name="files_<?php echo $config['using']; ?>_remove[]" value="<?php echo $row['id']; ?>" onclick="return imagesRemove(this);"> удалить
										</label>
									</div>
								</p>
							</div>
						</li>
						<?php endforeach; ?>
					</ul>
					<?php else: ?>	
					<div class="thumbnail-control">
						<input type="file" name="<?php echo $config['name']; ?>" <?php echo $this->getAttrs(array('class', 'id', 'disabled', 'tabindex', 'style'), $config); ?>>
					</div>
					<?php echo $this->getHelp($config); ?>
					<?php endif; ?>
				</div>
		
				<?php
			} else {
				$count = (empty($file['data'])) ? 0 : count($file['data']);
				?>
	
				<div class="form-group">
					<?php echo $this->getLabel($config); ?>
					<?php if (!empty($file['data'])): ?>
					<ul class="thumbnail-images<?php if ($count > 1) echo ' thumbnail-sort'; ?>">
						<?php foreach($file['data'] as $row): ?>
						<li class="thumbnail">
							<a target="_blank" href="/uploads/<?php echo $row['module'] . '/' . $row['filename']; ?>">
								<img src="/uploads/<?php echo $row['module'] . '/155x130/' . $row['filename']; ?>" />
							</a>
							<div class="caption">
								<p class="form-group">
									<input class="form-control" type="text" name="files_<?php echo $config['using']; ?>_alt[<?php echo $row['id']; ?>]" value="<?php echo $row['alt']; ?>" placeholder="Описание">
								</p>
								<p class="form-inline">
									<span class="text-muted pull-right"><?php echo convert_bytes($row['size']); ?></span>
									<div class="checkbox">
										<label class="checkbox text-danger">
											<input type="checkbox" name="files_<?php echo $config['using']; ?>_remove[]" value="<?php echo $row['id']; ?>" onclick="return imagesRemove(this);"> удалить
										</label>
									</div>
								</p>
							</div>
						</li>
						<?php endforeach; ?>
					</ul>
					<?php endif; ?>	
					<div class="thumbnail-control">
						<?php if ($count > 1): ?>
						<div class="pull-right">
							<a href="#" class="btn btn-primary btn-xs" onclick="return imagesState(this);"><i class="ico ico-checkbox-checked"></i> Отметить/снять все</a>
						</div>
						<?php endif; ?>	
						<input type="file" name="<?php echo $config['name']; ?>[]" multiple <?php echo $this->getAttrs(array('class', 'id', 'disabled', 'tabindex', 'style'), $config); ?>>
					</div>
					<?php echo $this->getHelp($config); ?>
				</div>
				<?php
			}
		} else {
            // Файлы
			if (empty($file['multiple'])) {
                ?>
				<div class="control-group">	
					<label class="control-label"><?php echo $config['label']; ?></label>
					<div class="controls">
						<?php if (empty($file['data'])): ?>
						<input type="file" name="files_<?php echo $config['using'] ?>">
						<?php if (!empty($file['allow'])): ?><span class="help-block">Формат файла: <?php echo implode(', ', $file['allow']); ?></span><?php endif; ?>
						<?php else: ?>
						<table class="filelist">
							<tbody>
								<?php foreach($file['data'] as $row): ?>
								<tr>
									<td><input type="checkbox" name="files_<?php echo $config['using']; ?>_remove[]" value="<?php echo $row['id']; ?>" title="Удалить" onclick="setRemoveFile(this);"></td>
									<td><a target="_blank" href="/uploads/<?php echo $row['module'] . '/' . $row['filename']; ?>"><?php echo $row['filename']; ?></a>&ensp;<span class="muted"><?php echo convert_bytes($row['size']); ?></span></td>
									<td><input class="form-control form-control-text" type="text" name="files_<?php echo $config['using']; ?>_alt[<?php echo $row['id']; ?>]" value="<?php echo $row['alt']; ?>" placeholder="Описание файла"></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						<?php endif; ?>
					</div>
				</div>
				<?php
			} else { ?>
                <div class="border-control-group">
                <?php
                // Множество файлов
				if (!empty($file['data'])): ?>
				<div class="control-group">
					<?php if ($config['control-label']): ?>
						<label class="control-label"><?php echo $config['label']; ?></label>
					<?php endif; ?>
					<div class="controls">
						<table class="filelist filelist-sortable">
							<tbody>
								<?php foreach($file['data'] as $row): ?>
								<tr>
									<td><i class="ico-move"></i></td>
									<td><input type="checkbox" name="files_<?php echo $config['using']; ?>_remove[]" value="<?php echo $row['id']; ?>" title="Удалить" onclick="setRemoveFile(this);"></td>
									<td><a target="_blank" href="/uploads/<?php echo $row['module'] . '/' . $row['filename']; ?>"><?php echo $row['filename']; ?></a>&ensp;<span class="muted"><?php echo convert_bytes($row['size']); ?></span></td>
									<td><input class="form-control form-control-text" type="text" name="files_<?php echo $config['using']; ?>_alt[<?php echo $row['id']; ?>]" value="<?php echo $row['alt']; ?>" placeholder="Описание файла"></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
				<?php endif; ?> 
				<div class="control-group">
					<label class="control-label"><?php echo $config['label']; ?></label>
					<div class="controls">
						<input type="file" multiple="multiple" name="files_<?php echo $config['using']; ?>[]">
						<?php if (!empty($file['allow'])): ?><span class="help-block">Формат файлов: <?php echo implode(', ', $file['allow']); ?></span><?php endif; ?>
					</div>
				</div>
                <?php echo $this->getHelp($config); ?>
                </div>
				<?php
			}
		}

		return ob_get_clean();
	}
}