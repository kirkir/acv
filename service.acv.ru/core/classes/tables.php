<?php
/**
 * Гениратор таблиц Bootstrap 3.
 */
class Tables
{
	/**
	 * Массив данных.
	 * 
	 * @var array
     */
	private $items = array();	

	/**
	 * Название модуля для изображений.
	 * 
	 * @var string
     */
	public $imageModule = '';
	
	/**
	 * Название группы для изображений.
	 * 
	 * @var string
     */
	public $imageUsing = 'thumb';

	/**
	 * Конструктор.
	 * 
	 * @paran array $content
	 * @return void
     */
	public function __construct($items)
	{
		$this->items = $items;
	}

	/**
	 * Вывод.
	 * 
	 * @return struing
     */
	public function out($conf, $order = true, $class = '')
	{
		if (empty($this->items)) {
			return Alerts::show(Alerts::INFO, 'Список пуст.');
		} else {
			if (empty($conf['plugins']['order'])) {
				$res = '<table class="selection table table-bordered table-hover table-sort">';
			} else {
				$res = '<table class="selection table table-bordered table-hover table-sort table-order">';
			}

			if (!empty($conf['thead'])) {
				$res .= '<thead><tr>' . implode('', $conf['thead']) . '</tr></thead>';
			}
			
			if (!empty($conf['tbody'])) {
				$res .= '<tbody>' .  $this->render($conf['tbody']) . '</tbody>';
			}
			
			if (!empty($conf['body'])) {
				$res .= '<tbody>';
				foreach ($this->items as $items) {
					$res .= '<tr>';
					foreach ($conf['body'] as $row) {
						$res .= $row;
					}
					$res .= '</tr>';
				}
				$res .= '</tbody>';
			}
			
			

			return $res . '</table>';
		}
	}

	/**
     * Рендер файла.
     * 
     * @param string $filename.
     * @return string
     */
	public function render($filename, $_parent = 0, $livel = 0) 
	{
		$res = '';
		if (!isset($this->items[0]['parent'])) {
			// Список.
			foreach ($this->items as $i => $this->item) {
				array_slice($this->items, $i);
				$this->item['tr_livel'] = 0;
				ob_start();
				include $filename;
				$res .= ob_get_clean();
			}	
		} else {
			// Дерево
			$livel++;
			foreach ($this->items as $i => $this->item) {
				if ($this->item['parent'] == $_parent) {
					// Изымаем текущую строку из массива.
					array_slice($this->items, $i);
					
					// Уровень вложености строки.
					$this->item['tr_livel'] = $livel;

					// Есть ли наследники.
					$this->item['tr_childs'] = false;
					foreach ($this->items as $row) {
						if ($row['parent'] == $this->item['id']) {
							$this->item['tr_childs'] = true;
							break;
						}
					}

					ob_start();
					include $filename;
					$res .= ob_get_clean();

					if ($this->item['tr_childs']) {
						$res .= $this->render($filename, $this->item['id'], $livel);
					}
				}
			}		
		}

		return $res;
	}
	
	/**
	 * Строка таблицы.
	 * 
	 * @return string
     */
	public function tr($content)
	{  
		$class = '';
		if (isset($this->item['approve']) && empty($this->item['approve'])) {
			$class .= 'muted';
		} elseif (isset($this->item['executed']) && empty($this->item['executed'])) {
			$class .= 'bg-warning';
		} elseif (@$this->item['process'] == 1) {
            $class .= ' green';
        } elseif (Auth::perm(4) && @$this->item['late']) {
            $class .= ' red_order';
        }

		return '<tr class="' . $class . '" id="item-' . $this->item['id'] . '">' . implode('', $content) . '</tr>';
	}

	/**
	 * Заголовок "Отметить/снять все".
	 * 
	 * @return string
     */
	public function thCheck()
	{
		return '
		<th class="th-check">
			<i class="toogle-check ico ico-checkbox-checked" title="Отметить/снять все"></i>
		</th>';
	}	
	
	/**
	 * Ячейка "Отметить/снять все".
	 * 
	 * @return string
     */
	public function tdCheck()
	{
		return '
		<td class="td-check">
			<input type="checkbox" name="check_id[]" value="' . $this->item['id'] . '">
			<input type="hidden" name="sort_id[]" value="' . $this->item['id'] . '">
		</td>'; 
	}

	/**
	 * ID запеси.
	 * 
	 * @return string
     */
	public function thId($label = 'ID')
	{
		return '
		<th class="th-id">
			' . $label . '
		</th>';
	}	

	public function tdId()
	{
		return '
		<td class="td-id" data-order="' . $this->item['id'] . '">
			' . $this->item['id'] . '
			
		</td>';		
	}	
	
	/**
	 * ID запеси.
	 * 
	 * @return string
     */
	public function thCount($label = 'Кол-во')
	{
		return '<th class="th-count">' . $label . '</th>';
	}	

	public function tdCount($config)
	{
		if (!empty($config['href'])) {
			return '<td class="td-count"><a class="badge" href="' . $config['href'] . '">' . $config['value'] . '</a></td>';
		} else {
			return '<td class="td-count"><span class="badge">' . $config['value'] . '</span></td>';
		}
	}

	/**
	 * Изображение.
	 * 
	 * @return string
     */
	public function thText($label)
	{
		return '<th class="th-name">' . $label . '</th>';
	}
	
	/**
	 * Изображение.
	 * 
	 * @return string
     */
	public function tdText($text, $options = array(), $class = '')
	{
		return '
		<td class="td-name '.$class.'" style="' . @$options['style'] . '">
			' . $text . '
		</td>';		
	}

	/**
	 * Изображение.
	 * 
	 * @return string
     */
	public function thImage($label = 'Изображение')
	{
		return '<th class="th-image">' . $label . '</th>';
	}
	
	/**
	 * Изображение.
	 * 
	 * @return string
     */
	public function tdImage()
	{
		$image = (empty($this->imageModule)) ? '-' : Files::getImage($this->imageModule, $this->item['id'], $this->imageUsing, '80x0');

		return '
		<td class="td-image">
			<a href="' . get_url(Router::$url . '/edit/' . $this->item['id']) . '">' . $image . '</a>
		</td>';		
	}

    public function tdImageType()
	{
		$image = (empty($this->imageModule)) ? '-' : $item = DB::getRow("SELECT * FROM `#__files` WHERE `module` = ? AND `item` = ? AND `using` = ? ORDER BY `sort`", array($this->imageModule, $this->item['id'], 'thumb'));;
//		$image = (empty($this->imageModule)) ? '-' : Files::getImage($this->imageModule, $this->item['id'], $this->imageUsing, '80x0');

        if (!empty($image)) {
            $image = '<img src="/uploads/' . $this->imageModule . '/80x0/' . urlencode($item['filename']) . '" alt="' . $item['alt'] . '">';
        } else {
            $image = '<img src="/uploads/types/' . $this->item['type_d'] . '.png" alt="" width="80">';
        }

		return '
		<td class="td-image">
			<a href="' . get_url(Router::$url . '/edit/' . $this->item['id']) . '">' . $image . '</a>
		</td>';
	}
	
	
	/**
	 * Название.
	 * 
	 * @return string
     */
	public function thName($label = 'Название')
	{
		return '<th class="th-name">' . $label . '</th>';
	}
	
	public function tdName($config = null)
	{
		$type = '';
		if (!empty($this->item['type'])) {
			if ($this->item['type'] == 1) {
				$type = 'text-success';
			} elseif ($this->item['type'] == 2) {
				$type = 'text-danger';
			}
		}

		$name = no_empty(@$this->item['name'], '<em>Без имени</em>');
		$res = '
		<td class="td-name" id="item_' . $this->item['id'] . '" data-order="' . $name . '">
			<div class="livel_' . $this->item['tr_livel'] . '">';
				if (!empty($this->item['tr_childs'])) {
					//$res .= '1-';
				}
				$res .= '
				<a class="' . $type . '" href="' . get_url(Router::$url . '/edit/' . $this->item['id']) . @$config['url'] . '">' . $name . '</a>';
				if (isset($config['text'])) {
					$res .= '<small>' . $config['text'] . '</small>';	
				} 
				$res .= '
			</div>
		</td>';	

		return $res;
	}

	/**
	 * ЧПУ.
	 * 
	 * @return string
     */
	public function thSef($label = 'ЧПУ')
	{
		return '<th class="th-sef">' . $label . '</th>';
	}		
	
	/**
	 * ЧПУ.
	 * 
	 * @return string
     */
	public function tdSef()
	{
		return '<td class="td-sef">' . no_empty(@$this->item['sef'], @$this->item['alias']) . '</td>';
	}	

	/**
	 * Дата.
	 * 
	 * @return string
     */
	public function thDate($label = 'Дата')
	{
		return '<th class="th-date">' . $label . '</th>';
	}		
	
	/**
	 * Дата.
	 * 
	 * @return string
     */
	public function tdDate($label = 'Дата')
	{
		return '
		<td class="td-date">
			' . date_short(@$this->item['date_add']) . '
		</td>';
	}	

	/**
	 * TH - статус публикации.
	 * 
	 * @return string
     */
	public function thApprove($label = 'Статус')
	{
		return '<th class="th-icon">' . $label . '</th>';
	}	
	
	/**
	 * TD - статус публикации.
	 * 
	 * @return string
     */
	public function tdApprove($label = 'Статус')
	{
		if ($this->item['approve']) {
			$class = 'ico ico-checkmark';
			$title = 'Снять с публикации';
		} else {
			$class = 'ico ico-lock';
			$title = 'Опубликовать на сайте';
		}

		return '
		<td class="td-icon" data-order="' . $this->item['approve'] . '">
			<a class="' . $class . '" title="' . $title . '" href="' . get_url(Router::$url . '/approve/' . $this->item['id']) . '"></a>
		</td>';
	}		
	
	/**
	 * TH - статус публикации.
	 * 
	 * @return string
     */
	public function thExecuted($label = 'Статус')
	{
		return '<th class="th-icon">' . $label . '</th>';
	}	
	
	/**
	 * TD - статус публикации.
	 * 
	 * @return string
     */
	public function tdExecuted($label = 'Статус')
	{
		if ($this->item['executed']) {
			return '
			<td class="td-icon" data-order="' . $this->item['executed'] . '">
                <a class="ico ico-checkmark" title="Пометить как новое" href="' . get_url(Router::$url . '/executed/' . $this->item['id']) . '"></a>
			</td>';
		} else {
			return '
			<td class="td-icon" data-order="' . $this->item['executed'] . '">
				<a class="ico ico-flag" title="Пометить как прочитанное" href="' . get_url(Router::$url . '/executed/' . $this->item['id']) . '"></a>
			</td>';
		}
	}	
	

	public function thMain($label = 'На главной')
	{
		return '<th class="th-icon">' . $label . '</th>';
	}	

	public function tdMain()
	{
		if ($this->item['main']) {
			$class = 'ico ico-star-3';
			$title = 'Снять с главной';
		} else {
			$class = 'ico ico-star';
			$title = 'Вывести на главной';
		}

		return '
		<td class="td-icon" data-order="' . $this->item['main'] . '">
			<a class="' . $class . '" title="' . $title . '" href="' . get_url(Router::$url . '/main/' . $this->item['id']) . '"></a>
		</td>';
	}

	public function thNotYet($label = 'Недавние работы')
	{
		return '<th class="th-icon">' . $label . '</th>';
	}	

	public function tdNotYet()
	{
		if ($this->item['not_yet']) {
			$class = 'ico ico-star-3';
			$title = 'Убрать из блока недавние работы';
		} else {
			$class = 'ico ico-star';
			$title = 'Вывести в блоке недавние работы';
		}

		return '
		<td class="td-icon" data-order="' . $this->item['not_yet'] . '">
			<a class="' . $class . '" title="' . $title . '" href="' . get_url(Router::$url . '/not_yet/' . $this->item['id']) . '"></a>
		</td>';
	}

	/**
	 * TH - удалить (только для администратора).
	 * 
	 * @return string
     */
	public function thRemove($label = 'Удалить')
	{
		if (Auth::perm(3)) {
			return '<th class="th-icon">' . $label . '</th>';
		} else {
			return null;
		}
	}
	
	/**
	 * TD - удалить (только для администратора).
	 * 
	 * @return string
     */
	public function tdRemove()
	{
		if (Auth::perm(3)) {
			return '
			<td class="td-icon">
				<a class="ico-remove" title="Удалить" onclick="return confirmRemove();" href="' . get_url(Router::$url . '/remove/' . $this->item['id']) . '"></a>
			</td>';
		} else {
			return null;
		}
	}

    public function tdRemoveShip()
    {
        if (Auth::perm(3)) {
            return '
			<td class="td-icon">
				<a class="ico-remove" title="Удалить" onclick="return confirmRemove();" href="' . get_url(Router::$url . '/removeShip/' . $this->item['id']) . '"></a>
			</td>';
        } else {
            return null;
        }
    }
	
	
	public function tdEdit($text = null, $style = '', $class="")
	{
		$text = no_empty($text, '<em>Без значения</em>');
		$res = '
		<td class="'.$class.'" style="'.$style.'" class="td-name">
			<div>';
				$res .= '
				<a href="' . get_url(Router::$url . '/edit/' . $this->item['id']) . '">' . $text . '</a>';
				$res .= '
			</div>
		</td>';	

		return $res;
	}

	public function tdEditable($text = null, $style = '', $class="")
	{
		$text = no_empty($text, '<em>Без значения</em>');
		$res = '
		<td class="'.$class.'" style="'.$style.'" class="td-name">
			<div>';
		$res .= $text;
		$res .= '
			</div>
		</td>';

		return $res;
	}
    
    /**
	 * Категория для работ.
	 * 
	 * @return string
     */
	public function thWorkCategory($label = 'Категория')
	{
		return '<th class="th-name">' . $label . '</th>';
	}		
	
	/**
	 * Категория для работ.
	 * 
	 * @return string
     */
	public function tdWorkCategory()
	{
        $cat = DB::getValue("SELECT `name` FROM `#__category` WHERE `approve` = 1 AND `id` = ?", $this->item['portfol_id']);
		return '<td class="td-name">' . @$cat . '</td>';
	}

	public function tdType()
	{
        $type = DB::getValue("SELECT `type` FROM `#__category_items` WHERE `approve` = 1 AND `id` = ?", $this->item['id']);
		return '<td class="td-name">' . @Config::$types[$type['type']] . '</td>';
	}

    public function tdAuthor()
	{
        $type = DB::getValue("SELECT `type` FROM `#__category_items` WHERE `approve` = 1 AND `id` = ?", $this->item['id']);
		return '<td class="td-name">' . @Config::$types[$type['type']] . '</td>';
	}
}