<?php
/**
 * Получение значений настроек сайта.
 */
class Settings
{
	/**
	 * Локальное хранилеще данных из БД.
	 * 
	 * @var array
	 */
	private static $_data = null;

	/**
	 * Инициализация, получение данных из БД.
	 * 
	 * @return array
	 */
	public static function getData()
	{
		if (empty(self::$_data)) {
			self::$_data = DB::getRow("SELECT * FROM `#__settings` WHERE id = 1 LIMIT 1");
		} 

		return self::$_data;
	}

	/**
	 * Получение значения по ключу.
	 * 
	 * @param string $key
	 * @return string
	 */
	public static function get($key)
	{
		$data = self::getData();
		if (empty($data[$key])) {
			return null;
		} else {
			return stripcslashes($data[$key]);
		}
	}
	
	/**
	 * Получение всех значений.
	 * 
	 * @return array
	 */
	public static function getAll()
	{
		return self::getData();
	}
}