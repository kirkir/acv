<?php
/**
 * Класс для работы с ЧПУ
 */
class Sef
{
	/**
	 * Перевод кирилицы в транслит
	 */
	public function translit($value)
    {
		$value = Input::cleanStr($value);
		$value = str_replace(array('.html', '.htm'), '', $value);
		$value = str_replace(array('.', '&', '/', '\\', ' '), '-', $value);
		$value = mb_strtolower($value);
		$value = strtr($value, array(
				'а' => 'a', 'б' => 'b', 'в' => 'v',
				'г' => 'g', 'д' => 'd', 'е' => 'e',
				'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
				'и' => 'i', 'й' => 'y', 'к' => 'k',
				'л' => 'l', 'м' => 'm', 'н' => 'n',
				'о' => 'o', 'п' => 'p', 'р' => 'r',
				'с' => 's', 'т' => 't', 'у' => 'u',
				'ф' => 'f', 'х' => 'h', 'ц' => 'c',
				'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
				'ь' => '', 'ы' => 'y', 'ъ' => '',
				'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
				"ї" => "yi", "є" => "ye"
			)
		);
	
		$value = mb_ereg_replace('[^-0-9a-z]', '', $value);
		$value = mb_ereg_replace('[-]+', '-', $value);
        
		$value = trim($value, '-');

		return $value;
    }
}