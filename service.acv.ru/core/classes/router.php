<?php
/**
 * Запуск модулей.
 */
class Router
{
	/**
	 * Название текущего модуля.
	 * 
	 * @var string
	 */
	static public $module = '';

	/**
	 * Текущий экшен.
	 * 
	 * @var string
	 */
	static public $action = '';

	/**
	 * URL до модуля.
	 * 
	 * @var string
	 */
	static public $url = '';

	/**
	 * self::$url в виде массива.
	 * 
	 * @var array
	 */
	static public $route = array();	
	
	/**
	 * Путь до дериктории модуля.
	 * 
	 * @var string
	 */
	static public $path = '';
	
	/**
	 * Массив разрешенных GET параметров.
	 * 
	 * @var array
	 */
	static public $allow_get = array();

	/**
	 * Проверка - админка или нет.
	 * 
     * @return bool
	 */
	public static function isAdmin()
	{
		return (@self::$route[0] == Config::$url_admin);
	}
	
	/**
	 * Установка разрешенных GET параметров.
	 * 
     * @param array $keys
     * @return array
	 */
	public static function allowGET($keys)
	{
		return self::$allow_get = array_merge(self::$allow_get, $keys);
	}

	/**
     * Создание экземпляра класса модуля.
     * 
     * @param string $path Путь до модуля
     * @param mixed $data Данные передоваемые в конструктор класса
     * @return object
     */
	public static function getInstance($path, $data = null)
	{
		$path   = CORE_DIR . '/modules/' . trim($path, ' /\\');
		$route  = explode('/', $path);
		$module = end($route);
		$class  = 'module_' . $module;
		$file   = $path . '/' . $module . '.php';

		if (is_file($file)) {
			require_once $file;
			if (class_exists($class, false)) {
				return new $class($data);
			}
		}
		
		trigger_error('Module «' . $file . '»" not found', E_USER_ERROR);
		return null;
	}
	
	/**
     * Выполнение метода класса модуля.
     * 
     * @param string $path   Путь до модуля
     * @param string $method Название метода
     * @param mixed $data    Данные передоваемые в вызываемый метод класса
     * @return object
     */
	static public function runMethod($path, $method, $data = null)
	{
		$object = self::getInstance($path);
		if (method_exists($object, $method)) {
			return $object->$method($data);
		} else {
			trigger_error('Method «' . $method . '» not found in «' . $path . '»', E_USER_ERROR);
			return null;		
		}
	}

    /**
     * Поиск и выполнение модуля.
     * 
     * @param string $uri
     * @return string
     */
	static public function get($uri)
	{
		$uri = trim($uri, ' /\\');
	
		if (Config::$url_ext != '/') {
			$uri = str_replace(Config::$url_ext, '', $uri);
		}

		// Разбор URL.
		$parts = parse_url($uri );
		if (empty($parts['path'])) {
			$parts['path'] = '';
			$route = array();
		} else {
			$parts['path'] = trim($parts['path'], ' /\\');
			$route = explode('/', $parts['path']);
			foreach ($route as $row) {
				if (in_array($row, array('', ' ', '.', '..'))) {
					return self::get('404');
				}
			}
		}

		// Поиск модуля.
		self::$module = '';
		self::$route  = $route;
		self::$path   = CORE_DIR . '/modules/';
		$url = '/';
		if (!empty($route)) {
			foreach ($route as $part) {
				$fullpath = self::$path . $part;
				if (is_dir($fullpath)) {
					self::$path .= $part . '/';
					$url   .= $part . '/';
					self::$module = array_shift($route);
					continue;
				}
			}
		}

		self::$url = trim($url, '/');

		if (empty(self::$module)) {
			self::$module = 'main';
		}

		if (is_file(self::$path . self::$module . '.php')) {
			self::$action = array_shift($route);
			if (mb_substr(self::$action, 0, 7) != 'action_' && !in_array(self::$action, array('index', 'call'))) {
				include_once self::$path . self::$module . '.php';
				$class = 'module_' . self::$module;
				if (class_exists($class, false)) {
					$object = new $class;
					if (empty(self::$action)) {
						self::$action = 'index';
					}

					$method = 'action_' . self::$action;
					$id = array_shift($route);
					if (is_callable(array($object, $method))) {
						echo $object->$method($id, $route);
						exit;
					} else {
						$method = 'action_call';
						if (is_callable(array($object, $method))) {
							echo $object->$method(self::$action, array_diff(array_merge(array($id), $route), array(null)));
							exit;
						}
					}
				}
			}
		}

		return self::get('404');
	}
   
	/**
     * Выполнение модуля.
     * 
     * @param string $path   Путь до модуля
     * @param string $action Название метода
     * @param mixed $data    Данные передоваемые в метод модуля
     * @return string
     */
	static public function run($path, $action, $data = null)
	{
		self::$url    = trim($path, '/');
		self::$route  = explode('/', self::$url);
		self::$module = end(self::$route);
		self::$action = $action;
	
		self::$path = CORE_DIR . '/modules/' . self::$url;
		if (is_file(self::$path . '/' . self::$module . '.php')) {
			require_once self::$path . '/' . self::$module . '.php';
			$class = 'module_' . self::$module;
			if (class_exists($class, false)) {
				$object = new $class;
				return $object->$action($data);
			}
		}

		return self::get('404');
	}
	
	/**
     * Поиск модуля по масиву SEF в БД (костыли).
     */
	public static function find($route, $config) 
	{
		$route = array_diff($route, array(''));	

		foreach ($config as $row) {
			$res = self::_findHelper($row[0], $row[1], $row[2], $route);
			if ($res) {
				return $res;
			}
		}
		return self::get('404');
	}	

	private static function _findHelper($to, $tables, $module, $route) 
	{
		$parents = array();
		$id = $num = 0;

		$table = key($tables);
		$field = $tables[$table];			

		if (!empty($to)) {
			array_shift($route);
		}
		if (count($tables) == 1) {
			// Поиск в одной таблице.
			foreach ($route as $part) {
				if ($res = DB::getRow("SELECT * FROM `{$table}` WHERE `{$field}` = ? AND `sef` = ? AND `approve` = 1", array($id, $part))) {
					$parents[] = $res;
					$id = $res['id'];
					$num++;
				} else {
					break;
				}
			}
		
			if (count($route) == $num) {
				$current = array_pop($parents);
				return self::run($module[0], $module[1], array($parents, $current));
			}
		} else {			
			// Поиск в двух таблицах:
			$last  = array_pop($route);
			foreach ($route as $part) {
				if ($res = DB::getRow("SELECT * FROM `{$table}` WHERE `{$field}` = ? AND `sef` = ? AND `approve` = 1", array($id, $part))) {
					$parents[] = $res;
					$id = $res['id'];
					$num++;
				} else {
					break;
				}
			}
			
			if (count($route) == $num) {
				next($tables);
				$table = key($tables);
				$field = $tables[$table];	
	
				if (empty($field)) {
					if ($current = DB::getRow("SELECT * FROM `{$table}` WHERE `sef` = ? AND `approve` = 1", array($last))) {
						return self::run($module[0], $module[1], array($parents, $current));
					}	
				} else {
					$end = end($parents);
					if ($current = DB::getRow("SELECT * FROM `{$table}` WHERE `{$field}` = ? AND `sef` = ? AND `approve` = 1", array($end['id'], $last))) {
						return self::run($module[0], $module[1], array($parents, $current));
					}				
				}
			}
		}
		
		return false;
	}
}