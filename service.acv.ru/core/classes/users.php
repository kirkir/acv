<?php
class Users
{
	/**
	 * Возвращает массив пользователей у которых установлен флаг $field
	 */
	public static function getNoticeList($field)
    {
		return DB::getAll("SELECT * FROM `#__users` WHERE `email` <> '' AND `approve` = 1 AND `{$field}` = 1");
    }

	public static function getAuthorName($id)
    {
        return DB::getValue("SELECT `name` FROM `#__users` WHERE `id` = ? AND `approve` = 1", $id);
    }

    public static function getAuthorLogin($id)
    {
			return DB::getValue("SELECT `login` FROM `#__users` WHERE `id` = ? AND `approve` = 1", $id);
    }

	public static function all_users()
	{
		$count = DB::getAll("SELECT `id` FROM `#__users` WHERE `approve` = 1");
		return number_format(count($count), 0, ' ', ',');
	}

    public static function get_email($id = false)
    {
        return DB::getValue("SELECT `email` FROM `#__users` WHERE `id` = ? AND `approve` = 1", (!$id) ? Auth::$user['id'] : $id);
    }

    public static function get_level($id)
    {
        $level = DB::getValue("SELECT `level` FROM `#__users` WHERE `id` = ? AND `approve` = 1", $id);
        if($level) {
            return $level;
        } else {
            return 0;
        }
    }

    public static function getUser($id)
    {
        return DB::getRow("SELECT * FROM `#__users` WHERE `id` = ? AND `approve` = 1", $id);
    }

    public static function getServiceOrders()
    {
//        Auth::$user['id']
    }

    public static function getServiceOrdersNewByRegion()
    {
        $param = '';
        if (Auth::$user['city_id'] == 0) {
            $param .= ' AND region_id = ' . Auth::$user['region_id'];
        } else {
            $param .= ' AND city_id = ' . Auth::$user['city_id'] . ' AND region_id = ' . Auth::$user['region_id'];
        }

        return $sql = "SELECT * FROM `#__service` WHERE 1=1 $param AND `to_user_id` = 0 OR to_user_id = " . Auth::$user['id'] . " ORDER BY `date_add` DESC";
    }

    public static function getServiceOrdersNew()
    {
        if (Auth::perm(4)) {
            return count(DB::getAll("SELECT * FROM `#__service` WHERE `to_user_id` = 0"));
        } else {
            $param = '';
            if (Auth::$user['city_id'] == 0) {
                $param .= ' AND region_id = ' . Auth::$user['region_id'];
            } else {
                $param .= ' AND city_id = ' . Auth::$user['city_id'] . ' AND region_id = ' . Auth::$user['region_id'];
            }
            return count(DB::getAll("SELECT * FROM `#__service` WHERE `to_user_id` = 0 $param"));
        }
    }

}