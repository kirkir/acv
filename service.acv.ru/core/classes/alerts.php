<?php
/**
 * Установка и вывод сообщений через сессию.
 */
class Alerts
{
	/**
	 * Ключ собщений в сессии.
	 */
	private static $_session_key = 'alerts';

	/**
	 * Типы сообщений (значения должны совподать с CSS классами bootstrap).
	 */
	const ERROR   = 'danger';
	const SUCCESS = 'success';
	const INFO    = 'info';
	const WARNING = 'warning';

	/**
	 * Формирование HTML кода сообщения.
	 * 
	 * @param string $type       тип	 
	 * @param string|array $text текст
	 * @return string
	 */
	public static function show($type, $text) 
	{
		switch ($type) {
			case self::ERROR:   $class = 'alert alert-' . self::ERROR; break;
			case self::SUCCESS: $class = 'alert alert-' . self::SUCCESS; break;
			case self::INFO:    $class = 'alert alert-' . self::INFO; break;
			case self::WARNING: $class = 'alert alert-' . self::WARNING; break;
			default:            return null; //@todo зделать triger_error
		}

		$text = (array) $text;
		if (empty($text)) {
			return null;
		} elseif (count($text) == 1) {
			$text = array_shift($text);
		} else {	
			$text = '<ul><li>' . implode('</li><li>', $text) . '</li></ul>';
		}

		if (Router::isAdmin()) {
			// Для админки вывод кнопки "Закрыть".
			return '
			<div class="' . $class . ' alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
				' . $text . '
			</div>';
		} else {
			return '<div class="' . $class . '">' . $text . '</div>';
		}
	}

	/**
	 * Установка сообщений.
	 * 
	 * @param string|array $text текст
	 * @param string $type тип
	 * @return bool
	 */
	public static function set($type, $text) 
	{
		foreach ((array) $text as $row) {
			$_SESSION[self::$_session_key][$type][] = $row;
		}
		return true;
	}
	
	/**
	 * Установка сообщения error.
	 * 
	 * @param string|array $text текст
	 * @return bool
	 */
	public static function setError($text) 
	{
		return self::set(self::ERROR, $text);
	}
	
	/**
	 * Установка сообщения success.
	 * 
	 * @param string|array $text текст
	 * @return bool
	 */
	public static function setSuccess($text) 
	{
		return self::set(self::SUCCESS, $text);
	}
	
	/**
	 * Установка сообщения info.
	 * 
	 * @param string|array $text текст
	 * @return bool
	 */
	public static function setInfo($text) 
	{
		return self::set(self::INFO, $text);
	}

	/**
	 * Установка сообщения warning.
	 * 
	 * @param string|array $text текст
	 * @return bool
	 */
	public static function setWarning($text) 
	{
		return self::set(self::WARNING, $text);
	}

	/**
	 * Вывод сообщений.
	 * 
	 * @param string $default
	 * @return string
	 */
	public static function get($default = null) 
	{
		$res = '';
		if (!empty($_SESSION[self::$_session_key])) {
			foreach ($_SESSION[self::$_session_key] as $type => $row) {
				$res .= self::show($type, $row);
			}
		}

		unset($_SESSION[self::$_session_key]);
		return (empty($res)) ? $default : $res;
	}
}