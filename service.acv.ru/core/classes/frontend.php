<?php

class Frontend extends Core
{
	/**
     * Таблица модуля.
     * 
     * @var string
     */
	public $table = '';		
	
	/**
     * Поля формы.
     * 
     * @var array
     */
	public $form = array();		

	/**
     * Ошибки формы.
     * 
     * @var array
     */
	public $error = array();		

	/**
     * Рендер главного шаблона в дериктории "themes/sites".
     * 
     * @param string $tpl название шаблона.
     * @return string
     */
    public function display($file)
    {
		// 404-я страница если в url есть неразрешенные GET параметры.
		if (Config::$allow_get && Router::$module != '404') {
			foreach ($_GET as $i => $row) {
				if (!in_array($i, Router::$allow_get)) {
					return Router::get('404');
				}
			}
		}
		
		$path  = ROOT_DIR . '/themes/site/';
		$path .= (empty($this->themes)) ? $file : $this->themes;

		if (file_exists($path)) {
            ob_start();
            include $path; 
			return ob_get_clean(); 
        }

		return 'Cannot open tpl file: ' . $path;
	}
	
	/**
     * Вывод мета-тегов.
     * 
     * @return void
     */
    public function getMata()
    {
		?>  
		<meta http-equiv="content-type" content="text/html; charset=utf-8">		
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="format-detection" content="telephone=no">
		
		<?php if (!empty($this->keywords)): ?><meta name="keywords" content= "<?php echo $this->keywords; ?>"><?php endif; ?>  
		<?php if (!empty($this->description)): ?><meta name="description" content="<?php echo $this->description; ?>"><?php endif; ?>  
		<?php if (!empty($this->robots)): ?><meta name="robots" content="<?php echo $this->robots; ?>"><?php endif; ?>  
		<title><?php 
		
		$page = Registry::get('page', 1);
		if ($page > 1) {
			$this->title .= ' - Cтраница ' . $page;
		}
		
		echo $this->title; 
		
		
		?></title>

		<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon">
		<link href="/favicon.ico" type="image/x-icon" rel="icon">  
		<?php
	}

	/**
	 * Получение данных страницы.
	 * Зависимость /classes/pages.php
	 * 
	 * @param array $data
	 * @return bool
	 */
	public function getPage($sef)
	{
		$this->page = Pages::get($sef);
		return (empty($this->page)) ? Router::get('404') : true;
	}

	public function getItem($config)
	{
		$field = key($config);
		if ($field == 'id') {
			$val = Input::cleanInt($config[$field]);
		} elseif ($field == 'sef') {
			$val = Input::cleanSef($config[$field]);
		} else {
			trigger_error('The «' . $field . '» field is not supported.', E_USER_ERROR);
			exit;
		}

		if (!empty($val)) {
			$this->item = DB::getRow("SELECT * FROM `{$this->table}` WHERE `{$field}` = ? AND `approve` = 1", $val);
			if (!empty($this->item)) {
				return true;
			} 
		}

		return Router::get('404');
	}	

	public function getItemId($id)
	{
		$id = Input::cleanInt($id);
		if (!empty($id)) {
			$this->item = DB::getRow("SELECT * FROM `{$this->table}` WHERE `id` = ? AND `approve` = 1", $id);
			if (!empty($this->item)) {
				return true;
			} 
		}

		return Router::get('404');
	}

	public function getParentsItem($sef, $args)
	{
		$route = array_merge(array($sef), $args);
		$parents = array();
		$id = 0;
		$num = 0;

		foreach ($route as $sef) {
			$sef = Input::cleanSef($sef);
			if ($res = DB::getRow("SELECT * FROM `{$this->table}` WHERE `parent` = ? AND `sef` = ? AND `approve` = 1", array($id, $sef))) {
				$parents[] = $res;
				$id = $res['id'];
				$num++;
			} else {
				break;
			}
		}

		if (count($route) == $num) {
			$current = array_pop($parents);
			$this->parents = $parents;
			$this->item    = $current;
			return true;
		} else {
			return Router::get('404');
		}
	}
	
	/**
	 * Установка мета-тегов шаблона.
	 * 
	 * @param array $data
	 * @return void
	 */
	public function setMeta($data = null) 
	{
		$this->title       = no_empty($data['title'], $data['name']);
		$this->keywords    = @$data['keywords']; 
		$this->description = @$data['description'];
		$this->robots      = @$data['robots'];		
		$this->themes      = @$data['themes'];
	}

	/**
	 * H1 и текст сверху.
	 * 
	 * 
	 * @param array $data
	 * @return string
	 */
	public function text($data) 
	{
		$h1 = '<h1>' . no_empty($data['h1'], $data['name']) .  '</h1>';

		if (!empty($data['text']) && Registry::get('page', 1) == 1) {
			$text = prepare_text($data['text']);
			$pre = (stristr($text, '<h1') === false) ? $h1 : '';
			return $pre . '<div class="text">' . $text .  '</div>';
		}
		
		return $h1;
	}

	/**
	 * Текст снизу.
	 * 
	 * @param array $data
	 * @return string
	 */
	public function text2($data, $pre = '') 
	{
		if (!empty($data['text_2']) && (Registry::get('page', 1) == 1)) {
			return $pre . '<div class="text">' . prepare_text($data['text_2']) .  '</div>';
		} else {
			return null;
		}
	}
	
	
	/**
	 * Ссылка админку редактирования страницы.
	 */
	public function setAdminUrl($url) 
	{
		$this->admin_url = $url;
	}
	
	public function adminWidgets() 
	{
		if (Auth::perm(3) &&!empty($this->admin_url)) {
			return '<a class="admin_widgets" target="blank" href="' . get_url_admin($this->admin_url) . '" title="Редактировать страницу" rel="nofollow"></a>';
		} else {
			return null;
		}
	}
}