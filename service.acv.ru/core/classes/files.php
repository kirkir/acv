<?php
/** 
 * Загрузка и управление файлами.
 * 
 * Параметры массива $config:
 * type     - тип файлов (0 - изображение, 1 - файл).
 * allow    - разрешенные для загрузки файлы (например: array('jpg', 'jpeg', 'gif', 'png') - по умолчанию для изображений).
 * multiple - множественная загрузка вайлов (true, false).
 * unique   - переименовывать файлы в уникальные (true, false).
 * translit - имя файла переводится в транслит, не используется если unique = true.
 * prefix   - префикс в начале имени файла.
 */
class Files
{
	/**
	 * Дефалтовые разрешенные типы файлов (для изображений).
	 * 
	 * @var array 
	 */
	public static $default_ext = array('jpg', 'jpeg', 'gif', 'png');

	/**
	 * Запрещённые для загрузки файлы.
	 */
	public static $files_disabled = array(
		'phtml', 'php', 'php3', 'php4', 'php5', 'php6', 'phps', 'cgi', 'pl', 'asp', 'aspx', 'shtml', 'shtm', 
		'htaccess', 'htpasswd', 'ini', 'log', 'sh', 'js', 'html', 'htm', 'css', 'sql', 'spl', 'scgi', 'fcgi'
	);
	
	/**
	 * Загрузка данных в конфиг.
	 * 
	 * @param array $config 
	 * @param string $module 
	 * @param int $item 
	 * @return void 
	 */
	public static function init(&$config, $module, $item)
	{
		foreach ($config as $using => $config_row) {
			$config[$using]['data'] = DB::getAll("SELECT * FROM `#__files` WHERE `module` = ? AND `using` = ? AND `item` = ? ORDER BY `sort`", array($module, $using, $item));
		}
	}
	
	/**
	 * Обновление файлов.
	 * 
	 * @param array $config 
	 * @param string $module 
	 * @param int $item 
	 * @return void 
	 */
	public static function update($config, $module, $item)
	{
		foreach ($config as $using => $config_row) {
			// Алетернативный текст и сортировка:
			if (isset($_POST['files_' . $using . '_alt'])) {
				$sort = 0;
				foreach ($_POST['files_' . $using . '_alt'] as $id => $val) {
					$id = Input::cleanInt($id);
					if (!empty($id)) {
						DB::set("UPDATE `#__files` SET `alt` = ?, `sort` = ? WHERE `id` = ?", array(Input::cleanStr($val), ++$sort, $id));
					}
				}
			}

			// Загрузка файлов:
			if (isset($_FILES['files_' . $using])) {
				// Преоброзавание массива $_FILES.
				$files = array();
				if (empty($config_row['multiple'])) {
					$files = array($_FILES['files_' . $using]);
				} else {
					foreach($_FILES['files_' . $using] as $k => $l) {
						foreach($l as $i => $v) {
							$files[$i][$k] = $v;
						}
					}				
				}
				
				if (!empty($files[0]['name'])) {
					// Разрешенные для загрузки файлы (для изображений).
					if (empty($config_row['allow']) && $config_row['type'] == 0) {
						$config_row['allow'] = self::$default_ext;
					}

					// Новые файлы будут добовляться в конец списка.
					$sort = DB::getValue("SELECT MAX(`sort`) FROM `#__files` WHERE `module` = ? AND `using` = ? AND `item` = ?", array($module, $using, $item));

					foreach ($files as $files_row) {
						if ($files_row['error'] != 4) {
							// Имя файла.
							$name     = Input::cleanFilename($files_row['name']);
							$ext      = mb_strtolower(mb_substr(mb_strrchr($name, '.'), 1));
							$basename = basename($name, '.' . $ext);
			
							// Имя файла для сообщений.
							if (empty($basename) && empty($ext)) {
								$msg_name = '';
							} else {
								$msg_name = '<strong>' . $basename . '.' . $ext . '</strong> - '; 
							}
							
							$error = 'не удалось загрузить файл.';
		
							if (!empty($files_row['error'])) {
								switch ($files_row['error']) {
									case 1:  $error = 'привышен размер загружаемого файла.'; break;
									case 2:  $error = 'привышен размер загружаемого файла.'; break;
									case 3:  $error = 'файл был получен только частично.'; break;
									case 6:  $error = 'Файл не был загружен - отсутствует временная папка.'; break;
									case 7:  $error = 'не удалось записать файл на диск.'; break;
									case 8:  $error = 'PHP-расширение остановило загрузку файла.'; break;
									case 9:  $error = 'файл не был загружен - директория не существует или не доступная для записи.'; break;
									case 10: $error = 'превышен максимально допустимый размер файла.'; break;
									case 11: $error = 'данный тип файла запрещен.'; break;
									case 12: $error = 'ошибка при копировании файла.'; break;
									default: $error = 'файл не был загружен - неизвестная ошибка.'; break;
								}
							} elseif (empty($files_row['tmp_name']) || $files_row['tmp_name'] == 'none' || !is_uploaded_file($files_row['tmp_name'])) {
								$error = 'не удалось загрузить файл.';
							} elseif (empty($basename) || empty($ext)) {
								$error = 'недопустимое имя файла.';
							} elseif (in_array($ext, self::$files_disabled)) {
								$error = 'запрещённый тип файла.';
							} elseif (!empty($config_row['allow']) && !in_array($ext, $config_row['allow'], true)) {
								$error = 'запрещённый тип файла.';
							} else {
								// Подкотовка дериктории
								if ($config_row['type'] == 0) {
									$path = ROOT_DIR . '/uploads/' . $module . '/sources';
								} else {
									$path = ROOT_DIR . '/uploads/' . $module;
								}
			
								if (!is_dir($path)) {
									mkdir($path, 0777, true);
								}
									
								// Преоброзавание имени файла
								if (!empty($config_row['unique'])) {
									$new = md5(uniqid(mt_rand() . time(), true));
								} elseif (!empty($config_row['translit'])) {
									$new = Sef::translit($basename);
								} else {
									$new = $basename;
								}
									
								if (!empty($config_row['prefix'])) {
									$new = $config_row['prefix'] . $new;
								}

								$new = self::getUniqueName($path, $new, $ext) . '.' . $ext;

								// Перемещение файла в дерикторию и сохранение в БД.
								if (move_uploaded_file($files_row['tmp_name'], $path . '/' . $new)) {
									$insert_id = DB::add("
										INSERT INTO 
											`#__files` 
										SET
											`id`       = NULL,	
											`type`     = :type,
											`module`   = :module,
											`using`    = :using,
											`item`     = :item,
											`filename` = :filename,
											`size`     = :size,
											`alt`      = '',
											`sort`     = :sort
										", array(
											'type'     => $config_row['type'],
											'module'   => $module,
											'using'    => $using,
											'item'     => $item,
											'filename' => $new,
											'size'     => intval($files_row['size']),
											'sort'     => ++$sort
										)
									);

									if (empty($insert_id)) {
										@unlink($path . '/' . $new);
										$error = 'не удалось сохранить файл в базе данных.'; 
									} else {
										continue;
									}
								}
							}

							Message::set(MSG_ERROR, $msg_name . $error);
						}
					}
				}
			}
			
			// Удаление файлов:
			if (isset($_POST['files_' . $using . '_remove'])) {
				foreach ($_POST['files_' . $using . '_remove'] as $id) {
					$id = Input::cleanInt($id);
					if (!empty($id) && $item = DB::getRow("SELECT * FROM `#__files` WHERE `id` = ? LIMIT 1", $id)) {
						if (!empty($item['filename'])) {
							@unlink(ROOT_DIR . '/uploads/' . $module . '/' . $item['filename']);
							
							// Удаление файлов в подкатегориях (для изображений):
							if ($item['type'] == 0) {
								if ($handle = opendir(ROOT_DIR . '/uploads/' . $module)) {
									while (false !== ($dir = readdir($handle))) { 
										if ($dir != '.' && $dir != '..' && is_dir(ROOT_DIR . '/uploads/' . $module . '/' . $dir)) {
											@unlink(ROOT_DIR . '/uploads/' . $module . '/' . $dir . '/' . $item['filename']);
										}				
									}
									closedir($handle); 
								}							
							}
						} 

						DB::set("DELETE FROM `#__files` WHERE `id` = ? LIMIT 1", $id);
					}
				}
			}
        }
	}

	/**
	 * Защита от затирания файлов
	 * 
	 * @param string $path 
	 * @param string $basename 
	 * @param string $ext 
	 * @param int $i 
	 * @return string 
	 */
	public static function getUniqueName($path, $name, $ext, $i = 0)
	{
		$prefix = (empty($i)) ? '' : '_' . $i;
		if (file_exists($path . '/' . $name . $prefix . '.' . $ext)) {
			return self::getUniqueName($path, $name, $ext, ++$i);
		} else {
			return $name . $prefix;
		}
	}

	/**
	 * Удаление всех файлов связанных с определённой страницей.
	 * 
	 * @param string $module 
	 * @param int $item 
	 * @return void 
	 */
	public static function remove($module, $item)
	{
		$items = DB::getAll("SELECT * FROM `#__files` WHERE `module` = ? AND `item` = ?", array($module, $item));
		if (!empty($items)) {
			foreach ($items as $row) {
				@unlink(ROOT_DIR . '/uploads/' . $module . '/' . $row['filename']);
				if ($row['type'] == 0) {
					// Удаление файлов в подкатегориях (для изображений).
					if ($handle = opendir(ROOT_DIR . '/uploads/' . $module)) {
						while (false !== ($dir = readdir($handle))) { 
							if ($dir != '.' && $dir != '..' && is_dir(ROOT_DIR . '/uploads/' . $module . '/' . $dir)) {
								@unlink(ROOT_DIR . '/uploads/' . $module . '/' . $dir . '/' . $row['filename']);
							}				
						}
						closedir($handle); 
					}							
				}				
			}

			DB::set("DELETE FROM `#__files` WHERE `module` = ? AND `item` = ?", array($module, $item));
		}
	}
	
	public static function get($module, $item, $using)
	{
		return DB::getAll("SELECT * FROM `#__files` WHERE `module` = ? AND `item` = ? AND `using` = ? ORDER BY `sort`", array($module, $item, $using));
	}

	/**
	 * Вывод изображениея на сайте.
	 * 
	 * @param string $module 
	 * @param int $item 
	 * @return void 
	 */
	public static function getImage($module, $item, $using, $size, $alt = "")
	{
		$item = DB::getRow("SELECT * FROM `#__files` WHERE `module` = ? AND `item` = ? AND `using` = ? ORDER BY `sort`", array($module, $item, $using));
		$sizes = explode('x', $size);
		
		$w = (empty($sizes[0])) ? '' : ' width="' . $sizes[0] . '"';
		$h = (empty($sizes[1])) ? '' : ' height="' . $sizes[1] . '"';
		
		if (!empty($item)) {
			return '<img src="/uploads/' . $module . '/' . $size . '/' . urlencode($item['filename']) . '" alt="' . no_empty($item['alt'], $alt) . '"' . $w . $h . '>';
		} else {
			return '<img src="/uploads/caps/' . $size . '.png" alt=""' . $w . $h . '>';
		}
	}
	
	public static function getSrc($module, $item, $using, $size)
	{
		$item = DB::getRow("SELECT * FROM `#__files` WHERE `module` = ? AND `item` = ? AND `using` = ? ORDER BY `sort`", array($module, $item, $using));
		if (!empty($item)) {
			return '/uploads/' . $module . '/' . $size . '/' . $item['filename'];
		} else {
			return '/uploads/caps/' . $size . '.png';
		}
	}
	
	/**
	 * Вывод нескольких изображений (галерея карточки, с большим первым изображением).
	 * 
	 * @param string $module 
	 * @param int $item 
	 * @return void 
	 */
	public static function getImagesLimit($module, $item, $using, $size_big, $size_small, $limit)
	{
		$item = DB::getAll("SELECT * FROM `#__files` WHERE `module` = ? AND `item` = ? AND `using` = ? ORDER BY `sort` LIMIT {$limit}", array($module, $item, $using));
		$first = array_shift($item);
		
		$sizes_big = explode('x', $size_big);
		$sizes_small = explode('x', $size_small);
		
		if (!empty($item)) {
			$out = '<div class="big_image">';
			if (!empty($first)){
				$out .= '<a class="fancybox" href="/uploads/' . $module . '/sources/' . $first['filename'] . '" rel="gallery">
							<img src="/uploads/' . $module . '/' . $size_big . '/' . $first['filename'] . '" alt="' . $first['alt'] . '" width="' . $sizes_big[0] . '" height="' . $sizes_big[1] . '">
						</a>';
			} else {
				$out .= '<img src="/uploads/caps/' . $size_big . '.png" alt="" width="' . $sizes_big[0] . '" height="' . $sizes_big[1] . '">';
			}
			$out .= '</div>
			<div class="small_images">';			
			foreach($item as $row){
				if (!empty($row['filename'])){
					$out .= '<a class="fancybox" href="/uploads/' . $module . '/sources/' . $row['filename'] . '" rel="gallery">
								<img src="/uploads/' . $module . '/' . $size_small . '/' . $row['filename'] . '" alt="' . $row['alt'] . '" width="' . $sizes_small[0] . '" height="' . $sizes_small[1] . '">
							</a>';
				} else {
					$out .= '<img src="/uploads/caps/' . $size_small . '.png" alt="" width="' . $sizes_small[0] . '" height="' . $sizes_small[1] . '">';
				}
			}
			$out .= '</div>';
		}
		
		return $out;
	}
}