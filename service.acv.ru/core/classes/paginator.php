<?php

class Paginator
{
	public $all     = false;
	public $page    = 1;
	public $limit   = 10;
	public $url     = '';
	public $carrier = '';
	public $total   = 0;	
	public $delta   = 0;
	public $display = null;	

	/**
	 * Конструктор
	 * 
	 * @param string $url 
	 * @param string $limit
	 * @param string $carrier
	 * @return void
	 */
	public function paginator($url, $limit = null, $carrier = '?page=')
	{
		Router::allowGET(array('page'));
	
		if (isset($_GET['page']) && $_GET['page'] === 'all') {
			$this->all = false;
		} else {
			$this->all = false;
			$this->url     = $url;
			$this->carrier = $carrier;
			$this->limit   = (empty($limit)) ? Settings::get('limit') : $limit;			
			$this->page  = Input::getInt($_GET['page'], 1);
			Registry::set('page', $this->page);
		}
	}

	/**
	 * Выполнение запросса
	 * 
	 * @param string $sql
	 * @param array $param
	 * @return array
	 */
	public function process($sql = '', $param = array(), $data = array())
	{
		if ($this->all == true) {
			$items = DB::getAll($sql, $param);
			$this->delta   = 0;
			$this->total   = count($items);
			$this->display = null;

			Registry::set('page', 2);	
		} else {
			$start       = ($this->page != 1) ? $this->page * $this->limit - $this->limit : 0;		
			$sql         = str_replace('SELECT ', 'SELECT SQL_CALC_FOUND_ROWS ', $sql) . ' LIMIT ' . $start . ', ' . $this->limit;
			if($data) {
                $items = array_slice($data, $start, $this->limit);
			    $this->total = count($data);
            } else {
                $items       = DB::getAll($sql, $param);
			    $this->total = DB::getValue("SELECT FOUND_ROWS()");
            }

			$adjacents   = 2;
			$prev        = $this->page - 1;							
			$next        = $this->page + 1;						
			$lastpage    = ceil($this->total / $this->limit);	
			$lpm1        = $lastpage - 1;	
			$this->delta = $prev * $this->limit;

			$out = null;
			if ($lastpage > 1) {
				$out .= '
				<div class="pagination_row">
					<ul class="pagination">';

				if ($this->page == 1) {
					$out .= '<li class="prev"><span>«</span></li>';
				} elseif ($this->page == 2) {
					$out .= '<li class="prev"><a href="' . $this->url . '">«</a></li>';
				} else {
					$out .= '<li class="prev"><a href="' . $this->url . $this->carrier . $prev . '">«</a></li>';
				}

				if ($lastpage < 7 + ($adjacents * 2)) {
					for($counter = 1; $counter <= $lastpage; $counter++){
						if ($counter == 1) {
							if ($counter == $this->page) {
								$out .= '<li class="active"><a href="' . $this->url . '">' . $counter.'</a></li>';
							} else {
								$out .= '<li><a href="' . $this->url . '">' . $counter . '</a></li>';
							}
						} else {
							if ($counter == $this->page) {
								$out .= '<li class="active"><a href="' . $this->url . $this->carrier . $counter . '">' . $counter . '</a></li>';
							} else {
								$out .= '<li><a href="' . $this->url . $this->carrier . $counter . '">' . $counter . '</a></li>';
							}
						}				
					}
				} elseif ($lastpage > 5 + ($adjacents * 2)) {
					if ($this->page < 1 + ($adjacents * 2)){
						for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
							if ($counter == 1) {
								if ($counter == $this->page) {
									$out.= '<li class="active"><a href="' . $this->url . '">' . $counter . '</a></li>';
								} else {
									$out.= '<li><a href="' . $this->url . '">' . $counter . '</a></li>';
								}
							} else {
								if ($counter == $this->page) {
									$out.= '<li class="active"><a href="' . $this->url . '">' . $counter . '</a></li>';
								} else {
									$out.= '<li><a href="' . $this->url . $this->carrier . $counter . '">' . $counter . '</a></li>';
								}
							}	
						}
						
						$out.= '<li><span>...</span></li>';
						$out.= '<li><a href="' . $this->url . $this->carrier . $lpm1 . '">' . $lpm1 . '</a></li>';
						$out.= '<li><a href="' . $this->url . $this->carrier . $lastpage . '">' . $lastpage . '</a></li>';		
					} elseif ($lastpage - ($adjacents * 2) > $this->page && $this->page > ($adjacents * 2)) {
						$out.= '<li><a href="' . $this->url . '">1</a></li>';
						$out.= '<li><a href="' . $this->url . $this->carrier . '2">2</a></li>';
						$out.= '<li><span>...</span></li>';
						
						for ($counter = $this->page - $adjacents; $counter <= $this->page + $adjacents; $counter++) {
							if ($counter == $this->page) {
								$out .= '<li class="active"><a href="' . $this->url . '">' . $counter . '</a></li>';
							} else {
								$out .= '<li><a href="' . $this->url . $this->carrier . $counter . '">' . $counter . '</a></li>';
							}
						}

						$out .= '<li><span>...</span></li>';
						$out .= '<li><a href="' . $this->url . $this->carrier . $lpm1 . '">' . $lpm1 . '</a></li>';
						$out .= '<li><a href="' . $this->url . $this->carrier . $lastpage . '">'.$lastpage . '</a></li>';		
					} else {
						$out .= '<li><a href="' . $this->url . '">1</a></li>';
						$out .= '<li><a href="' . $this->url . $this->carrier . '2">2</a></li>';
						$out .= '<li><span>...</span></li>';
						
						for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
							if ($counter == $this->page) {
								$out.= '<li class="active"><a href="' . $this->url . '">' . $counter . '</a></li>';
							} else {
								$out.= '<li><a href="' . $this->url . $this->carrier . $counter . '">' . $counter . '</a></li>';
							}
						}
					}
				}


				if ($this->page == $lastpage) {
					$out.= '<li class="next"><span>»</span></li>';			
				} else {			
					$out.= '<li class="next"><a href="' . $this->url . $this->carrier . $next . '">»</a></li>';	
				}

				if ($this->all) {
					$out.= '
					<li class="all">
						<noindex>
						<a rel="nofollow" href="' . $this->url . $this->carrier . 'all">Показать все</a>
						</noindex>
					</li>';
				}
	
				$out .= '
					</ul>
				</div>';
			}

			$this->display = $out;	
		}	
		
		return $items;
	}
}