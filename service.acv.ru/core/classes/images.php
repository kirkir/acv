<?php
/** 
 * Обработка изображений.
 * @version 1.3
 */
class Images 
{
	public function __construct($filename)
	{
		$this->filename = $filename;
		if (empty($this->filename)) {
			throw new Exception('Файл не найден');
		} else {
			$info = getimagesize($this->filename);
			if (empty($info)) {
				throw new Exception('Файл не найден');
			} else {
				$this->width  = $info[0];
				$this->height = $info[1];
				$this->type   = $info[2];	
				switch ($this->type) { 
					case 1: 
						$this->img = imageCreateFromGif($this->filename);
						break;					
					case 2: 
						$this->img = imageCreateFromJpeg($this->filename); 
						break;	
					case 3: 
						$this->img = imageCreateFromPng($this->filename); 
						//imageSaveAlpha($this->img, true);	
						break;
					default:
						throw new Exception('Формат файла не подерживается');
						break;
				}
			}
		}
	}

	public function crop($x, $y, $width, $height)
	{
		if (empty($height)) {
			$height = $width;
		}
		
		if (empty($width)) {
			$width = $height;
		}
		
		$tmp = imageCreateTrueColor($width, $height);
		if ($this->type == 1 || $this->type == 3) {
			imagealphablending($tmp, true); 
			imageSaveAlpha($tmp, true);
			$transparent = imagecolorallocatealpha($tmp, 0, 0, 0, 127); 
			imagefill($tmp, 0, 0, $transparent); 
			imagecolortransparent($tmp, $transparent);	
		}

		imageCopyResampled($tmp, $this->img, 0, 0, $x, $y, $this->width, $this->height, $this->width, $this->height);

		$this->img = $tmp;
		unset($tmp);

		$this->width = $width;
		$this->height = $height;
	}

	/**
	 * Изминение размера изображения.
	 *
	 * @param int $width
	 * @param int $height
	 */
	public function resize($width, $height)
	{
		if (empty($width)) {
			$width = ceil($height / ($this->height / $this->width));
		}
		if (empty($height)) {
			$height = ceil($width / ($this->width / $this->height));
		}

		$tmp = imageCreateTrueColor($width, $height);
		if ($this->type == 1 || $this->type == 3) {
			imagealphablending($tmp, true); 
			imageSaveAlpha($tmp, true);
			$transparent = imagecolorallocatealpha($tmp, 0, 0, 0, 127); 
			imagefill($tmp, 0, 0, $transparent); 
			imagecolortransparent($tmp, $transparent);	
		}		

		if ($width < $this->width || $height < $this->height) {
			// Прьвью меньше оригинала.
			$tw = ceil($height / ($this->height / $this->width));
			if ($tw < $width) {
				imageCopyResampled($tmp, $this->img, ceil(($width - $tw) / 2), ceil(($height - $height) / 2), 0, 0, $tw, $height, $this->width, $this->height);		
			} else {
				$th = ceil($width / ($this->width / $this->height));
				imageCopyResampled($tmp, $this->img, ceil(($width - $width) / 2), ceil(($height - $th) / 2), 0, 0, $width, $th, $this->width, $this->height);	
			}			
		} else {	
			// Прьвью больше оригинала.	
			imageCopyResampled($tmp, $this->img, 0, 0, 0, 0, $width, $height, $this->width, $this->height);
		}

		$this->img = $tmp;
		unset($tmp);

		$this->width = $width;
		$this->height = $height;
	}

	/**
	 * Получение основной части изображения.
	 *
	 * @param int $width
	 * @param int $height
	 */
	public function cut($width, $height)
	{
		if (empty($width)) {
			$width = ceil($height / ($this->height / $this->width));
		}
		if (empty($height)) {
			$height = ceil($width / ($this->width / $this->height));
		}	
		
		if ($this->width != $width && $this->height != $height) {
			$tw = ceil($height / ($this->height / $this->width));
			$th = ceil($width / ($this->width / $this->height));

			if ($this->width == $this->height) {
				// Источник - квадратная фотка
				if ($width == $height) {
					// Превью - квадратная.
					$this->resize($width, $height);
				} elseif ($width > $height) {
					// Превью - горизонтальная.
					$this->resize($width, $width);
					$this->crop(0, ceil(($this->height - $height) / 2), $width, $height);				
				} else {
					// Превью - вертикальная.
					$this->resize($height, $height);
					$this->crop(ceil(($this->width - $width) / 2), 0, $width, $height);
				}
			} elseif ($this->width > $this->height) {
				// Источник - горизонтальная фотка
				if ($width == $height) {
					// Превью - квадратная.
					$this->resize(0, $height);
					$this->crop(ceil(($this->width - $width) / 2), 0, $width, $height);					
				} elseif ($width > $height) {
					// Превью - горизонтальная.
					if ($width <= $tw) {
						$this->resize(0, $height);
						$this->crop(ceil(($this->width - $width) / 2), 0, $width, $height);
					} else {
						$this->resize($width + 1, 0);
						$this->crop(0, ceil(($this->height - $height) / 2), $width, $height);
					}
				} else {
					// Превью - вертикальная.
					$this->resize(0, $height);
					$this->crop(ceil(($this->width - $width) / 2), 0, $width, $height);					
				}
			} else {
				// Источник - вертикальная фотка
				if ($width == $height) {
					// Превью - квадратная.
					$this->resize($width, 0);
					$this->crop((ceil($this->width - $width) / 2), ceil((($this->height - $height) / 2) / 2), $width, $height);
				} elseif ($width > $height) {
					// Превью - горизонтальная.
					$this->resize($width, 0);
					$this->crop(0, ceil((($this->height - $height) / 2) / 3), $width, $height);
				} else {
					// Превью - вертикальная.
					if ($tw > $width) {
						$this->resize(0, $height);
						$this->crop(ceil(($this->width - $width) / 2), 0, $width, $height);
					} else {
						$this->resize(0, $th);
						$this->crop(ceil(($this->width - $width) / 2), 0, $width, $height);
					}
				}
			}

			$this->width = $width;
			$this->height = $height;
		}

		return true;
	}

	/**
	 * Превью.
	 *
	 * @param int $width
	 * @param int $height
	 */
	public function thumb($width, $height)
	{
		if (empty($width)) {
			$width = ceil($height / ($this->height / $this->width));
		}
		if (empty($height)) {
			$height = ceil($width / ($this->width / $this->height));
		}		
	
		$tw = ceil($height / ($this->height / $this->width));
		$th = ceil($width / ($this->width / $this->height));

		if ($this->width != $width && $this->height != $height) {
			$tmp = imageCreateTrueColor($width, $height);
			if ($this->type == 1 || $this->type == 3) {
				imagealphablending($tmp, true); 
				imageSaveAlpha($tmp, true);
				$transparent = imagecolorallocatealpha($tmp, 0, 0, 0, 127); 
				imagefill($tmp, 0, 0, $transparent); 
				imagecolortransparent($tmp, $transparent);
			}

			// Определение цвета фона...
			// top, bottom
			$entry_1 = $entry_2 = array();
			for ($n = 0; $n < $this->width; $n++) {
				$color = imagecolorat($this->img, $n, 0);
				$entry_1[$color] = (isset($entry_1[$color])) ? $entry_1[$color] + 1 : 1;
				
				$color = imagecolorat($this->img, $n, $this->height - 1);
				$entry_2[$color] = (isset($entry_2[$color])) ? $entry_2[$color] + 1 : 1;
			}

			arsort($entry_1); 
			$top_color = key($entry_1);
			$top_defined = (100 * $entry_1[$top_color] / $this->width) > 45;	

			arsort($entry_2); 
			$bottom_color = key($entry_2);
			$bottom_defined = (100 * $entry_2[$bottom_color] / $this->width) > 45;			
			unset($entry_1, $entry_2);
			
			// left, right
			for ($n = 0; $n < $this->height; $n++) {
				$color = imagecolorat($this->img, 0, $n);
				$entry_1[$color] = (isset($entry_1[$color])) ? $entry_1[$color] + 1 : 1;
				
				$color = imagecolorat($this->img, $this->width - 1, $n);
				$entry_2[$color] = (isset($entry_2[$color])) ? $entry_2[$color] + 1 : 1;
			}

			arsort($entry_1); 
			$left_color = key($entry_1);
			$left_defined = (100 * $entry_1[$left_color] / $this->height) > 45;		
			
			arsort($entry_2); 
			$right_color = key($entry_2);
			$right_defined = (100 * $entry_2[$right_color] / $this->height) > 45;
			unset($entry_1, $entry_2);
	
			if ($top_defined || $bottom_defined || $right_defined || $left_defined) {
				if (imageistruecolor($this->img)) {
					$top_rgb    = imagecolorsforindex($tmp, $top_color);
					$right_rgb  = imagecolorsforindex($tmp, $right_color);
					$bottom_rgb = imagecolorsforindex($tmp, $bottom_color);
					$left_rgb   = imagecolorsforindex($tmp, $left_color);				
					
					if (!empty($top_rgb['alpha']) && !empty($top_rgb['alpha']) && !empty($top_rgb['alpha']) && !empty($top_rgb['alpha'])) {	
						$top_defined = $right_defined = $bottom_defined = $left_defined = true;
					} elseif (empty($top_rgb['alpha']) || empty($top_rgb['alpha']) || empty($top_rgb['alpha']) || empty($top_rgb['alpha'])) {			
						if ($top_defined && $bottom_defined && $left_defined == false && $right_defined == false) {
							if ($th < $height) {
								imagefilledrectangle($tmp, 0, 0, $width - 1, $height / 2, $top_color);
								imagefilledrectangle($tmp, 0, $height / 2, $width - 1, $height - 1, $bottom_color);							
							} else {
								return $this->cut($width, $height);
							}
						} elseif ($left_defined && $right_defined && $top_defined == false && $bottom_defined == false) {
							if ($tw < $width) {
								imagefilledrectangle($tmp, 0, 0, $width / 2, $height - 1, $left_color);
								imagefilledrectangle($tmp, $width / 2, 0, $width - 1, $height - 1, $right_color);
							} else {
								return $this->cut($width, $height);
							}
						} else {
							if ($top_defined) {
								imagefill($tmp, 0, 0, $top_color);
							} elseif($right_defined) {
								imagefill($tmp, 0, 0, $right_color);
							} elseif($bottom_defined) {
								imagefill($tmp, 0, 0, $bottom_color);
							} else {
								imagefill($tmp, 0, 0, $left_color);
							}				
						}
					}
				}

				if ($top_defined == false && $right_defined && $bottom_defined && $left_defined) {
					// top
					if ($width >= $this->width && $height >= $this->height) {
						$seze = array(ceil(($width - $this->width) / 2), 0, $this->width, $this->height);
					} elseif ($width >= $this->width) {
						$seze = array(ceil(($width - ($height / ($this->height / $this->width))) / 2), 0, ceil($height / ($this->height / $this->width)), $height);
					} elseif ($height >= $this->height) {
						$seze = array(0, 0, $width, ceil($width / ($this->width / $this->height)));
					} elseif ($tw < $width) {
						$seze = array(ceil(($width - $tw) / 2), 0, $tw, $height);	
					} else {
						$seze = array(ceil(($width - $width) / 2), 0, $width, $th);
					}				
				} elseif ($top_defined == false && $right_defined && $bottom_defined && $left_defined == false) {
					// top-left 
					if ($width >= $this->width && $height >= $this->height) {
						$seze = array(0, 0, $this->width, $this->height);
					} elseif ($width >= $this->width) {
						$seze = array(0, 0, ceil($height / ($this->height / $this->width)), $height);
					} elseif ($height >= $this->height) {
						$seze = array(0, 0, $width, ceil($width / ($this->width / $this->height)));
					} elseif ($tw < $width) {
						$seze = array(0, 0, $tw, $height);
					} else {
						$seze = array(0, 0, $width, $th);
					}
				} elseif ($top_defined == false && $right_defined == false && $bottom_defined && $left_defined) {
					// top-right 
					if ($width >= $this->width && $height >= $this->height) {
						$seze = array($width - $this->width, 0, $this->width, $this->height);
					} elseif ($width >= $this->width) {
						$seze = array($width - $tw, 0, ceil($height / ($this->height / $this->width)), $height);
					} elseif ($height >= $this->height) {
						$seze = array(0, 0, $width, ceil($width / ($this->width / $this->height)));
					} elseif ($tw < $width) {
						$seze = array($width - $tw, 0, $tw, $height);		
					} else {
						$seze = array(0, 0, $width, $th);
					} 
				} elseif ($top_defined && $right_defined && $bottom_defined == false && $left_defined == false) {
					// bottom-left 
					if ($width >= $this->width && $height >= $this->height) {
						$seze = array(0, $height - $this->height, $this->width, $this->height);
					} elseif ($width >= $this->width) {
						$seze = array(0, 0, ceil($height / ($this->height / $this->width)), $height);
					} elseif ($height >= $this->height) {
						$seze = array(0, $height - $th, $width, ceil($width / ($this->width / $this->height)));
					} elseif ($tw < $width) {
						$seze = array(0, 0, $tw, $height);	
					} else {
						$seze = array(0, $height - $th, $width, $th);	
					}
				} elseif ($top_defined && $right_defined == false && $bottom_defined == false && $left_defined) {
					// bottom-right 
					
					if ($width >= $this->width && $height >= $this->height) {
						$seze = array($width - $this->width, $height - $this->height, $this->width, $this->height);
					} elseif ($width >= $this->width) {
						$seze = array($width - $tw, 0, ceil($height / ($this->height / $this->width)), $height);
					} elseif ($height >= $this->height) {
						$seze = array(0, $height - $th, $width, ceil($width / ($this->width / $this->height)));
					} elseif ($tw < $width) {
						$seze = array($width - $tw, 0, $tw, $height);	
					} else {
						$seze = array($width - $tw, $height - $th, $width, $th);	
					}
				} elseif ($top_defined && $right_defined == false && $bottom_defined && $left_defined && ($top_color == $bottom_color && $bottom_color == $left_color)) {
					// right
					if ($width >= $this->width && $height >= $this->height) {
						$seze = array($width - $this->width, ceil(($height - $this->height) / 2), $this->width, $this->height);
					} elseif ($width >= $this->width) {
						$seze = array($width - $tw, 0, ceil($height / ($this->height / $this->width)), $height);
					} elseif ($height >= $this->height) {
						$seze = array(0, $height - $th, $width, ceil($width / ($this->width / $this->height)));
					} elseif ($tw < $width) {
						$seze = array($width - $tw, ceil(($height - $height) / 2), $tw, $height);	
					} else {
						$seze = array(0, ceil(($height - $th) / 2), $width, $th);
					} 
				} elseif ($top_defined && $right_defined && $bottom_defined == false && $left_defined) {
					// Прижатие к низу.
					if ($width >= $this->width && $height >= $this->height) {
						$seze = array(ceil(($width - $this->width) / 2), $height - $this->height, $this->width, $this->height);
					} elseif ($width >= $this->width) {
						$seze = array(ceil(($width - ($height / ($this->height / $this->width))) / 2), 0, ceil($height / ($this->height / $this->width)), $height);
					} elseif ($height >= $this->height) {
						$seze = array(0, $height - $th, $width, ceil($width / ($this->width / $this->height)));
					} elseif ($tw < $width) {
						$seze = array(ceil(($width - $tw) / 2), 0, $tw, $height);	
					} else {
						$seze = array(ceil(($width - $width) / 2), $height - $th, $width, $th);	
					}
				} elseif ($top_defined && $right_defined && $bottom_defined && $left_defined == false) {
					// Прижатие в лево.
					if ($width >= $this->width && $height >= $this->height) {
						$seze = array(0, ceil(($height - $this->height) / 2), $this->width, $this->height);
					} elseif ($width >= $this->width) {
						$seze = array(0, 0, ceil($height / ($this->height / $this->width)), $height);
					} elseif ($height >= $this->height) {
						$seze = array(0, ceil(($height - $th) / 2), $width, ceil($width / ($this->width / $this->height)));
					} elseif ($tw < $width) {
						$seze = array(0, ceil(($height - $height) / 2), $tw, $height);		
					} else {
						$seze = array(0, ceil(($height - $th) / 2), $width, $th);	
					} 
				} elseif (($top_defined && $bottom_defined) || ($right_defined && $left_defined)) {
					// More
					if ($width >= $this->width && $height >= $this->height) {
						$seze = array(ceil(($width - $this->width) / 2), ceil(($height - $this->height) / 2), $this->width, $this->height);
					} elseif ($width >= $this->width) {
						$seze = array(ceil(($width - $tw) / 2), 0, ceil($height / ($this->height / $this->width)), $height);
					} elseif ($height >= $this->height) {
						$seze = array(0, ceil(($height - $th) / 2), $width, ceil($width / ($this->width / $this->height)));
					} elseif ($tw < $width) {
						$seze = array(ceil(($width - $tw) / 2), ceil(($height - $height) / 2), $tw, $height);		
					} else {
						$seze = array(0, ceil(($height - $th) / 2), $width, $th);	
					} 					
				} else {
					return $this->cut($width, $height);
				}

				imageCopyResampled($tmp, $this->img, $seze[0], $seze[1], 0, 0, $seze[2], $seze[3], $this->width, $this->height);
				$this->img = $tmp;
				unset($tmp);
				
				return true;
			} else {
				return $this->cut($width, $height);
			}
		}
	}

	/**
	 * Поворот изображения по часовой стрелки.
	 *
	 */
	public function rotateRight()
	{
		$this->img = imagerotate($this->img, 90, 0);
		
		$this->width = $height;
		$this->height = $width;
	}
	
	public function rotateLeft()
	{
		$this->img = imagerotate($this->img, -90, 0);
		
		$this->width = $height;
		$this->height = $width;
	}
	
	public function watermark($file, $position = 'center') 
	{
		if (empty($file)) {
			throw new Exception('Файл маски не найден');
		} else {
			$info = getimagesize($file);
			if (empty($info)) {
				throw new Exception('Файл маски не найден');
			} else {
				switch ($info[2]) { 
					case 1: 
						$dest = imageCreateFromGif($file);
						break;					
					case 2: 
						$dest = imageCreateFromJpeg($file); 
						break;	
					case 3: 
						$dest = imageCreateFromPng($file); 
						imageAlphaBlending($dest, false);
						imageSaveAlpha($dest, true);	
						break;
					default:
						throw new Exception('Формат файла маски не подерживается');
						break;
				}
				
				switch ($position) {
					case 'top': 
						$x = ceil(($this->width - $info[0]) / 2);
						$y = 0;
						break;					
					case 'top-left': 
						$x = 0;
						$y = 0;
						break;
					case 'top-right': 
						$x = ceil($this->width - $info[0]);
						$y = 0;
						break;
					case 'left': 
						$x = 0;
						$y = ceil(($this->height - $info[1]) / 2);
						break;
					case 'right': 				 
						$x = ceil($this->width - $info[0]);
						$y = ceil(($this->height - $info[1]) / 2);
						break;
					case 'bottom':  
						$x = ceil(($this->width - $info[0]) / 2);
						$y = ceil($this->height - $info[1]);
						break;					
					case 'bottom-left': 
						$x = 0;
						$y = ceil($this->height - $info[1]);
						break;
					case 'bottom-right':
						$x = ceil($this->width - $info[0]);
						$y = ceil($this->height - $info[1]);
						break;
					default:
						$x = ceil(($this->width - $info[0]) / 2);
						$y = ceil(($this->height - $info[1]) / 2);
						break;
				}

				imagecopy($this->img, $dest, $x, $y, 0, 0, $info[0], $info[1]); 
				imagedestroy($dest);
			}
		}	
	}
	
	/**
	 * Сохраняет изображение в файл.
	 *
	 * @param string $filename
	 * @param int $quality качество для jpg
	 */
	public function save($filename, $quality = 100)
	{
		if (empty($filename)) {
			$filename = $this->filename;
		}

		switch ($this->type) {
			case 1: 
				imageGif($this->img, $filename);
				break;			
			case 2: 
				imageJpeg($this->img, $filename, $quality);
				break;			
			case 3: 
				imagePng($this->img, $filename);
				break;
		}
		
		imagedestroy($this->img);
	}

	/**
	 * Выводит изображение в браузер.
	 *
	 * @param int $quality качество для jpg
	 */
	public function output($quality = 90)
	{
		switch ($this->type) {
			case 1: 
				header('Content-Type: image/gif'); 
				imageGif($this->img);
				break;			
			case 2: 
				header('Content-Type: image/jpeg');
				imageJpeg($this->img, null, $quality);
				break;			
			case 3: 
				header('Content-Type: image/x-png');
				imagePng($this->img);
				break;
		}

		imagedestroy($this->img);
	}
	
	/**
	 * Выводит изображение в браузер и Сохраняет его.
	 *
	 * @param string $filename
	 * @param int $quality качество для jpg
	 */
	public function saveOut($filename, $quality = 90)
	{
		if (empty($filename)) {
			$filename = $this->filename;
		}

		switch ($this->type) {
			case 1:
				header('Content-Type: image/gif'); 
				imageGif($this->img, $filename);
				break;			
			case 2:
				header('Content-Type: image/jpeg');
				imageJpeg($this->img, $filename, $quality);
				break;			
			case 3:
				header('Content-Type: image/x-png');
				imagePng($this->img, $filename);
				break;
		}
	
		imagedestroy($this->img);
		readfile($filename);
	}
}