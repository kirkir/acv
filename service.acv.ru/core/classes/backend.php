<?php
/**
 * Базовый класс для администраторского раздела.
 * Все модули админки должны быть наследниками этого класса. Кроме авторизации.
 */
class Backend extends Core
{
	/**
     * Название модуля.
     *
	 * @var string
     */
	public $module = '';
	
	/**
     * Название модуля.
     *
	 * @var string
     */
	public $items = array();
	
	/**
     * БД модуля.
     *
	 * @var string
     */
	public $table = '';

	/**
     * Конфиг загрузки файлов.
     *
	 * @var array
     */
	public $files = array();	
	
	/**
     * Структура БД модуля.
     *
	 * @var array
     */
	public $structure = array();

	/**
     * Текущая запись из БД.
     *
	 * @var array
     */
	public $item = array();

	/**
     * Поля формы.
     *
	 * @var array
     */
	public $form = array();
	
	/**
     * Ошибки полей формы.
     *
	 * @var array
     */
	public $error = array();

	/**
     * Вывод мета-тегов.
     * 
     * @return void
     */
    public function getMata()
    {
		?>  
		<meta http-equiv="content-type" content="text/html; charset=utf-8">		
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="imagetoolbar" content="no">
		<meta name="format-detection" content="telephone=no">
		
		<?php if (!empty($this->keywords)): ?><meta name="keywords" content= "<?php echo $this->keywords; ?>"><?php endif; ?>  
		<?php if (!empty($this->description)): ?><meta name="description" content="<?php echo $this->description; ?>"><?php endif; ?>  
		<?php if (!empty($this->robots)): ?><meta name="robots" content="<?php echo $this->robots; ?>"><?php endif; ?>  
		<title><?php echo $this->title; ?></title>

		<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon">
		<link href="/favicon.ico" type="image/x-icon" rel="icon">  
		<?php
	}
	
	/**
     * Рендер шаблона в дериктории "themes/admin".
     * 
     * @param string $tpl название шаблона.
     * @return string
     */
    public function display($tpl)
    {
		$tpl  = ROOT_DIR . '/themes/admin/' . $tpl;
		if (file_exists($tpl)) {
            ob_start();
            include $tpl; 
			return ob_get_clean(); 
        }

		return 'Cannot open tpl file: ' . $tpl;
	}
	
	/**
     * 
     *
     * @param string $file
	 * @return string
     */	
	protected function backendDisplay($file)
	{
		$this->content  = $this->render(Router::$path . '/' . $file);
		return $this->display('page.tpl');
	}

	
	/**
     * Инициализация.
     *
     * @param int $livel уровень доступа.
	 * @return void
     */
	protected function init($livel = 2)
	{
		Auth::allow($livel);

		// Получение структуры БД модуля.
		if (!empty($this->table)) {
			$this->structure = DB::getStructure($this->table);
		}
	}
	
	/**
     * Получение записи из БД по её id.
     * 
     * @param int $id
     * @return bool
     */
	protected function getItem($id)
	{
		$id = Input::cleanInt($id);
		if (!empty($id)) {
			$this->item = DB::getRow("SELECT * FROM `{$this->table}` WHERE `id` = ? LIMIT 1", $id);
			if (!empty($this->item)) {
				return true;
			}
		}
		
		return false;
	}

	/**
     * Добовление в БД. Метод сам формирует и выполняет SQL запрос по массиву $this->form. И добавляя в него значения sort, user_id, date_add.
     * 
     * @return int
     */
	public function dbAdd()
    {
		$form = $this->form;
		unset($form['id']);

		$param = array();
		foreach ($form as $key => $row) {
			if (is_null($row)) {
				unset($form[$key]);
			} else {
				$param[] = "`{$key}` = :{$key}";
			}
		}

		if (isset($this->structure['sort']) && !isset($form['sort'])) {
			$param[] = "`sort` = " . (DB::getValue("SELECT MAX(`sort`) FROM `{$this->table}`") + 1);
		}
		if (isset($this->structure['user_id']) && !isset($form['user_id'])) {
			$param[] = "`user_id` = " . Auth::$user['id'];
		}
		if (isset($this->structure['date_add']) && !isset($form['date_add'])) {
			$param[] = "`date_add` = UNIX_TIMESTAMP()";
		}

		return DB::add("INSERT INTO `{$this->table}` SET " . implode(', ', $param), $form);
	}
	
	/**
     * Сохранение в БД. Метод сам формирует и выполняет SQL запрос по массиву $this->form. И добавляя в него значения date_edit.
     * 
     * @return int
     */
	public function dbSave()
    {
		$form = $this->form;
		unset($form['id']);
		$param = array();
		foreach ($form as $key => $row) {
			if (is_null($row)) {
				unset($key);
			} else {
				$param[] = "`{$key}` = :{$key}";
			}
		}
	   
		if (isset($this->structure['date_edit']) && !isset($form['date_edit'])) {
			$param[] = "`date_edit` = UNIX_TIMESTAMP()";
		}

		return (DB::set("UPDATE `{$this->table}` SET " . implode(', ', $param) . " WHERE `id` = :id LIMIT 1", $this->form)) ? $this->form['id'] : 0;
	}

	/**
     * Загрузка файлов и редирект для метода handler.
     *
     * @param int $id id вставленной записи
     * @param string $action метод на который будет выполнен редирект.
	 * @return void
     */
	public function handlerСomplete($id, $action, $query = '')
	{
		// Загрузка прикреплённых файлов.
		if (!empty($this->files)) {
			Files::update($this->files, $this->module, $id);
		}

		// Редирект.
		Alerts::setSuccess("«<strong>" . no_empty(@$this->form['name'], 'ID: ' . $id) . "</strong>» - изменения успешно сохранены.");
		$url = ($_POST['send'] == 'save') ? Router::$url : Router::$url . '/' . $action . '/' . $id;

        $types = array();
        foreach (array('act', 'sh', 'f_d', 'f_u', 'f_o', 'doc_buy', 'foto_client') as $type) {
            if(DB::getRow("SELECT * FROM `#__files` WHERE `using` = ? AND `item` = ?", array($type, $id))) {
                $types[] = $type;
            }
        }

        if((Router::$module == 'demand')) {
            if(count($types) < 7) {
                $demand = DB::set("UPDATE `#__demand` SET `draft` = 1 WHERE `id` = ?", $id);
                Alerts::setError('Отчет отправлен в черновики т.к не загружены все фотографии. Пожалуйста, загрузите все фото что бы отчет был принят!');
            } else {
                Alerts::setInfo('Отчет не в черновиках, все фотографии были загружены.');
                DB::set("UPDATE `#__demand` SET `draft` = 0 WHERE `id` = ?", $id);
            }

            $step = Auth::perm(4) ? $this->form['step'] : $this->item['step'];
            if($step == 3 || $step == 4) {
                Alerts::setInfo('Сумма компенсации будет отображена после принятия решения представителем ACV RUS.');
            }
        }
        // Очиска кеша.
        $this->cacheClear();

		return redirect($url, $query . '#item_' . $id);
	}

	public function handlerActions()
	{
		if (isset($_POST['action']) && !empty($_POST['check_id'])) {
			$mode = Input::getStr($_POST['mode'], null);
			switch ($_POST['action']) {
				case 'approve': 
					foreach ($_POST['check_id'] as $row) {
						$this->exec_approve($row, $mode); 
					}
					break;					
				case 'executed': 
					foreach ($_POST['check_id'] as $row) {
						$this->exec_executed($row, $mode); 
					}
					break;					
				case 'main': 
					foreach ($_POST['check_id'] as $row) {
						$this->exec_main($row, $mode); 
					}
					break;				
				case 'remove':  
					foreach ($_POST['check_id'] as $row) {
						$this->exec_remove($row); 
					}
					break;
			}
			
			// Очиска кеша.
			$this->cacheClear();
		}	
		
		// Сохранение сортировки.
		if (isset($this->structure['sort'])) {
			$i = 0;
			if (isset($_POST['sort']) && isset($_POST['sort_id'])) {
				foreach ((array) $_POST['sort_id'] as $row) {
					DB::set("UPDATE `{$this->table}` SET `sort` = ? WHERE `id` = ?", array(++$i, intval($row)));
				}
			
				// Очиска кеша.
				$this->cacheClear();
			
				Alerts::setSuccess('Порядок успешно сохранен.');
			}
		}			
	}

	/**
     * Базовый экшен - список.
     *
	 * @return string
     */
    public function action_index()
    {
		$this->handlerActions();
		$where = '';
		$category = Input::getInt($_REQUEST['category']);
		$brands = Input::getInt($_REQUEST['brands']);
		$q = Input::getStr($_REQUEST['q']);
	
		if (!empty($category)) {
			$where = "WHERE `category` = {$category}";		
		} elseif (!empty($brands)) {
			$where = "WHERE `brands` = {$brands}";
		} elseif (!empty($q)) {
			$where = "WHERE `id` = '{$q}' OR `name` LIKE '{$q}' OR `sku` LIKE '{$q}'";
		}

		// Список.
		if (isset($this->structure['sort'])) {
			$sql = "SELECT * FROM `{$this->table}` {$where} ORDER BY `sort`";
		} elseif (isset($this->structure['date_add'])) {
			$sql = "SELECT * FROM `{$this->table}` {$where} ORDER BY `date_add` DESC";
		} else {
			$sql = "SELECT * FROM `{$this->table}` {$where} ORDER BY `id` DESC";
		}
	
		if (!empty($this->limit)) {
			$this->paginator = new Paginator(get_url(Router::$url), 100);
			$this->items = $this->paginator->process($sql);		
		} else {
			$this->items = DB::getAll($sql);
		}

		// Вывод в шаблон.
		$this->subtitle = 'список';
		$this->content  = $this->render(Router::$path . '/list.tpl');
		return $this->display('page.tpl');
    }
	
	/**
     * Базовый экшен - добовление.
     *
	 * @return string
     */
    public function action_add()
    {
		if (!is_callable(array($this, 'handler'))) {
			return Router::get('404');
		} else {
			// Инициализация и обработка формы.
			if (!$this->handler()) {
				$this->form = $this->structure;	
			}
		
			// Вывод в шаблон.
			$this->subtitle = 'добавление';
			$this->content  = $this->render(Router::$path . '/form.tpl');	
			return $this->display('page.tpl');
		}
    }	

	/**
     * Базовый экшен - редактирование.
     */
    public function action_edit($id)
    {
		if (!is_callable(array($this, 'handler'))) {
			return Router::get('404');
		} elseif ($this->getItem($id)) {
			// Получение ранее прикреплённых изображений.
			if (!empty($this->files)) {
				Files::init($this->files, $this->module, $this->item['id']);			
			}

			// Инициализация и обработка формы.
			if (!$this->handler()) {
				$this->form = $this->item;	
			}

			
			// Вывод в шаблон.
			$this->subtitle = 'редактирование';
			$this->content  = $this->render(Router::$path . '/form.tpl');
			return $this->display('page.tpl');
		} else {
			return Router::run('404', 'action_index');
		}
    }
	
	/**
     * Базовый экшен - копирование.
     */
    public function action_copy($id)
    {
		if (!is_callable(array($this, 'handler'))) {
			return Router::get('404');
		} elseif ($this->getItem($id)) {
			// Инициализация и обработка формы.
			if (!$this->handler()) {
				$this->form = $this->item;
				$this->form['id'] = 0;
				if (isset($this->form['sef'])) {
					$this->form['sef'] = '';
				}				
				if (isset($this->form['alias'])) {
					$this->form['alias'] = '';
				}
			}

			// Вывод в шаблон.
			$this->subtitle = 'копирование';
			$this->content  = $this->render(Router::$path . '/form.tpl');
			return $this->display('page.tpl');
		}
    }

	/**
     * Статус публикации.
     *
	 * @param int id записи
	 * @return string
     */
    public function action_approve($id)
    {
		$this->exec_approve($id, 'toggle');
		
		// Очиска кеша.
		$this->cacheClear();

		return redirect(Router::$url, '#item_' . $id);
    }



	/**
     * Выполнение действия: Статус публикации.
     *
	 * @param int id записи
	 * @param flag string действие (toogle, on, off)
	 * @return bool
     */
	public function exec_approve($id, $flag)
    {
		if (isset($this->structure['approve']) && $this->getItem($id)) {
			switch ($flag) {
				case 'toggle': 
					$val  = $this->item['approve'] ^ 1; 
					break;
				case 'on':     
					$val = 1; 
					break;				
				case 'off':   
					$val = 0; 
					break;
				default:
					Alerts::setError('Произошла ошибка (' . __LINE__ . ').');
					return false;
			}

			if (DB::set("UPDATE `{$this->table}` SET `approve` = ? WHERE `id` = ? LIMIT 1", array($val, $this->item['id']))) {
				$status = ($val) ? 'опубликовано на сайте' : 'снято с публикации';
				Alerts::setSuccess('«' . no_empty(@$this->item['name'], 'ID:' . $this->item['id']) . '» — ' . $status . '.');
				return true;
			}
		}

		Alerts::setError('Произошла ошибка (' . __LINE__ . ').');
		return false;
    }


	/**
     * .
     *
	 * @param int id записи
	 * @return string
     */
    public function action_executed($id)
    {
		$this->exec_executed($id, 'toggle');
		
		// Очиска кеша.
		$this->cacheClear();

		return redirect(Router::$url, '#item_' . $id);
    }

	/**
     * Выполнение действия: .
     *
	 * @param int id записи
	 * @param flag string действие (toogle, on, off)
	 * @return bool
     */
	public function exec_executed($id, $flag)
    {
		if (isset($this->structure['executed']) && $this->getItem($id)) {
			switch ($flag) {
				case 'toggle': 
					$val  = $this->item['executed'] ^ 1; 
					break;
				case 'on':     
					$val = 1; 
					break;				
				case 'off':   
					$val = 0; 
					break;
				default:
					Alerts::setError('Произошла ошибка (' . __LINE__ . ').');
					return false;
			}

			if (DB::set("UPDATE `{$this->table}` SET `executed` = ? WHERE `id` = ? LIMIT 1", array($val, $this->item['id']))) {
				$status = ($val) ? 'помечено как прочитанное' : 'помечено как новое';
				Alerts::setSuccess('«' . no_empty(@$this->item['name'], 'ID:' . $this->item['id']) . '» — ' . $status . '.');
				return true;
			}
		}

		Alerts::setError('Произошла ошибка (' . __LINE__ . ').');
		return false;
    }	


    public function action_main($id)
    {
		$this->exec_main($id, 'toggle');
		
		// Очиска кеша.
		$this->cacheClear();

		return redirect(Router::$url);
    }

	public function exec_main($id, $flag)
    {
		if (isset($this->structure['main']) && $this->getItem($id)) {
			switch ($flag) {
				case 'toggle': 
					$val  = $this->item['main'] ^ 1; 
					break;
				case 'on':     
					$val = 1; 
					break;				
				case 'off':   
					$val = 0; 
					break;
				default:
					Alerts::setError('Произошла ошибка (' . __LINE__ . ').');
					return false;
			}

			if (DB::set("UPDATE `{$this->table}` SET `main` = ? WHERE `id` = ? LIMIT 1", array($val, $this->item['id']))) {
				$status = ($val) ? 'выведен на главной' : 'снято с главной';
				Alerts::setSuccess('«' . no_empty(@$this->item['name'], 'ID:' . $this->item['id']) . '» — ' . $status . '.');
				return true;
			}
		}

		Alerts::setError('Произошла ошибка (' . __LINE__ . ').');
		return false;
    }	
	
	
	// Вывести в блоке недавние работы
    public function action_not_yet($id)
    {
		$this->exec_not_yet($id, 'toggle');
		
		// Очиска кеша.
		$this->cacheClear();

		return redirect(Router::$url);
    }

	public function exec_not_yet($id, $flag)
    {
		if (isset($this->structure['not_yet']) && $this->getItem($id)) {
			switch ($flag) {
				case 'toggle': 
					$val  = $this->item['not_yet'] ^ 1; 
					break;
				case 'on':     
					$val = 1; 
					break;				
				case 'off':   
					$val = 0; 
					break;
				default:
					Alerts::setError('Произошла ошибка (' . __LINE__ . ').');
					return false;
			}

			if (DB::set("UPDATE `{$this->table}` SET `not_yet` = ? WHERE `id` = ? LIMIT 1", array($val, $this->item['id']))) {
				$status = ($val) ? 'добавлен в блок недавние работы' : 'снят из блока недавние работы';
				Alerts::setSuccess('«' . no_empty(@$this->item['name'], 'ID:' . $this->item['id']) . '» — ' . $status . '.');
				return true;
			}
		}

		Alerts::setError('Произошла ошибка (' . __LINE__ . ').');
		return false;
    }	
	


	/**
     * Экшен: Удаление.
     *
     * @param int $id
	 * @return string
     */
	public function action_remove($id)
    {
		$this->exec_remove($id);
		
		// Очиска кеша.
		$this->cacheClear();
		
		return redirect(Router::$url);
	}

	/**
     * Выполнение действия: Удаление.
     *
     * @param int $id
	 * @return bool
     */
	public function exec_remove($id)
    {
		//только для администратора + СЦ
		Auth::allow(3);

		if ($this->getItem($id)) {
			if ($this->before_remove($this->item['id'])) {
                if(Router::$module == 'demand' && $this->item['cart'] == 0) {
                    $this->setToCart();
                    Alerts::setSuccess('«' . no_empty(@$this->item['name'], 'ID:' . $this->item['id']) . '» — успешно перемещено в корзину.');
                    return true;
                } else {
                    if (DB::set("DELETE FROM `{$this->table}` WHERE `id` = ? LIMIT 1", $this->item['id'])) {
                        // Удаление файлов.
                        if (!empty($this->files)) {
                            Files::remove($this->module, $this->item['id']);
                        }

                        // Колбек после удаления.
                        $this->after_remove($this->item['id']);

                        Alerts::setSuccess('«' . no_empty(@$this->item['name'], 'ID:' . $this->item['id']) . '» — успешно удалено.');
                        return true;
                    }
                }
			} else {
				return false;
			}
		}

		Alerts::setError('Произошла ошибка (' . __LINE__ . ').');	
		return false;
    }

    public function setToCart()
    {
        if (DB::set("UPDATE `{$this->table}` SET `cart` = 1 WHERE `id` = ? LIMIT 1", $this->item['id'])) {
            return true;
        }
    }

	/**
     * Колбек до удаления.
     *
     * @param int $id
	 * @return bool
     */
	public function before_remove($id)
    {
		return true;
	}	
	
	/**
     * Колбек после удаления.
     *
     * @param int $id
	 * @return bool
     */
	public function after_remove($id)
    {
		return true;
	}	 
   
	/**
     * Загаловок страницы админки.
     */
	public function pageHeader()
	{
		ob_start();
		?>  
		
		<div class="navbar-header">
			<a class="navbar-brand" href="<?php echo get_url(Router::$url); ?>">
				<?php echo $this->title; ?>
				<?php if (!empty($this->subtitle)): ?>
				<small class="muted"><?php echo $this->subtitle; ?></small>
				<?php endif; ?>
			</a>
		</div>

		<?php
		return ob_get_clean();
	}

	/**
	 * Инфо для формы и просмотра.
	 * 
	 * @paran array $addition дополнительные данные.
	 * @return string
     */
	public function blockquote($append = array())
	{
		// Автор.
		// if (Auth::perm(3) && isset($this->item['user_id'])) {
		// 	$user = DB::getRow("SELECT * FROM `#__users` WHERE `id` = ?", $this->item['user_id']);
		// 	$text = (empty($user)) ? 'Учетная запись не найдена' : '<a href="' . get_url_admin('users/edit/' . $this->item['user_id']) . '">' . no_empty($user['name'], $user['login']) . '</a>';
		// 	$append = array_merge(array('Автор' => $text), $append);
		// }			
		
		// Редакция.
		if (isset($this->item['date_edit'])) {
			$append = array_merge(array('Редакция' => date_human($this->item['date_edit'], true)), $append);
		}

		// Название.
		if (isset($this->item['name'])) {
            if(Router::$module == 'demand') {
    			$append = array_merge(array('Название СЦ' => DB::getValue("SELECT `name` FROM `#__users` WHERE `id` =?", @$this->item['user_id'])), $append);
            } else {
    			$append = array_merge(array('Название' => $this->item['name']), $append);
            }
		}
		
		// Дата.
		if (isset($this->item['date_add'])) {
			$append = array_merge(array('Дата' => date_human($this->item['date_add'], true)), $append);
		}			
	
		// ID.
		if (isset($this->item['id'])) {
			$append = array_merge(array('ID' => $this->item['id']), $append);
		}		
		
		ob_start();
		?>

		<blockquote>
			<?php foreach ($append as $key => $row): ?> 
			<p><strong><?php echo $key; ?>:</strong> <?php echo $row; ?></p> 
			<?php endforeach; ?> 
		</blockquote>

		<?php
		return ob_get_clean();	
	}

	/**
     * Кнопка "Сохранить".
     */
	public function buttonSave($label = 'Сохранить')
	{
		return '<button class="btn btn-default" type="submit" name="send" value="save">' . $label . '</button> ';
	}	

	/**
     * Кнопка "Применить".
     */
	public function buttonApply($label = 'Применить')
	{
		return '<button class="btn btn-default" type="submit" name="send" value="apply">' . $label . '</button> ';
	}
	
	/**
     * Кнопка "Копировать".
     */
	public function buttonCopy($label = 'Копировать')
	{
		if (!empty($this->form['id'])) {
			return '<a class="btn btn-default" href="' . get_url(Router::$url . '/copy/' . $this->form['id']) . '">' . $label . '</a> ';
		}
	}

	/**
     * Кнопка "Закрыть".
     */
	public function buttonClose($label = 'Закрыть')
	{
		if (empty($this->form['id'])) {
			return '<a class="btn btn-default" href="' . get_url(Router::$url) . '">' . $label . '</a> ';
		} else {
			return '<a class="btn btn-default" href="' . get_url(Router::$url) . '#item-' . $this->form['id'] . '">' . $label . '</a> ';
		}
	}
	
	/**
     * Кнопка "Удалить" (только для администратора).
     */
	public function buttonRemove($label = 'Удалить')
	{
		if (!empty($this->form['id']) && Auth::perm(3)) {
			return '<a class="btn btn-danger" onclick="return confirmRemove();" href="' . get_url(Router::$url . '/remove/' . $this->form['id']) . '">' . $label . '</a> ';
		}
	}
	
	/**
     * Кнопка "Добавить".
     */
	public function buttonAdd($label = 'Добавить')
	{
		if (Router::$action != 'add') {
			return '<a class="btn btn-success" href="' . get_url(Router::$url . '/add') . '">' . $label . '</a> ';
		}
	}

	/**
     * Кнопка "Сохранить порядок".
     */
	public function buttonSort($label = 'Сохранить порядок')
	{
		if (isset($this->structure['sort'])) {
			return '<button class="btn btn-default" type="submit" name="sort">' . $label . '</button> ';
		}
	}

	/**
     * Кнопка "Действие с отмечеными".
     */
	public function buttonAction()
	{
		ob_start();
		?>
	
		<div class="btn-group">
			<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">Действие с отмечеными <span class="caret"></span></button>
			<ul class="dropdown-menu" id="action-menu">
				<?php if (isset($this->structure['approve'])): ?>
				<li><a href="#" data-action="approve" data-mode="on"><i class="ico ico-checkmark"></i> Опубликовать</a></li>
				<li><a href="#" data-action="approve" data-mode="off"><i class="ico ico-lock"></i> Снять с публикации</a></li>
				<li class="divider"></li>	  
				<?php endif; ?>
				
				<?php if (isset($this->structure['main'])): ?>
				<li><a href="#" data-action="main" data-mode="on"><i class="ico ico-star-3"></i> Вывести на главной</a></li>
				<li><a href="#" data-action="main" data-mode="off"><i class="ico ico-star"></i> Снять с главной</a></li>
				<li class="divider"></li>	  
				<?php endif; ?>
				
				<?php if (isset($this->structure['executed'])): ?>
				<li><a href="#" data-action="executed" data-mode="on"><i class="ico ico-checkmark"></i> Пометить как прочитанное</a></li>
				<li class="divider"></li>	
				<?php endif; ?>
				
				<?php if (Auth::perm(3)): ?>
				<li><a href="#" data-action="remove"><span class="text-danger"><i class="ico ico-remove"></i> Удалить</span></a></li>
				<?php endif; ?>
			</ul>
		</div>

		<?php
		return ob_get_clean();	
	}

	/**
     * Кнопки для форм.
     */
	protected function buttonForms()
	{
		$out  = $this->buttonApply();
		$out .= $this->buttonSave();
		$out .= $this->buttonCopy();
		$out .= $this->buttonClose();
		$out .= $this->buttonRemove();
		$out .= $this->buttonAdd();
	
		if (!empty($this->form['id'])) {
			$out .= '<input type="hidden" name="id" value="' . $this->form['id'] . '">';
		}

		return $out;	
	}

	/**
     * Кнопки для списков.
     */
	protected function buttonLists()
	{
		$out  = $this->buttonAction();
		$out .= $this->buttonSort();
		$out .= $this->buttonAdd();
		return $out;
	}
}
