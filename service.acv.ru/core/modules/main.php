<?php
/**
 * Главная.
 */
class module_main extends Frontend
{
	public $module = 'main';

	public function action_index()
    {
        if ($this->getPage($this->module)) {

            $this->slides = DB::getAll("SELECT * FROM `#__slider` WHERE `approve` = 1 ORDER BY `sort`");
			$this->parent_cats = DB::getAll("SELECT * FROM `#__category` WHERE `parent` = 0 AND `approve` = 1 ORDER BY `name` LIMIT 4");

			// Установка мета тегов и вывод в шаблон.
			$this->setMeta($this->page);
			$this->content = $this->render(dirname(__FILE__ ) . '/main.tpl');
			return $this->display('page.tpl');
		} else {
			return Router::get('404');
		}			
    }

	/**
	 * Статические страницы.
	 */
    public function action_call($sef, $args)
    {
		$route[] = Input::cleanSef($sef);
		foreach ($args as $row) {
			$route[] = Input::cleanSef($row);
		}

		return Router::find(
			$route, array(
//				array(null, array('#__pages' => 'parent'), array('pages', 'show')),
//				array(null, array('#__forum_themes' => 'parent'), array('forum', 'show')),
//				array(null, array('#__category' => 'parent'), array('category', 'show')),
//				array(null, array('#__category' => 'parent', '#__category_items' => 'portfol_id'), array('category', 'childShow')),
//				array('print', array('#__print' => 'parent'), array('prints', 'show')),
			)
		);
    }
}