<?php
/** 
 * Администрирование / Экспорт.
 */
class module_export extends Backend
{
	public $module = 'import';
	public $table  = '#__demand';
	public $title  = 'Экспорт';
	
	public function __construct()
	{
        // Уровень доступа.
		$this->init(4);
	}

    public function action_index()
    {
        require_once PLUGINS_DIR . '/PHPExcel/Classes/PHPExcel.php';
        require_once PLUGINS_DIR . '/PHPExcel/Classes/PHPExcel/Writer/Excel5.php';
        require_once PLUGINS_DIR . '/PHPExcel/Classes/PHPExcel/IOFactory.php';

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $active_sheet = $objPHPExcel->getActiveSheet();

        //Создаем шапку таблички данных
        $active_sheet->setCellValue('A1','Номер договора');
        $active_sheet->setCellValue('B1','Дата истечения срока договора');
        $active_sheet->setCellValue('C1','Номер акта');
        $active_sheet->setCellValue('D1','Дата заполнения');
        $active_sheet->setCellValue('E1','Название СЦ');
        $active_sheet->setCellValue('F1','Модель оборудования, код');
        $active_sheet->setCellValue('G1','Серийный № изделия');
        $active_sheet->setCellValue('H1','Описание дефекта');
        $active_sheet->setCellValue('I1','код запчасти');
        $active_sheet->setCellValue('J1','название запчасти');
        $active_sheet->setCellValue('K1','Вид выполненных работ');
        $active_sheet->setCellValue('L1','№ гарантийного талона');
        $active_sheet->setCellValue('M1','Статус рекламации');
        $active_sheet->setCellValue('N1','Итого к оплате');
        $active_sheet->setCellValue('O1','Номер платежного поручения');
//        $active_sheet->setCellValue('O1','Ссылка на файл');

        //В цикле проходимся по элементам массива и выводим все в соответствующие ячейки
        $row_start = 2;
        $i = 0;
        foreach (DB::getAll("
          SELECT
            `a`.*,
            `b`.`login`,
            `b`.`date_end`,
            `b`.`name` as `name_sc`
          FROM
            `#__demand` as `a`
          LEFT JOIN
            `#__users` as `b`
          ON
            `a`.`user_id` = `b`.`id`
          WHERE
            1 = 1 AND `a`.`stage` = 3
          ORDER BY
            `date_add` DESC
         ") as $item) {

            $furniture = DB::getRow("SELECT `name`, `sku` FROM `#__furniture` WHERE `id` = ?", $item['furniture']);
            $parts = DB::getAll("SELECT `name`, `sku` FROM `#__parts` WHERE FIND_IN_SET(`id`, ?)", $item['parts']);
            $parts_k = implode(', ', extract_values($parts, 'sku'));
            $parts_n = implode(', ', extract_values($parts, 'name'));

            $works = DB::getAll("SELECT `name` FROM `#__works` WHERE FIND_IN_SET(`id`, ?)", $item['works_ids']);
            $works_n = implode(', ', extract_values($works, 'name'));

            $row_next = $row_start + $i;

            $active_sheet->getStyle('I'.$row_next)->getAlignment()->setWrapText(true);
            $active_sheet->getStyle('J'.$row_next)->getAlignment()->setWrapText(true);
            $active_sheet->getStyle('K'.$row_next)->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getRowDimension($row_next)->setRowHeight(-1);

            $active_sheet->setCellValue('A'.$row_next, $item['login']);
            $active_sheet->setCellValue('B'.$row_next, date_human($item['date_end']));
            $active_sheet->setCellValue('C'.$row_next, '155А2 -' . $item['id']);
            $active_sheet->setCellValue('D'.$row_next, date_human($item['date_work']));
            $active_sheet->setCellValue('E'.$row_next, $item['name_sc']);
            $active_sheet->setCellValue('F'.$row_next, ($furniture['sku']) ? $furniture['name'] . ' - ' . $furniture['sku'] : $furniture['name']);
            $active_sheet->setCellValue('G'.$row_next, $item['serial']);
            $active_sheet->setCellValue('H'.$row_next, Input::cleanStr($item['text']));
            $active_sheet->setCellValue('I'.$row_next, $parts_k);
            $active_sheet->setCellValue('J'.$row_next, $parts_n);
            $active_sheet->setCellValue('K'.$row_next, $works_n);
            $active_sheet->setCellValue('L'.$row_next, $item['warranty_num']);
            $active_sheet->setCellValue('M'.$row_next, Config::$step[$item['step']]);
            $active_sheet->setCellValue('N'.$row_next, $item['total']);
            $active_sheet->setCellValue('O'.$row_next, $item['num_pay']);
//            $active_sheet->setCellValue('O'.$row_next, '');
            $i++;
        }
        
        header("Content-Type:application/vnd.ms-excel");
        header("Content-Disposition:attachment;filename=order.xls");

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');

        // Редирект.
        Alerts::setSuccess("Экспорт успешно завершен.");

//
//    $this->subtitle = 'список';
//    $this->content  = $this->render(Router::$path . '/list.tpl');
//    return $this->display('page.tpl');
        return redirect(Config::$url_admin);

    }

}