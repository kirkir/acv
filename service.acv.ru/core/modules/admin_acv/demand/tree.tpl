<?php
$active = ($this->item['shows'] && !Auth::perm(4)) ? 'active' : '';


//(@$letter_link) ? '<a class="pdf_ico" href="'.get_url_admin($letter_link).'"></a>' : ''

echo $this->tr(
	array(
		$this->tdCheck(),
//		$this->tdId(),
		$this->tdEdit(DB::getValue("SELECT `name` FROM `#__users` WHERE `id` =?", @$this->item['user_id'])),
        $this->tdDate(),
		$this->tdEdit(Config::$stage[$this->item['stage']], 'background:' . Config::$stage_color[$this->item['stage']], ($this->item['stage'] == 3) ? 'white' : ''),
		$this->tdEdit('155А-' . $this->item['id']),
		$this->tdEdit(($this->item['step']) ? Config::$step[$this->item['step']] : '    '),
		$this->tdEdit(DB::getValue("SELECT `name` FROM `#__furniture` WHERE `id` = ?", $this->item['furniture'])),
		$this->tdEdit($this->item['serial']),
		$this->tdEdit($this->item['num_pay'] ? $this->item['num_pay'] : '&nbsp'),
		$this->tdEditable(
			(file_exists(UPLOADS . DIRECTORY_SEPARATOR . '/letter-'.$this->item['id'].'.pdf') && !in_array($this->item['step'], array(1,2,5))) ? '<a target="_blank" class="pdf_ico" href="'.get_url('uploads/letter-' . $this->item['id'].'.pdf?v=' . random_string(8, 'all')).'"></a>' : '&nbsp'
		),
		$this->tdEdit(crop_text($this->item['text'], 50)),
		$this->tdEdit("<div class='comment_hidden'>".$this->item['comment']."</div><a class='ico ". $active ." ico-comment' href='#'></a>", 'text-align: center', 'comment'),
		$this->tdApprove(),
		$this->tdRemove()
	)
);