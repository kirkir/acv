<?php
/** 
 * Администрирование / Отчеты.
 */
class module_demand extends Backend
{
	public $module = 'demand';
	public $table  = '#__demand';
	public $title  = 'Отчеты';
	public $not_cart  = ' AND `cart` = 0';
	public $in_cart  = ' AND `cart` = 1';

	public $files = array(
		'act' => array(
			'type'     => 1,
			'multiple' => true,
			'unique'   => true
		),
		'warranty' => array(
			'type'     => 1,
			'multiple' => true,  
			'unique'   => false
		),
		'sh' => array(
			'type'     => 1,
			'multiple' => true,
			'unique'   => false
		),
        'f_d' => array(
			'type'     => 1,
			'multiple' => true,
			'unique'   => false
		),
        'f_u' => array(
			'type'     => 1,
			'multiple' => true,
			'unique'   => false
		),
        'f_o' => array(
			'type'     => 1,
			'multiple' => true,
			'unique'   => false
		),
        'n_p' => array(
			'type'     => 1,
			'multiple' => false,
			'unique'   => false
        ),
        'doc_buy' => array(
			'type'     => 1,
			'multiple' => true,
			'unique'   => false
		),
        'foto_client' => array(
			'type'     => 1,
			'multiple' => true,
			'unique'   => false
		)
	);
	
	public function __construct()
	{
        // Уровень доступа.
		$this->init(3);
	}

	public function action_index()
    {
        $this->handlerActions();

        // Список.

//            $this->name     = (isset($_POST['name'])) ? Input::getStr($_POST['name']) : Input::getStr($_GET['name']);
//            $this->category = (isset($_POST['category'])) ? Input::getInt($_POST['category']) : Input::getInt($_GET['category']);
//            $this->type     = (isset($_POST['type'])) ? Input::getInt($_POST['type']) : Input::getInt($_GET['type']);
//            $this->author   = (isset($_POST['author'])) ? Input::getInt($_POST['author']) : Input::getInt($_GET['author']);
//            $this->approve  = (isset($_POST['approve'])) ? Input::getInt($_POST['approve']) : Input::getInt($_GET['approve']);
//
//            $sql = "SELECT * FROM `{$this->table}` WHERE 1 = 1";
//
//            if (!Auth::perm(4))  {
//                $sql .= " AND `user_id` = " . Auth::$user['id'];
//            }
//
//            $cur = array();
//
//            if($this->name) {
//                $sql .= " AND `name` LIKE '%{$this->name}%'";
//            }
//
//            if($this->category) {
//                $childs = get_childs('#__category', $this->category, 1);
//                $childs[]['id'] = $this->category;
//                $childs = extract_values($childs, 'id');
//                $childs = implode(',', $childs);
//                $sql .= " AND FIND_IN_SET(`portfol_id`, '{$childs}')";
//
//                $cur[] = "category={$this->category}";
//            }
//
//            if($this->type) {
//                $sql .= " AND `type` = {$this->type}";
//                $cur[] = "type={$this->type}";
//            }
//
//            if($this->author) {
//                $sql .= " AND `author_id` = {$this->author}";
//                $cur[] = "author={$this->type}";
//            }
//
//            if(isset($this->approve)) {
//                $approve = ($this->approve == 2) ? 0 : $this->approve;
//                $ap = ($this->approve == 2) ? 0 : 1;
//                $sql .= " AND `approve` = {$ap}";
//                $cur[] = "approve={$this->approve}";
//            } else {
//                $sql .= ' AND `approve` = 1';
//            }
//
//            $sql .= ' ORDER BY `name`';
//            $cur = implode('&', $cur);
//
////        dd($sql);
//
//            $this->paginator = new Paginator(get_url(Router::$url), 50, "?{$cur}&page=");
//            $this->items = $this->paginator->process($sql);

            $draft_archive = "WHERE `archive` = 0 AND `draft` = 0 {$this->not_cart}";
            $this->paginator = new Paginator(get_url(Router::$url), 20);
            if(Auth::perm(4)) {
                $this->items = $this->paginator->process("SELECT * FROM `{$this->table}` {$draft_archive} ORDER BY `date_add` DESC");
            } else {
                $this->items = $this->paginator->process("SELECT * FROM `{$this->table}` {$draft_archive}  AND `user_id` = ? ORDER BY `date_add` DESC", Auth::$user['id']);
            }

		// Вывод в шаблон.
		$this->subtitle = 'список';
		$this->content  = $this->render(Router::$path . '/list.tpl');
		return $this->display('page.tpl');
    }

    public function action_cart()
    {
        // Уровень доступа.
        $this->init(4);
        $this->paginator = new Paginator(get_url(Router::$url), 20);
        $this->items = $this->paginator->process("SELECT * FROM `{$this->table}` WHERE 1 = 1 $this->in_cart ORDER BY `date_add` DESC");

        // Вывод в шаблон.
        $this->subtitle = 'список';
        $this->content  = $this->render(Router::$path . '/cart_list.tpl');
        return $this->display('page.tpl');
    }

    public function action_draft()
    {
        $draft_archive = "WHERE `archive` = 0 AND `draft` = 1";
        $this->paginator = new Paginator(get_url(Router::$url), 20);
        if(Auth::perm(4)) {
            $this->items = $this->paginator->process("SELECT * FROM `{$this->table}` {$draft_archive} ORDER BY `date_add` DESC");
        } else {
            $this->items = $this->paginator->process("SELECT * FROM `{$this->table}` {$draft_archive}  AND `user_id` = ? ORDER BY `date_add` DESC", Auth::$user['id']);
        }
        // Вывод в шаблон.
        $this->subtitle = 'черновик';
        $this->content  = $this->render(Router::$path . '/list.tpl');
        return $this->display('page.tpl');
    }

    public function action_archive()
    {
        $draft_archive = "WHERE `archive` = 1 AND `draft` = 0";
        $this->paginator = new Paginator(get_url(Router::$url), 20);
        if(Auth::perm(4)) {
            $this->items = $this->paginator->process("SELECT * FROM `{$this->table}` {$draft_archive} ORDER BY `date_add` DESC");
        } else {
            $this->items = $this->paginator->process("SELECT * FROM `{$this->table}` {$draft_archive}  AND `user_id` = ? ORDER BY `date_add` DESC", Auth::$user['id']);
        }
        // Вывод в шаблон.
        $this->subtitle = 'архив';
        $this->content  = $this->render(Router::$path . '/list.tpl');
        return $this->display('page.tpl');
    }

	/**
     * Обработка форм.
     */
    public function handler()
    {

        if (isset($this->item['id'])) {
            if (Auth::$user['level'] < 4 && $this->item['user_id'] != Auth::$user['id']) {
                return Router::get('404');
            }
        }

        if (!isset($_POST['send'])) {
			return false;
		}


        $numPay = Auth::perm(4) ? $_POST['num_pay'] : (isset($this->item['num_pay']) ? $this->item['num_pay'] : '');


		$this->form['id']           = Input::getInt($_POST['id']);
		$this->form['name']         = Input::getStr($_POST['name']);
		$this->form['num_pay']      = $numPay;
		$this->form['text']         = Input::getHtml($_POST['text']);
        if(Auth::perm(4)) {
    		$this->form['comment']      = Input::getHtml($_POST['comment']);
        } else {
            $this->form['comment'] = (@$this->item['comment']) ? $this->item['comment'] : '';
        }
		$this->form['furniture']    = Input::getInt($_POST['furniture']);
		$this->form['serial']       = Input::getStr($_POST['serial']);
		$this->form['date_pusk']    = Input::getTime($_POST['date_pusk']);
		$this->form['adress']       = Input::getStr($_POST['adress']);
		$this->form['info']         = Input::getListInt($_POST['info']);
		$this->form['fio']          = Input::getStr($_POST['fio']);
		$this->form['approve']      = Input::getBool($_POST['approve'], 1);
		$this->form['warranty']     = (!Auth::perm(4)) ? Input::getBool($this->item['warranty']) : Input::getBool($_POST['warranty']);
//		$this->form['warranty_file']= (!Auth::perm(4)) ? Input::getBool($this->item['warranty_file']) : Input::getBool($_POST['warranty_file']);
		$this->form['warranty_file']= Input::getBool($_POST['warranty_file']);
		$this->form['archive']      = Input::getBool($_POST['archive']);
		$this->form['draft']        = Input::getBool($_POST['draft']);
		$this->form['warranty_num'] = Input::getStr($_POST['warranty_num']);
		$this->form['price_one_h']  = Input::getPrice($_POST['price_one_h']);
		$this->form['parts']        = Input::getStr($_POST['p_ids']);
		$this->form['works_ids']    = Input::getStr($_POST['works_ids']);
		$this->form['km']           = Input::getStr($_POST['km'], 0);
		$this->form['date_work']    = Input::getTime($_POST['date_work']);
		$this->form['stage']        = Input::getInt($_POST['stage'], 1);
        if(Auth::perm(4)) {
            $this->form['step']      = Input::getInt($_POST['step']);
        } else {
            $this->form['step'] = (@$this->item['step']) ? $this->item['step'] : 1;
        }
		$this->form['total']         = Input::getPrice($_POST['total']);
		$this->form['compensation_bool']         = Input::getBool($_POST['compensation_bool']);
		$this->form['compensation_value']         = Input::getInt($_POST['compensation_value']);
		$this->form['cart']         = Input::getBool($_POST['cart'], 0);


        if (empty($this->form['furniture'])) {
			$this->error['furniture'] = 'Укажите Модель оборудования';
		}

        if (empty($this->form['name'])) {
			$this->error['name'] = 'Укажите наименование продавца';
		}

        if (empty($this->form['adress'])) {
			$this->error['adress'] = 'Укажите место установки, адрес';
		}

        if (empty($this->form['serial'])) {
			$this->error['serial'] = 'Укажите cерийный номер изделия';
		}

        if (empty($this->form['fio'])) {
			$this->error['fio'] = 'Укажите Клиента';
		}

        if (empty($this->form['text'])) {
			$this->error['text'] = 'Укажите описание дефекта';
		}

        if (empty($this->form['warranty_num'])) {
			$this->error['warranty_num'] = 'Укажите номер гарантийного талона';
		}

        if (empty($this->form['km'])) {
            $this->form['km'] = 0;
//			$this->error['km'] = 'Укажите расстояние от СЦ';
		}

        if (empty($this->form['date_work'])) {
			$this->error['date_work'] = 'Укажите Дату ремонта';
		}

         if (empty($this->form['step'])) {
            $this->form['step'] = true;
        }

        $this->works_ids = (@$this->item['works_ids']) ? @$this->item['works_ids'] : Input::getStr($_POST['works_ids']);

        if (!$this->works_ids) {
			$this->error['work_list'] = 'Укажите вид работ';
		}

        if (empty($this->error)) {
            if(Router::$action == 'edit' && Auth::perm(4)) {
                if($this->item['stage'] != $this->form['stage']) {
                    $this->form['stage'] = Input::getInt($_POST['stage'], 1);
                } elseif ($this->item['stage'] == 1) {
                    $this->form['stage'] = 2;
                }
            }
			$id = (empty($this->form['id'])) ? $this->dbAdd() : $this->dbSave();
			if (!empty($id)) {
                Relations::setRelation(Router::$action);
                if(Router::$action == 'add') {
                    if ($_GET['service']) {
                        $service = DB::getRow('SELECT * FROM `#__service` WHERE `id` = ?', Input::getInt($_GET['service']));
                        if ($service) {
                            $user = Users::getUser($service['to_user_id']);
                            DB::set("
                                UPDATE
                                  `#__service`
                                SET
                                  `process` = 1
                                WHERE
                                  `id` = ?
                            ", array($service['id']));

                            $mail = new Mail();
                            $mail->from(Settings::get('email'), Settings::get('email_name'));
                            $mail->to($service['email']);
                            $mail->subject = 'ACV, заявка № ' . $service['order_name'];
                            $mail->body = "
                            <p>Здравствуйте, Уважаемый ".$service['name'].".</p>
                            <p>".date('d.m.Y', $service['date_add'])." вы обращались в ACV и оставляли заявку на (обслуживание или гарантийный выезд). К Вам приезжал АСЦ ".$user['login'].".</p>
                            <p>Для того, чтобы мы могли улучшить качество нашего сервиса, просим Вас ответным письмом поставить оценку от 1 до 5 (где 1 – совсем не удовлетворен, а 5 – все отлично). Также, при возможности, просим Вас оставить комментарии и пожелания. Спасибо!</p>
                            ";
                            $mail->send();
                        }
                    }
                    $demand = DB::getRow("SELECT * FROM `#__demand` WHERE `id` = ?", $id);
                    $mail = new Mail();
                    $mail->from(Settings::get('email'), Settings::get('email_name'));
                    $mail->to(Settings::get('email'));
                    $mail->subject = 'Добавлен новый отчет';
                    $mail->body = '
                        <p>
                            <strong>Ссылка на отчет: </strong><a href="' . get_url_admin('/demand/edit/' . $id) . '">Перейти</a><br />
                            <p>Это письмо создано автоматически, не отвечайте на него!</p>
                        </p>';
                    $mail->send();
                }

                return $this->handlerСomplete($id, 'edit');
			} else {
				Alerts::setError('Произошла ошибка.');
			}
		} else {
            Alerts::setError('Все поля должы быть заполнены.');
            return true;
        }
    }

    public function action_call($name, $arguments)
    {

        $this->init(4);
        $actions = $arguments;
        $id = array_pop($arguments);
        $id = Input::getInt($id);

        if($id && Users::getAuthorName($id) && $this->slogin = Users::getAuthorLogin($id)) {

            $this->paginator = new Paginator(get_url(Router::$url), 20);
            if($actions[0] == 'draft') {
                // Вывод в шаблон.
                $this->subtitle = 'черновик';
                $draft_archive = "WHERE `archive` = 0 AND `draft` = 1";
            } elseif($actions[0] == 'archive') {
                $this->subtitle = 'архив';
                $draft_archive = "WHERE `archive` = 1 AND `draft` = 0";
            } else {
                $this->subtitle = 'список';
                $draft_archive = "WHERE `archive` = 0 AND `draft` = 0";
            }

            $this->items = $this->paginator->process("SELECT * FROM `{$this->table}` {$draft_archive} AND `user_id` = ? ORDER BY `date_add` DESC", $id);
            $this->sname = Users::getAuthorName($id);
            $this->content  = $this->render(Router::$path . '/list_service.tpl', $this->id = $id);
            return $this->display('page.tpl');
        } else {
            return Router::get('404');
        }
    }

    public function action_updateCommit()
    {
        $id = Input::cleanInt($_POST['id']);
        $text = Input::getStr($_POST['text']);
        $data = DB::getRow("SELECT * FROM `#__demand` WHERE `id` = ?", $id);
        if(@$_POST['get_commit']) {
            if($data['shows'] && !Auth::perm(4)) {
                DB::set("UPDATE `#__demand` SET `shows` = 0 WHERE `id` = ?", array($id));
                $show_inactive = true;
            } else {
                $show_inactive = false;
            }
            return json_encode(array('succsess' => true, 'commit' => $data['comment'], 'show_inactive' => $show_inactive));
        } else {
            if(strlen($data['comment']) != strlen($text)) {
                if($ids = DB::set("UPDATE `#__demand` SET `comment` = ?, `shows` = 1, `stage` = IF(stage = 3, 3, 2) WHERE `id` = ?", array($text, $id))) {
                    $mail = new Mail();
                    $mail->from(Settings::get('email'), Settings::get('email_name'));
                    $mail->to(Users::get_email($data['user_id']));
                    $mail->subject = 'В отчете обновлен комментарий администратора';
                    $mail->body = '
                            <p>
                                <strong>Ссылка на отчет: </strong><a href="' . get_url_admin('/demand/edit/' . $id) . '">Перейти</a><br />
                                <p>Это письмо создано автоматически, не отвечайте на него!</p>
                            </p>';
                    $mail->send();
                    return json_encode(array('update' => $id));
                }
            }
        }
    }

    public function action_parts()
    {
        $q = Input::getStr($_GET['q']);
        $this->parts = DB::getAll("SELECT `id`, `name` FROM `#__parts` WHERE `sku` LIKE '%{$q}%' OR `name` LIKE '%{$q}%'");
        foreach ($this->parts as $part) {
            $data[] = array('id' => $part['id'], 'text' => $part['name']);
        }

        return json_encode($data);
    }

    public function action_add_part_to_demand()
    {
        return json_encode(DB::getRow("SELECT * FROM `#__parts` WHERE `id` = ?", Input::getInt($_POST['part'])));

    }

    public function action_add_work_to_demand()
    {
        return json_encode(array(DB::getRow("SELECT * FROM `#__works` WHERE `id` = ?", Input::getInt($_POST['work'])), 'level' => Auth::$user['level']));
    }

    public function isset_parts($ids)
    {
        return DB::getAll("SELECT `id`, `name`, `sku` FROM `#__parts` WHERE FIND_IN_SET(`id`, ?) ORDER BY `name`", $ids);
    }

    public function isset_works($ids)
    {
        return DB::getAll("SELECT * FROM `#__works` WHERE FIND_IN_SET(`id`, ?) ORDER BY `name`", $ids);
    }

    public function action_change_work_data()
    {
        if($demand_dinamic = DB::getRow("SELECT * FROM `#__demand_dinamic` WHERE `demand_id` = ? AND `work_id` = ?",
            array(Input::getInt($_POST['demand_id']), Input::getInt($_POST['work_id'])))) {
            DB::set("UPDATE `#__demand_dinamic` SET `hour` = ?, `price` = ? WHERE `id` = ?", array(Input::getFloat($_POST['hour']), Input::getFloat($_POST['price']), $demand_dinamic['id']));
        } else {
            DB::add("INSERT INTO `#__demand_dinamic` SET `demand_id` = ?, `work_id` = ?, `hour` = ?, `price` = ?", array(Input::getInt($_POST['demand_id']), Input::getInt($_POST['work_id']), Input::getFloat($_POST['hour']), Input::getFloat($_POST['price'])));
        }
    }

    public function delivery_total()
    {
        $price20km = 2000;
        if(!@$this->item['km']) {
            $price = 0;
        } elseif($this->item['km'] <= 20) {
            $price = $price20km;
        } elseif($this->item['km'] > 20 && $this->item['km'] <= 80 ) {
            $add = 40 * ($this->item['km'] - 20);
            $price = $price20km + $add;
        } elseif($this->item['km'] > 80 && $this->item['km'] < 130 ) {
            $add = 40 * (80 - 20) + 50 * ($this->item['km'] - 80);
            $price = $price20km + $add;
        } else {
            $add = 40 * (80 - 20) + 50 * (130 - 80) + 20 * ($this->item['km'] - 130);
            $price = $price20km + $add;
        }
        return $price;
    }

    // render letter form
    public function letter()
    {
        return $this->render(Router::$path . '/letter.tpl');
    }

    public function action_PDFOutput()
    {
        $this->init(4);
        include("mpdf60/mpdf.php");
        include(PLUGINS_DIR .'/PHPMailer/PHPMailerAutoload.php');

        $html = $_POST['html'];
        $id = Input::getInt($_POST['id']);

        $this->getItem($id);


        $mpdf = new mPDF();
        $mpdf->charset_in = 'cp1251';
        $stylesheet = file_get_contents(THEMES_DIR . '/admin/css/style.css');
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->list_indent_first_level = 0;
        $mpdf->WriteHTML($html, 2);

        if (file_exists(UPLOADS . DIRECTORY_SEPARATOR . '/letter-'.$id.'.pdf')) {
            unlink(UPLOADS . DIRECTORY_SEPARATOR . '/letter-'.$id.'.pdf');
        }

        $pdf = file_put_contents(UPLOADS . DIRECTORY_SEPARATOR . '/letter-'.$id.'.pdf', $mpdf->Output('mpdf.pdf', 'S'));

        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->isHTML(true);
        $emailTo = Input::getStr($_POST['email']);

        $mail->From      = Settings::get('email');
        $mail->FromName  = Settings::get('name');
        $mail->Subject   = 'Письмо с сайта ' . Settings::get('name');
        $mail->Body      =
            "
                <p>Во вложении информационное письмо. <br> Письмо сгенерировано автоматически, не отвечайте на него!</p>
                <p>текущий отчет № - {$id}</p>
                <p>текущий cтатус - ".Config::$step[$this->item['step']]."</p>
                <p>ссылка на отчет - ". get_url_admin('demand/edit') . DIRECTORY_SEPARATOR . $id ."</p>
            ";
        $mail->AddAddress($emailTo);
        $mail->AddAddress('acvrus@mail.ru');
        $mail->AddAddress('mml2011@yandex.ru');

        $file_to_attach = UPLOADS . DIRECTORY_SEPARATOR . '/letter-'.$id.'.pdf';

        $mail->AddAttachment( $file_to_attach , 'ACV.pdf' );
        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }
        exit();
    }

    public function action_PDFResave()
    {
        $this->init(4);
        include("mpdf60/mpdf.php");

        $html = $_POST['html'];
        $id = Input::getInt($_POST['id']);

        $mpdf = new mPDF();
        $mpdf->charset_in = 'cp1251';
        $stylesheet = file_get_contents(THEMES_DIR . '/admin/css/style.css');
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->list_indent_first_level = 0;
        $mpdf->WriteHTML($html, 2);

        
        
        if (file_exists(UPLOADS . DIRECTORY_SEPARATOR . 'letter-'.$id.'.pdf')) {
            unlink(UPLOADS . DIRECTORY_SEPARATOR . 'letter-'.$id.'.pdf');
        }

        file_put_contents(UPLOADS . DIRECTORY_SEPARATOR . '/letter-'.$id.'.pdf', $mpdf->Output('mpdf.pdf', 'S'));
        return true;
    }

    public function action_downloadArchive()
    {
        $this->init(4);

        $error = "";
        $id = Input::getInt($_POST['id']);

        // files
        $data = DB::getAll("SELECT * FROM `#__files` WHERE `module` = ? AND `item` = ? ORDER BY `sort`", array('demand', $id));
        $files = extract_values($data, 'filename');

        $id = Input::getInt($_POST['id']);
        $demand = DB::getRow("SELECT * FROM `#__demand` WHERE `id` = ? ", array($id));
        $furniture = DB::getRow("SELECT * FROM `#__furniture` WHERE `id` = ? ", array($demand['furniture']));
        $user = Users::getUser($demand['user_id']);

        // generate zip
        $zip = new ZipArchive();
        $file_name = date('Ymd', time()). '_' . $user['name']. '_' . $furniture['name'] . ".zip";
        $zip_name = UPLOADS . DIRECTORY_SEPARATOR . $file_name;
        
        if($zip->open($zip_name, ZIPARCHIVE::CREATE)!==TRUE)
        {
            $error .= "* Sorry ZIP creation failed at this time";
        }

        foreach ($files as $file) {
            $zip->addFile(UPLOADS . DIRECTORY_SEPARATOR . 'demand' . DIRECTORY_SEPARATOR . $file, $file);
        }

        // generate PDF
        require_once Router::$path . "../demand/mpdf60/mpdf.php";
//
        $this->form = $demand;
        $this->content  = $this->render(Router::$path . '/demand_pdf.tpl');
        $html = $this->content;

        $mpdf = new mPDF();
        $mpdf->charset_in = 'cp1251';
        $stylesheet = file_get_contents(THEMES_DIR . '/admin/css/style.css');
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->list_indent_first_level = 0;
        $mpdf->WriteHTML($html, 2);

        if (file_exists(UPLOADS . DIRECTORY_SEPARATOR . 'demand-' . $id . '.pdf')) {
            unlink(UPLOADS . DIRECTORY_SEPARATOR . 'demand-' . $id . '.pdf');
        }

        file_put_contents(UPLOADS . DIRECTORY_SEPARATOR . 'demand-' . $id . '.pdf', $mpdf->Output('mpdf.pdf', 'S'));

        $zip->addFile(UPLOADS . DIRECTORY_SEPARATOR . 'demand-' . $id . '.pdf', 'demand-' . $id . '.pdf');

        $zip->close();

        if(file_exists($zip_name))
        {
            return json_encode(array('file' => $file_name));
        }
    }
}