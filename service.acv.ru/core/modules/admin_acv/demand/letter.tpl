<?php
$warranty_item = DB::getVAlue("SELECT `name` FROM `#__furniture` WHERE `id` = ?", $this->item['furniture']);
$info_values = explode(',', $this->item['info']);

$rec = '
    Общество с ограниченной Tel. + 7 495 545 58 00 ОГРН 1107746434190 </br>
    ответственностью «ЭйСиВи Рус» Fax +7 495 545 58 06 ИНН / КПП 7733735048/ 772301001</br>
    E-mail mos@acv.com Р/с 4070281050000100411</br>
    Ул.8-я Текстильщиков, д.11, офис 220 www.acv.com «ИНГ Банк (ЕВРАЗИЯ) А</br>
    109129, Москва кор.счет. №30101810500000000222 в ОПЕРУ ГТУ</br>
    Российская Федерация Банка России, БИК 044525222
    ';

$info_messages = array();

if (!in_array(1, $info_values)) {
    $info_messages[] = "- нарушены условия в отношении монтажа оборудования, в частности, отсутствует 
    заземление внутреннего бака бойлера (зафиксировано в Акте № - {$this->item['id']} от ". date('d.m.Y', $this->item['date_add']) ."). Данное требование является необходимым для защиты бойлера от электрохимической коррозии 
    (требования приводятся в Инструкции по монтажу и эксплуатации, прилагаемой с оборудованием).";
}

if (!in_array(2, $info_values)) {
    $info_messages[] = "-  монтаж предохранительных устройств также произведен не в соответствии с инструкцией по монтажу и эксплуатации.";
}

if (!in_array(5, $info_values)) {
    $info_messages[] = "- монтаж предохранительных устройств осуществлен непосредственно над крышкой бойлера. В этом случае монтаж предполагает отвод дренажной линии от клапана, что не было сделано в данном случае. На верхней крышке отчетливо видны следы воды. Сброс воды на верхнюю крышку бака ведет к появлению сквозной коррозии в корпусе устройства.";
}

if (in_array(3, $info_values)) {
    if (!in_array(6, $info_values)) {
        $info_messages[] = "- в качестве теплоносителя используется низкозамерзающая жидкость, не одобренная компанией ACV.";
    }
}

$blank = 'Исх. письмо: № ' . $this->item['id'];
//$blank = 'Исх. письмо: № 031-16';
$msg = 'В ответ на ваше обращение по отнесению рекламационного случая к заводскому браку ---------';
$msg .= ' <strong>' . $warranty_item . '</strong> ' . $this->item['serial'];
if($this->item['step'] == 4) {
    $conclusion = ' Сообщаем, что ваша претензия попадает под гарантийные обязательства, и подлежит удовлетворению со стороны завода-изготовителя.';
} elseif ($this->item['step'] == 3 || $this->item['step'] == 7) {
    $conclusion = ' Cообщаем вам, что на основе предоставленной информации мы не можем рассматривать данную рекламацию в рамках гарантийных обязательств нашей компании
    , (приведены в Гарантийном талоне ООО ЭйСиВи Рус) по следующим причинам:';

    if ($info_messages) {
        foreach ($info_messages as $info_message) {
            $conclusion .= "<p>{$info_message}</p>";
        }
    }

    $conclusion .= "<br><p>
        В связи с вышеописанными нарушениями вам необходимо обратиться с соответсвующей притензией в компанию, которая осуществляла монтаж, в связи
        с некачественно проделанными работами по монтажу/установке.
    </p>";
} else {
    $conclusion = ' Cообщаем вам, что на основе предоставленной информации мы не можем рассматривать данную 
    рекламацию в рамках гарантийных обязательств нашей компании
    , (приведены в Гарантийном талоне ООО ЭйСиВи Рус) по следующим причинам:';

    if ($info_messages) {
        foreach ($info_messages as $info_message) {
            $conclusion .= "<p>{$info_message}</p>";
        }
    }

    $conclusion .= "<br><p>
        Однако, наша компания старается разрешить сложившуюся ситуацию самым наилучшим образом, по этой причине мы приняли 
        решение предоставить вам аналогичный новый <strong>---------</strong>.
    </p>
    <p>При монтаже оборудования необходимо обеспечивать требования производителя 
    в области электрических подключений, а также в отношении монтажа предохранительных устройств.</p>";
}
?>

<div id="letter" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Генерация письма</h4>
            </div>
            <?php if ($this->item['step'] != 5 && $this->item['step'] != 1 && $this->item['step'] != 2) : ?>
            <div class="modal-body">
                <?php $user = Users::getUser($this->item['user_id']) ?>
                <div class="top_info">
                    <div class="name"><?php echo $user['name']; ?></div>
                    <div class="city"><?php echo $user['city']; ?></div>
                </div>
                <div class="top_info2">
                    <date><?php echo date('Y.m.d'); ?></date>
                    <div class="city">Москва</div>
                    <div class="num"><?php echo $blank; ?></div>
                </div>
                <h4 class="title_l">Информационное письмо</h4>
                <div class="message">
                    <?php echo $msg; ?>
                    <div class="text_l">
                        <textarea name="text_l" id="" cols="30" rows="10"></textarea>
                        <div class="textarea_hidden"></div>
                    </div>
                    <?php echo $conclusion; ?>
                    <div class="signature">
                        <div class="print"><img src="/themes/admin/img/print.png" alt=""></div>
                        <div class="fio">
                            Технический директор	Бабенко Т.В. <br>
                            ООО «ЭйСиВи Рус» <br>
                            <div class="sg"><img src="/themes/admin/img/sg.png" alt=""></div>
                        </div>
                        <br><br><br>
                        <br><br><br>
                        <br><br><br>
                        <br><br><br>
                        <div class="fio">
                            Исп: Федотов А.Е. <br>
                        </div>
                    </div>
                    <div class="rec"><?php echo @$rec; ?></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" id="resave_pdf">Перезаписать PDF</button>
                <button type="button" class="btn btn-success" id="send_pdf">Выслать PDF на почту СЦ</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
            <?php else: ?>
                <div class="modal-body">
                    С текущим статусом рекламации генерация письма невозможна! <br>
                    Статусы для генерации писем: <br>
                    <ul>
                        <li>Гарантийный случай</li>
                        <li>Не гарантийный случай</li>
                        <li>Коммерческая замена</li>
                        <li>Не гарантийный случай с компенсацией выезда</li>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<script>
    $('.top_info, .top_info2, .message').click(function(e) {
        $(this).attr('contenteditable', true);
    });
    $('#send_pdf').on('click', function () {
        $('.textarea_hidden').html($('.text_l textarea').val());
        $.ajax({
             type: 'POST',
             dataType: 'html',
             url: '/admin_acv/demand/PDFOutput',
             data: {
                 html: $("#letter .modal-body").html(),
                 email: '<?php echo $user['email']; ?>',
                 id: '<?php echo $this->item['id']; ?>'
             },
             success: function(data){
                window.location.reload();
             }
         });
         $('#letter').modal('hide');
        return false;
    });

    $('#resave_pdf').on('click', function () {
        $('.textarea_hidden').html($('.text_l textarea').val());
        $.ajax({
             type: 'POST',
             dataType: 'html',
             url: '/admin_acv/demand/PDFResave',
             data: {
                 html: $("#letter .modal-body").html(),
                 email: '<?php echo $user['email']; ?>',
                 id: '<?php echo $this->item['id']; ?>'
             },
             success: function(data){
                window.location.reload();
             }
         });
         $('#letter').modal('hide');
        return false;
    });
</script>
