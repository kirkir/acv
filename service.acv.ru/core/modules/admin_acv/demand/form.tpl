<style>
    .border-control-group:hover .help-block {
        color: #428bca;
    }
    .help-block {
        color: inherit;
    }
</style>

<div id="nav" style="margin-bottom: 44px";>
	<?php echo $this->pageHeader(); ?>
	<div class="navbar-right navbar-btn" style="clear: both; float: none !important;">
		<?php echo
//            $this->buttonForms(),
            $this->buttonApply(),
            $this->buttonSave(),
//            $this->buttonCopy(),
            $this->buttonClose(),
            (Auth::$user['level'] != 4) ?
                $this->buttonRemove() .
                $this->buttonAdd()
            : '';

        if (!empty($this->form['id'])) {
            echo '<input type="hidden" name="id" value="' . $this->form['id'] . '">';
        }

        echo '<a class="btn btn-primary print_btn" href="#">Распечатать документ</a>';

        echo (file_exists(UPLOADS . DIRECTORY_SEPARATOR . '/letter-'.@$this->item['id'].'.pdf') && !in_array(@$this->item['step'], array(1,2,5))) ? ' <a target="_blank" class="btn btn-danger" href="'.get_url('uploads/letter-' . $this->item['id'].'.pdf?v=' . random_string(8, 'all')).'">Информационное письмо</a>' : '';
        if(Auth::perm(4)) {
            echo ' <a class="btn btn-warning" data-target="#letter" data-toggle="modal" href="#">Сгенерировать письмо</a>';
            echo ' <a class="btn btn-info" href="'.get_url_admin('demand/service/' . @$this->item['user_id']).'">Отчеты сервисного центра</a>';
            echo ' <a class="btn btn-success download_archive" data-id="'. $this->item['id'] .'" href="#">Скачать архив</a>';
        }
        ?>
	</div>
</div>

<?php
// Сообщения.
echo Alerts::get(); 

// Инфо.
if (!empty($this->item)) {
	echo $this->blockquote(
		array(
//			'URL' => Html::a(Tree::url($this->table, $this->item['id'], $this->module))
		)
	);
}
?>
<strong class="number_act">Акт № - <input disabled="disabled" class="form-control form-control-text" id="fid-name" placeholder="Заполняется автоматически" name="number" value="<?php echo @$this->item['id']; ?>" type="text" tabindex="5"> о проведении ремонта с использованием запасных частей</strong>
<br><br>

<?php if(@$this->item['id']) : $user = Users::getUser($this->item['user_id']); ?>
    <p>СЦ: <?php echo Users::getAuthorName($this->item['user_id']); ?> (<?php echo $user['login']?>) произведен ремонт оборудования</p><br><br>
<?php endif; ?>

<?php
$forms = new Forms($this);

echo $forms->select(
    array(
        'label'            => 'Модель оборудования',
        'name'             => 'furniture',
        'options_first'    => '',
        'options'          => DB::getAll("SELECT * FROM `#__furniture` ORDER BY `name`"),
        'required' => true,
        'class' => array('model_select')
    )
);

echo $forms->group(
    array(
        'cols'  => 2,
        'items' => array(
            $forms->text(
                array(
                    'label' => 'Наименование продавца',
                    'name'  => 'name',
                    'required' => true
                )
            ),
            $forms->text(
                array(
                    'label' => 'Серийный номер изделия',
                    'name'  => 'serial',
                    'required' => true
                )
            ),
            $forms->datetime(
                array(
                    'label' => 'Дата пуска оборудования',
                    'name' => 'date_pusk'
                )
            ),
            $forms->text(
                array(
                    'label' => 'Место установки, адрес',
                    'name'  => 'adress',
                    'required' => true,
                    'class'   => 'input_adress'
                )
            ),
            $forms->text(
                array(
                    'label' => 'ФИО клиента',
                    'name'  => 'fio',
                    'required' => true
                )
            )
        )
    )
);
?>
<input type="hidden" name="data_adress_hidden">
<?php
echo $forms->textarea(
    array(
        'label' => 'Описание дефекта',
        'name'  => 'text',
        'required' => true
    )
);

echo $forms->group(
    array(
        'label' => 'Дополнительная информация об установке:',
        'items' => array(
            $forms->listCheckbox(
                array(
                    'name'  => 'info[]',
                    'chunk' => true,
                    'options' => Config::$questions
                )
            )
        )
    )
);

echo '<div class="block_commit_admin">' . $forms->group(
    array(
        'items' => array(
            $forms->textarea(
                array(
                    'label' => 'Комментарий администратора',
                    'name'  => 'comment',
                    'disabled' => Auth::perm(4) ? false : true
                )
            )
        )
    )
) . '</div>';
?>

<div class="legend-group">
    <div class="search_part">
        <select name="parts" id="part" class="form-control form-control-text"></select>
        <input type="hidden" name="p_ids" value="<?php echo @$this->item['parts']; ?>">
    </div>
    <br>
    <div class="legend-body">
        <table class="selection table table-bordered table-hover table-sort">
            <thead>
                <tr>
                    <th class="th-name">Код</th>
                    <th class="th-name">Название</th>
                    <th class="th-name"></th>
                </tr>
            </thead>
            <tbody class="zch_list">
            <?php if(@$this->item['parts']) : ?>
                <?php if($this->isset_parts($this->item['parts'])) : ?>
                    <?php foreach($this->isset_parts($this->item['parts']) as $part) : ?>
                        <tr data-part_id="<?php echo $part['id']; ?>">
                            <td><?php echo $part['sku']; ?></td>
                            <td><?php echo $part['name']; ?></td>
                            <td><a onclick="removeWork(this, 'part_id', 'p_ids'); return false;" href="#">Удалить</a></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>

<?php
echo $forms->text(
    array(
        'label' => 'Номер платежного поручения',
        'name'  => 'num_pay',
        'disabled' => (Auth::perm(4) ? false : true)
    )
);
echo $forms->files(
    array(
        'label' => '',
        'using' => 'n_p',
    )
) . '</br></<br>';
?>

<div class="demand_upload form-group">
    <?php if(@$this->error['files']) : ?>
        <p class="help-block error-block" style="color: #a94442;"><?php echo @$this->error['files']; ?></p>
    <?php endif; ?>
<?php
echo $forms->group(
	array(
		'label' => 'Загрузка файлов',
        'cols' => 2,
		'items' => array(
			$forms->files(
				array(
                    'label' => 'Акт дефектации <br> <span style="font-weight: normal">Необходимо прикрепить файл</span> <span class="file_required">*</span>',
					'using' => 'act',
                    'control-label' => false,
                    'help' => '<span data-target="#description_files_upload" data-toggle="modal" data-value="act" class="glyphicon glyphicon-exclamation-sign" style="font-size: 24px; cursor: pointer"></span>'
				)
			),
            $forms->files(
                array(
                    'label' => 'Шильдик <br> <span style="font-weight: normal">Необходимо прикрепить файл</span> <span class="file_required">*</span>',
                    'using' => 'sh',
                    'control-label' => false,
                    'help' => '<span data-target="#description_files_upload" data-toggle="modal" data-value="shild" class="glyphicon glyphicon-exclamation-sign" style="font-size: 24px; cursor: pointer"></span>'
                )
            ),
            $forms->files(
                array(
                    'label' => 'Фото дефекта <br> <span style="font-weight: normal">Необходимо прикрепить файл</span> <span class="file_required">*</span>',
                    'using' => 'f_d',
                    'control-label' => false,
                    'help' => '<span data-target="#description_files_upload" data-toggle="modal" data-value="defect" class="glyphicon glyphicon-exclamation-sign" style="font-size: 24px; cursor: pointer"></span>'
                )
            ),
            $forms->files(
                array(
                    'label' => 'Фото устройства <br> <span style="font-weight: normal">Необходимо прикрепить файл</span> <span class="file_required">*</span>',
                    'using' => 'f_u',
                    'control-label' => false,
                    'help' => '<span data-target="#description_files_upload" data-toggle="modal" data-value="photo-item " class="glyphicon glyphicon-exclamation-sign" style="font-size: 24px; cursor: pointer"></span>'
                )
            ),
            $forms->files(
                array(
                    'label' => 'Фото обвязки <br> <span style="font-weight: normal">Необходимо прикрепить файл</span> <span class="file_required">*</span>',
                    'using' => 'f_o',
                    'control-label' => false,
                    'help' => '<span data-target="#description_files_upload" data-toggle="modal" data-value="obvyaz" class="glyphicon glyphicon-exclamation-sign" style="font-size: 24px; cursor: pointer"></span>'
                )
            ),
            $forms->files(
                array(
                    'label' => 'Документы о покупке <br> <span style="font-weight: normal">Необходимо прикрепить файл</span> <span class="file_required">*</span>',
                    'using' => 'doc_buy',
                    'control-label' => false,
                    'help' => '<span data-target="#description_files_upload" data-toggle="modal" data-value="doc_buy" class="glyphicon glyphicon-exclamation-sign" style="font-size: 24px; cursor: pointer"></span>'
                )
		    ),
            $forms->files(
                array(
                    'label' => 'Фото претензии клиента <br> <span style="font-weight: normal">Необходимо прикрепить файл</span> <span class="file_required">*</span>',
                    'using' => 'foto_client',
                    'control-label' => false,
                    'help' => '<span data-target="#description_files_upload" data-toggle="modal" data-value="foto_client" class="glyphicon glyphicon-exclamation-sign" style="font-size: 24px; cursor: pointer"></span>'
                )
            )
		)
	)
); ?>
</div>
<?php
if(@$this->item['user_id']) {
    $user_order = DB::getRow("SELECT * FROM `#__users` WHERE `id` = ?", @$this->item['user_id']);
    if(@$user_order) {
        $category_h_price = DB::getValue("SELECT `price` FROM `#__category` WHERE `id` = ?", @$user_order['category']);
    }
    $this->form['price_one_h'] = (@$this->form['price_one_h'] == 0.00 && @$category_h_price) ? @$category_h_price : @$this->form['price_one_h'];
}

echo $forms->Checkbox(
    array(
        'label' => 'Гарантийный талон оборудования проданного до 01.01.2017',
        'name'  => 'warranty_file',
        'class' => 'toggle_warranty_file'
    )
);
?>
<div class="demand_upload wrnt" style="width: 50%;<?php echo (!$this->form['warranty_file']) ? ' display : none' : '' ?>">
    <?php
        echo $forms->files(
            array(
                'label' => 'Гарантийный талон <br> <span style="font-weight: normal">Необходимо прикрепить файл</span> <span class="file_required">*</span>',
                'using' => 'warranty',
                'control-label' => false,
                'help' => '<span data-target="#description_files_upload" data-toggle="modal" data-value="warranty" class="glyphicon glyphicon-exclamation-sign" style="font-size: 24px; cursor: pointer"></span>'
            )
        );
    ?>
</div>

<?php
echo $forms->group(
    array(
        'items' => array(
            $forms->text(
                array(
                    'label' => 'Введите номер гарантийного талона',
                    'name'  => 'warranty_num',
                    'required' => true
                )
            )
        )
    )
);

$steps = Config::$step;
asort($steps);
?>

<div class="alert alert-warning">
<?php
echo (@$this->item['stage'] && Auth::perm(4)) ?
    $forms->select(
        array(
            'label'            => 'Текущий статус',
            'required' => true,
            'name'             => 'step',
            'options_first'    => '',
            'options'          => $steps,
        )
    )
    :
    $forms->text(
        array(
            'label'            => 'Текущий статус',
            'name'             => 'step',
            'value'            => @Config::$step[$this->item['step']],
            'disabled' => true
        )
    );
?>
</div>
<?php

echo $forms->group(
    array(
        'items' => array(
//            (Auth::perm(4)) ?
//                $forms->Checkbox(
//                    array(
//                        'label' => 'Гарантийный случай',
//                        'name'  => 'warranty',
//                        'class' => 'toggle_warranty'
//                    )
//                ) : '',
            (Auth::perm(4)) ?
            $forms->text(
                array(
                    'help'  => 'Стоимость одного нормо-часа составляет',
                    'name'  => 'price_one_h',
                    'class'  => 'price_one_h',
                    'value' => $this->form['price_one_h'],
//                    'group_style'  => (!$this->form['warranty']) ? 'display : none' : ''
                )
            ) : ''
        )
    )
);


?>

<!--    --><?php //if(!@$this->item['works_ids']) : ?>
    <div class="work_list_select form-group <?php echo (@$this->error['work_list']) ? ' has-error' : ' required' ?>">
        <label for="fid-furniture" class="required">Виды работ:</label>
        <select name="work_list" id="work_list" class="form-control form-control-text">
            <option value=""></option>
            <?php foreach(DB::getAll("SELECT * FROM `#__works` ORDER BY `name`") as $work) : ?>
                <option value="<?php echo $work['id']; ?>"><?php echo $work['name']; ?></option>
            <?php endforeach; ?>
        </select>
        <p class="help-block error-block"><?php echo @$this->error['work_list'] ?></p>
    </div>
<!--    --><?php //endif; ?>
    <input type="hidden" name="works_ids" value="<?php echo @$this->item['works_ids']; ?>">
    <br><br>
    <table class="selection table table-bordered table-hover">
        <thead>
        <tr>
            <th class="th-name">Виды выполненных работ</th>
            <th class="th-name">Нормо-часов</th>
            <th class="th-name">Стоимость</th>
            <th class="th-name"></th>
        </tr>
        </thead>
        <tbody class="work_list">
        <?php if(@$this->item['works_ids']) : ?>
            <?php if($this->isset_works($this->item['works_ids'])) : $total = 0;?>
                <?php foreach($this->isset_works($this->item['works_ids']) as $work) : $dinamic = DB::getRow("SELECT * FROM `#__demand_dinamic` WHERE `demand_id` = ? AND `work_id` = ?",
                    array($this->item['id'], $work['id'])); ?>
                    <tr data-work_id="<?php echo $work['id']; ?>">
                        <td><?php echo $work['name']; ?></td>
                        <?php
                        if($dinamic) {
                            $work['hour'] = $dinamic['hour'];
                            $work['price'] = $dinamic['price'];
                        }
                        ?>
                        <td class="d_h" <?php if (is_admin()) echo 'onclick="tdRedactor(this, 1); return false;"';?>><?php echo ($work['hour'] != 0.0) ? $work['hour'] : '-'; ?></td>
                        <td class="d_p" <?php if (is_admin()) echo 'onclick="tdRedactor(this, 1); return false;"';?>><?php echo ($work['price'] != 0.00) ? $work['price'] : $this->form['price_one_h']; ?></td>
                        <td><a onclick="removeWork(this, 'work_id', 'works_ids'); return false;" href="#">Удалить</a></td>
                    </tr>
                    <?php
                        if($work['hour'] == 0.00) {
                            $total += $work['price'];
                        } else {
                            if($dinamic) {
                                $total += $work['hour'] * $work['price'];
                            } else {
                                $total += $work['hour'] * $this->item['price_one_h'];
                            }
                        }
                    ?>
                <?php endforeach; ?>
                <input type="hidden" name="price_works" value="<?php echo $total; ?>">
                <input type="hidden" name="delivery_total" value="<?php echo $this->delivery_total(); ?>">
            <?php endif; ?>
        <?php endif; ?>
        </tbody>
    </table>
<?php
echo $forms->group(
    array(
        'items' => array(
            $forms->text(
                array(
                    'label' => 'Расстояние от СЦ «Исполнителя» до места установки оборудования в одном направлении, км',
                    'name'  => 'km',
                    'style' => 'width: 70%',
                    'required' => true,
                    'placeholder' => 'Оставить поле пустое когда клиент сам привозит оборудование в сервис'
                )
            )
        )
    )
);
?>

<?php if(@$this->item['step'] > 2 && @$this->item['step'] != 3) : ?>
<div style="" class="form-group">
    <p class="help-block">Размер компенсации выезда сервисного центра:</p>
</div>

<?php
//echo $forms->group(
//    array(
//        'cols'  => 2,
//        'items' => array(
//            (Auth::perm(4)) ?
//                $forms->Checkbox(
//                    array(
//                        'label' => 'Компенсация выполненных работ',
//                        'name'  => 'compensation_bool',
////                        'class' => 'toggle_warranty'
//                    )
//                ) : '',
//        )
//    )
//);
?>

<?php // if(@$this->item['warranty']) : ?>
<div style="text-align: right; margin-right: 70px" class="form-group">
    <p class="help-block"><strong>Итого: </strong><span class="total"> <?php echo @$total + $this->delivery_total() . ' р.'; ?></span></p>
    <input type="hidden" name="total" value="<?php echo @$total + $this->delivery_total(); ?>">
</div>
<?php endif; ?>
<?php // endif; ?>
<?php
echo $forms->group(
    array(
        'items' => array(
            $forms->datetime(
                array(
                    'label' => 'Дата Ремонта',
                    'name' => 'date_work',
                    'require' => true
                )
            )
        )
    )
);

if (Auth::perm(4)) {
    // Параметры публикации.
    echo '<div class="public_block">' . $forms->group(
        array(
            'label' => 'Параметры публикации',
            'items' => array(
                (Auth::perm(4)) ?
                $forms->select(
                    array(
                        'label'            => 'Текущая стадия',
                        'name'             => 'stage',
                        'options_first'    => '',
                        'options'          => Config::$stage,
                    )
                ) .
                $forms->Checkbox(
                    array(
                        'label' => 'В архиве',
                        'name'  => 'archive'
                    )
                ) .
                $forms->Checkbox(
                    array(
                        'label' => 'В черновиках',
                        'name'  => 'draft'
                    )
                )
                    : '',
                $forms->Checkbox(
                    array(
                        'label' => 'Публиковать на сайте',
                        'name'  => 'approve'
                    )
                ),
                ($this->item['cart']) ?
                $forms->Checkbox(
                    array(
                        'label' => 'Отчет в корзине, снимите галочку что бы убрать из корзины',
                        'name'  => 'cart'
                    )
                ) : ''
            )
        )
    ) . '</div>';
}

if(@$this->item['archive']) {
    echo "
        <script>
            $('select, input, textarea').attr('disabled', true);
            $('button[name=' + 'send' + ']').fadeOut();
            var arch = $('input[name=' + 'archive' + ']').removeAttr('disabled');
            arch.change(function() {
                if($(this).prop('checked')) {
                    $('select, input, textarea').attr('disabled', true);
                    $('button[name=' + 'send' + ']').fadeOut();
                    $(this).removeAttr('disabled');
                } else {
                    $('select, input, textarea').removeAttr('disabled');
                    $('button[name=' + 'send' + ']').fadeIn();
                    $('input[name=' + 'number').attr('disabled', true);
                }
            });
        </script>
    ";
}
?>

<div id="description_files_upload" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Описание требования к загружаемым файлам</h4>
            </div>
            <div class="modal-body">
                <div class="act type">
                    <?php echo Blocks::get('act'); ?>
                </div>
                <div class="shild type">
                    <?php echo Blocks::get('shild'); ?>
                </div>
                <div class="photo-item type">
                    <?php echo Blocks::get('photo-item'); ?>
                </div>
                <div class="warranty type">
                    <?php echo Blocks::get('warranty'); ?>
                </div>
                <div class="defect type">
                    <?php echo Blocks::get('defect'); ?>
                </div>
                <div class="obvyaz type">
                    <?php echo Blocks::get('obvyaz'); ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<?php if($this->item && Auth::perm(4)) : ?>
    <?php  echo $this->letter(); ?>
<?php endif; ?>

<script type="text/javascript">
    $(document).ready(function() {
        $(".model_select").select2();
    });
</script>