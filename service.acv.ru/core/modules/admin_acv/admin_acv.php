<?php
/**
 * Администрирование.
 */
class module_admin_acv extends Backend
{
	public $title  = 'Администрирование';	
	
	public function __construct()
	{
		// Уровень доступа.
		$this->init(2);
	}

	public function action_index()
    {
        if (!Auth::perm(4))  {
    		return Router::get('admin_acv/demand');
        } else {
    		return Router::get('admin_acv/users');
        }
    }
}