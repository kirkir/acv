<?php
/**
 * Администрирование / Авторизация.
 */
class module_login extends Backend
{
	public $module = 'login';
	public $table  = '#__users';
	public $title  = 'Авторизация';		

	/**
     * Форма авторизации.
     */
	public function action_index()
    {
		$this->captcha = Router::getInstance('captcha');
		
		$this->form['login']    = Input::getStr($_POST['login'], '');
		$this->form['password'] = Input::getStr($_POST['password'], '');
		$this->form['remember'] = Input::getBool($_POST['remember'], 0);				
		$this->form['referer']  = Input::getStr($_SERVER['REQUEST_URI'], '');


		// Обработка формы.
		if (isset($_POST['send'])) {
			if (Auth::$attempts > 3 && !$this->captcha->check()) {
				$this->error['captcha'] = $this->captcha->error;
			} else {
				if (empty($this->form['login'])) {
//					$this->error['login'] = 'Укажите логин или e-mail';
					$this->error['login'] = 'Укажите Номер договора';
				}
		
				if (empty($this->form['password'])) {
					$this->error['password'] = 'Укажите пароль';
				}			
			}

			if (empty($this->error)) {
				$auth = Auth::login($this->form['login'], $this->form['password'], 3, $this->form['remember']);
				if ($auth['response']) {
					return redirect($this->form['referer']);
				} else {
					$this->error['message'] = $auth['message'];
				}
			}
			
			Alerts::setError($this->error);
        } else {
			$this->form['remember'] = 1;
		}


		// Вывод в шаблон.
		$this->content = $this->render(dirname(__FILE__) . '/form.tpl');	
		return $this->display('blank.tpl');
    }

    /**
     * Завершение сеанса.
     */
    public function action_out()
    {
		return Auth::logout();
    }
}