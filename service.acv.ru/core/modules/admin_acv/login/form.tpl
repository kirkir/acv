    <style type="text/css">
body {
	background-color: #222;
}
</style>

<div class="container" style="margin-top:5%">
	<div class="col-md-4 col-md-offset-4">
		<div class="panel panel-default" style="width: 350px;">
			<div class="panel-heading"><h3 class="panel-title">
                    <strong><?php // echo Settings::get('name'); ?></strong>
                </h3></div>
			<div class="panel-body">
                <img class="logo" src="/themes/admin/img/logo.png" alt="">
                <?php echo Alerts::get(); ?>
				
				<form role="form" method="post">
					<div class="form-group">
						<label>Номер договора</label>
<!--						<label>Логин или e-mail</label>-->
						<input type="text" class="form-control" name="login" value="<?php echo $this->form['login']; ?>">
					</div>
					<div class="form-group">
						<label>Пароль</label>
						<input type="password" class="form-control" name="password" value="<?php echo $this->form['password']; ?>">
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label><input <?php echo checked($this->form['remember']); ?> type="checkbox" name="remember"> Запомнить меня</label>
						 </div>
					</div>
					
					<?php if (Auth::$attempts > 3): ?>
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
								<img id="captcha" onclick="updateCaptcha(this);" style="border: 1px solid #ddd; cursor: pointer;" src="<?php echo get_url('captcha'); ?>" alt="" width="152" height="72">
							</div>
							<div class="col-md-6" style="padding-top:20px">
								<input type="captcha" class="form-control" name="captcha" placeholder="Код на картинке">
							</div>
						</div>
					</div>	
					<?php endif; ?>

					<div class="text-center" style="border-top: 1px solid #ddd; padding-top: 15px;">
						<button type="submit" class="btn btn-success" name="send">Войти</button>
						&ensp;
<!--						<a class="btn btn-default" href="--><?php //echo get_url(); ?><!--">Перейти на сайт</a>-->
					</div>
				</form>
			</div>
		</div>
	</div>
</div>