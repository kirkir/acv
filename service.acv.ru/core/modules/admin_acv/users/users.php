<?php
/** 
 * Администрирование / Пользователи.
 */
class module_users extends Backend
{
	public $module = 'users';
	public $table = '#__users';
	public $title = 'Сервисные центры';

    public $files = array(
        'thumb' => array(
            'type'     => 0,
            'multiple' => false,
            'unique'   => true
        )
    );

	public function __construct()
	{
        if(@Auth::$user['level'] < 4) {
            if(@Auth::$user['id'] != array_pop(Router::$route) || Router::$action == 'add') {
                Auth::form();
            }
        } else {
    		// Уровень доступа.
	    	$this->init(4);
        }
	}

	/**
     * Обработка форм.
     */
	public function handler()
    {
		if (!isset($_POST['send'])) {
            return false;
		} 

		$this->send_password = Input::getBool($_POST['send_password']);
		
		$this->form['id']      = Input::getInt($_POST['id']);
		$this->form['name']    = Input::getStr($_POST['name']);
		$this->form['city']    = Input::getStr($_POST['city']);
        $this->form['region_id']    = Input::getInt($_POST['region_id'], 0);
        $this->form['city_id']    = Input::getInt($_POST['city_id'], 0);
		$this->form['category']   = (Input::getInt($_POST['category'])) ? Input::getInt($_POST['category']) : Auth::$user['category'];
		$this->form['login']   = (Input::getStr($_POST['login'])) ? Input::getStr($_POST['login']) : Auth::$user['login'];
		$this->form['email']   = Input::getStr($_POST['email']);
		$this->form['email2']    = Input::getStr($_POST['email2']);
		$this->form['phone']    = Input::getStr($_POST['phone']);
		$this->form['adress']    = Input::getStr($_POST['adress']);
		$this->form['innkpp']    = Input::getStr($_POST['innkpp']);
		$this->form['person']    = Input::getStr($_POST['person']);
		$this->form['date_end']    = (Input::getTime($_POST['date_end'])) ? Input::getTime($_POST['date_end']) : Auth::$user['date_end'];
		$this->form['level']   =  (Input::getInt($_POST['level'])) ? Input::getInt($_POST['level']) : Auth::$user['level'];
		if(@$_POST['approve']) {
            $this->form['approve'] = Input::getBool($_POST['approve']);
        }


		// Проверка заполнения полей:
		if (empty($this->form['name'])) {
			$this->error['name'] = 'Укажите Название';
		}

        if (empty($this->form['category'])) {
			$this->error['category'] = 'Укажите Название Категории СЦ';
		}

		if (empty($this->form['login'])) {
			$this->error['login'] = 'Укажите номер договора';
		} elseif (DB::getRow("SELECT `id` FROM `{$this->table}` WHERE `login` = ? AND `id` <> ?", array($this->form['login'], $this->form['id']))) {
			$this->error['login'] = 'Такой номер договора уже существует';
		}

        if (!Auth::perm(4) && $this->form['login'] !== Auth::$user['login']) {
			$this->error['login'] = 'Даже и не пробуйте менять номер договора!';
        }
        if (!Auth::perm(4) && $this->form['date_end'] !== Auth::$user['date_end']) {
			$this->error['date_end'] = 'Даже и не пробуйте менять Дату истечения срока договора!';
        }

        if (!empty($this->form['email']) && DB::getRow("SELECT `id` FROM `{$this->table}` WHERE `email` = ? AND `id` <> ?", array($this->form['email'], $this->form['id']))) {
			$this->error['email'] = 'Такой e-mail уже существует';
		}

		$password = Input::getStr($_POST['password'], '');
		if (Router::$action == 'add') {
			if (empty($password)) {
				$this->error['password'] = 'Укажите пароль';
			} elseif (!empty($password) && mb_strlen($password) < 6) {
				$this->error['password'] = 'Пароль должен быть не менее 6-ти символов';
			} else {
				$this->form['password'] = Auth::getHash($password);
			}
		} elseif (!empty($password)) {
			$this->form['password'] = Auth::getHash($password);
		}

		if (empty($this->error)) {
			$id = (empty($this->form['id'])) ? $this->dbAdd() : $this->dbSave();
			if (!empty($id)) {
                if(Router::$action == 'add') {
                    $mail = new Mail();
                    $mail->from(Settings::get('email'), Settings::get('email_name'));
                    $mail->to($this->form['email'], $this->form['email2']);
                    $mail->subject = 'Регистрация ACV';
                    $mail->body = '
                        <p>
                            <strong>Вы зарегистрированы на сайте:</strong> ' . Settings::get('name') . '<br />
                            <strong>Ваш логин(номер договора):</strong> ' . $this->form['login'] . '<br />
                            <strong>Ваш пароль:</strong> ' . Input::getStr($_POST['password']) . '<br />
                            <p>Это письмо создано автоматически, не отвечайте на него!</p>
                        </p>';
                    $mail->send();
                }
				return $this->handlerСomplete($id, 'edit');
			} else {
				Alerts::setError('Произошла ошибка.');
			}
		}

		return true;
    }

    public function action_executedRelation() {
        if(DB::set("UPDATE `#__relations` SET `executed` = 1 WHERE `id` = ?", array($_POST['id']))) {
            return true;
        }
    }
    public function action_getRelations()
    {
        foreach (Relations::getRelation(Input::cleanInt($_POST['id'])) as $item) { ?>
            <div class="relation_item">
                <div class="relation_item_left">
                    <strong><?php echo date_human($item['date_add']); ?></strong> было выполнено <?php echo Relations::$RELATION_NAME[$item['relation']]; ?>
                </div>
            <div class="relation_item_right">
                <a class="ico-remove executed_relation" title="Прочитано" data-relation_id="<?php echo $item['id']; ?>" onclick="return executedRelation(<?php echo $item['id']; ?>);" href="#"></a>
            </div>
            </div>
        <?php }
    }
}