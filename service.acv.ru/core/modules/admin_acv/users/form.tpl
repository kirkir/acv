<div id="nav" style="margin-bottom: 44px;">
    <?php if(Auth::perm(4)): ?>
        <?php echo $this->pageHeader(); ?>
    <?php else: ?>
        <a class="navbar-brand" href="#">
            Ваш профиль <small class="muted">редактирование</small>
        </a>
    <?php endif; ?>
	<div class="navbar-right navbar-btn" style="clear: both; float: none !important;">
        <?php if(Auth::perm(4)): ?>
            <?php if(Users::get_level($this->form['id']) != 4) : ?>
                <a class="btn btn-info" href="<?php echo get_url_admin('demand/service/' . $this->form['id']); ?>">Отчеты сервисного центра</a>
            <?php endif; ?>
    		<?php echo $this->buttonForms(); ?>
        <?php else: ?>
            <button class="btn btn-default" type="submit" name="send" value="apply">Применить</button>
            <input type="hidden" name="id" value="<?php echo $this->form['id']; ?>">
        <?php endif; ?>
	</div>
</div>

<?php
// Сообщения.
echo Alerts::get(); 

// Инфо.
if (!empty($this->item)) {
	echo $this->blockquote(
		array(
			'Последний визит' => date_human($this->item['date_visit'])
		)
	);
}

$forms = new Forms($this);


// Основное.
echo $forms->group(
	array(
        'cols' => 2,
		'label' => 'Основное',
		'items' => array(
			$forms->text(
				array(
					'label'    => 'Название сервисного центра',
					'name'     => 'name', 
					'required' => true
				)
			),
			$forms->text(
				array(
					'label'   => 'Номер договора',
					'name'    => 'login',
					'required' => true,
                    'disabled' => !Auth::perm(4)
				)
			),
            $forms->selectTree(
				array(
					'label'         => 'Категория СЦ',
					'name'          => 'category',
					'required'      => true,
                    'options_first' => '',
                    'options'       => DB::getAll("SELECT * FROM `#__category` WHERE `approve` = 1 ORDER BY `sort`"),
                    'disabled'      => !Auth::perm(4)
				)
			),
            $forms->text(
				array(
					'label'    => 'ИНН/КПП',
					'name'     => 'innkpp'
				)
			),
            $forms->text(
				array(
					'label'    => 'Контактное лицо',
					'name'     => 'person'
                )
			),
            $forms->text(
				array(
					'label'    => 'Электронная почта',
					'name'     => 'email',
					'required' => true
				)
			),
            $forms->text(
				array(
					'label'    => 'Доп. электронная почта',
					'name'     => 'email2'
				)
			),
			$forms->text(
				array(
					'label' => 'Телефон для связи',
					'name'  => 'phone'
                )
			),
            $forms->datetime(
				array(
					'label' => 'Дата истечения срока договора',
					'name'  => 'date_end',
                    'disabled' => !Auth::perm(4)
                )
			),
			$forms->password(
				array(
					'label'    => 'Пароль', 
					'name'     => 'password',
                    'disabled' => !Auth::perm(4)
                )
			),
            $forms->select(
                array(
                    'label'            => 'Регион',
                    'name'             => 'region_id',
//                    'options_first'    => '',
                    'options'          => DB::getAll("SELECT * FROM `region` ORDER BY `id`"),
                    'required' => true,
                    'class' => array('model_select')
                )
            ),
            $forms->select(
                array(
                    'label'            => 'Город',
                    'name'             => 'city_id',
                    'options_first'    => '',
                    'options'          => DB::getAll("SELECT * FROM `city` ORDER BY `name`"),
                    'class' => array('model_select')
                )
            ),
            $forms->text(
                array(
                    'label'    => 'Адрес сервисного центра',
                    'name'     => 'adress'
                )
            ),
		)
	)
);
/// Изображение.
//echo $forms->group(
//    array(
//        'label' => 'Фото',
//        'items' => array(
//            $forms->files(
//                array(
//                    'using' => 'thumb'
//                )
//            )
//        )
//    )
//);

// Параметры публикации.
if(Auth::perm(4)) {
    echo $forms->group(
	array(
		'label' => 'Параметры публикации',
		'items' => array(
			$forms->select(
				array(
					'label'         => 'Уровень доступа', 
					'name'          => 'level', 
					'options'       => Config::$level
				)
			),
			$forms->Checkbox(
				array(
					'label' => 'Активная учетная запись', 
					'name'  => 'approve'
				)
			)
		)
	)
);
}
