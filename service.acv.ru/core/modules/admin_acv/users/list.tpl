<?php $this->init(4); ?>
<div id="nav">
	<?php echo $this->pageHeader(); ?>
	<div class="navbar-right navbar-btn">
		<?php echo $this->buttonLists(); ?>
	</div>
</div>

<?php

// Сообщения.
echo Alerts::get();

// Таблица.
$tables = new Tables($this->items);
$tables->imageModule = $this->module;

echo $tables->out(
	array(
		'thead' => array(
			$tables->thCheck(),
			$tables->thId(),
			$tables->thName('Название клиента'),
			$tables->thText('Номер договора'),
			$tables->thText('E-mail'),
			$tables->thText('Уровень'),
			$tables->thDate(),
			$tables->thText('Уведомление'),
			$tables->thApprove(),
			$tables->thRemove()
		),
		'tbody' => dirname(__FILE__) . '/tree.tpl'
	)
);