<?php
echo $this->tr(
	array(
		$this->tdCheck(),
		$this->tdId(),
		$this->tdName(),
		$this->tdText($this->item['login']),
		$this->tdText($this->item['email']),
		$this->tdText(Config::$level[$this->item['level']], array('style' => 'color:' . Config::$level_color[$this->item['level']])),
		$this->tdDate(),
        (Relations::getRelation($this->item['id'])) ?
		    $this->tdText('<a href="#" data-user_id="'.$this->item['id'].'" class="show_relations btn btn-sm btn-info"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span></a>', array('style' => 'text-align: center !important'))
            :
            $this->tdText(''),
		$this->tdApprove(),
		$this->tdRemove()
	)
); ?>

<div id="show_relations" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="">
                <!-- Заголовок модального окна -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Уведомления</h4>
                </div>
                <!-- Основное содержимое модального окна -->

                <div class="modal-body">

                </div>
                <!-- Футер модального окна -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </form>
        </div>
    </div>
</div>


