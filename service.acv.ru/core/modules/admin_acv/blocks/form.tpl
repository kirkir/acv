<div id="nav">
	<?php echo $this->pageHeader(); ?>
	<div class="navbar-right navbar-btn">
		<?php echo $this->buttonForms(); ?>
	</div>
</div>

<?php
// Сообщения.
echo Alerts::get(); 

// Инфо.
if (!empty($this->item)) {
	echo $this->blockquote(
		array(
			'Текст дла замены' => '<code>{bloks::' . $this->item['sef'] . '}</code>'
		)
	);
}

$forms = new Forms($this);

// Основное.
echo $forms->group(
	array(
		'label' => 'Основное',
		'items' => array(
			$forms->selectTree(
				array(
					'label'            => 'Родительский блок', 
					'name'             => 'parent',
					'options_first'    => '',
					'options'          => DB::getAll("SELECT * FROM `{$this->table}` ORDER BY `sort`"),
					'options_disabled' => @$this->item['id']
				)
			),
			$forms->text(
				array(
					'label'    => 'Название', 
					'name'     => 'name', 
					'required' => true
				)
			),
			$forms->text(
				array(
					'label' => 'Алиас', 
					'name'  => 'sef'
				)
			)
		)
	)
);

// Содержание.
echo $forms->group(
	array(
		'label' => 'Содержание блока',
		'items' => array(
			$forms->html(
				array(
					'name'  => 'text'
				)
			)
		)
	)
);

// Параметры публикации.
echo $forms->group(
	array(
		'label' => 'Параметры публикации',
		'items' => array(
			$forms->Checkbox(
				array(
					'label' => 'Публиковать на сайте', 
					'name'  => 'approve'
				)
			)
		)
	)
);