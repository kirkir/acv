<?php
/** 
 * Администрирование / Текстовые блоки.
 */
class module_blocks extends Backend
{
	public $module = 'blocks';		
	public $table  = '#__blocks';
	public $title  = 'Текстовые блоки';			

	public function __construct()
	{
		// Уровень доступа.
		$this->init(3);
	}

	/**
     * Обработка форм.
     */
    public function handler()
    {
		if (!isset($_POST['send'])) {
			return false;
		} 

		$this->form['parent']  = Input::getInt($_POST['parent']);
		$this->form['id']      = Input::getInt($_POST['id']);
		$this->form['name']    = Input::getStr($_POST['name']);	
		$this->form['sef']     = Input::getSef($_POST['sef'], $this->form['name']);	
		$this->form['text']    = Input::getHtml($_POST['text']);
		$this->form['approve'] = Input::getBool($_POST['approve']);			

		if (empty($this->form['name'])) {
			$this->error['name'] = 'Укажите название';
		}

		if (empty($this->form['sef'])) {
			$this->error['sef'] = 'Укажите алиас';
		} elseif (DB::getRow("SELECT `id` FROM `{$this->table}` WHERE `sef` = ? AND `id` <> ?", array($this->form['sef'], $this->form['id']))) {
			$this->error['sef'] = 'Такой алиас уже существует';
		}
	
		if (empty($this->error)) {
			$id = (empty($this->form['id'])) ? $this->dbAdd() : $this->dbSave();
			if (!empty($id)) {
				return $this->handlerСomplete($id, 'edit');
			} else {
				Alerts::setError('Произошла ошибка.');
			}
		}

		return true;
    }
}