<?php
/**
 * Администрирование / Отчеты.
 */
class module_registrylist extends Backend
{
    public $module = 'registrylist';
    public $table  = '#__registrylist';
    public $title  = 'Лист регистрации оборудования';

    public $files = array(
        'ship' => array(
            'type'     => 1,
            'multiple' => false,
            'unique'   => false
        ),
        'warrantydays' => array(
            'type'     => 1,
            'multiple' => false,
            'unique'   => false
        ),
    );

    public function __construct()
    {
        // Уровень доступа.
        $this->init(3);
    }

    public function action_index()
    {
        if (!Auth::perm(4)) {
            return redirect(Router::$url . '/add');
        }
        $this->handlerActions();

        $this->paginator = new Paginator(get_url(Router::$url), 20);
        $this->items = $this->paginator->process("SELECT * FROM `{$this->table}` ORDER BY `date_add` DESC");

        // Вывод в шаблон.
        $this->subtitle = 'список';
        $this->content  = $this->render(Router::$path . '/list.tpl');
        return $this->display('page.tpl');
    }

    public function action_search()
    {
        $this->init(3);

        $this->form['serial'] = Input::getStr($_POST['serial']);
        $this->form['name']   = Input::getStr($_POST['name']);

        if (isset($_POST['send'])) {
            if (empty($this->form['serial'])) {
                $this->error['serial'] = 'Обязательно для заполнения';
            }

            if (empty($this->form['name'])) {
                $this->error['name'] = 'Обязательно для заполнения';
            }

            if (!$this->error) {
                // проданное оборудование
                
                $furniture = DB::getRow("SELECT * FROM `#__import_warranty` WHERE `id` = ?", $this->form['name']);

                $this->data_ship = DB::getRow("
                    SELECT 
                      * 
                    FROM
                      `#__import_ship`
                    WHERE 
                      `serial` = ? AND `sku` = ?
                      
                ", array($this->form['serial'], $furniture['sku'])
                );

                $this->data_pusk = DB::getRow("
                    SELECT 
                      * 
                    FROM
                      `#__registrylist`
                    WHERE 
                      `serial` = ?
                      
                ", array($this->form['serial'])
                );

                $this->service = DB::getAll("
                    SELECT 
                      * 
                    FROM
                      `#__demand`
                    WHERE 
                      `serial` = ? AND `furniture` = ?
                      
                ", array($this->form['serial'], $furniture['id'])
                );
            }
        }
//        if (!Auth::perm(4)) {
//            return redirect(Router::$url . '/add');
//        }

//        $this->handlerActions();
//
//        $this->paginator = new Paginator(get_url(Router::$url), 20);
//        $this->items = $this->paginator->process("SELECT * FROM `{$this->table}` ORDER BY `date_add` DESC");
//
//         Вывод в шаблон.
        $this->title = 'Проверить историю оборудования';
        $this->content  = $this->render(Router::$path . '/search.tpl');
        return $this->display('page.tpl');
    }


    /**
     * Обработка форм.
     */
    public function handler()
    {

        if (isset($this->item['id'])) {
            if (Auth::$user['level'] < 4 && $this->item['user_id'] != Auth::$user['id']) {
                return Router::get('404');
            }
        }

        if (!isset($_POST['send'])) {
            return false;
        }

        $this->form['id']                                  = Input::getInt($_POST['id']);
        $this->form['model']                               = Input::getStr($_POST['model']);
        $this->form['serial']                              = Input::getStr($_POST['serial']);
        $this->form['location']                            = Input::getStr($_POST['location']);
        $this->form['fio']                                 = Input::getStr($_POST['fio']);
        $this->form['email']                               = Input::getStr($_POST['email']);
        $this->form['settings']                            = Input::getBool($_POST['settings']);
        $this->form['pressure_before_burner']              = Input::getStr($_POST['pressure_before_burner']);
        $this->form['pressure_after_gas_valve']            = Input::getStr($_POST['pressure_after_gas_valve']);
        $this->form['pressure_of_the_oil_pump']            = Input::getStr($_POST['pressure_of_the_oil_pump']);
        $this->form['type_characteristics_jets']           = Input::getStr($_POST['type_characteristics_jets']);
        $this->form['co2']                                 = Input::getStr($_POST['co2']);
        $this->form['chimney_height']                      = Input::getStr($_POST['chimney_height']);
        $this->form['chimney_value_power']                 = Input::getStr($_POST['chimney_value_power']);
        $this->form['chimney_devices_stabilize']           = Input::getBool($_POST['chimney_devices_stabilize']);
        $this->form['chimney_type_system']                 = Input::getStr($_POST['chimney_type_system']);
        $this->form['inflow_ventilation']                  = Input::getStr($_POST['inflow_ventilation']);
        $this->form['exhaust_ventilation']                 = Input::getStr($_POST['exhaust_ventilation']);
        $this->form['voltage_regulation_device']           = Input::getStr($_POST['voltage_regulation_device']);
        $this->form['voltage_on_the_phases_of_the_boiler'] = Input::getStr($_POST['voltage_on_the_phases_of_the_boiler']);
        $this->form['is_ground_bus']                       = Input::getBool($_POST['is_ground_bus']);
        $this->form['is_earthed_separately']               = Input::getBool($_POST['is_earthed_separately']);
        $this->form['is_check_connections']                = Input::getBool($_POST['is_check_connections']);
        $this->form['dielectric_paste']                    = Input::getStr($_POST['dielectric_paste']);
        $this->form['type_of_coolant']                     = Input::getStr($_POST['type_of_coolant']);
        $this->form['is_preporation_coolant']              = Input::getBool($_POST['is_preporation_coolant']);
        $this->form['is_has_test_co']                      = Input::getBool($_POST['is_has_test_co']);
        $this->form['is_node_co']                          = Input::getBool($_POST['is_node_co']);
        $this->form['is_water_meter']                      = Input::getBool($_POST['is_water_meter']);
        $this->form['comment']                             = Input::getStr($_POST['comment']);
        $this->form['date_add']                            = Input::getTime($_POST['date_add']);
        $this->form['name_service']                            = Input::getStr($_POST['name_service']);
        $this->form['name_pusk_start']                            = Input::getStr($_POST['name_pusk_start']);

        if (empty($this->form['model'])) {
            $this->error['model'] = 'Обязательно для заполнения';
        }

        if (empty($this->form['serial'])) {
            $this->error['serial'] = 'Обязательно для заполнения';
        }


        if (@$this->item['serial'] != $this->form['serial'] && DB::getRow("SELECT `id` FROM `#__registrylist` WHERE `serial` = ? LIMIT 1", $this->form['serial'])) {
            $this->error['serial'] = 'Такой серийный номер уже существует';
        }

        if ($this->form['serial'] && $this->form['model']) {
            if (!$import_ship = DB::getRow("SELECT * FROM `#__import_ship` WHERE `serial` = ? AND `sku` = ? LIMIT 1", array($this->form['serial'], $this->form['model']))) {
                $this->error['serial'] = 'Нет оборудования с таким артикулом или серийным номером среди проданных';
            }
        }


        if (empty($this->form['location'])) {
            $this->error['location'] = 'Обязательно для заполнения';
        }

        if (empty($this->form['pressure_before_burner'])) {
            $this->error['pressure_before_burner'] = 'Обязательно для заполнения';
        }

        if (empty($this->form['chimney_height'])) {
            $this->error['chimney_height'] = 'Обязательно для заполнения';
        }

        if (empty($this->form['inflow_ventilation'])) {
            $this->error['inflow_ventilation'] = 'Обязательно для заполнения';
        }

        if (empty($this->form['voltage_regulation_device'])) {
            $this->error['voltage_regulation_device'] = 'Обязательно для заполнения';
        }

        if (empty($this->form['type_of_coolant'])) {
            $this->error['type_of_coolant'] = 'Обязательно для заполнения';
        }

        if (empty($this->form['comment'])) {
            $this->error['comment'] = 'Обязательно для заполнения';
        }

        if (empty($this->form['date_add'])) {
                $this->error['date_add'] = 'Обязательно для заполнения';
        }

        if (empty($this->error)) {

            $id = (empty($this->form['id'])) ? $this->dbAdd() : $this->dbSave();

            if (!empty($id)) {

                $type = DB::getRow("SELECT * FROM `#__import_warranty` WHERE `sku` = ? LIMIT 1", array($this->form['model']));

                if ($type['type'] == 2 && Router::$action == 'add') {

                    $full = $type['base'] + $type['more'];

                    $endWarrantyTimestamp = setWarrantyTimestamp($full, $import_ship['date']);

                    DB::set("UPDATE `#__import_ship` 
                             SET
                              `start_pusk` = UNIX_TIMESTAMP(), 
                              `end_warranty` = {$endWarrantyTimestamp} 
                              WHERE
                               `id` = ?"
                        , array($import_ship['id']));
                }
                return $this->handlerСomplete($id, 'edit');
            } else {
                Alerts::setError('Произошла ошибка.');
            }
        } else {
            Alerts::setError('Все поля должы быть заполнены.');
            return true;
        }
    }
    
    public function action_generatePdf()
    {
        require_once Router::$path . "../demand/mpdf60/mpdf.php";

        $id = Input::getInt($_GET['id']);
        if ($this->getItem($id)) {
            $this->form = $this->item;

            $this->content  = $this->render(Router::$path . '/pdf.tpl');
            $html = $this->content;

            $this->init(4);

            $mpdf = new mPDF();
            $mpdf->charset_in = 'cp1251';
            $stylesheet = file_get_contents(THEMES_DIR . '/admin/css/style.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->list_indent_first_level = 0;
            $mpdf->WriteHTML($html, 2);
            $mpdf->Output('list_reg_'.$id.'.pdf','D');
        }
    }

    public function action_importShip()
    {
        if (@$_POST['send']) {
            if (@$_FILES['ship']['tmp_name']) {
//                if (@$_FILES['ship']['type'] == 'application/octet-stream') {
                    require_once PLUGINS_DIR . '/PHPExcel/Classes/PHPExcel.php';
                    $objPHPExcel = PHPExcel_IOFactory::load($_FILES['ship']['tmp_name']);
                    $objPHPExcel->setActiveSheetIndex(0);
                    $aSheet = $objPHPExcel->getActiveSheet();

                    $array = array();
                    //получим итератор строки и пройдемся по нему циклом
                    foreach($aSheet->getRowIterator() as $row){
                        //получим итератор ячеек текущей строки
                        $cellIterator = $row->getCellIterator();
                        //пройдемся циклом по ячейкам строки
                        //этот массив будет содержать значения каждой отдельной строки
                        $item = array();
                        foreach($cellIterator as $cell){
                            //заносим значения ячеек одной строки в отдельный массив
                            array_push($item, $cell->getCalculatedValue());
                        }
                        //заносим массив со значениями ячеек отдельной строки в "общий массв строк"
                        array_push($array, $item);
                    }

                    foreach ($array as $item) {
                        if ($item) {
                            $dt = DateTime::createFromFormat("d.m.Y H:i:s", Input::getStr($item[0]));
                            if ($dt) {
                                $ts = $dt->getTimestamp();
                                $serial = $item[1];
                                $sku = $item[2];
                                $contragent = $item[3];

                                if (!$iseet = DB::getRow("SELECT `id` FROM `#__import_ship` WHERE `serial` = ? LIMIT 1", Input::getStr($serial))) {
                                    DB::add("INSERT INTO `#__import_ship` SET `date` = ?, `serial` = ?, `sku` = ?, `contragent` = ?",
                                        array(Input::getInt($ts), Input::getStr($serial), Input::getStr($sku), Input::getStr($contragent)));
                                }
                            }
                        }
                    }
                    Alerts::setSuccess('Файл успешно импортирован.');
//                }else {
//                    Alerts::setWarning('Расширение файла не соотвествует формату xlsx');
//                }
            } else {
                Alerts::setWarning('Необходимо выбрать файл формата xlsx для загрузки');
            }
        }

        $this->paginator = new Paginator(get_url(Router::$url . '/importShip'), 100);
        $this->items = $this->paginator->process("SELECT * FROM `#__import_ship` ORDER BY `id` DESC");

        // Вывод в шаблон.
        $this->title = 'Импорт проданного оборудования';
        $this->content  = $this->render(Router::$path . '/import_ship.tpl');
        return $this->display('page.tpl');
    }
    
    public function action_importWarrantyDates()
    {
        if (!empty($_FILES['dates']['name'])) {

            DB::set("TRUNCATE `#__import_warranty`");

            require_once PLUGINS_DIR . '/PHPExcel/Classes/PHPExcel.php';
            $objPHPExcel = PHPExcel_IOFactory::load($_FILES['dates']['tmp_name']);
            $objPHPExcel->setActiveSheetIndex(0);
            $aSheet = $objPHPExcel->getActiveSheet();

            $array = array();
            //получим итератор строки и пройдемся по нему циклом
            foreach($aSheet->getRowIterator() as $row){
                //получим итератор ячеек текущей строки
                $cellIterator = $row->getCellIterator();
                //пройдемся циклом по ячейкам строки
                //этот массив будет содержать значения каждой отдельной строки
                $item = array();
                foreach($cellIterator as $cell){
                    //заносим значения ячеек одной строки в отдельный массив
                    array_push($item, $cell->getCalculatedValue());
                }
                //заносим массив со значениями ячеек отдельной строки в "общий массв строк"
                array_push($array, $item);
            }

            foreach ($array as $item) {
                if (!$item[0] || $item[0] == 'Артикул') {
                    continue;
                } else {
                    $sku = $item[0];
                    $name = $item[1];
                    $base = $item[2];
                    $more = $item[3];
                    $electric = $item[4];
                    $type = @$item[5];
                    $brand = @$item[6];

                    if (!DB::getRow("SELECT `id` FROM `#__import_warranty` WHERE `sku` = ? LIMIT 1", Input::getStr($sku))) {
                        DB::add("INSERT INTO `#__import_warranty` SET `sku` = ?, `name` = ?, `base` = ?, `more` = ?, `electric` = ?, `type` = ?, `brand` = ?",
                            array(Input::getStr($sku), Input::getStr($name), Input::getInt($base), Input::getInt($more), Input::getInt($electric), Input::getInt($type), Input::getStr($brand)));
                    }
                }
            }
            Alerts::setSuccess('Файл успешно импортирован.');
            return redirect(Router::$url);

        }

        $this->paginator = new Paginator(get_url(Router::$url . '/importWarrantyDates'), 100);
        $this->items = $this->paginator->process("SELECT * FROM `#__import_warranty` ORDER BY `id` DESC");

        // Вывод в шаблон.
        $this->title = 'Импорт гарантийных сроков';
        $this->content  = $this->render(Router::$path . '/import_warranty_dates.tpl');
        return $this->display('page.tpl');
    }

    public function action_export()
    {
        require_once PLUGINS_DIR . '/PHPExcel/Classes/PHPExcel.php';
        require_once PLUGINS_DIR . '/PHPExcel/Classes/PHPExcel/Writer/Excel5.php';
        require_once PLUGINS_DIR . '/PHPExcel/Classes/PHPExcel/IOFactory.php';

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $active_sheet = $objPHPExcel->getActiveSheet();

        //Создаем шапку таблички данных
        $active_sheet->setCellValue('A1','Серийный номер');
        $active_sheet->setCellValue('B1','Дата отгрузки');
        $active_sheet->setCellValue('C1','Артикул');
        $active_sheet->setCellValue('D1','Контрагент');
        $active_sheet->setCellValue('E1','Дата пуска оборудования');
        $active_sheet->setCellValue('F1','Название компании производившей пуск оборудования');
        $active_sheet->setCellValue('G1','Замечания при пуске');
        $active_sheet->setCellValue('H1','Дата проводимого ремонта');
        $active_sheet->setCellValue('I1','№ Акта по которому проводились ремонтные работы');

        //В цикле проходимся по элементам массива и выводим все в соответствующие ячейки
        $row_start = 2;
        $i = 0;
        foreach (DB::getAll("
          SELECT
            a.serial,
            a.date,
            a.sku,
            a.contragent,
            b.id
          FROM 
            `#__import_ship` AS a
          LEFT JOIN 
            `#__registrylist` as b
          ON
            a.serial = b.serial
          ORDER BY
            a.`id` DESC
         ") as $item) {

            $row_next = $row_start + $i;

//            $active_sheet->getStyle('I'.$row_next)->getAlignment()->setWrapText(true);
//            $active_sheet->getStyle('J'.$row_next)->getAlignment()->setWrapText(true);
//            $active_sheet->getStyle('K'.$row_next)->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getRowDimension($row_next)->setRowHeight(-1);

            $active_sheet->setCellValue('A'.$row_next, $item['serial']);
            $active_sheet->setCellValue('B'.$row_next, date('d.m.Y h:i:s', $item['date']));
            $active_sheet->setCellValue('C'.$row_next, $item['sku']);
            $active_sheet->setCellValue('D'.$row_next, $item['contragent']);
            $active_sheet->setCellValue('E'.$row_next, '');
            $active_sheet->setCellValue('F'.$row_next, '');
            $active_sheet->setCellValue('G'.$row_next, '');
            $active_sheet->setCellValue('H'.$row_next, '');
            $active_sheet->setCellValue('I'.$row_next, '');
            $i++;
        }

        header("Content-Type:application/vnd.ms-excel");
        header("Content-Disposition:attachment;filename=Комплексный_экспорт.xls");

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
//
        // Редирект.
        Alerts::setSuccess("Экспорт успешно завершен.");
        return redirect(Config::$url_admin);
    }

    public function action_removeShip($id)
    {
        //только для администратора + СЦ
        Auth::allow(3);

        if (DB::set("DELETE FROM `#__import_ship` WHERE `id` = ? LIMIT 1", Input::getInt($id))) {
            Alerts::setSuccess('«' . no_empty(@$this->item['name'], 'ID:' . $id) . '» — успешно удалено.');
        } else {
            Alerts::setError('Произошла ошибка (' . __LINE__ . ').');
        }

        // Очиска кеша.
        $this->cacheClear();

        return redirect(Router::$url . '/importShip');
    }
}