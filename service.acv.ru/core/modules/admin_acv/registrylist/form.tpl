<style>
    .border-control-group:hover .help-block {
        color: #428bca;
    }
    .help-block {
        color: inherit;
    }
</style>

<div id="nav" style="margin-bottom: 44px";>
	<?php echo $this->pageHeader(); ?>
	<div class="navbar-right navbar-btn" style="clear: both; float: none !important;">
		<?php
            echo $this->buttonForms();
        ?>
	</div>
</div>

<?php
// Сообщения.
echo Alerts::get(); 

// Инфо.
if (!empty($this->item)) {
	echo $this->blockquote(
		array(
//			'URL' => Html::a(Tree::url($this->table, $this->item['id'], $this->module))
		)
	);
}
?>



<?php
$forms = new Forms($this);

echo $forms->group(
    array(
        'label' => '',
        'items' => array(
            $forms->select(
//                array(
//                    'label'    => 'Модель оборудования, код',
//                    'name'     => 'model',
//                    'required' => true
//                )
				array(
					'label'            => 'Модель оборудования',
					'name'             => 'model',
					'options_first'    => '',
					'options'          => DB::getAll("SELECT `sku` as id, `name` FROM `#__import_warranty` ORDER BY `name`"),
					'required' => true,
					'class' => array('model_select')
				)
            ),
            $forms->text(
                array(
                    'label' => 'Серийный № изделия',
                    'name'  => 'serial',
                    'required' => true
                )
            ),
            $forms->text(
                array(
                    'label' => 'Место установки(город/н.п)',
                    'name'  => 'location',
                    'required' => true
                )
            ),
            $forms->text(
                array(
                    'label' => 'Имя владельца или представителя',
                    'name'  => 'fio'
                )
            ),
            $forms->text(
                array(
                    'label' => 'Email владельца оборудования',
                    'name'  => 'email'
                )
            ),
        )
    )
);

echo $forms->group(
    array(
        'label' => 'Топливо',
        'items' => array(
            $forms->checkbox(
                array(
                    'label'    => 'Выполнена ли настройка',
                    'name'     => 'settings',
                    'required' => true
                )
            ),
            $forms->text(
                array(
                    'label'    => 'Значение давления газа перед горелкой',
                    'name'     => 'pressure_before_burner',
                    'required' => true
                )
            ),
            $forms->text(
                array(
                    'label'    => 'Давление газа за газовым клапаном',
                    'name'     => 'pressure_after_gas_valve',
                )
            ),
            $forms->text(
                array(
                    'label'    => 'Давление за топлевным насосом (диз)',
                    'name'     => 'pressure_of_the_oil_pump',
                )
            ),
            $forms->text(
                array(
                    'label'    => 'Тип и характеристики форсунки (диз)',
                    'name'     => 'type_characteristics_jets',
                )
            ),
            $forms->text(
                array(
                    'label'    => "Значение  CO<sub>2</sub> CO сажевого числа",
                    'name'     => 'co2',
                )
            ),
        )
    )
);

echo $forms->group(
    array(
        'label' => 'Дымоход',
        'items' => array(
            $forms->text(
                array(
                    'label'    => 'Укажите высоту/диаметр дымохода',
                    'name'     => 'chimney_height',
                    'required' => true
                )
            ),
            $forms->text(
                array(
                    'label'    => 'Укажите значения тяги в дымоходе',
                    'name'     => 'chimney_value_power',
                )
            ),
            $forms->checkbox(
                array(
                    'label'    => 'Наличие доп. устройств стабилизации тяги',
                    'name'     => 'chimney_devices_stabilize',
                )
            ),
            $forms->text(
                array(
                    'label'    => 'Укажите тип системы дымоотведения',
                    'name'     => 'chimney_type_system',
                )
            )
        )
    )
);

echo $forms->group(
    array(
        'label' => 'Вентиляция',
        'items' => array(
            $forms->text(
                array(
                    'label'    => 'Наличие приточной вентиляции, если есть - укажите площадь сечения',
                    'name'     => 'inflow_ventilation',
                    'required' => true
                )
            ),
            $forms->text(
                array(
                    'label'    => 'Наличие вытяжной вентиляции, если есть - укажите площадь сечения',
                    'name'     => 'exhaust_ventilation',
                )
            ),
        )
    )
);

echo $forms->group(
    array(
        'label' => 'Электрика',
        'items' => array(
            $forms->text(
                array(
                    'label'    => 'Есть ли устройство стабилизации напряжения. Если да - укажите его тип',
                    'name'     => 'voltage_regulation_device',
                    'required' => true
                )
            ),
            $forms->text(
                array(
                    'label'    => 'Выполнен ли замер напряжения на фазах котла? Укажите значение',
                    'name'     => 'voltage_on_the_phases_of_the_boiler',
                )
            ),
            $forms->checkbox(
                array(
                    'label'    => 'Подключен ли котел к шине заземления?',
                    'name'     => 'is_ground_bus',
                    'required' => true
                )
            ),
            $forms->checkbox(
                array(
                    'label'    => 'Заземлен отдельно бойлер котла?',
                    'name'     => 'is_earthed_separately',
                )
            ),
            $forms->checkbox(
                array(
                    'label'    => 'Выполнена проверка электроподключений',
                    'name'     => 'is_check_connections',
                )
            ),
            $forms->text(
                array(
                    'label'    => 'Диэлектрическая вставка на газовой трубе',
                    'name'     => 'dielectric_paste',
                )
            ),
        )
    )
);

echo $forms->group(
    array(
        'label' => 'Теплоноситель',
        'items' => array(
            $forms->text(
                array(
                    'label'    => 'Тип теплоносителя',
                    'name'     => 'type_of_coolant',
                    'required' => true
                )
            ),
            $forms->checkbox(
                array(
                    'label'    => 'Выполнена ли подготовка теплоносителя?',
                    'name'     => 'is_preporation_coolant',
                    'required' => true
                )
            ),
            $forms->checkbox(
                array(
                    'label'    => 'Проведены ли гидралические испытания С.О.',
                    'name'     => 'is_has_test_co',
                )
            ),
            $forms->checkbox(
                array(
                    'label'    => 'Наличие узла автоподпитки С.О.',
                    'name'     => 'is_node_co',
                    'required' => true
                )
            ),
            $forms->checkbox(
                array(
                    'label'    => 'Есть ли счетчик подпиточной воды?',
                    'name'     => 'is_water_meter',
                    'required' => true
                )
            ),
            $forms->textarea(
                array(
                    'label'    => 'Укажите список замечаний к монтажу котла (недостатвки системы, обвязки и т.д.)',
                    'name'     => 'comment',
                    'required' => true
                )
            ),
        )
    )
);
?>

<?php if (@$this->item['user_id'] == 171): ?>
    <div class="alert alert-warning alert-dismissible" role="alert">
        <?
        echo $forms->group(
            array(
                'label' => 'Дополнительная информация по листу регистрации',
                'items' => array(
                    $forms->text(
                        array(
                            'label'    => 'Наименование организации, производившей монтаж котла',
                            'name'     => 'name_service',
                        )
                    ),
                    $forms->text(
                        array(
                            'label'    => 'Наименование организации, производившей пуск оборудования',
                            'name'     => 'name_pusk_start',
                        )
                    ),
                    $forms->text(
                        array(
                            'label'    => 'Контактный телефон',
                            'name'     => 'phone',
                        )
                    ),
                )
            )
        );
        ?>

        <strong>Место установки оборудования:</strong>
        <br>
        <?= DB::getValue("SELECT `name` FROM `region` WHERE `id` = ?", @$this->item['region_id']) ?> /
        <?= DB::getValue("SELECT `name` FROM `city` WHERE `id` = ?", @$this->item['city_id']) ?> /
        <?= @$this->item['street'] ?>

        <br>
        <br>
        <strong>Файлы:</strong>(фотографии с обвязкой котла / (сканы) актов о пуске котла / акты о гидравлических испытаниях С.О)
        <br>
        <?php
            $path = realpath($_SERVER['DOCUMENT_ROOT'] ) . '/../garanty.acv.ru/web/uploads/registrylist/' . $this->item['id']. '/';
            if (is_dir($path)) {
                $files = scandir($path);
                $files = array_diff($files, array('.', '..'));
                if ($files) {
                    foreach ($files as $file) {
                        echo "<a target='_blank' href='".'http://warranty.acv.ru/uploads/registrylist/' . $this->item['id'] . '/' . $file ."'>$file</a></br>";
                    }
                }
            }
        ?>
    </div>

<?php endif; ?>
<br>
<br>
<?
echo $forms->group(
    array(
        'label' => '',
        'items' => array(
            $forms->datetime(
                array(
                    'label'    => 'Дата регистрации',
                    'name'     => 'date_add',
                )
            ),
        )
    )
);


?>


<script type="text/javascript">
    $(document).ready(function() {
        $(".model_select").select2();
    });
</script>
