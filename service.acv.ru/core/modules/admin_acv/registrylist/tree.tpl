<?php
echo $this->tr(
	array(
		$this->tdCheck(),
		$this->tdEdit($this->item['serial']),
		$this->tdEdit(Users::getAuthorName($this->item['user_id'])),
		$this->tdDate(),
		$this->tdEdit($this->item['comment']),
//		$this->tdEdit('<a class="pdf_ico" href="'.get_url_admin('/registrylist/generatePdf').'?id='.$this->item['id'].'"></a>'),
		$this->tdEditable('<a class="pdf_ico" href="'.get_url_admin('/registrylist/generatePdf').'?id='.$this->item['id'].'"></a>'),
	)
);