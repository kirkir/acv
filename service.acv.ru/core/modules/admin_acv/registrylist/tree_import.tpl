<?php
echo $this->tr(
    array(
        $this->tdId(),
        $this->tdText($this->item['sku']),
        $this->tdText($this->item['name']),
        $this->tdText($this->item['base']),
        $this->tdText(($this->item['more']) ? $this->item['more'] : ' 0 '),
        $this->tdText($this->item['electric']),
        $this->tdText(Config::$types[$this->item['type']]),
        $this->tdText($this->item['brand']),
    )
);