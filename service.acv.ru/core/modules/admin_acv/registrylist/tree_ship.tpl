<?php
echo $this->tr(
    array(
        $this->tdId(),
        $this->tdText($this->item['serial']),
        $this->tdText(date('d.m.Y h:i:s', $this->item['date'])),
        $this->tdText($this->item['sku']),
        $this->tdText($this->item['contragent']),
        $this->tdRemoveShip()
    )
);