<div id="nav">
    <?php echo $this->pageHeader(); ?>
    <div class="navbar-right navbar-btn">
        <button class="btn btn-default" type="submit" name="send" value="save">Загрузить</button>
    </div>
</div>

<?php
// Сообщения.
echo Alerts::get();

?>

<div class="legend-group">
    <legend>Загрузка файлов</legend>
    <div class="legend-body">
        <div class="control-group">
            <label class="control-label">Загрузите файл</label>
            <div class="controls">
                <input type="file" name="ship">
            </div>
        </div>
    </div>
</div>

<?php
$tables = new Tables($this->items);
$tables->imageModule = $this->module;
echo $tables->out(
    array(
        'thead' => array(
//            $tables->thCheck(),
            $tables->thId(),
            $tables->thText('Серийный номер'),
            $tables->thText('Дата'),
            $tables->thText('Артикул'),
            $tables->thText('Контрагент'),
//            $tables->thApprove(),
            $tables->thRemove()
        ),
        'tbody' => dirname(__FILE__) . '/tree_ship.tpl'
    )
);
echo $this->paginator->display;
?>
