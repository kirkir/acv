<?php
/**
 * Администрирование / Отчеты.
 */
class module_works extends Backend
{
    public $module = 'works';
    public $table  = '#__works';
    public $title  = 'Выполненные работы';

    public function __construct()
    {
        // Уровень доступа.
        $this->init(4);
    }

    public function action_index()
    {
        $this->handlerActions();

        $this->paginator = new Paginator(get_url(Router::$url), 3000);
        $this->items = $this->paginator->process("SELECT * FROM `{$this->table}` ORDER BY `name`");

        // Вывод в шаблон.
        $this->subtitle = 'список';
        $this->content  = $this->render(Router::$path . '/list.tpl');
        return $this->display('page.tpl');
    }


    /**
     * Обработка форм.
     */
    public function handler()
    {
        if (!isset($_POST['send'])) {
            return false;
        }

        $this->form['id']       = Input::getInt($_POST['id']);
        $this->form['name']    = Input::getStr($_POST['name']);
        $this->form['hour']   = Input::getStr($_POST['hour']);
        $this->form['price'] = Input::getPrice($_POST['price']);

        if (empty($this->form['name'])) {
            $this->error['name'] = 'Обязательно для заполнения';
        }

        if (empty($this->form['hour'])) {
            $this->error['hour'] = 'Обязательно для заполнения';
        }

        if (empty($this->error)) {

            $id = (empty($this->form['id'])) ? $this->dbAdd() : $this->dbSave();

            if (!empty($id)) {
                return $this->handlerСomplete($id, 'edit');
            } else {
                Alerts::setError('Произошла ошибка.');
            }
        } else {
            Alerts::setError('Все поля должы быть заполнены.');
            return true;
        }
    }
}