<?php
echo $this->tr(
	array(
		$this->tdCheck(),
		$this->tdId(),
		$this->tdName(),
		$this->tdDate(),
		$this->tdApprove(),
		$this->tdRemove()
	)
);