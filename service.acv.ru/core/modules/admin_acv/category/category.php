<?php
/** 
 * Администрирование / Категории.
 */
class module_category extends Backend
{
	public $module = 'category';
	public $table  = '#__category';
	public $title  = 'Категории СЦ';

	public function __construct()
	{
		// Уровень доступа.
		$this->init(4);
	}

	/**
     * Обработка форм.
     */
    public function handler()
    {
		if (!isset($_POST['send'])) {
			return false;
		}

		$this->form['id']          = Input::getInt($_POST['id']);
		$this->form['parent']      = Input::getInt($_POST['parent']);
		$this->form['name']        = Input::getStr($_POST['name']);	
		$this->form['price']        = Input::getPrice($_POST['price']);

		$this->form['approve']     = Input::getBool($_POST['approve']);	


		if (empty($this->form['name'])) {
			$this->error['name'] = 'Укажите название';
		}

		if (empty($this->error)) {
            $id = (empty($this->form['id'])) ? $this->dbAdd() : $this->dbSave();
			if (!empty($id)) {
				return $this->handlerСomplete($id, 'edit');
			} else {
				Alerts::setError('Произошла ошибка.');
			}
		}

		return true;
    }
}