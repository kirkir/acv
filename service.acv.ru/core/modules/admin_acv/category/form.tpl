<div id="nav">
	<?php echo $this->pageHeader(); ?>
	<div class="navbar-right navbar-btn">
		<?php echo $this->buttonForms(); ?>
	</div>
</div>

<?php
// Сообщения.
echo Alerts::get(); 

// Инфо.
if (!empty($this->item)) {
	echo $this->blockquote(
		array(
		)
	);
}

$forms = new Forms($this);

// Основное.
echo $forms->group(
	array(
		'label' => 'Основное',
		'items' => array(
			$forms->text(
				array(
					'label'    => 'Название категории',
					'name'     => 'name', 
					'required' => true
				)
			),
            $forms->text(
				array(
					'label'    => 'Стоимость',
					'name'     => 'price',
					'style'    => 'width: 100px'
				)
			)
		)
	)
);


// Параметры публикации.
echo $forms->group(
	array(
		'label' => 'Параметры публикации',
		'items' => array(
			$forms->Checkbox(
				array(
					'label' => 'Публиковать на сайте', 
					'name'  => 'approve'
				)
			),
        )
	)
);