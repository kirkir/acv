<div id="nav">
	<?php echo $this->pageHeader(); ?>
	<div class="navbar-right navbar-btn">
		<?php echo $this->buttonLists(); ?>
	</div>
</div>

<?php 
// Сообщения.
echo Alerts::get();

// Таблица.
$tables = new Tables($this->items);
$tables->imageModule = $this->module;

echo $tables->out(
	array(
		'thead' => array(
			$tables->thCheck(),
			$tables->thId(),
			$tables->thName(),
			$tables->thDate(),
			$tables->thApprove(),
			$tables->thRemove()
		),
		'tbody' => dirname(__FILE__) . '/tree.tpl'
	)
);