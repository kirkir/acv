<?php
//$active = ($this->item['shows'] && !Auth::perm(4)) ? 'active' : '';


//(@$letter_link) ? '<a class="pdf_ico" href="'.get_url_admin($letter_link).'"></a>' : ''

echo $this->tr(
	array(
		$this->tdCheck(),
		$this->tdId(),
//		$this->tdEdit(DB::getValue("SELECT `name` FROM `#__users` WHERE `id` =?", @$this->item['user_id'])),
		$this->tdEdit(($this->item['to_user_id'] == Auth::$user['id'] || Auth::perm(4)) ? @$this->item['name'] : '&nbsp;'),
		$this->tdEdit(($this->item['to_user_id'] == Auth::$user['id'] || Auth::perm(4)) ? @$this->item['surname'] : '&nbsp;'),
		$this->tdEdit(($this->item['to_user_id'] == Auth::$user['id'] || Auth::perm(4)) ? @$this->item['last_name'] : '&nbsp;'),
        $this->tdEdit(DB::getValue("SELECT `name` FROM `#__import_warranty` WHERE `sku` = ?", @$this->item['model'])),
        $this->tdEdit(($this->item['to_user_id'] == Auth::$user['id'] || Auth::perm(4)) ? @$this->item['serial'] : '&nbsp;'),
        $this->tdEdit(($this->item['to_user_id'] == Auth::$user['id'] || Auth::perm(4)) ? @$this->item['warranty_item'] : '&nbsp;'),
        $this->tdEdit(($this->item['to_user_id'] == Auth::$user['id'] || Auth::perm(4)) ? DB::getValue("SELECT `name` FROM `region` WHERE `id` = ?", @$this->item['region_id']) : '&nbsp;'),
        $this->tdEdit(($this->item['to_user_id'] == Auth::$user['id'] || Auth::perm(4)) ? DB::getValue("SELECT `name` FROM `city` WHERE `id` = ?", @$this->item['city_id']) : '&nbsp;'),
        $this->tdEdit(($this->item['to_user_id'] == Auth::$user['id'] || Auth::perm(4)) ? @$this->item['street'] : '&nbsp;'),
//		$this->tdEdit(Config::$stage[$this->item['stage']], 'background:' . Config::$stage_color[$this->item['stage']], ($this->item['stage'] == 3) ? 'white' : ''),
//		$this->tdApprove(),
        $this->tdDate(),
        $this->tdRemove()
	)
);