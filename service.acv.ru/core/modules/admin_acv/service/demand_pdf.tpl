<div id="nav" style="margin-bottom: 44px";>
    <?php echo $this->pageHeader(); ?>
</div>


<?php
$forms = new Forms($this);

echo  $forms->text(
    array(
        'label'            => 'Модель оборудования',
        'name'             => 'furniture',
        'value' => DB::getValue("SELECT `name` FROM `#__furniture` WHERE id = ?", $this->form['furniture']),
        'style' => 'width: 100%'
    )
);

echo $forms->group(
    array(
        'cols'  => 2,
        'items' => array(
            $forms->text(
                array(
                    'label' => 'Наименование продавца',
                    'name'  => 'name',
                    'required' => true,
                    'style' => 'width: 100%'
                )
            ),
            $forms->text(
                array(
                    'label' => 'Серийный номер изделия',
                    'name'  => 'serial',
                    'required' => true
                )
            ),
            $forms->datetime(
                array(
                    'label' => 'Дата пуска оборудования',
                    'name' => 'date_pusk'
                )
            ),
            $forms->text(
                array(
                    'label' => 'Место установки, адрес',
                    'name'  => 'adress',
                    'required' => true,
                    'class'   => 'input_adress',
                    'style' => 'width: 100%'
                )
            ),
            $forms->text(
                array(
                    'label' => 'ФИО клиента',
                    'name'  => 'fio',
                    'required' => true,
                    'style' => 'width: 70%',
                )
            )
        )
    )
);

echo '<div class="block_commit_admin">' . $forms->group(
        array(
            'items' => array(
                $forms->textarea(
                    array(
                        'label' => 'Описание дефекта',
                        'name'  => 'text',
                        'required' => true,
                        'style' => 'width: 100%'
                    )
                ),
                $forms->textarea(
                    array(
                        'label' => 'Комментарий администратора',
                        'name'  => 'comment',
                        'style' => 'width: 100%;',
                        'rows' => 10,
                        'disabled' => Auth::perm(4) ? false : true
                    )
                )
            )
        )
    ) . '</div>';
?>

<div class="legend-body">
    <table class="selection table table-bordered table-hover table-sort">
        <thead>
        <tr>
            <th class="th-name">Код</th>
            <th class="th-name">Название</th>
        </tr>
        </thead>
        <?php
        if(@$this->form['parts']) : ?>
            <?php foreach($this->isset_parts($this->form['parts']) as $part) : ?>
                <tr data-part_id="<?php echo $part['id']; ?>">
                    <td><?php echo $part['sku']; ?></td>
                    <td><?php echo $part['name']; ?></td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>
</div>

<?php
echo $forms->text(
    array(
        'label' => 'Номер платежного поручения',
        'name'  => 'num_pay',
        'disabled' => (Auth::perm(4) ? false : true),
        'style' => 'width: 70%',
    )
);
?>


<?php
if(@$this->form['user_id']) {
    $user_order = DB::getRow("SELECT * FROM `#__users` WHERE `id` = ?", @$this->form['user_id']);
    if(@$user_order) {
        $category_h_price = DB::getValue("SELECT `price` FROM `#__category` WHERE `id` = ?", @$user_order['category']);
    }
    $this->form['price_one_h'] = (@$this->form['price_one_h'] == 0.00 && @$category_h_price) ? @$category_h_price : @$this->form['price_one_h'];
}

echo $forms->group(
    array(
        'cols'  => 2,
        'items' => array(
            (Auth::perm(4)) ?
                $forms->Checkbox(
                    array(
                        'label' => 'Гарантийный случай',
                        'name'  => 'warranty',
                        'class' => 'toggle_warranty'
                    )
                ) : '',
            (Auth::perm(4)) ?
                $forms->text(
                    array(
                        'help'  => 'Стоимость одного нормо-часа составляет',
                        'name'  => 'price_one_h',
                        'class'  => 'price_one_h',
                        'value' => $this->form['price_one_h'],
                        'group_style'  => (!$this->form['warranty']) ? 'display : none' : ''
                    )
                ) : ''
        )
    )
);

echo $forms->group(
    array(
        'items' => array(
            $forms->text(
                array(
                    'label' => 'Введите номер гарантийного талона',
                    'name'  => 'warranty_num',
                    'required' => true,
                    'style' => 'width: 70%',
                )
            )
        )
    )
);
?>


<?php if(!@$this->form['works_ids']) : ?>
    <div class="work_list_select form-group <?php echo (@$this->error['work_list']) ? ' has-error' : ' required' ?>">
        <label for="fid-furniture" class="required">Виды работ:</label>
        <select name="work_list" id="work_list" class="form-control form-control-text">
            <option value=""></option>
            <?php foreach(DB::getAll("SELECT * FROM `#__works` ORDER BY `name`") as $work) : ?>
                <option value="<?php echo $work['id']; ?>"><?php echo $work['name']; ?></option>
            <?php endforeach; ?>
        </select>
        <p class="help-block error-block"><?php echo @$this->error['work_list'] ?></p>
    </div>
<?php endif; ?>
<input type="hidden" name="works_ids" value="<?php echo @$this->form['works_ids']; ?>">
<br><br>
<table class="selection table table-bordered table-hover">
    <thead>
    <tr>
        <th class="th-name">Виды работ:</th>
        <th class="th-name">Нормо-часов</th>
        <th class="th-name">Стоимость</th>
    </tr>
    </thead>
    <tbody class="work_list">
    <?php if(@$this->form['works_ids']) : ?>
        <?php if($this->isset_works($this->form['works_ids'])) : $total = 0;?>
            <?php foreach($this->isset_works($this->form['works_ids']) as $work) : $dinamic = DB::getRow("SELECT * FROM `#__demand_dinamic` WHERE `demand_id` = ? AND `work_id` = ?",
                array($this->form['id'], $work['id'])); ?>
                <tr data-work_id="<?php echo $work['id']; ?>">
                    <td><?php echo $work['name']; ?></td>
                    <?php
                    if($dinamic) {
                        $work['hour'] = $dinamic['hour'];
                        $work['price'] = $dinamic['price'];
                    }
                    ?>
                    <td class="d_h" <?php if (is_admin()) echo 'onclick="tdRedactor(this, 1); return false;"';?>><?php echo ($work['hour'] != 0.0) ? $work['hour'] : '-'; ?></td>
                    <td class="d_p" <?php if (is_admin()) echo 'onclick="tdRedactor(this, 1); return false;"';?>><?php echo ($work['price'] != 0.00) ? $work['price'] : $this->form['price_one_h']; ?></td>
                </tr>
                <?php
                if($work['hour'] == 0.00) {
                    $total += $work['price'];
                } else {
                    if($dinamic) {
                        $total += $work['hour'] * $work['price'];
                    } else {
                        $total += $work['hour'] * $this->form['price_one_h'];
                    }
                }
                ?>
            <?php endforeach; ?>
            <input type="hidden" name="price_works" value="<?php echo $total; ?>">
            <input type="hidden" name="delivery_total" value="<?php echo $this->delivery_total(); ?>">
        <?php endif; ?>
    <?php endif; ?>
    </tbody>
</table>

<?php
echo $forms->group(
    array(
        'items' => array(
            $forms->text(
                array(
                    'label' => 'Расстояние от СЦ «Исполнителя» до места установки оборудования в одном направлении, км',
                    'name'  => 'km',
                    'style' => 'width: 70%',
                    'required' => true,
                    'placeholder' => 'Оставить поле пустое когда клиент сам привозит оборудование в сервис'
                )
            )
        )
    )
);
?>
<div style="" class="form-group">
    <p class="help-block">Размер компенсации выезда сервисного центра:</p>
</div>

<?php
echo $forms->group(
    array(
        'cols'  => 2,
        'items' => array(
            (Auth::perm(4)) ?
                $forms->Checkbox(
                    array(
                        'label' => 'Компенсация выполненных работ',
                        'name'  => 'compensation_bool',
                    )
                ) : '',
        )
    )
);
?>

<div style="text-align: right; margin-right: 70px" class="form-group">
    <p class="help-block"><strong>Итого: </strong><span class="total"> <?php echo @$total + $this->delivery_total() . ' р.'; ?></span></p>
    <input type="hidden" name="total" value="<?php echo @$total + $this->delivery_total(); ?>">
</div>
<?php // endif; ?>
<?php
echo $forms->group(
    array(
        'items' => array(
            $forms->datetime(
                array(
                    'label' => 'Дата Ремонта',
                    'name' => 'date_work',
                    'require' => true
                )
            )
        )
    )
);

