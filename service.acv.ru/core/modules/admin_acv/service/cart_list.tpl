<div id="nav">
    <?php echo $this->pageHeader(); ?>
    <div class="navbar-right demand navbar-btn">
        <?php
        if(Auth::perm(4)) {
            echo
            $this->buttonAction(),
//                    $this->buttonSort(),
//                    $this->buttonAdd('Создать отчет'),
                '<a class="btn btn-success" href="' . get_url(Router::$url) . '">Обновить</a> ',
                '<a class="btn btn-success" href="' . get_url(Router::$url . '/draft') . '">Черновик</a> ',
                '<a class="btn btn-success" href="' . get_url(Router::$url . '/archive') . '">Архив</a> ',
                '<a class="btn btn-success" href="' . get_url(Config::$url_admin . '/users/edit/' . Auth::$user['id']) . '">Личные данные</a> ',
                '<a class="btn btn-success" href="' . get_url(Router::$url . '/cart') . '">Корзина</a>';

        } else {
            echo
            $this->buttonAdd('Создать отчет'),
                '<a class="btn btn-success" href="' . get_url(Router::$url) . '">Обновить</a> ',
                '<a class="btn btn-success" href="' . get_url(Router::$url . '/draft') . '">Черновик</a> ',
                '<a class="btn btn-success" href="' . get_url(Router::$url . '/archive') . '">Архив</a> ',
                '<a class="btn btn-success" href="' . get_url(Config::$url_admin . '/users/edit/' . Auth::$user['id']) . '">Личные данные</a> ';
        }
        ?>
    </div>
</div>
<br><br><br>
<?php
// Сообщения.
echo Alerts::get();
// Таблица.
$tables = new Tables($this->items);
$tables->imageModule = $this->module;
echo $tables->out(
    array(
        'thead' => array(
            $tables->thCheck(),
//			$tables->thId(),
            $tables->thText('Название СЦ'),
            $tables->thDate(),
            $tables->thText('Текущая стадия'),
            $tables->thText('Порядковый номер'),
            $tables->thText('Текущий статус'),
            $tables->thText('Модель оборудования'),
            $tables->thText('Серийный номер изделия'),
            $tables->thText('Номер платежного поручения'),
            $tables->thText('Информационное письмо'),
            $tables->thText('Описание дефекта'),
            $tables->thText('Комментарий ACV'),
            $tables->thApprove(),
            $tables->thRemove()
        ),
        'tbody' => dirname(__FILE__) . '/tree.tpl'
    )
);
echo $this->paginator->display;
?>

<div id="myModalBox" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="">
                <!-- Заголовок модального окна -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Комментарий ACV</h4>
                </div>
                <!-- Основное содержимое модального окна -->

                <div class="modal-body">
                    <?php if(Auth::perm(4)) : ?>
                        <textarea class="commit" data-id="" style="width: 100%" id="<?php echo (Auth::perm(4) ? 'textarea_adm' : ''); ?>" cols="30" rows="10"></textarea>
                    <?php else: ?>
                        <div class="commit_t"></div>
                    <?php endif; ?>
                </div>
                <!-- Футер модального окна -->
                <div class="modal-footer">
                    <?php if(Auth::perm(4)) : ?>
                        <button type="button" class="btn btn-success">Сохранить</button>
                    <?php endif; ?>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </form>
        </div>
    </div>
</div>