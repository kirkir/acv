<style>
    .border-control-group:hover .help-block {
        color: #428bca;
    }
    .help-block {
        color: inherit;
    }
</style>

<div id="nav" style="margin-bottom: 44px";>
	<?php echo $this->pageHeader(); ?>
	<div class="navbar-right navbar-btn" style="clear: both; float: none !important;">
		<?php
        echo $this->buttonClose();
        if(!Auth::perm(4) && $this->item['to_user_id'] == Auth::$user['id'] && $this->item['process'] != 1) {
            echo '<a target="_blank" href="'.get_url_admin('demand/add?service=' . $this->item['id']).'" class="btn btn-danger">Закрыть заявку</a>';
        }
        if(!Auth::perm(4) && $this->item['to_user_id'] != Auth::$user['id']) {
            echo '<button class="btn btn-info" type="submit" name="take" value="take">Откликнуться на заявку</button>';
        } elseif (Auth::perm(4)) {
            echo '<button class="btn btn-info" type="submit" name="other_service" value="take">Переназначить заявку на другой СЦ</button>';
        }
        ?>
	</div>
</div>

<?php
// Сообщения.
echo Alerts::get();

// Инфо.
//if (!empty($this->item)) {
//	echo $this->blockquote(
//		array(
////			'URL' => Html::a(Tree::url($this->table, $this->item['id'], $this->module))
//		)
//	);
//}
?>

<div class="legend-group">
    <legend>информация о заявке:</legend>
    <div class="legend-body">
        <div class="form-group">
            <div class="alert alert-success alert-dismissible" role="alert">
                <?php if ($this->item['to_user_id'] == Auth::$user['id'] || Auth::perm(4)): ?>
                    <div>Номер заявки: <?= $this->item['order_name']; ?></div>
                    <div>Имя: <?= $this->item['name']; ?></div>
                    <div>Фамилия: <?= $this->item['surname']; ?></div>
                    <div>Отчество: <?= $this->item['last_name']; ?></div>
                    <div>Модель оборудования: <?= DB::getValue("SELECT `name` FROM `#__import_warranty` WHERE `sku` = ?", @$this->item['model']) ?></div>
                    <div>Серийный номер: <?= $this->item['serial']; ?></div>
                    <div>Номер гарантийного талона: <?= $this->item['warranty_item']; ?></div>
                    <div>Регион: <?= DB::getValue("SELECT `name` FROM `region` WHERE `id` = ?", @$this->item['region_id']) ?></div>
                    <div>Город: <?= DB::getValue("SELECT `name` FROM `city` WHERE `id` = ?", @$this->item['city_id']) ?></div>
                    <div>Улица: <?= $this->item['street']; ?></div>
                    <div>Телефон: <?= $this->item['phone']; ?></div>
                    <div>Email: <?= $this->item['email']; ?></div>
                    <div>Причина обращения: <?= $this->item['event']; ?></div>
                    <?php if ($this->data_ship): ?>
                        <?php $date = warranty_end($this->data_ship['end_warranty']); ?>
                        <div style="margin: 10px 0">
                            <strong>Данные о гарантии:</strong> <br>
                            месяцев - <?= $date['month'] ?> <br>
                            дней - <?= $date['day'] ?>
                        </div>
                    <?php endif; ?>
                <?php else: ?>
                    <div>Регион: <?= DB::getValue("SELECT `name` FROM `region` WHERE `id` = ?", @$this->item['region_id']) ?></div>
                    <div>Город: <?= DB::getValue("SELECT `name` FROM `city` WHERE `id` = ?", @$this->item['city_id']) ?></div>
                    <div>Улица: <?= $this->item['street']; ?></div>
                    <div>Модель оборудования: <?= DB::getValue("SELECT `name` FROM `#__import_warranty` WHERE `sku` = ?", @$this->item['model']) ?></div>
                    <div>Причина обращения: <?= $this->item['event']; ?></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php
    if (Auth::perm(4)) {
        $forms = new Forms($this);
        echo $forms->select(
            array(
                'label'            => 'СЦ',
                'name'             => 'to_user_id',
                'options_first'    => '',
                'options'          => DB::getAll("SELECT * FROM `#__users` ORDER BY `name`"),
                'required' => true,
//                'class' => array('model_select')
            )
        );
    }
?>
<input type="hidden" name="id" value="<?= $this->item['id']; ?>">

<script type="text/javascript">
    $(document).ready(function() {
        $(".model_select").select2();

        $('form').attr('action', '<?php echo get_url_admin('service/take') ?>')
    });
</script>

