<?php
/** 
 * Администрирование / Отчеты.
 */
class module_service extends Backend
{
	public $module = 'service';
	public $table  = '#__service';
	public $title  = 'Заявки';
	public $not_cart  = ' AND `cart` = 0';
	public $in_cart  = ' AND `cart` = 1';


	public function __construct()
	{
        // Уровень доступа.
		$this->init(3);
	}

	public function action_index()
    {
        $this->handlerActions();

//            $draft_archive = "WHERE `archive` = 0 AND `draft` = 0 {$this->not_cart}";
        $this->paginator = new Paginator(get_url(Router::$url), 20);
        if(Auth::perm(4)) {
//                $this->items = $this->paginator->process("SELECT * FROM `{$this->table}` {$draft_archive} ORDER BY `date_add` DESC");
            $this->items = $this->paginator->process("SELECT * FROM `{$this->table}` ORDER BY `late` DESC , `date_add` DESC");
        } else {
            $this->items = $this->paginator->process(Users::getServiceOrdersNewByRegion());
//            $this->items = $this->paginator->process("SELECT * FROM `{$this->table}`  AND `user_id` = ? ORDER BY `date_add` DESC", Auth::$user['id']);
        }

		// Вывод в шаблон.
		$this->subtitle = 'список';
		$this->content  = $this->render(Router::$path . '/list.tpl');
		return $this->display('page.tpl');
    }

    public function action_take()
    {

        if (isset($_POST['take'])) {
            $id = Input::getInt($_POST['id']);
            $service = DB::getRow("SELECT * FROM `#__service` WHERE `id` = ?", $id);

            if ($service) {
                if (DB::set("
                UPDATE 
                  `#__service`
                SET
                  `to_user_id` = ?, 
                  `late` = 0
                WHERE 
                  `id` = ?
            ", array(Auth::$user['id'], $id))) {

                    // send email to client
                    $mail = new Mail();
                    $mail->from(Settings::get('email'), Settings::get('email_name'));
                    $mail->to($service['email']);
                    $mail->subject = 'Информация по вашей заявке';
                    $mail->body = "
                        <p>
                            В соответствии с Вашей заявкой №{$service['order_name']}, Вам назначен АСЦ «".Auth::$user['login']."» тел.: " . Auth::$user['phone'] . " 
                        email:".Auth::$user['email'].". Представители АСЦ свяжутся с Вами в ближайшее время. Если с Вами не связались в течение 24 часов просим Вас связаться с нами : https://acv.ru/contacts
                        </p>";
                    $mail->send();
                    Alerts::setSuccess('Заявка прикреплена за вашим сервисом! Перейдите в заявку, что бы увидеть полные данные.');
                } else {
                    Alerts::setError('Ошибка что то пошло не так, обратитесь к администратору!');
                }
            }
            return redirect(Router::$url);
        } elseif(isset($_POST['other_service'])) {
            $id = Input::getInt($_POST['id']);
            $to_user_id = Input::getInt($_POST['to_user_id']);

            $service = DB::getRow("SELECT * FROM `#__service` WHERE `id` = ?", $id);
            $user = Users::getUser($to_user_id);

            if ($service) {
                if (DB::set("
                    UPDATE 
                      `#__service`
                    SET
                      `to_user_id` = ?,
                      `late` = 0
                    WHERE 
                      `id` = ?
                ", array($to_user_id, $id))) {

                    // send email to client
                    $mail = new Mail();
                    $mail->from(Settings::get('email'), Settings::get('email_name'));
                    $mail->to($service['email']);
                    $mail->subject = 'Информация по вашей заявке';
                    $mail->body = "
                        <p>
                            В соответствии с Вашей заявкой №{$service['order_name']}, Вам назначен АСЦ «".$user['login']."» тел.: " . $user['phone'] . " 
                        email:".$user['email'].". Представители АСЦ свяжутся с Вами в ближайшее время. Если с Вами не связались в течение 24 часов просим Вас связаться с нами : https://acv.ru/contacts
                        </p>";
                    $mail->send();
                    Alerts::setSuccess('Заявка переназначена');
                } else {
                    Alerts::setError('Ошибка что то пошло не так, обратитесь к администратору!');
                }
            }
            return redirect(Router::$url);
        }
    }

    public function action_add()
    {
        return false;
    }

    public function action_remove($id)
    {
        if (Auth::$user['level'] != 4) {
            Alerts::setError('Ошибка что то пошло не так, обратитесь к администратору!');
            return redirect(Router::$url);
        } else {
            parent::action_remove($id);
        }
    }


    /**
     * Обработка форм.
     */
    public function handler()
    {
        if (isset($this->item['id'])) {
            if (Auth::$user['level'] != 4 && ($this->item['to_user_id'] != Auth::$user['id'] && $this->item['to_user_id'] != 0)) {
                return Router::get('404');
            }
        }


        $this->data_ship = DB::getRow("
                    SELECT 
                      * 
                    FROM
                      `#__import_ship`
                    WHERE 
                      `serial` = ? AND `sku` = ?
                      
                ", array($this->item['serial'], $this->item['model'])
        );

        if (!isset($_POST['send'])) {
			return false;
		}

		$this->form['id']           = Input::getInt($_POST['id']);
		$this->form['name']         = Input::getStr($_POST['name']);
		$this->form['surname']         = Input::getHtml($_POST['surname']);
		$this->form['last_name']         = Input::getHtml($_POST['last_name']);

//        if (empty($this->form['furniture'])) {
//			$this->error['furniture'] = 'Укажите Модель оборудования';
//		}


        if (empty($this->error)) {
//            if(Router::$action == 'edit' && Auth::perm(4)) {
//                    $this->form['to_user_id'] = 12;
//            }
			$id = (empty($this->form['id'])) ? $this->dbAdd() : $this->dbSave();
			if (!empty($id)) {
                return $this->handlerСomplete($id, 'edit');
			} else {
				Alerts::setError('Произошла ошибка.');
			}
		} else {
            Alerts::setError('Все поля должы быть заполнены.');
            return true;
        }
    }

}