<div id="nav">
	<?php echo $this->pageHeader(); ?>
	<div class="navbar-right navbar-btn">
		<?php echo $this->buttonSave(); ?>
	</div>
</div>

<?php
// Сообщения.
echo Alerts::get(); 

$forms = new Forms($this);

// Основное.
echo $forms->group(
	array(
		'label' => 'Основное',
		'items' => array(
			$forms->text(
				array(
					'label'    => 'Название сайта', 
					'name'     => 'name', 
					'required' => true
				)
			)
		)
	)
);

// Уведомления.
// echo $forms->group(
// 	array(
// 		'label' => 'Уведомления о заявках на e-mail',
// 		'items' => array(
// 			$forms->cols(
// 				array(
// 					'cols'  => 4,
// 					'items' => array(					
// 						$forms->text(
// 							array(
// 								'label' => 'Заказы', 
// 								'name'  => 'notice_orders', 
// 							)
// 						),
// 						$forms->text(
// 							array(
// 								'label' => 'Вызов мастера', 
// 								'name'  => 'notice_master', 
// 							)
// 						),
// 						$forms->text(
// 							array(
// 								'label' => 'Обратный звонок', 
// 								'name'  => 'notice_callback', 
// 							)
// 						),
// 						$forms->text(
// 							array(
// 								'label' => 'Вопрос-ответ', 
// 								'name'  => 'notice_faq', 
// 							)
// 						)
// 					)
// 				)
// 			)
// 		)
// 	)
// );

// Отправка e-mail с сайта.
echo $forms->group(
	array(
		'label' => 'Отправка e-mail с сайта',
		'items' => array(
			$forms->cols(
				array(
					'cols'  => 2,
					'items' => array(
						$forms->text(
							array(
								'label' => 'E-mail отправителя', 
								'name'  => 'email', 
							)
						),			
						$forms->text(
							array(
								'label' => 'Имя отправителя', 
								'name'  => 'email_name', 
							)
						)
					)
				)
			),
			$forms->html(
				array(
					'label' => 'Подпись в письме', 
					'name'  => 'email_signature', 
				)
			),
		)
	)
);