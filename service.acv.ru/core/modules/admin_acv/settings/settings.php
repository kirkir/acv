<?php
/** 
 * Администрирование / Настройки
 */
class module_settings extends Backend
{
	public $module = 'settings';
	public $table  = '#__settings';
	public $title  = 'Настройки';	

	public function __construct()
	{
		// Уровень доступа.
		$this->init(3);
	}

    /**
     * Форма
     */
    public function action_index()
    {
		return $this->action_edit(1);
    }

	/**
     * Обработка форм.
     */
	public function handler()
    {
		if (!isset($_POST['send'])) {
			return false;
		} 
		
		$this->form['id']              = 1;	
		$this->form['name']            = Input::getStr($_POST['name'], '');			
		$this->form['email']           = Input::getStr($_POST['email'], '');
		$this->form['email_name'] = Input::getHtml($_POST['email_name'], '');
//		$this->form['email_signature'] = Input::getHtml($_POST['email_signature'], '');

		// Проверка заполнения полей.
		if (empty($this->form['name'])) {
			$this->error['name'] = 'Укажите название сайта';
		}		
		

		if (empty($this->error)) {
			if ($this->dbSave()) {
				Alerts::setSuccess('Изменения успешно сохранены.');	
				return redirect(Router::$url);	
			} else {
				Alerts::setError('Произошла ошибка.');
			}
		}

		return true;
    }
}