<div id="nav">
	<?php echo $this->pageHeader(); ?>
	<div class="navbar-right navbar-btn">
		<?php echo $this->buttonLists(); ?>
	</div>
</div>

<?php 
// Сообщения.
echo Alerts::get();

// Таблица.
$tables = new Tables($this->items);

echo $tables->out(
	array(
		'thead' => array(
			$tables->thCheck(),
			$tables->thId(),
			$tables->thName('Старый URL'),
			$tables->thName('Новый URL'),
			$tables->thText('Комментарий'),
			$tables->thCount('Кол-во переходов'),
			$tables->thDate(),
			$tables->thRemove()
		),
		'tbody' => dirname(__FILE__) . '/tree.tpl'
	)
);