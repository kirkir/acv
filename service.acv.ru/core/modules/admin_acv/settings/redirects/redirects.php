<?php
/** 
 * Администрирование / Редиректы.
 */
class module_redirects extends Backend
{
	public $module = 'redirects';	
	public $table  = '#__redirects';
	public $title  = 'Редиректы';		

	public function __construct()
	{
		// Уровень доступа.
		$this->init(3);
	}

	/**
     * Обработка форм.
     */
    public function handler()
    {
		if (!isset($_POST['send'])) {
			return false;
		} 

		$this->form['id']   = Input::getInt($_POST['id'], 0);
		$this->form['old']  = trim(Input::getStr($_POST['old'], ''), '/');	
		$this->form['new']  = trim(Input::getStr($_POST['new'], ''), '/');		
		$this->form['text'] = Input::getHtml($_POST['text'], '');		

		if (empty($this->form['old'])) {
			$this->error['old'] = 'Укажите URL';
		} elseif (DB::getRow("SELECT `id` FROM `{$this->table}` WHERE (`old` = :old OR `new` = :old) AND `id` <> :id", array('old' => $this->form['old'], 'id' => $this->form['id']))) {
			$this->error['old'] = 'Такой URL уже существует.';
		}

		if (DB::getRow("SELECT `id` FROM `{$this->table}` WHERE `old` = :new AND `id` <> :id", array('new' => $this->form['new'], 'id' => $this->form['id']))) {
			$this->error['new'] = 'Такой URL уже существует.';
		} elseif ($this->form['new'] == $this->form['old']) {
			$this->error['new'] = 'Укажите другой URL';
		}
	
		if (empty($this->error)) {
			$id = (empty($this->form['id'])) ? $this->dbAdd() : $this->dbSave();
			if (!empty($id)) {
				return $this->handlerСomplete($id, 'edit');
			} else {
				Alerts::setError('Произошла ошибка.');
			}
		}

		return true;
    }
}