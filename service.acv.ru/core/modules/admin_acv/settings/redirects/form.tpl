<div id="nav">
	<?php echo $this->pageHeader(); ?>
	<div class="navbar-right navbar-btn">
		<?php echo $this->buttonForms(); ?>
	</div>
</div>

<?php
// Сообщения.
echo Alerts::get(); 

// Инфо.
if (!empty($this->item)) {
	echo $this->blockquote();
}

$forms = new Forms($this);

// Основное.
echo $forms->group(
	array(
		'label' => 'Основное',
		'items' => array(
			$forms->text(
				array(
					'label'    => 'Старый URL', 
					'name'     => 'old', 
					'required' => true
				)
			),
			$forms->text(
				array(
					'label' => 'Новый URL', 
					'name'  => 'new', 
					'required' => true
				)
			),
			$forms->html(
				array(
					'label' => 'Комментарий', 
					'name'  => 'text', 
				)
			)
		)
	)
);