<?php
echo $this->tr(
	array(
		$this->tdCheck(),
		$this->tdId(),
		$this->tdEdit('/' . $this->item['old']),
		$this->tdEdit('/' . $this->item['new']),
		$this->tdText($this->item['text']),
		$this->tdCount(array('value' => $this->item['views'])),
		$this->tdDate(),
		$this->tdRemove()
	)
);