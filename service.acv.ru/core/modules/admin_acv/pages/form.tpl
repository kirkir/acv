<div id="nav">
	<?php echo $this->pageHeader(); ?>
	<div class="navbar-right navbar-btn">
		<?php echo $this->buttonForms(); ?>
	</div>
</div>

<?php
// Сообщения.
echo Alerts::get(); 

// Инфо.
if (!empty($this->item)) {
	echo $this->blockquote(
		array(
			'URL' => Html::a(Tree::url($this->table, $this->item['id']))
		)
	);
}

$forms = new Forms($this);
?>

<div role="tabpanel">
	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#main" aria-controls="main" role="tab" data-toggle="tab">Основное</a></li>
<!--		<li role="presentation" class=""><a href="#blockstab" aria-controls="blockstab" role="tab" data-toggle="tab">Дополнительные блоки</a></li>-->
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="main" style="padding-top: 20px;">
			<?php
			// Основное.
			echo $forms->group(
				array(
					'label' => '',
					'items' => array(
						$forms->selectTree(
							array(
								'label'            => 'Родительская страница', 
								'name'             => 'parent',
								'options_first'    => '',
								'options'          => DB::getAll("SELECT * FROM `{$this->table}` ORDER BY `sort`"),
								'options_disabled' => @$this->item['id']
							)
						),	
						$forms->text(
							array(
								'label'    => 'Название', 
								'name'     => 'name', 
								'required' => true
							)
						),
						$forms->text(
							array(
								'label' => 'ЧПУ', 
								'name'  => 'sef'
							)
						)
					)
				)
			);

			// SEO.
			echo $forms->group(
				array(
					'label' => 'SEO',
					'cols'  => 2,
					'items' => array(
						$forms->text(
							array(
								'label' => 'Заголовок (title)', 
								'name'  => 'title'
							)
						),		
						$forms->text(
							array(
								'label' => 'Описание (description)', 
								'name'  => 'description'
							)
						),		
						$forms->text(
							array(
								'label' => 'Ключевые слова (keywords)',
								'name'  => 'keywords'
							)
						),
						$forms->select(
							array(
								'label'   => 'Индексирование страницы (мета-тег robots)', 
								'name'    => 'robots', 
								'options' => Config::$robots
							)
						)
					)
				)
			);

			// Содержание.
			echo $forms->group(
				array(
					'label' => 'Содержание',
					'items' => array(
						$forms->text(
							array(
								'label' => 'Заголовок H1', 
								'name'  => 'h1'
							)
						),
						$forms->html(
							array(
								'label' => 'Основной текст (верх)', 
								'name'  => 'text'
							)
						),
						//$forms->html(
						//	array(
						//		'label' => 'Основной текст (низ)', 
						//		'name'  => 'text_2'
						//	)
						//)
					)
				)
			);

//			echo $forms->group(
//				array(
//					'label' => 'Изображения',
//					'items' => array(
//						$forms->files(
//							array(
//								'using' => 'thumb',
//							)
//						),
//						$forms->files(
//							array(
//								'label' => 'Иконка (26x25px)',
//								'using' => 'icon'
//							)
//						)
//					)
//				)
//			);
			?>

			<div class="legend-group">
				<legend>Дополнительное содержание</legend>
				<div class="legend-body" id="texts">
					<?php foreach (DB::getAll("SELECT * FROM `{$this->table}_items` WHERE `page_id` = ? ORDER BY `sort`", @$this->item['id']) as $row): ?>
					<div class="well">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<i class="move ico ico-move"></i>

						<?php
						$items_forms = new Forms();
						$items_forms->instance->files = array(
							'thumb_' . $row['id'] => array(
								'type'     => 0,
								'multiple' => true,
								'unique'   => true
							)
						);
						Files::init($items_forms->instance->files, $this->module, $this->item['id']);

						echo $items_forms->text(
							array(
								'label' => 'Заголовок H2',
								'name' => 'data[' . $row['id'] . '][h2]',
								'value' => $row['h2'],

							)
						);
						echo $items_forms->html(
							array(
								'id'  => 'more_text_' . $row['id'],
								'name' => 'data[' . $row['id'] . '][text]',
								'value' => $row['text'],
							)
						);

						echo $items_forms->files(
							array(
								'using' => 'thumb_' . $row['id'],
							)
						)
						?>
						<script>
							CKEDITOR.replace("more_text_<?php echo $row['id']; ?>");
						</script>
					</div>
					<?php endforeach; ?>
				</div>
			</div>

<!--			<div class="form-group">-->
<!--				<button type="button" class="btn btn-primary"  onclick="addText(); return false;">Добавить текст</button>-->
<!--			</div>-->

			<script>
			$("#texts").sortable({
				handle: '.move'
			});

			function addText() {
				$.ajax({
					type: 'POST',
					url: '<?php echo get_url_admin($this->module . '/addtext'); ?>',
					success: function(data){
						$('#texts').append(data);

						$("#texts").sortable({
							handle: '.move'
						});
					}
				});
			}
			</script>

			<?php
			// Параметры публикации.
			echo $forms->group(
				array(
					'label' => 'Параметры публикации',
					'items' => array(
						$forms->select(
							array(
								'label'         => 'Шаблон вывода страницы', 
								'name'          => 'themes', 
								'options_first' => '',
								'options'       => list_files(ROOT_DIR . '/themes/site/', true)
							)
						),
						$forms->Checkbox(
							array(
								'label' => 'Публиковать на сайте', 
								'name'  => 'approve'
							)
						)
					)
				)
			);

//			echo $forms->group(
//				array(
//					'label' => 'Вывести анонс в меню',
//					'items' => array(
//						$forms->listCheckbox(
//							array(
//								'name'  => 'in_menu[]',
//								'options' => array(1 => 'Идея', 2 => 'Макет', 3 => 'Печать', /* 4 => 'Web', */ 5 => 'Контакты')
//							)
//						)
//					)
//				)
//			);
			
			
			?>
		</div>
		<div role="tabpanel" class="tab-pane" id="blockstab" style="padding-top: 20px;">
			<div class="legend-group">
				<div class="legend-body" id="blocks">
					<?php foreach (DB::getAll("SELECT * FROM `{$this->table}_blocks` WHERE `page_id` = ? ORDER BY `sort`", @$this->item['id']) as $row): ?>
					<div class="well">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<i class="move ico ico-move"></i>
						<?php
						?>
						<script>
							CKEDITOR.replace("text_<?php echo $row['id']; ?>");
						</script>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
			
			<script>
			$("#blocks").sortable({
				handle: '.move'
			});

			function addBlock() {
				$.ajax({
					type: 'POST',
					url: '<?php echo get_url_admin($this->module . '/addblocks'); ?>',
					success: function(data){
						$('#blocks').append(data);

						$("#blocks").sortable({
							handle: '.move'
						});
					}
				});
			}
			</script>	
			
<!--			<div class="form-group">-->
<!--				<button type="button" class="btn btn-primary"  onclick="addBlock(); return false;">Добавить блок</button>-->
<!--			</div>-->
		</div>
	</div>
</div>