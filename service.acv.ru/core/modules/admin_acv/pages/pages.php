<?php
/** 
 * Администрирование / Страницы.
 */
class module_pages extends Backend
{
	public $module = 'pages';
	public $table  = '#__pages';
	public $title  = 'Страницы';			

	public $files = array(
		'thumb' => array(
			'type'     => 0,     
			'multiple' => true,  
			'unique'   => true
		),
		'icon' => array(
			'type'     => 0,     
			'multiple' => false,  
			'unique'   => true
		)
	);

	public function __construct()
	{
		// Уровень доступа.
		$this->init(3);
	}

	/**
     * Обработка форм.
     */
    public function handler()
    {
		if (!isset($_POST['send'])) {
			return false;
		} 

		$this->form['id']          = Input::getInt($_POST['id']);
		$this->form['parent']      = Input::getInt($_POST['parent']);
		$this->form['name']        = Input::getStr($_POST['name']);	
		$this->form['h1']          = Input::getStr($_POST['h1'], '');	
		$this->form['sef']         = Input::getSef($_POST['sef'], $this->form['name']);	
		$this->form['title']       = Input::getStr($_POST['title']);			
		$this->form['description'] = Input::getStr($_POST['description']);
		$this->form['keywords']    = Input::getStr($_POST['keywords']);
		$this->form['text']        = Input::getHtml($_POST['text']);
		$this->form['text_2']      = Input::getHtml($_POST['text_2']);
		$this->form['approve']     = Input::getBool($_POST['approve']);	
		$this->form['in_menu']     = Input::getListInt($_POST['in_menu']);	
		$this->form['robots']      = Input::getStr($_POST['robots']);
		$this->form['themes']      = Input::getStr($_POST['themes']);

		if (empty($this->form['name'])) {
			$this->error['name'] = 'Укажите название';
		}
		
		if (empty($this->form['sef'])) {
			$this->error['sef'] = 'Укажите ЧПУ';
		} elseif (DB::getRow("SELECT `id` FROM `{$this->table}` WHERE `sef` = ? AND `id` <> ? AND `parent` = ? ", array($this->form['sef'], $this->form['id'], $this->form['parent']))) {
			$this->error['sef'] = 'Такой ЧПУ уже существует';
		}
	
		if (empty($this->error)) {
			$id = (empty($this->form['id'])) ? $this->dbAdd() : $this->dbSave();
			if (!empty($id)) {
				// Сохранение доп текстов
				$no_delete = array();
				if (!empty($_POST['data'])) {
					$sort = 1;
					foreach ($_POST['data'] as $item_id =>$row) {
						$item_id = Input::cleanInt($item_id);
						DB::SET("
							UPDATE 
								`{$this->table}_items` 
							SET
								`page_id` = :page_id,
								`h2`      = :h2,
								`text`    = :text,
								`sort`    = :sort
							WHERE
								`id` = :id
							", array(
								'page_id' => $id,
								'h2'      => Input::getStr($row['h2']),
								'text'    => Input::getHtml($row['text']),
								'sort'    => $sort++,
								'id'      => $item_id,
							)
						);	

						$no_delete[] = $item_id;

						Files::update(
							array(
								'thumb_' . $item_id => array(
									'type'     => 0,     
									'multiple' => true,  
									'unique'   => true
								)
							),
							'pages', 
							$id
						);
					}
				}	

				if (!empty($no_delete)) {
					DB::set("DELETE FROM `{$this->table}_items` WHERE `page_id` = ? AND `id` NOT IN(" . implode(',', $no_delete) . ")", $id);
				} else {
					DB::set("DELETE FROM `{$this->table}_items` WHERE `page_id` = ?", $id);
				}		

				// Сохранение доп. блоков
				$no_delete = array();
				if (!empty($_POST['blocks'])) {
					$sort = 1;
					foreach ($_POST['blocks'] as $item_id =>$row) {
						$item_id = Input::cleanInt($item_id);
						DB::SET("
							UPDATE 
								`{$this->table}_blocks` 
							SET
								`page_id` = :page_id,
								`color`   = :color,
								`text`    = :text,
								`sort`    = :sort
							WHERE
								`id` = :id
							", array(
								'page_id' => $id,
								'color'   => Input::getInt($row['color']),
								'text'    => Input::getHtml($row['text']),
								'sort'    => $sort++,
								'id'      => $item_id,
							)
						);	

						$no_delete[] = $item_id;
					}
				}	
				if (!empty($no_delete)) {
					DB::set("DELETE FROM `{$this->table}_blocks` WHERE `page_id` = ? AND `id` NOT IN(" . implode(',', $no_delete) . ")", $id);
				} else {
					DB::set("DELETE FROM `{$this->table}_blocks` WHERE `page_id` = ?", $id);
				}	
	
				return $this->handlerСomplete($id, 'edit');
			} else {
				Alerts::setError('Произошла ошибка.');
			}
		}

		return true;
    }
	
    public function action_addtext()
    {
		$item_id = DB::add("
			INSERT INTO 
				`{$this->table}_items` 
			SET
				`page_id`  = 0,
				`h2`       = '',
				`text`     = '',
				`sort`     = ''
		");

		$this->files = array(
			'thumb_' . $item_id => array(
				'type'     => 0,     
				'multiple' => true,  
				'unique'   => true
			)
		);
		$forms = new Forms($this);

		return 
			'<div class="well">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<i class="move ico ico-move"></i>
				' . 
				$forms->text(
					array(
						'label' => 'Заголовок H2',
						'name' => 'data[' . $item_id . '][h2]'
					)
				) .
				$forms->html(
					array(
						'id'  => 'more_text_' . $item_id,
						'name' => 'data[' . $item_id . '][text]'
					)
				) .
				$forms->files(
					array(
						'using' => 'thumb_' . $item_id,
					)
				) . '
			</div>
			<script>
			CKEDITOR.replace("more_text_' . $item_id . '");
			</script>';
	}   

	public function action_addblocks()
    {
		$item_id = DB::add("
			INSERT INTO 
				`{$this->table}_blocks` 
			SET
				`page_id`    = 0,
				`color`      = '',
				`text`       = '',
				`sort`       = ''
		");
		$forms = new Forms($this);
		return 
			'<div class="well">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<i class="move ico ico-move"></i>
				' . 
				$forms->select(
					array(
						'label'   => 'Цвет',
						'name'    => 'blocks[' . $item_id . '][color]',
						'options' => Config::$blocks_colors
					)
				) . 
				$forms->html(
					array(
						'id'  => 'text_' . $item_id,
						'name' => 'blocks[' . $item_id . '][text]'
					)
				) . '
			</div>
			<script>
				CKEDITOR.replace("text_' . $item_id . '");
			</script>';
	}
}