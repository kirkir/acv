<?php
echo $this->tr(
	array(
		$this->tdCheck(),
		$this->tdId(),
		$this->tdName(
			array(
				'text' => preview('', $this->item['text'], 200)
			)
		),
		$this->tdSef(),
		$this->tdDate(),
		$this->tdApprove(),
		$this->tdRemove()
	)
);