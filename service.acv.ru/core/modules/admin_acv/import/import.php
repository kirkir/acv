<?php
/** 
 * Администрирование / Импорт.
 */
class module_import extends Backend
{
	public $module = 'import';
	public $title  = 'Импорт';
	public $file_path  = 'uploads/import/';

	public $files = array(
        'furniture' => array(
            'type'     => 1,
            'multiple' => false,
            'unique'   => false
        ),
        'works' => array(
            'type'     => 1,
            'multiple' => false,
            'unique'   => false
        ),
        'parts' => array(
            'type'     => 1,
            'multiple' => false,
            'unique'   => false
        )
	);
	
	public function __construct()
	{
        // Уровень доступа.
		$this->init(4);
	}

    public function action_index()
    {
        return redirect();
    }

	public function action_furniture()
    {
        $this->upload(1, 'furniture', 'оборудования', '#__furniture');

        // Вывод в шаблон.
		$this->subtitle = 'список';
		$this->content  = $this->render(Router::$path . '/furniture.tpl');
		return $this->display('page.tpl');
    }

    public function action_works()
    {
        $this->upload(2, 'works', 'работ', '#__works');

        // Вывод в шаблон.
        $this->subtitle = 'список';
        $this->content  = $this->render(Router::$path . '/works.tpl');
        return $this->display('page.tpl');
    }

    public function action_parts()
    {
        $this->upload(3, 'parts', 'чапчастей', '#__parts');

        // Вывод в шаблон.
        $this->subtitle = 'список';
        $this->content  = $this->render(Router::$path . '/parts.tpl');
        return $this->display('page.tpl');
    }

    public function upload($item = 0, $mod = '', $import_word = '', $table)
    {
        // Загрузка прикреплённых файлов.
        if (!empty($this->files)) {
            Files::update($this->files, $this->module, $item);
            if(@$_POST['files_'.$mod.'_remove']) {
                Alerts::setInfo('Файл удален!');
            } elseif(@$_FILES['files_' . $mod]['size']) {
                $file = Files::get($this->module, $item, $mod);
                $file = array_shift($file);
                Alerts::setInfo('Файл обновлен!');
                $this->import('Импорт '.$import_word.' успешно завершен.', $this->file_path . $file['filename'], $table);
            } else {
                false;
            }
        }

        Files::init($this->files, $this->module, $item);
    }

    public function import($flashMessage = '', $file_path = '', $table)
    {
        require_once PLUGINS_DIR . '/PHPExcel/Classes/PHPExcel.php';
        $objPHPExcel = PHPExcel_IOFactory::load($file_path);
        $objPHPExcel->setActiveSheetIndex(0);
        $aSheet = $objPHPExcel->getActiveSheet();

        $array = array();
        //получим итератор строки и пройдемся по нему циклом
        foreach($aSheet->getRowIterator() as $row){
            //получим итератор ячеек текущей строки
            $cellIterator = $row->getCellIterator();
            //пройдемся циклом по ячейкам строки
            //этот массив будет содержать значения каждой отдельной строки
            $item = array();
            foreach($cellIterator as $cell){
                //заносим значения ячеек одной строки в отдельный массив
                array_push($item, $cell->getCalculatedValue());
            }
            //заносим массив со значениями ячеек отдельной строки в "общий массв строк"
            array_push($array, $item);
        }

//        DB::set("TRUNCATE `{$table}`");
        foreach ($array as $row) {
            if($row[0]) {
                if (!DB::getRow("SELECT `id` FROM {$table} WHERE `name` = ?", Input::getStr($row[0]))) {
                    if($table == '#__works') {
                        DB::add("INSERT INTO `{$table}` SET `name` = ?, `hour` = ?, `price` = ?, `date_add` = UNIX_TIMESTAMP()",
                            array(Input::getStr($row[0]), Input::getFloat($row[1]), Input::getFloat($row[2])));
                    } else {
                        DB::add("INSERT INTO `{$table}` SET `name` = ?, `sku` = ?, `price` = ?, `date_add` = UNIX_TIMESTAMP()",
                            array(Input::getStr($row[0]), Input::getStr($row[1]), Input::getFloat($row[2])));
                    }
                }
            }
        }
        Alerts::setSuccess($flashMessage);
    }
}