<div id="nav">
    <?php echo $this->pageHeader(); ?>
    <div class="navbar-right navbar-btn">
        <button class="btn btn-default" type="submit" name="send" value="save">Сохранить</button>
        <a class="btn btn-default" href="http://sevlestrans.ru/admin_acv/import/">Закрыть</a>
    </div>
</div>

<?php
// Сообщения.
echo Alerts::get();

$forms = new Forms($this);

echo $forms->group(
    array(
        'label' => 'Загрузка файлов',
        'items' => array(
            $forms->files(
                array(
                    'label' => 'Excel файл работ',
                    'using' => 'works'
                )
            )
        )
    )
); ?>