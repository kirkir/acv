<?php
echo $this->tr(
	array(
		$this->tdCheck(),
		$this->tdEdit($this->item['serial']),
		$this->tdEdit(Users::getAuthorName($this->item['name_service'])),
		$this->tdDate(),
	)
);