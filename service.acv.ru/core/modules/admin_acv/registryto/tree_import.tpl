<?php
echo $this->tr(
    array(
        $this->tdId(),
        $this->tdEdit($this->item['sku']),
        $this->tdEdit($this->item['name']),
        $this->tdEdit($this->item['base']),
        $this->tdEdit(($this->item['more']) ? $this->item['more'] : ' 0 '),
        $this->tdEdit($this->item['electric']),
    )
);