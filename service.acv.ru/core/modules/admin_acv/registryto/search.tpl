<div id="nav">
	<?php echo $this->pageHeader(); ?>
	<div class="navbar-right navbar-btn">
		<?php echo $this->buttonClose()?>
	</div>
</div>
<br><br><br>
<?php
// Сообщения.
echo Alerts::get(); 

// Инфо.
if (!empty($this->item)) {
	echo $this->blockquote(
		array(
//			'URL' => Html::a(Tree::url($this->table, $this->item['id'], $this->module))
		)
	);
}
?>



<?php
$forms = new Forms($this);

echo $forms->group(
	array(
		'label' => '',
		'items' => array(
			$forms->text(
				array(
					'label'    => 'Серийный №',
					'name'     => 'serial',
					'required' => true
				)
			),
			$forms->select(
				array(
					'label' => 'Наименование из списка',
					'name'  => 'name',
					'options' => DB::getAll("SELECT * FROM `#__furniture` ORDER BY `name`"),
					'required' => true
				)
			)
		)
	)
);

?>

<div>
	<?php echo $this->buttonSave('Найти'); ?>
</div>

<?php if (@$this->data_ship): ?>
    <br>
    <br>
    <br>
    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
        <b>Оборудование продано</b><br><br>
        <p>Органицация: <?php echo $this->data_ship['contragent']; ?></p>
        <p>Дата: <?php echo date('d.m.Y h:i:s', $this->data_ship['date']); ?></p>
    </div>

    <?php if (@$this->data_pusk): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
            <b>Пуск оборудования</b><br><br>
            <p>Дата: <?php echo date('d.m.Y h:i:s', $this->data_pusk['date_add']); ?></p>
            <p>Компания: <?php echo Users::getAuthorName($this->data_pusk['user_id']); ?></p>
            <p>Замечания: <?php echo $this->data_pusk['comment']; ?></p>
            <p><a href="<?php echo get_url_admin('registrylist/edit/' . $this->data_pusk['id']);?>">Посмотреть лист регистрации котла</a> | <a href="<?php echo get_url_admin('registrylist/generatePdf/?id=' . $this->data_pusk['id']);?>">В PDF</a></p>
        </div>
    <?php endif;?>

    <?php if ($this->service): ?>
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
            <b>Рекламация оборудования</b><br><br>
            <table class="table_service_r">
                <tr>
                    <th width="14%">Дата</th>
                    <th width="43%">Описание дефекта</th>
                    <th width="43%">Вид выполненных работ</th>
                </tr>
            <?php foreach ($this->service as $service): ?>
                <tr>
                    <td><?php echo date('Y.m.d', $service['date_work']); ?></td>
                    <td><?php echo $service['text']; ?></td>
                    <td>
                        <ul>
                            <?php
                            $works =  DB::getAll("SELECT * FROM `#__works` WHERE FIND_IN_SET(`id`, ?)", array($service['works_ids']));
                            foreach ($works as $work) { ?>
                            <li><?php echo $work['name']; ?></li>
                            <?php }
                            ?>
                        </ul>
                    </td>
                </tr>
            <?php endforeach; ?>
            </table>
            <br>
            <p>
                <a href="<?php echo get_url_admin('demand');?>">Добавить рекламацию</a>
<!--                <a href="--><?php //echo get_url_admin('registrylist/generatePdf/?id=' . $this->data_pusk['id']);?><!--">Добавить данные о сервисном обслуживании</a>-->
            </p>
        </div>
    <?php endif; ?>
<?php else: ?>
    <br>
    <br>
    <br>
    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
        По вашему запросу ничего не найдено!
    </div>
<?php endif; ?>
