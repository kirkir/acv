<?php
/**
 */
class module_registryto extends Backend
{
    public $module = 'registryto';
    public $table  = '#__registry_to';
    public $title  = 'Форма регистрации ТО';

    public $files = array(
        'actto' => array(
            'type'     => 1,
            'multiple' => false,
            'unique'   => true
        )
    );

    public function __construct()
    {
        // Уровень доступа.
        $this->init(3);
    }

    public function action_index()
    {
        if (!Auth::perm(4)) {
            return redirect(Router::$url . '/add');
        }
        $this->handlerActions();

        $this->paginator = new Paginator(get_url(Router::$url), 20);
        $this->items = $this->paginator->process("SELECT * FROM `{$this->table}` ORDER BY `date_add` DESC");

        // Вывод в шаблон.
        $this->subtitle = 'список';
        $this->content  = $this->render(Router::$path . '/list.tpl');
        return $this->display('page.tpl');
    }


    /**
     * Обработка форм.
     */
    public function handler()
    {
        if (!isset($_POST['send'])) {
            return false;
        }

        $this->form['id']                                  = Input::getInt($_POST['id']);
        $this->form['serial']                              = Input::getStr($_POST['serial']);
        $this->form['name_service']                        = Input::getInt($_POST['name_service']);
        $this->form['date_add']                            = Input::getTime($_POST['date_add']);

        if (empty($this->form['serial'])) {
            $this->error['serial'] = 'Обязательно для заполнения';
        }

        if (@$this->item['serial'] != $this->form['serial'] && DB::getRow("SELECT `id` FROM `#__registry_to` WHERE `serial` = ? LIMIT 1", $this->form['serial'])) {
            $this->error['serial'] = 'Такой серийный номер уже существует';
        }

        if (empty($this->form['name_service'])) {
            $this->error['name_service'] = 'Обязательно для заполнения';
        }

        if (empty($this->form['date_add'])) {
            $this->error['date_add'] = 'Обязательно для заполнения';
        }


        if (empty($this->error)) {
            $id = (empty($this->form['id'])) ? $this->dbAdd() : $this->dbSave();
            if (!empty($id)) {
                return $this->handlerСomplete($id, 'edit');
            } else {
                Alerts::setError('Произошла ошибка.');
            }
        } else {
            Alerts::setError('Все поля должы быть заполнены.');
            return true;
        }
    }
}