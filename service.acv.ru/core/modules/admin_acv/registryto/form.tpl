<style>
    .border-control-group:hover .help-block {
        color: #428bca;
    }
    .help-block {
        color: inherit;
    }
</style>

<div id="nav" style="margin-bottom: 44px";>
	<?php echo $this->pageHeader(); ?>
	<div class="navbar-right navbar-btn" style="clear: both; float: none !important;">
		<?php
            echo $this->buttonForms();
        ?>
	</div>
</div>

<?php
// Сообщения.
echo Alerts::get(); 

// Инфо.
if (!empty($this->item)) {
	echo $this->blockquote(
		array(
//			'URL' => Html::a(Tree::url($this->table, $this->item['id'], $this->module))
		)
	);
}
?>



<?php
$forms = new Forms($this);

echo $forms->group(
    array(
        'label' => '',
        'items' => array(
            $forms->text(
                array(
                    'label' => 'Серийный № изделия',
                    'name'  => 'serial',
                    'required' => true
                )
            ),
            $forms->datetime(
                array(
                    'label' => 'Дата ТO',
                    'name'  => 'date_add',
                    'required' => true
                )
            ),
            $forms->select(
                array(
                    'label' => 'Наименование сервиса',
                    'name'  => 'name_service',
                    'options_first'    => '',
                    'options' => DB::getAll("SELECT * FROM `#__users` WHERE category != 283 ORDER BY name")
                )
            ),
        )
    )
);

echo $forms->group(
    array(
        'label' => 'Загрузка файлов',
        'items' => array(
            $forms->files(
                array(
                    'label' => 'Акт в PDF или JPG',
                    'using' => 'actto',
                )
            ),
        )
    )
); ?>


