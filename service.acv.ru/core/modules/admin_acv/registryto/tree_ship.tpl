<?php
echo $this->tr(
    array(
        $this->tdId(),
        $this->tdEdit($this->item['serial']),
        $this->tdEdit(date('d.m.Y h:i:s', $this->item['date'])),
        $this->tdEdit($this->item['sku']),
        $this->tdEdit($this->item['contragent']),
    )
);