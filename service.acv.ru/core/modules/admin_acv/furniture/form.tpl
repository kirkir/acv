<style>
    .border-control-group:hover .help-block {
        color: #428bca;
    }
    .help-block {
        color: inherit;
    }
</style>

<div id="nav" style="margin-bottom: 44px";>
	<?php echo $this->pageHeader(); ?>
	<div class="navbar-right navbar-btn" style="clear: both; float: none !important;">
		<?php
            echo $this->buttonForms();
        ?>
	</div>
</div>

<?php
// Сообщения.
echo Alerts::get(); 

// Инфо.
if (!empty($this->item)) {
	echo $this->blockquote(
		array(
//			'URL' => Html::a(Tree::url($this->table, $this->item['id'], $this->module))
		)
	);
}
?>

<?php
$forms = new Forms($this);

echo $forms->group(
    array(
        'label' => '',
        'items' => array(
            $forms->text(
                array(
                    'label'    => 'Название',
                    'name'     => 'name',
                    'required' => true
                )
            ),
            $forms->text(
                array(
                    'label' => 'Артикул',
                    'name'  => 'sku',
                    'required' => true
                )
            ),
            $forms->text(
                array(
                    'label' => 'Цена',
                    'name'  => 'price',
                )
            ),
        )
    )
);

