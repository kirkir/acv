<?php
/**
 * Администрирование / Отчеты.
 */
class module_furniture extends Backend
{
    public $module = 'furniture';
    public $table  = '#__furniture';
    public $title  = 'Основное оборудование';

    public $files = array(
//        'ship' => array(
//            'type'     => 1,
//            'multiple' => false,
//            'unique'   => false
//        ),
    );

    public function __construct()
    {
        // Уровень доступа.
        $this->init(4);
    }

    public function action_index()
    {
        $this->handlerActions();

        $this->paginator = new Paginator(get_url(Router::$url), 3000);
        $this->items = $this->paginator->process("SELECT * FROM `{$this->table}` ORDER BY `name`");

        // Вывод в шаблон.
        $this->subtitle = 'список';
        $this->content  = $this->render(Router::$path . '/list.tpl');
        return $this->display('page.tpl');
    }


    /**
     * Обработка форм.
     */
    public function handler()
    {
        if (!isset($_POST['send'])) {
            return false;
        }

        $this->form['id']       = Input::getInt($_POST['id']);
        $this->form['name']    = Input::getStr($_POST['name']);
        $this->form['sku']   = Input::getStr($_POST['sku']);
        $this->form['price'] = Input::getPrice($_POST['price']);

        if (empty($this->form['name'])) {
            $this->error['name'] = 'Обязательно для заполнения';
        }

        if (empty($this->form['sku'])) {
            $this->error['sku'] = 'Обязательно для заполнения';
        }

        if (empty($this->error)) {

            $id = (empty($this->form['id'])) ? $this->dbAdd() : $this->dbSave();

            if (!empty($id)) {
                return $this->handlerСomplete($id, 'edit');
            } else {
                Alerts::setError('Произошла ошибка.');
            }
        } else {
            Alerts::setError('Все поля должы быть заполнены.');
            return true;
        }
    }
}