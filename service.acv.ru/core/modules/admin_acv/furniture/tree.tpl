<?php
echo $this->tr(
	array(
		$this->tdCheck(),
		$this->tdEdit($this->item['name']),
		$this->tdEdit($this->item['sku']),
		$this->tdEdit($this->item['price']),
		$this->tdDate(),
        $this->tdRemove()
    )
);