<div id="nav">
	<?php echo $this->pageHeader(); ?>
	<div class="navbar-right demand navbar-btn">
		<?php
            if(Auth::perm(4)) {
                echo
                    $this->buttonAction(),
                    $this->buttonAdd();
            }
        ?>
	</div>
</div>
<br><br><br>
<?php
// Сообщения.
echo Alerts::get();

// Таблица.
$tables = new Tables($this->items);
$tables->imageModule = $this->module;

echo $tables->out(
	array(
		'thead' => array(
			$tables->thCheck(),
			$tables->thText('Название'),
			$tables->thText('Артикул'),
			$tables->thText('Цена'),
			$tables->thDate(),
			$tables->thRemove()
		),
		'tbody' => dirname(__FILE__) . '/tree.tpl'
	)
);
//echo $this->paginator->display;
?>
