<?php
/**
 * Ошибка 404.
 */
class module_404 extends Frontend
{
    public function action_index()
    {
		header('HTTP/1.0 404 Not Found');

		if ($this->page = Pages::get('404')) {
			// Установка мета тегов и вывод в шаблон.
			$this->setMeta($this->page);
			$this->content = $this->render(dirname(__FILE__) . '/view.tpl');
			return $this->display('page.tpl');				
			return 'Страница не найдена (ошибка 404)';
		}	
    }
}