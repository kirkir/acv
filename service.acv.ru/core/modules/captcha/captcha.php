<?php
/**
 * Капча.
 */
class module_captcha
{
	/**
	 * Ключ в сессии.
	 * 
     * @var string
	 */
	public $session_key = 'captcha';	
	
	/**
	 * Текст ошибки проверки введенного кода (после метода check).
	 * 
     * @var string
	 */
	public $error = null;

	/**
	 * Изображение.
	 * 
     * @return void
	 */
	public function action_index()
    {
		include_once dirname(__FILE__) .  '/kcaptcha/kcaptcha.php';
		$captcha = new KCAPTCHA();
		$_SESSION[$this->session_key] = $captcha->getKeyString();
	}

	/**
	 * Проверка введённых символов.
	 * 
     * @return bool
	 */
	public function check()
    {
		if (empty($_SESSION[$this->session_key])) {
			$this->error = 'В Вашем браузере отключена поддержка cookie';
			return false;
		} elseif (empty($_POST[$this->session_key])) {
			$this->error = 'Введите код с картинки';
			return false;
		} elseif ($_SESSION[$this->session_key] != $_POST[$this->session_key]) {
			$this->error = 'Неверный код';
			return false;
		} else {
			return true;
		}
	}
}