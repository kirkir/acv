<?php
/**
 * Автоматическая генерация превью и заглушек.
 */
class module_uploads
{
    public function action_call($module, $args)
    {
		if (!empty($module)) {
			$config = Config::$thumbs;
			
			// Добовляем в конфиг размеры для админки.
			foreach ($config as $i => $row) {
				$config[$i]['155x130'] = false;
				$config[$i]['80x0'] = false;
			}		

			$dir = ROOT_DIR . '/uploads/' . $module;

			if ($module === 'caps') {
				// Генератор заглушек.
				if (!empty($args[0]) && empty($args[1])) {
					$name  = Input::cleanFilename($args[0]);
					$ext   = mb_strtolower(mb_substr(mb_strrchr($name, '.'), 1));
					$size  = basename($name, '.' . $ext);
					$sizes = explode('x', $size);

					if ($ext === 'png' && count($sizes) == 2) {
						foreach ($config as $row) {
							if (array_key_exists($size, $row)) {
								try {
									$image = new Images(ROOT_DIR . Config::$caps);
									$image->thumb($sizes[0], $sizes[1]);
									$image->saveOut($dir . '/' . $size . '.png');	
									exit();
								} catch (Exception $e) {
									error_log($e, 0);
								}
							}
						}
					}
				}
			} elseif (array_key_exists($module, $config)) {
				if (empty($args[1])) {
					// Запрос на оригинальное изображение.
					$file = Input::cleanFilename($args[0], '');
					if (file_exists($dir . '/sources/' . $file)) {
						$file = Input::cleanFilename($args[0], '');
						copy($dir . '/sources/' . $file, $dir . '/' . $file);
						readfile($dir . '/' . $file);
						exit();					
					}
				} else {
					// Запрос на превью.
					$size  = $args[0];
					$sizes = explode('x', $size);
					$file  = Input::cleanStr(@$args[1], '');

					if (!empty($file) && isset($config[$module][$size]) && file_exists($dir . '/sources/' . $file) && count($sizes) == 2) {
						if (!is_dir($dir . '/' . $size)) {
							mkdir($dir . '/' . $size, 0777, true);
						}

						try {	
							$image = new Images($dir . '/sources/' . $file);
							$image->thumb($sizes[0], $sizes[1]);
							$image->saveOut($dir . '/' . $size . '/' . $file);	
							exit();
						} catch (Exception $e) {
							error_log($e, 0);
						}		
					}
				}
			}
		}

		return Router::get('404');
	}
}