<?php
/** 
 * Инициализация.
 */

// Системные дериктории.
define('ROOT_DIR',    dirname(__FILE__));
define('CORE_DIR',    ROOT_DIR . '/core');
define('MOD_DIR',     CORE_DIR . '/modules');
define('PLUGINS_DIR', CORE_DIR . '/plugins');  
define('THEMES_DIR',  ROOT_DIR . '/themes');
define('UPLOADS',     ROOT_DIR . '/uploads');

// Конфиг.
require_once CORE_DIR . '/config.php';

// Настройки вывода ошибок.
error_reporting(E_ALL);
ini_set('display_errors', Config::$debug); 
ini_set('log_errors', 'On');
ini_set('error_log', CORE_DIR . '/php.log');

// Локаль.
setlocale(LC_ALL, 'ru_RU');
date_default_timezone_set('Europe/Moscow');
header('Content-type: text/html; charset=utf-8');

mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');
mb_http_output('UTF-8');
mb_language('uni');

// Набор функций.
require_once CORE_DIR . '/function.php';

// Контроль доступа
session_start();
Auth::init();