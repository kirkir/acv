    <!DOCTYPE html>
<html lang="ru-RU">
	<head>
		<?php require dirname(__FILE__) . '/.head.tpl'; ?>
	</head>
	<body>
		<nav id="header" class="navbar navbar-inverse" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" target="_blank" href="<?php echo get_url(); ?>"><img width="80" src="/themes/admin/img/logo_b.png" alt=""></a>
<!--					<a class="navbar-brand" target="_blank" href="--><?php //echo get_url(); ?><!--">--><?php //echo Settings::get('name'); ?><!--</a>-->
				</div>

				<div class="collapse navbar-collapse">
					<?php include_once(dirname(__FILE__) . '/.menu.tpl'); ?>
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="<?php echo (Auth::perm(4)) ? get_url_admin('users/edit/' . Auth::$user['id']) : '#'; ?>">
								<i class="ico ico-user"></i>
								<?php echo no_empty(Auth::$user['login'], Auth::$user['name']); ?>
							</a>
						</li>
						<li><a href="<?php echo get_url_admin('login/out'); ?>"><i class="ico ico-exit"></i> Выход</a></li>
					</ul>
				</div>
			</div>
		</nav>

		<div class="container">
			<div class="row">
				<!-- <aside class="col-md-3 side-left">
					<?php  echo @$this->left; ?>

					<ul class="nav" id="left-menu">
						<?php include_once(dirname(__FILE__) . '/menu_left.tpl'); ?>
					</ul>
					<script>
					$(function($){
						var menu = $('#top-menu').clone();
						$(menu).find('*')
							.removeClass('dropdown')
							.removeClass('dropdown-menu')
							.removeClass('dropdown-toggle');

						$(menu).find('ul').addClass('nav');
						$(menu).find('.caret').remove();

						$('#left-menu').append($(menu).html());
					});

					$(function($){
						var menu = $('#left-menu #top-menu2');
						$(menu).find('*')
							.removeClass('dropdown')
							.removeClass('dropdown')
							.removeClass('dropdown-menu')
							.removeClass('dropdown-toggle');

						$(menu).find('ul').addClass('nav');
						$(menu).find('.caret').remove();

						$('#left-menu').text('');
						$('#left-menu').append($(menu).html());
					});
					</script>
				</aside> -->
				<section class="col-md-9 side-content">
					<form method="post" enctype="multipart/form-data">
						<?php echo @$this->content; ?>
					</form>				
				</section>
			</div>
		</div>
	</body>
</html>