<ul class="nav navbar-nav" id="top-menu">
    <li>
		<a href="<?php echo get_url_admin('service'); ?>" class="first">Cервисные заявки <span class="badge">новых - <?= Users::getServiceOrdersNew(); ?></span></a>
	</li>
    <li>
		<a href="<?php echo get_url_admin('demand'); ?>" class="first"><?php echo (Auth::perm(4)? 'Все отчеты' : 'Отчеты'); ?></a>
	</li>
    <li class="dropdown"> 
        <a href="#" class="first dropdown-toggle" data-toggle="dropdown">Гарантийный талон<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a class="first" href="<?php echo get_url_admin('registrylist/add/'); ?>">Регистрация пуска оборудования</a></li>
            <li><a class="first" href="<?php echo get_url_admin('registrylist/search'); ?>">Проверка истории оборудования</a></li>
            <?php if (Auth::perm(4)): ?>
                <li><a class="first" href="<?php echo get_url_admin('registrylist/'); ?>">Список листов регистрации</a></li>
                <li><a class="first" href="<?php echo get_url_admin('registrylist/importShip/'); ?>">Импорт проданного оборудования</a></li>
                <li><a class="first" href="<?php echo get_url_admin('registrylist/importWarrantyDates/'); ?>">Импорт гарантийных сроков</a></li>
                <li><a class="first" href="<?php echo get_url_admin('registrylist/export'); ?>">Комплексный экспорт</a></li>
                <li><a class="first" href="<?php echo get_url_admin('registryto/'); ?>">Форма регистрации ТО</a></li>
            <?php endif; ?>
        </ul>
    </li>
    <?php if (Auth::perm(4)): ?>
        <li>
            <a href="<?php echo get_url_admin('blocks'); ?>" class="first">Блоки</a>
        </li>
        <li>
            <a href="<?php echo get_url_admin('furniture'); ?>" class="first">Оборудование</a>
        </li>
        <li>
            <a href="<?php echo get_url_admin('works'); ?>" class="first">Работы</a>
        </li>
        <li class="dropdown">
            <a href="#" class="first dropdown-toggle" data-toggle="dropdown">Импорт <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a class="first" href="<?php echo get_url_admin('import/furniture'); ?>">Импорт Оборудования</a></li>
                <li><a class="first" href="<?php echo get_url_admin('import/parts'); ?>">Импорт Запчастей</a></li>
                <li><a class="first" href="<?php echo get_url_admin('import/works'); ?>">Импорт Работ</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="first dropdown-toggle" data-toggle="dropdown">Экспорт <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a class="first" href="<?php echo get_url_admin('export'); ?>">Выгрузить сводную таблицу</a></li>
            </ul>
        </li>
        <li><a class="first" href="<?php echo get_url_admin('users'); ?>">Сервисные центры</a></li>
        <li class="dropdown">
            <a href="#" class="first dropdown-toggle" data-toggle="dropdown">Настройки <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a class="" href="<?php echo get_url_admin('category'); ?>">Категории СЦ</a></li>
                <li><a href="<?php echo get_url_admin('settings'); ?>">Настройки сайта</a></li>
                <li><a href="<?php echo get_url_admin('settings/redirects'); ?>">Редиректы</a></li>
            </ul>
        </li>
	<?php endif; ?>
</ul>