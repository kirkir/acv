<meta name="robots" content="none">
<meta name="viewport" content="width=1024">
<?php $this->getMata(); ?>

<!-- Jquery -->
<!--<script src="http://yandex.st/jquery/2.1.1/jquery.min.js"></script>-->
<script src="/plugins/jquery.js"></script>


<!-- UI -->
<link rel="stylesheet" href="/plugins/ui/jquery-ui.css">
<script src="/plugins/ui/jquery-ui.js"></script>

<!-- bootstrap -->
<link rel="stylesheet" href="http://yastatic.net/bootstrap/3.1.1/css/bootstrap.min.css">
<script src="/themes/site/js/bootstrap.min.js"></script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


<script src="/plugins/treeview/jquery.treeview.js"></script>
<link rel="stylesheet" href="/plugins/treeview/jquery.treeview.css">

<!-- DataTables -->
<link rel="stylesheet" href="/plugins/datatables/css/jquery.dataTables.css">
<script src="/plugins/datatables/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css">
<script src="/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>

<!-- colorpicker -->
<link rel="stylesheet" media="screen" type="text/css" href="/plugins/colorpicker/css/colorpicker.css" />
<script type="text/javascript" src="/plugins/colorpicker/js/colorpicker.js"></script>


<script src="/plugins/ckeditor/ckeditor.js"></script>

<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->	

<link rel="stylesheet" href="/plugins/normalize.css">
<link rel="stylesheet" href="/plugins/iconfont/style.css">
<link rel="stylesheet" href="/themes/admin/css/print.css">
<link rel="stylesheet" href="/themes/admin/css/style.css">
<script src="/themes/admin/js/scripts.js"></script>