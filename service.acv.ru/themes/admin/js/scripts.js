function updateCaptcha(){
    var src = $('#captcha').attr('src');
    $('#captcha').removeAttr('src').attr('src', src);
}

/* Удаление изображений */
function imagesRemove(target) {
    $(target).parents('.thumbnail').toggleClass('opacity');
}

function removeWork(target, data_name, input_name) {
    // id удаленного элемента
    var dataId = $(target).parent().parent().data(data_name);
    // ids выбранных элементов из DB
    var works_ids = $('input[name='+input_name+']').val().split(',');
    if($.inArray(dataId + '', works_ids) != -1) {
        var n = works_ids.splice($.inArray(dataId + '', works_ids), 1);
    }
    $('input[name='+input_name+']').val(works_ids.join(','));
    $(target).parent().parent().remove();
    return false;
}

function tdRedactor(e)	{
    //получаем название тега
    var elm_name = e.tagName.toLowerCase();
    //если это инпут - ничего не делаем
    if($(e).find('input').length !== 0)	{return false;}
    //if(elm_name == 'input')	{return false;}
    var val = $(e).html();
    var code = '<input type="text" class="dinamic_rows form-control form-control-text" id="edit" value="'+val+'" />';
    $(e).empty().append(code);
    $('#edit').focus();
    $('.dinamic_rows').change(function() {
        if(!isNumber($(this).val())) {
            alert("Значение должно быть числом!");
            $(e).find('input').val(val);
        }
    });
    $('#edit').blur(function()	{
        var val = $(e).find('input').val();
        $(e).empty().html(val);
        // отправим список работ ajax-ом на сервер и сохраним измененные значения
        // demand_id, work_id, hour, price
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/admin_acv/demand/change_work_data',
            data: {
                demand_id: $("input[name='id']").val(),
                work_id: $(e).parent().data('work_id'),
                hour: $(e).parent().find('.d_h').text(),
                price: $(e).parent().find('.d_p').text()
            },
            success: function(data){

            }
        });
    });
}

function isNumber(s){
    return !isNaN(s) ? true : false;
}



/* Отметить/снять все */
function imagesState(target) {
    var box = $(target).parents('.form-group');
    var checkbox = $(box).find('input[type="checkbox"]');
    if ($(checkbox).filter(':checked').length == 0) {
        $(checkbox)
            .prop('checked', 'checked')
            .parents('.thumbnail').addClass('opacity');
    } else {
        $(checkbox)
            .prop('checked', false)
            .parents('.thumbnail').removeClass('opacity');
    }

    return false;
}

/* Перетаскивание изображений */
$(function($){
    $(".thumbnail-sort").sortable();
    $(".sortable_list").sortable();
});

$(function() {
    $(".datetimepicker").datetimepicker();
});


$(".datepicker").datepicker({
    dateFormat: 'dd-mm-yy'
});

function setRemoveFile(target) {
    if ($(target).is(':checked')) {
        $(target).parents('tr').addClass('filelist-removed');
    } else {
        $(target).parents('tr').removeClass('filelist-removed');
    }
}
/* Потверждение удаления */
function confirmRemove() {
    return confirm("Вы подтверждаете удаление?");
}

function executedRelation(id) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/admin_acv/users/executedRelation',
        data: {
            id: id
        },
        success: function(data){
            if(data) {
                $('[data-relation_id=' + id + ']').parent().parent().remove();
            }
        }
    });
    return false;
}


$(function($){
    /* Фиксированная панель c кнопками. */
    var at = 0;
    $('#nav .navbar-btn')
        .affix({offset: {top: $('#nav .navbar-btn').offset().top}})
        .on('affix.bs.affix', function(){
            at = $('#nav .navbar-btn').offset().left;
        })
        .on('affixed.bs.affix', function(){
            $(this).css({'left': at});
        });

    /* Перетаскивание файлов */
    $(".filelist-sortable tbody").sortable({
        helper: fixHelper,
        handle: '.ico-move'
    });

    /* Отправка формы при выборе пункта из выпадающего списка */
    $(".action-submit").change(function(){
        $(this).parents("form").submit();
    });


    var url = document.location.href;
    $.each($('#left-menu a'), function(index, value) {
        if (url.indexOf($(this).attr('href')) + 1) {
            $(this).parent().addClass('active');
        }
    });

    /* Перетаскивание строк таблиц */
    $(".table-sort tbody").sortable({
        helper: fixHelper
    });

    /* Сортировка таблиц */
    $('.table-order').dataTable({
        paging:   false,
        info:     false,
        searching: false,
        "dom": 'Rlfrtip',
        "columnDefs": [{
            "targets": [0],
            "orderable": false
        }]
    });

    /* Чекбоксы у таблиц */
    $(".toogle-check").click(function(){
        var target = $(this).parents('table').find('tbody input[name="check_id[]"]');
        if ($(target).filter(':checked').length == 0) {
            $(target)
                .prop('checked', 'checked')
                .parents('tr').addClass('warning');
        } else {
            $(target)
                .prop('checked', false)
                .parents('tr').removeClass('warning');
        }
    });

    $('.selection tbody input[name="check_id[]"]').click(function(){
        if ($(this).is(':checked')) {
            $(this).parents('tr').addClass('warning');
        } else {
            $(this).parents('tr').removeClass('warning');
        }
    });

    $('.selection tbody tr').click(function(event){
        if (event.target.type != "checkbox" && event.target.nodeName != 'A') {
            var check_id = $(this).find('input[name="check_id[]"]');
            if ($(check_id).is(':checked')) {
                $(this).removeClass('warning');
                $(check_id).prop('checked', false);
            } else {
                $(this).addClass('warning');
                $(check_id).prop('checked', 'checked');
            }
        }
    });

    /* Действие с отмечеными */
    $("#action-menu a").click(function(){
        var confirm_val = true;
        if ($(this).data('action') == 'remove') {
            if (confirmRemove() == false) {
                var confirm_val = false;
            }
        }
        if (confirm_val) {
            var form = $(this).parents('form');
            $(form).append(
                '<input type="hidden" name="action" value="' + $(this).data('action') +'">' +
                '<input type="hidden" name="mode" value="' + $(this).data('mode') +'">'
            ).submit();
        }
    });

    $( "#part" ).select2({
        ajax: {
            url: "/admin_acv/demand/parts",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term // search term
                };
            },
            processResults: function (data) {
                // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data
                return {
                    results: data
                };
            },
            cache: true
        },
        placeholder: 'Введите код запчасти'
    });

    $('#part').change(function() {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/admin_acv/demand/add_part_to_demand',
            data: {
                part: $(this).val()
            },
            success: function(data){
                var p_ids = $('input[name="p_ids"]').val();
                if(p_ids.length == 0) {
                    p_ids = $('#part').val();
                } else {
                    p_ids = p_ids + ',' + $('#part').val();
                }
                $('input[name="p_ids"]').val(p_ids);

                $('.zch_list').append('<tr data-part_id="'+data.id+'"><td>'+ data.sku +'</td><td>'+data.name+'</td><td><a onclick="removeWork(this, '+'\'part_id\''+', '+'\'p_ids\''+'); return false;" href="#">Удалить</a></td></tr>');
            }
        });
    });

    $( "#work_list" ).select2({
        placeholder: 'Выберите вид работ'
    });

    $('#work_list').change(function() {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/admin_acv/demand/add_work_to_demand',
            data: {
                work: $(this).val()
            },
            success: function(data){
                var works_ids = $('input[name="works_ids"]').val();
                if(works_ids.length == 0) {
                    works_ids = $('#work_list').val();
                } else {
                    works_ids = works_ids + ',' + $('#work_list').val();
                }

                $('input[name="works_ids"]').val(works_ids);
                if(data.level > 3) {
                    $('.work_list').append('<tr data-work_id="'+data[0].id+'"><td>'+data[0].name+'</td><td class="d_h" onclick="tdRedactor(this); return false;">'+data[0].hour+'</td><td class="d_p" onclick="tdRedactor(this); return false;">'+data[0].price+'</td><td><a class="remove_work" onclick="removeWork(this, '+'\'work_id\''+', '+'\'works_ids\''+'); return false;" href="#">Удалить</a></td></tr>');
                } else {
                    $('.work_list').append('<tr data-work_id="'+data[0].id+'"><td>'+data[0].name+'</td><td class="d_h">'+data[0].hour+'</td><td class="d_p">'+data[0].price+'</td><td><a class="remove_work" onclick="removeWork(this, '+'\'work_id\''+', '+'\'works_ids\''+'); return false;" href="#">Удалить</a></td></tr>');
                }
            }
        });
    });

    $('.comment .ico').click(function() {
        var $this = $(this);
        var id = $(this).parent().parent().parent().find('input[name="check_id[]"]').val();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/admin_acv/demand/updateCommit',
            data: {
                id: id,
                get_commit: true
            },
            success: function(data){
                var commit = data.commit;
                $('#myModalBox .modal-body .commit_t').text(commit);
                $('#myModalBox .modal-body .commit').val(commit);
                $('#myModalBox .modal-body .commit').attr('data-id', id);
                $('#myModalBox').modal('show');
                if(data.show_inactive) {
                    function setColor() {
                        $this.removeClass('active');
                        $this.css({'color': '#428bca'});
                    }
                    setInterval(setColor, 500);
                }
            }
        });
    });

    var cols = "#ffcc00,#eeeeee,#3b5998".split(",");
    var cPos = 0;

    $(document).ready(function() {
        swapC();
    });

    function swapC() {
        $('.ico.active').animate({ color:cols[cPos] }, 500);
        cPos++;
        if (cPos == cols.length) {
            cPos = 0;
        }
        window.setTimeout(function() { swapC() }, 500);
    }


    $('#myModalBox .modal-body #textarea_adm').focusout(function() {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/admin_acv/demand/updateCommit',
            data: {
                id: $(this).attr('data-id'),
                text: $(this).val()
            },
            success: function(data){
            }
        });
    });

    $('#myModalBox .modal-footer .btn-success').click(function() {
        return false;
    });


    // гарантийный случай
    // $('.toggle_warranty').click(function() {
    //     $('.total').empty();
    //     $('.price_one_h').parent().toggle('slow');
    //     if($(this).is(':checked')) {
    //         var total = $('input[name=price_works]').val();
    //         var delivery_total = $('input[name=delivery_total]').val();
    //         var total = parseInt(total) + parseInt(delivery_total);
    //         $('.total').text(total + ' р.');
    //     }
    // });

    $('.toggle_warranty_file').click(function() {
        $('.wrnt').toggle('slow');
    });


    //  распечатать отчет
    $('.print_btn').click(function(){
        window.print() ;
        return false;
    })

    $('#description_files_upload').on('show.bs.modal', function (event) {
        var target = $(event.relatedTarget); // Button that triggered the modal
        var recipient = target.data('value'); // Extract info from data-* attributes
        var modal = $(this);
        modal.find('.modal-body .' + recipient).show();
    });

    $('#description_files_upload').on('hidden.bs.modal', function (event) {
        var modal = $(this);
        modal.find('.modal-body .type').hide();
    });
});

$(document).ready(function() {
    $(".show_relations").click(function() {
        var id = $(this).data('user_id');
        $.ajax({
            type: 'POST',
            dataType: 'html',
            url: '/admin_acv/users/getRelations',
            data: {
                id: id
            },
            success: function(data){
                //$('#myModalBox .modal-body .commit_t').text(commit);
                $("#show_relations .modal-body").empty();
                $("#show_relations .modal-body").html(data);
                $("#show_relations").modal('show');
            }
        });
        return false;
    });

    var input_adress_val = $('.input_adress').val();
    $('.input_adress').after("<div class='adress_print'>"+input_adress_val+"</div>");

    $('.download_archive').on('click', function () {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/admin_acv/demand/downloadArchive',
            data: {
                id: $(this).data('id')
            },
            success: function(data){
                if(data.file) {
                    location.href = "/uploads/" + data.file;
                }
            }
        });
        return false;
    })
});

/**
 * @author Maxim Vasiliev
 * Date: 21.01.2010
 * Time: 14:00
 */
function html_entity_decode(string, quote_style) {
    //  discuss at: http://phpjs.org/functions/html_entity_decode/
    // original by: john (http://www.jd-tech.net)
    //    input by: ger
    //    input by: Ratheous
    //    input by: Nick Kolosov (http://sammy.ru)
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: marc andreu
    //  revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    //  revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // bugfixed by: Onno Marsman
    // bugfixed by: Brett Zamir (http://brett-zamir.me)
    // bugfixed by: Fox
    //  depends on: get_html_translation_table
    //   example 1: html_entity_decode('Kevin &amp; van Zonneveld');
    //   returns 1: 'Kevin & van Zonneveld'
    //   example 2: html_entity_decode('&amp;lt;');
    //   returns 2: '&lt;'

    var hash_map = {},
        symbol = '',
        tmp_str = '',
        entity = '';
    tmp_str = string.toString();

    if (false === (hash_map = this.get_html_translation_table('HTML_ENTITIES', quote_style))) {
        return false;
    }

    // fix &amp; problem
    // http://phpjs.org/functions/get_html_translation_table:416#comment_97660
    delete(hash_map['&']);
    hash_map['&'] = '&amp;';

    for (symbol in hash_map) {
        entity = hash_map[symbol];
        tmp_str = tmp_str.split(entity)
            .join(symbol);
    }
    tmp_str = tmp_str.split('&#039;')
        .join("'");

    return tmp_str;
}

(function($)
{
    /**
     * jQuery.deserialize plugin
     * Fills elements in selected containers with data extracted from URLencoded string
     * @param data URLencoded data
     * @param clearForm if true form will be cleared prior to deserialization
     */
    $.fn.deserialize = function(data, clearForm)
    {
        this.each(function(){
            deserialize(this, data, !!clearForm);
        });
    };

    /**
     * Fills specified form with data extracted from string
     * @param element form to fill
     * @param data URLencoded data
     * @param clearForm if true form will be cleared prior to deserialization
     */
    function deserialize(element, data, clearForm)
    {
        var splits = decodeURIComponent(data).split('&'),
            i = 0,
            split = null,
            key = null,
            value = null,
            splitParts = null;

        if (clearForm)
        {
            $('input[type="checkbox"],input[type="radio"]', element).removeAttr('checked');
            $('select,input[type="text"],input[type="password"],input[type="hidden"],textarea', element).val('');
        }

        var kv = {};
        while(split = splits[i++]){
            splitParts = split.split('=');
            key = splitParts[0] || '';
            splitParts.splice(0, 1);
            value = splitParts.join('=').replace(/\+/g, ' ');

            if (key != ''){
                if( key in kv ){
                    if( $.type(kv[key]) !== 'array' )
                        kv[key] = [kv[key]];

                    kv[key].push(value);
                }else
                    kv[key] = value;
            }
        }

        for( key in kv ){
            value = kv[key];
            //$('input[type="checkbox"][name="'+ key +'"][value="'+ value +'"],input[type="radio"][name="'+ key +'"][value="'+ value +'"]', element).prop('checked', true);
            $('select[name="'+ key +'"],input[type="text"][name="'+ key +'"],input[type="password"][name="'+ key +'"],input[type="hidden"][name="'+ key +'"],textarea[name="'+ key +'"]', element).val(value);
        }
    }
})(jQuery);