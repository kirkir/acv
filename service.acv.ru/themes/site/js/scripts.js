/**
 * @function      Include
 * @description   Includes an external scripts to the page
 * @param         {string} scriptUrl
 */
function include(scriptUrl) {
    document.write('<script src="' + scriptUrl + '"></script>');
}


/**
 * @function      isIE
 * @description   checks if browser is an IE
 * @returns       {number} IE Version
 */
function isIE() {
    var myNav = navigator.userAgent.toLowerCase();
    return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
};


/**
 * @module       Copyright
 * @description  Evaluates the copyright year
 */
;
(function ($) {
    var currentYear = (new Date).getFullYear();
    $(document).ready(function () {
        $("#copyright-year").text((new Date).getFullYear());
    });
})($);


/**
 * @module       IE Fall&Polyfill
 * @description  Adds some loosing functionality to old IE browsers
 */
;
(function ($) {
    if (isIE() && isIE() < 11) {
        include('/themes/site/js/pointer-events.min.js');
        $('html').addClass('lt-ie11');
        $(document).ready(function () {
            PointerEventsPolyfill.initialize({});
        });
    }

    if (isIE() && isIE() < 10) {
        $('html').addClass('lt-ie10');
    }
})($);


/**
 * @module       WOW Animation
 * @description  Enables scroll animation on the page
 */
;
(function ($) {
    var o = $('html');
    if (o.hasClass('desktop') && o.hasClass("wow-animation") && $(".wow").length) {
        include('/themes/site/js/wow.min.js');

        $(document).ready(function () {
            new WOW().init();
        });
    }
})($);


/**
 * @module       Smoothscroll
 * @description  Enables smooth scrolling on the page
 */
;
(function ($) {
    if ($("html").hasClass("smoothscroll")) {
        include('/themes/site/js/smoothscroll.min.js');
    }
})($);

/**
 * @module       RD Smoothscroll
 * @description  Enables smooth scrolling on the page for all platforms
 */
;
(function ($) {
    if ($("html").hasClass("smoothscroll-all")) {
        include('/themes/site/js/rd-smoothscroll.min.js');
    }
})($);


/**
 * @module       ToTop
 * @description  Enables ToTop Plugin
 */
;
(function ($) {
    var o = $('html');
    if (o.hasClass('desktop')) {
        include('/themes/site/js/jquery.ui.totop.min.js');

        $(document).ready(function () {
            $().UItoTop({
                easingType: 'easeOutQuart',
                containerClass: 'ui-to-top fa fa-angle-up'
            });
        });
    }
})($);

/**
 * @module       Responsive Tabs
 * @description  Enables Easy Responsive Tabs Plugin
 */
;
(function ($) {
    var o = $('.responsive-tabs');
    if (o.length > 0) {
        include('/themes/site/js/jquery.easy-responsive-tabs.min.js');

        $(document).ready(function () {
            o.each(function () {
                var $this = $(this);
                $this.easyResponsiveTabs({
                    type: $this.attr("data-type") === "accordion" ? "accordion" : "default"
                });
            })
        });
    }
})($);


/**
 * @module       RD Google Map
 * @description  Enables RD Google Map Plugin
 */
;
(function ($) {
    var o = $('#google-map');

    if (o.length) {
        include('//maps.google.com/maps/api/js');
        include('/themes/site/js/jquery.rd-google-map.min.js');
        $(document).ready(function () {
            var head = document.getElementsByTagName('head')[0],
                insertBefore = head.insertBefore;

            head.insertBefore = function (newElement, referenceElement) {
                if (newElement.href && newElement.href.indexOf('//fonts.googleapis.com/css?family=Roboto') != -1 || newElement.innerHTML.indexOf('gm-style') != -1) {
                    return;
                }
                insertBefore.call(head, newElement, referenceElement);
            };

            //lazyInit(o, function () {
            o.googleMap({
                styles: [{
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{"color": "#e9e9e9"}, {"lightness": 17}]
                }, {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [{"color": "#f5f5f5"}, {"lightness": 20}]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [{"color": "#ffffff"}, {"lightness": 17}]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [{"color": "#ffffff"}, {"lightness": 29}, {"weight": 0.2}]
                }, {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [{"color": "#ffffff"}, {"lightness": 18}]
                }, {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [{"color": "#ffffff"}, {"lightness": 16}]
                }, {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [{"color": "#f5f5f5"}, {"lightness": 21}]
                }, {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [{"color": "#dedede"}, {"lightness": 21}]
                }, {
                    "elementType": "labels.text.stroke",
                    "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"lightness": 16}]
                }, {
                    "elementType": "labels.text.fill",
                    "stylers": [{"saturation": 36}, {"color": "#333333"}, {"lightness": 40}]
                }, {"elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [{"color": "#f2f2f2"}, {"lightness": 19}]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [{"color": "#fefefe"}, {"lightness": 20}]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [{"color": "#fefefe"}, {"lightness": 17}, {"weight": 1.2}]
                }]
            });
            //});
        });
    }
})($);


/**
 * @module       RD Navbar
 * @description  Enables RD Navbar Plugin
 */
;
(function ($) {
    var o = $('.rd-navbar');
    if (o.length > 0) {
        include('/themes/site/js/jquery.rd-navbar.min.js');

        $(document).ready(function () {
            var responsive = {};

            var aliaces = ["-xs-", "-sm-", "-md-", "-lg-"],
                values = [480, 768, 992, 1200],
                i, j, val;

            responsive[0] = {
                layout: o.attr("data-layout") || "rd-navbar-fixed",
                focusOnHover: o.attr("data-hover-on") === "true",
                stickUp: o.attr("data-stick-up") === "true"
            };

            for (i = 0; i < values.length; i++) {

                //for (j = i; j >= -1; j--) {
                val = '';
                if (o.attr("data" + aliaces[i] + "layout")) {
                    if (!responsive[values[i]]) responsive[values[i]] = {};
                    if (!responsive[values[i]]["layout"]) {
                        responsive[values[i]]["layout"] = o.attr("data" + aliaces[i] + "layout");
                    }
                }

                if (o.attr("data" + aliaces[i] + "hover-on")) {
                    if (!responsive[values[i]]) responsive[values[i]] = {};
                    if (!responsive[values[i]]["focusOnHover"]) {
                        val = o.attr("data" + aliaces[i] + "hover-on") === 'true';
                        responsive[values[i]]["focusOnHover"] = val;
                    }
                }

                if (o.attr("data" + aliaces[i] + "stick-up")) {
                    if (!responsive[values[i]]) responsive[values[i]] = {};
                    if (!responsive[values[i]]["stickUp"] && responsive[values[i]]["stickUp"] !== 0) {
                        val = o.attr("data" + aliaces[i] + "stickUp") === 'true';
                        responsive[values[i]]["stickUp"] = val;
                    }
                }
                //}
            }

            //console.log(responsive);

            o.RDNavbar({
                responsive: responsive
            });
        });
    }
})($);


/**
 * @module       Swiper Slider
 * @description  Enables Swiper Plugin
 */
;
(function ($, undefined) {

    var o = $(".swiper-slider");
    if (o.length) {
        include('/themes/site/js/jquery.swiper.min.js');


        function getSwiperHeight(object, attr) {
            var val = object.attr("data-" + attr),
                dim;

            if (!val) {
                return undefined;
            }

            dim = val.match(/(px)|(%)|(vh)$/i);

            if (dim.length) {
                switch (dim[0]) {
                    case "px":
                        return parseFloat(val);
                    case "vh":
                        return $(window).height() * (parseFloat(val) / 100);
                    case "%":
                        return object.width() * (parseFloat(val) / 100);
                }
            } else {
                return undefined;
            }
        }

        function toggleSwiperInnerVideos(swiper) {
            var prevSlide = $(swiper.slides[swiper.previousIndex]),
                nextSlide = $(swiper.slides[swiper.activeIndex]),
                videos;

            prevSlide.find("video").each(function () {
                this.pause();
            });

            videos = nextSlide.find("video");
            if (videos.length) {
                videos.get(0).play();
            }
        }

        function toggleSwiperCaptionAnimation(swiper) {
            var prevSlide = $(swiper.container),
                nextSlide = $(swiper.slides[swiper.activeIndex]);

            prevSlide
                .find("[data-caption-animate]")
                .each(function () {
                    var $this = $(this);
                    $this
                        .removeClass("animated")
                        .removeClass($this.attr("data-caption-animate"))
                        .addClass("not-animated");
                });

            nextSlide
                .find("[data-caption-animate]")
                .each(function () {
                    var $this = $(this),
                        delay = $this.attr("data-caption-delay");

                    setTimeout(function () {
                        $this
                            .removeClass("not-animated")
                            .addClass($this.attr("data-caption-animate"))
                            .addClass("animated");
                    }, delay ? parseInt(delay) : 0);
                });
        }

        $(document).ready(function () {
            o.each(function () {
                var s = $(this);

                var pag = s.find(".swiper-pagination"),
                    next = s.find(".swiper-button-next"),
                    prev = s.find(".swiper-button-prev"),
                    bar = s.find(".swiper-scrollbar"),
                    h = getSwiperHeight(o, "height"), mh = getSwiperHeight(o, "min-height");
                s.find(".swiper-slide")
                    .each(function () {
                        var $this = $(this),
                            url;
                        if (url = $this.attr("data-slide-bg")) {
                            $this.css({
                                "background-image": "url(" + url + ")",
                                "background-size": "cover"
                            })
                        }
                    })
                    .end()
                    .find("[data-caption-animate]")
                    .addClass("not-animated")
                    .end()
                    .swiper({
                        autoplay: s.attr('data-autoplay') ? s.attr('data-autoplay') === "false" ? undefined : s.attr('data-autoplay') : 5000,
                        direction: s.attr('data-direction') ? s.attr('data-direction') : "horizontal",
                        effect: s.attr('data-slide-effect') ? s.attr('data-slide-effect') : "slide",
                        speed: s.attr('data-slide-speed') ? s.attr('data-slide-speed') : 600,
                        keyboardControl: s.attr('data-keyboard') === "true",
                        mousewheelControl: s.attr('data-mousewheel') === "true",
                        mousewheelReleaseOnEdges: s.attr('data-mousewheel-release') === "true",
                        nextButton: next.length ? next.get(0) : null,
                        prevButton: prev.length ? prev.get(0) : null,
                        pagination: pag.length ? pag.get(0) : null,
                        allowSwipeToNext: true,
                        allowSwipeToPrev: true,
                        paginationClickable: pag.length ? pag.attr("data-clickable") !== "false" : false,
                        paginationBulletRender: pag.length ? pag.attr("data-index-bullet") === "true" ? function (index, className) {
                            return '<span class="' + className + '">' + (index + 1) + '</span>';
                        } : null : null,
                        scrollbar: bar.length ? bar.get(0) : null,
                        scrollbarDraggable: bar.length ? bar.attr("data-draggable") !== "false" : true,
                        scrollbarHide: bar.length ? bar.attr("data-draggable") === "false" : false,
                        loop: s.attr('data-loop') !== "false",
                        simulateTouch: false,
                        //threshold: 2000,
                        onTransitionStart: function (swiper) {
                            toggleSwiperInnerVideos(swiper);
                        },
                        onTransitionEnd: function (swiper) {
                            toggleSwiperCaptionAnimation(swiper);
                        },
                        onInit: function (swiper) {
                            toggleSwiperInnerVideos(swiper);
                            toggleSwiperCaptionAnimation(swiper);
                        }
                    });

                $(window)
                    .on("resize", function () {
                        var mh = getSwiperHeight(s, "min-height"),
                            h = getSwiperHeight(s, "height");
                        if (h) {
                            s.css("height", mh ? mh > h ? mh : h : h);
                        }
                    })
                    .trigger("resize");
            });
        });
    }

    // Gallery init
    var gallery = $('.swiper-container');
    if (gallery.length) {
        $(document).ready(function () {
            var galleryTop = new Swiper('.gallery-top', {
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev',
                spaceBetween: 10
            });

            var galleryThumbs = new Swiper('.gallery-thumbs', {
                spaceBetween: 10,
                centeredSlides: true,
                slidesPerView: 'auto',
                touchRatio: 0.2,
                slideToClickedSlide: true
            });

            galleryTop.params.control = galleryThumbs;
            galleryThumbs.params.control = galleryTop;
            //galleryThumbs.slideTo( $('.first-el').index(),1000,false );
            $(".first-el").click(function () {
                var v = $(this).index();
                galleryThumbs.slideTo(v, 1000, false);
            });
            $('.first-el').click();
        });
    }
// End Gallery init

})($);


/**
 * @module       Progress Bar custom
 * @description  Enables Progress Bar Plugin
 */
;
(function ($) {
    var o = $(".progress-bar-custom");
    if (o.length) {
        include('/themes/site/js/progressbar.min.js');

        function isScrolledIntoView(elem) {
            var $window = $(window);
            return elem.offset().top + elem.outerHeight() >= $window.scrollTop() && elem.offset().top <= $window.scrollTop() + $window.height();
        }

        $(document).ready(function () {
            o.each(function () {
                var bar, type;
                if (
                    this.className.indexOf("progress-bar-horizontal") > -1
                ) {
                    type = 'Line';
                }

                if (
                    this.className.indexOf("progress-bar-radial") > -1
                ) {
                    type = 'Circle';
                }

                if (this.getAttribute("data-stroke") && this.getAttribute("data-value") && type) {
                    //console.log(this.offsetWidth);
                    //console.log(parseFloat(this.getAttribute("data-stroke")) / this.offsetWidth * 100);
                    bar = new ProgressBar[type](this, {
                        strokeWidth: Math.round(parseFloat(this.getAttribute("data-stroke")) / this.offsetWidth * 100)
                        ,
                        trailWidth: this.getAttribute("data-trail") ? Math.round(parseFloat(this.getAttribute("data-trail")) / this.offsetWidth * 100) : 0
                        ,
                        text: {
                            value: this.getAttribute("data-counter") === "true" ? '0' : null
                            , className: 'progress-bar__body'
                            , style: null
                        }
                    });

                    bar.svg.setAttribute('preserveAspectRatio', "none meet");
                    if (type === 'Line') {
                        bar.svg.setAttributeNS(null, "height", this.getAttribute("data-stroke"));
                    }

                    bar.path.removeAttribute("stroke");
                    bar.path.className.baseVal = "progress-bar__stroke";
                    if (bar.trail) {
                        bar.trail.removeAttribute("stroke");
                        bar.trail.className.baseVal = "progress-bar__trail";
                    }

                    if (this.getAttribute("data-easing") && !isIE()) {
                        $(document)
                            .on("scroll", $.proxy(function () {
                                //console.log(isScrolledIntoView(this));
                                if (isScrolledIntoView($(this)) && this.className.indexOf("progress-bar--animated") === -1) {
                                    console.log(1);
                                    this.className += " progress-bar--animated";
                                    bar.animate(parseInt(this.getAttribute("data-value")) / 100.0, {
                                        easing: this.getAttribute("data-easing")
                                        ,
                                        duration: this.getAttribute("data-duration") ? parseInt(this.getAttribute("data-duration")) : 800
                                        ,
                                        step: function (state, b) {
                                            if (b._container.className.indexOf("progress-bar-horizontal") > -1 ||
                                                b._container.className.indexOf("progress-bar-vertical") > -1) {
                                                b.text.style.width = Math.abs(b.value() * 100).toFixed(0) + "%"
                                            }
                                            b.setText(Math.abs(b.value() * 100).toFixed(0));
                                        }
                                    });
                                }
                            }, this))
                            .trigger("scroll");
                    } else {
                        bar.set(parseInt(this.getAttribute("data-value")) / 100.0);
                        bar.setText(this.getAttribute("data-value"));
                        if (type === 'Line') {
                            bar.text.style.width = parseInt(this.getAttribute("data-value")) + "%";
                        }
                    }
                } else {
                    console.error(this.className + ": progress bar type is not defined");
                }
            });
        });
    }
})($);


/**
 * @module       Count To
 * @description  Enables Count To Plugin
 */
;
(function ($) {
    var o = $('.counter');
    if (o.length > 0) {
        include('/themes/site/js/jquery.countTo.js');
        $(document).ready(function () {
            $(document)
                //$(this).scroll(function () {
                .on("scroll", $.proxy(function () {
                    o.not('.animated').each(function () {
                        var $this = $(this);
                        var position = $this.offset().top;

                        if (($(window).scrollTop() + $(window).height()) > position) {

                            $this.countTo();
                            $this.addClass('animated');
                        }
                    });
                }, $(this)))
                .trigger("scroll");
        });
    }
})($);

/**
 * @module      Shop Gallery
 * @description  Custom
 */
;
(function ($) {
    var o = $('.shop-gallery');
    if (o.length > 0) {
        $(document).ready(function () {
            $(this).find('#' + $(this).attr('id')).removeClass('active');

            var gallPrevie = $("[data-shop-gallery]");

            gallPrevie.on("click", function () {


                    var $t = $(this).attr('data-shop-gallery');
                console.log($(this));

                    $("[data-lightbox]").removeClass("active");

                    $('#' + $t).addClass('active');
            } );

        });
    }
})($);

/**
 * @module      Progress Horizontal Bootstrap
 * @description  Enables Animation
 */
;
(function ($) {
    var o = $('.progress-bar');
    if (o.length > 0) {
        include('/themes/site/js/jquery.counter.js');
        $(document).ready(function () {
            $(document)
                //$(this).scroll(function () {
                .on("scroll", $.proxy(function () {
                    o.not('.animated').each(function () {

                        var position = $(this).offset().top;

                        if (($(window).scrollTop() + $(window).height()) > position) {
                            var $this = $(this);
                            var start = $this.attr("aria-valuemin");
                            var end = $this.attr("aria-valuenow");

                            if ($this.hasClass("vertical-bar")) {
                                $this.css({height: end + '%'});
                                $this.parent().find('span').css({bottom: end + '%'});
                            }
                            else {
                                $this.css({width: end + '%'});
                                $this.parent().find('span').css({left: end + '%'});
                            }


                            $this.parent().find('span').counter({
                                start: start,
                                end: end,
                                time: 0.4,
                                step: 20
                            });

                            //var span = $this.parent().find('span');
                            //
                            //span.prop('Counter', start).animate({
                            //    Counter: end
                            //}, {
                            //    duration: 1000,
                            //    easing: 'linear',
                            //    step: function (now) {
                            //        $(this).text(Math.ceil(now));
                            //    }
                            //});
                            $this.addClass('animated');
                        }

                    });
                }, $(this)))
                .trigger("scroll");
        });
    }
})($);


//
///**
// * @module       magnifierRentgen
// * @description   magnifierRentgen
// */
//


//(function ($) {
//    var o = $('.img_zoom');
//    if (o.length) {
//        include("/themes/site/js/jQuery.MagnifierRentgen.min.js");
//        $(document).ready(function () {
//            o.each(function () {
//                $(this).magnifierRentgen();
//            });
//        });
//    }
//})($);

//
///**
// * @module       ElevateZoom
// * @description   Elevate Web Design
// */
//

//;
//(function ($) {
//    var o = $('.img_zoom');
//    if (o.length) {
//
//        include("/themes/site/js/jquery.elevatezoom.js");
//        include("/themes/site/js/jquery.elevateZoom-3.0.8.min.js");
//
//        add_dataZoom = function(el){
//            var s = el;
//            if(s.parents(".swiper-slide-active").length) {
//
//                //var src = s.attr('src');
//                var res = s.attr("src").match(/([\w\d-\/]+)(.jpg$)/i);
//                console.log(res);
//                s.attr('data-zoom-image',res[1] + "_original" + res[2]);
//
//                s.elevateZoom({
//                    //zoomType : "inner",
//                    zoomType: "lens",
//                    //gallery:'gallery_01',
//                    //cursor: 'pointer',
//                    cursor: "crosshair",
//                    //galleryActiveClass: 'active',
//                    lensShape: "round",
//                    lensSize: 200,
//                    zoomWindowFadeIn: 500,
//                    zoomWindowFadeOut: 500,
//                    //imageCrossfade: true,
//                    //loadingIcon: 'http://www.elevateweb.co.uk/spinner.gif'
//                });
//            }
//        };
//
//        $(document).ready(function () {
//            o.each(function () {
//                var image = $(this);
//                add_dataZoom(image);
//
//                    $('.gallery-thumbs .swiper-slide').on("click", function(){
//                        $(".zoomContainer").remove();
//                        $("[data-zoom-image]").removeAttr("data-zoom-image");
//                        add_dataZoom(image);
//                });
//            });
//        });
//    }
//})($);


/**
 * @module       tooltip
 * @description  Bootstrap tooltips
 */


$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})


/**
 * @module       Modal
 * @description  Bootstrap Modal
 */

//$('#myModal').modal('show')


/**
 * @module       Dropdown
 * @description  Bootstrap Dropdown
 */

$('.dropdown-toggle').dropdown()

/**
 * @module       Tabs
 * @description  Bootstrap tabs
 */


$('#myTabs a').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
})

$('#myTabs2 a').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
})




/**
 * @module     Owl Carousel
 * @description Enables Owl Carousel Plugin
 */
;
(function ($) {
    var o = $('.owl-carousel');
    if (o.length) {
        //include('/themes/site/js/jquery.owl-carousel.js');

        var isTouch = "ontouchstart" in window;

        function preventScroll(e) {
            e.preventDefault();
        }

        $(document).ready(function () {
            o.each(function () {
                var c = $(this),
                    responsive = {};

                var aliaces = ["-", "-xs-", "-sm-", "-md-", "-lg-"],
                    values = [0, 480, 768, 992, 1200],
                    i, j;

                for (i = 0; i < values.length; i++) {
                    responsive[values[i]] = {};
                    for (j = i; j >= -1; j--) {
                        if (!responsive[values[i]]["items"] && c.attr("data" + aliaces[j] + "items")) {
                            responsive[values[i]]["items"] = j < 0 ? 1 : parseInt(c.attr("data" + aliaces[j] + "items"));
                        }
                        if (!responsive[values[i]]["stagePadding"] && responsive[values[i]]["stagePadding"] !== 0 && c.attr("data" + aliaces[j] + "stage-padding")) {
                            responsive[values[i]]["stagePadding"] = j < 0 ? 0 : parseInt(c.attr("data" + aliaces[j] + "stage-padding"));
                        }
                        if (!responsive[values[i]]["margin"] && responsive[values[i]]["margin"] !== 0 && c.attr("data" + aliaces[j] + "margin")) {
                            responsive[values[i]]["margin"] = j < 0 ? 30 : parseInt(c.attr("data" + aliaces[j] + "margin"));
                        }
                    }
                }
                //console.log('string', c);
                //c.owlCarousel({
                //    autoplay: c.attr("data-autoplay") === "true",
                //    loop: c.attr("data-loop") !== "false",
                //    item: 1,
                //    animateOut: c.attr("data-fadeout"),
                //    mouseDrag: c.attr("data-mouse-drag") !== "false",
                //    nav: c.attr("data-nav") === "true",
                //    dots: c.attr("data-dots") === "true",
                //    //dotsEach: c.attr("data-dots-each") ? parseInt(c.attr("data-dots-each")) : false,
                //    dotsEach: true,
                //    responsive: responsive,
                //    navText: [],
                //    //onInitialized: function () {
                //    //    //if ($.fn.magnificPopup) {
                //    //    //    var o = this.$element.attr('data-lightbox') !== "gallery",
                //    //    //        g = this.$element.attr('data-lightbox') === "gallery";
                //    //    //
                //    //    //    if (o) {
                //    //    //        this.$element.each(function () {
                //    //    //            var $this = $(this);
                //    //    //            $this.magnificPopup({
                //    //    //                type: $this.attr("data-lightbox"),
                //    //    //                callbacks: {
                //    //    //                    open: function () {
                //    //    //                        if (isTouch) {
                //    //    //                            $(document).on("touchmove", preventScroll);
                //    //    //                            $(document).swipe({
                //    //    //                                swipeDown: function () {
                //    //    //                                    $.magnificPopup.close();
                //    //    //                                }
                //    //    //                            });
                //    //    //                        }
                //    //    //                    },
                //    //    //                    close: function () {
                //    //    //                        if (isTouch) {
                //    //    //                            $(document).off("touchmove", preventScroll);
                //    //    //                            $(document).swipe("destroy");
                //    //    //                        }
                //    //    //                    }
                //    //    //                }
                //    //    //            });
                //    //    //        })
                //    //    //    }
                //    //    //
                //    //    //    if (g) {
                //    //    //        this.$element.each(function () {
                //    //    //            var $gallery = $(this);
                //    //    //
                //    //    //            $gallery
                //    //    //                .find('[data-lightbox]').each(function () {
                //    //    //                    var $item = $(this);
                //    //    //                    $item.addClass("mfp-" + $item.attr("data-lightbox"));
                //    //    //                })
                //    //    //                .end()
                //    //    //                .magnificPopup({
                //    //    //                    delegate: '.owl-item:not(.cloned) .owl-item [data-lightbox]',
                //    //    //                    type: "image",
                //    //    //                    gallery: {
                //    //    //                        enabled: true
                //    //    //                    },
                //    //    //                    callbacks: {
                //    //    //                        open: function () {
                //    //    //                            if (isTouch) {
                //    //    //                                $(document).on("touchmove", preventScroll);
                //    //    //                                $(document).swipe({
                //    //    //                                    swipeDown: function () {
                //    //    //                                        $.magnificPopup.close();
                //    //    //                                    }
                //    //    //                                });
                //    //    //                            }
                //    //    //                        },
                //    //    //                        close: function () {
                //    //    //                            if (isTouch) {
                //    //    //                                $(document).off("touchmove", preventScroll);
                //    //    //                                $(document).swipe("destroy");
                //    //    //                            }
                //    //    //                        }
                //    //    //                    }
                //    //    //                });
                //    //    //        })
                //    //    //    }
                //    //    //}
                //    //}
                //});
            });
        });
    }
})($);


/**
 * @module       SVG-Animate
 * @description  Enables SVG-Animate *
 */

;
(function ($) {
    var o = $('#svg-phone_1'),
        msie = !!navigator.userAgent.match(/Trident\/7\./);
    //(!document.all) - is IE11-
    if ((o.length) && (!msie)) {

        $(document).ready(function () {
            $(this).on("scroll", $.proxy(function () {
                o.not('.active').each(function () {
                    var $this = $(this);
                    var position = $this.offset().top;

                    if (($(window).scrollTop() + $(window).height()) > position) {
                        $this.attr("class", "active");
                        $this.parent().find('.phone_1').addClass('active');
                    }
                });
            }, $(this)))
                .trigger("scroll");
        });
    }
})($);


/**
 * @module       ViewPort Universal
 * @description  Add class in viewport
 */


;
(function ($) {
    var o = $('.view-animate');
    if (o.length) {

        $(document).ready(function () {
            $(this).on("scroll", $.proxy(function () {
                o.not('.active').each(function () {
                    var $this = $(this);
                    var position = $this.offset().top;

                    if (($(window).scrollTop() + $(window).height()) > position) {
                        $this.addClass("active");
                    }
                });
            }, $(this)))
                .trigger("scroll");
        });
    }
})($);


/**
 * @module       Scroll To
 * @description  Enables Scroll To
 */
;
(function ($) {
    var o = $('.faq-section');
    if (o.length) {
        include('/themes/site/js/scrollTo.js');
        $(document).ready(function () {
            o.scrollTo({});
        });
    }
})($);


/**
 * @module       RD Search
 * @description  Enables RD Search Plugin
 */
//;
//(function ($) {
//    var o = $('.rd-navbar-search');
//    if (o.length) {
//        include('/themes/site/js/jquery.search.min.js');
//        $(document).ready(function () {
//            o.RDSearch({});
//        });
//    }
//})($);


/**
 * @module       TimeCircles
 * @description  Enables RD Search Plugin
 */
;
(function ($) {
    var o = $('#DateCountdown');
    if (o.length) {
        include('/themes/site/js/TimeCircles.js');
        $(document).ready(function () {
            var time = {
                "Days": {
                    "text": "Days",
                    "color": "#FFF",
                    "show": true
                },
                "Hours": {
                    "text": "Hours",
                    "color": "#fff",
                    "show": true
                },
                "Minutes": {
                    "text": "Minutes",
                    "color": "#fff",
                    "show": true
                },
                "Seconds": {
                    "text": "Seconds",
                    "color": "#fff",
                    "show": true
                }
            };
            o.TimeCircles({
                "animation": "smooth",
                "bg_width": 0.4,
                "fg_width": 0.02666666666666667,
                "circle_bg_color": "rgba(0,0,0,.2)",
                "time": time
            });
            $(window).on('load resize orientationchange', function () {
                if ($(window).width() < 479) {
                    o.TimeCircles({
                        time: {
                            //Days: {show: true},
                            //Hours: {show: true},
                            Minutes: {show: true},
                            Seconds: {show: false}
                        }
                    }).rebuild();
                } else if ($(window).width() < 767) {
                    o.TimeCircles({
                        time: {
                            //Minutes: {show: true},
                            Seconds: {show: false}
                        }
                    }).rebuild();
                } else {
                    o.TimeCircles({time: time}).rebuild();
                }
            });
        });

    }
})(jQuery);


/**
 * @module       Countdown
 * @description  Enables RD Search Plugin
 */


/**
 * @module       Countdown
 * @description  Enables Countdown Plugin
 */
;
(function ($) {
    var o = $('.countdown'),
        type = o.attr('data-type'),
        time = o.attr('data-time'),
        format = o.attr('data-format');

    if (o.length) {
        include('/themes/site/js/jquery.plugin.min.js');
        include('/themes/site/js/jquery.countdown.js');

        var d = new Date();
        d.setTime(Date.parse(time)).toLocaleString();

        $(document).ready(function () {
            var settings = [];

            settings[type] = d;
            settings['format'] = format;

            o.countdown(settings);
        });
    }
})(jQuery);


/**
 * @module       Magnific Popup
 * @description  Enables Magnific Popup Plugin
 */
;
(function ($) {
    var o = $('[data-lightbox]').not('[data-lightbox="gallery"] [data-lightbox]'),
        g = $('[data-lightbox^="gallery"]');
    if (o.length > 0 || g.length > 0) {
        include('/themes/site/js/jquery.magnific-popup.min.js');

        $(document).ready(function () {
            if (o.length) {
                o.each(function () {
                    var $this = $(this);
                    $this.magnificPopup({
                        type: $this.attr("data-lightbox")
                    });
                })
            }

            if (g.length) {
                g.each(function () {
                    var $gallery = $(this);
                    $gallery
                        .find('[owl-carousel-lightbox]').each(function () {
                            var $item = $(this);
                            $item.addClass("mfp-" + $item.attr("data-lightbox"));
                        })
                        .end()
                        .magnificPopup({
                            delegate: '[data-lightbox]',
                            type: "image",
                            // Delay in milliseconds before popup is removed
                            removalDelay: 300,
                            // Class that is added to popup wrapper and background
                            // make it unique to apply your CSS animations just to this exact popup
                            mainClass: 'mfp-fade',
                            gallery: {
                                enabled: true
                            }
                        });
                })
            }
        });
    }
})(jQuery);


/**
 * @module       Isotope
 * @description  Enables Isotope Plugin
 */
;
(function ($) {
    var o = $(".isotope");
    if (o.length) {
        include('/themes/site/js/isotope.pkgd.min.js');

        $(document).ready(function () {
            o.each(function () {
                var _this = this
                    , iso = new Isotope(_this, {
                        itemSelector: '[class*="col-"], .isotope-item',
                        layoutMode: _this.getAttribute('data-layout') ? _this.getAttribute('data-layout') : 'masonry'
                    });

                $(window).on("resize", function () {
                    iso.layout();
                });

                $(window).load(function () {
                    iso.layout();
                    setTimeout(function () {
                        _this.className += " isotope--loaded";
                        iso.layout();
                    }, 600);
                });
            });

            $(".isotope-filters-trigger").on("click", function () {
                $(this).parents(".isotope-filters").toggleClass("active");
            });

            $('.isotope').magnificPopup({
                delegate: ' > :visible .mfp-image',
                type: "image",
                gallery: {
                    enabled: true
                },
            });

            $("[data-isotope-filter]").on("click", function () {
                $('[data-isotope-filter][data-isotope-group="' + this.getAttribute("data-isotope-group") + '"]').removeClass("active");
                $(this).addClass("active");
                $(this).parents(".isotope-filters").removeClass("active");
                $('.isotope[data-isotope-group="' + this.getAttribute("data-isotope-group") + '"]')
                    .isotope({filter: this.getAttribute("data-isotope-filter") == '*' ? '*' : '[data-filter="' + this.getAttribute("data-isotope-filter") + '"]'});
            })
        });
    }
})(jQuery);


/**
 * @module       Onclick functions
 * @description  Add ... to onclick
 */

;
(function ($) {
    var o = $('.timeline');
    if (o.length) {
        $(document).ready(function () {
            o.find(".timeline-btn").on("click", function () {
                $(this).toggleClass("active");
                // o.find(".timeline-hidden").toggleClass("active");
                if (o.find(".timeline-hidden").is(':hidden')) {
                    o.find(".timeline-hidden").slideDown(800);
                } else {
                    o.find(".timeline-hidden").slideUp(800);
                }
            });

            $(window).on('load resize', lineWidth);

        });

        function lineWidth() {
            var col = o.find('.left-part').first();
            if (window.innerWidth > 991) {
                var colRightPosition = col.offset().left + col.outerWidth() - 15, //15px - padding
                    center = (o.offset().left + o.outerWidth()) / 2,
                    width = center - colRightPosition - 3; // 3px - offset from article
            } else {
                var colLeftPosition = col.find('article').offset().left,
                    width = colLeftPosition - 15 - 3; //15px - timeline left position in css, 3px - offset from article
                console.log(colLeftPosition);

            }
            o.find('.line').css('width', width);
        }
    }
})($);


/**
 * @module     RD Input Label
 * @description Enables RD Input Label Plugin
 */
;
(function ($) {
    var o = $('.form-label');
    if (o.length) {
        include('/themes/site/js/jquery.rd-input-label.js');

        $(document).ready(function () {
            o.RDInputLabel();
        });
    }
})(jQuery);

/* Mailform
 =============================================*/
;
(function ($) {
    var o = $('.rd-mailform');
    if (o.length > 0) {
        include('/themes/site/js/jquery.form.min.js');
        include('/themes/site/js/jquery.rd-mailform.min.js');

        $(document).ready(function () {
            var o = $('.rd-mailform');
            if (o.length) {
                o.rdMailForm({
                    validator: {
                        'constraints': {
                            '@LettersOnly': {
                                message: 'Please use letters only!'
                            },
                            '@NumbersOnly': {
                                message: 'Please use numbers only!'
                            },
                            '@NotEmpty': {
                                message: 'Поле не должно быть пустым!'
                            },
                            '@Email': {
                                message: 'Email адрес не валидный!'
                            },
                            '@Phone': {
                                message: 'Enter valid phone number!'
                            },
                            '@Date': {
                                message: 'Use MM/DD/YYYY format!'
                            },
                            '@SelectRequired': {
                                message: 'Please choose an option!'
                            }
                        }
                    }
                }, {
                    'MF000': 'Сообщение отправлено',
                    'MF001': 'Recipients are not set!',
                    'MF002': 'Form will not work locally!',
                    'MF003': 'Please, define email field in your form!',
                    'MF004': 'Please, define type of your form!',
                    'MF254': 'Something went wrong with PHPMailer!',
                    'MF255': 'There was an error submitting the form!'
                    //'MF255': 'Отправляю!'
                });
            }
        });
    }
})(jQuery);

/**
 * @module       FB
 * @description  FB comments
 */
;
(function ($) {
    var o = $('#fb-root');
    if (o.length) {
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }
})(jQuery);


/**
 * @module       Flickr
 * @description  Flickr
 */

(function ($) {
    var o = $('.flickr');
    if (o.length) {
        include('/themes/site/js/rd-flickrfeed.min.js');
        $(document).ready(function () {
            o.RDFlickr({}); // Additional options
        });
    }
})(jQuery);


/**
 * @module       RD Parallax 3.5.0
 * @description  Enables RD Parallax 3.5.0 Plugin
 */

;
(function ($) {
    var o = $('.rd-parallax');
    if (o.length) {
        include('/themes/site/js/jquery.rd-parallax.min.js');
        $(document).ready(function () {
            o.each(function () {
                if (!$(this).parents(".swiper-slider").length) {
                    $.RDParallax();
                }
            });
        });
    }
})(jQuery);




$('#count_downloads').click(function() {
    $.ajax({
        type: 'POST',
        url: '/category/countShow',
        data: {
            id: $(this).attr('data-id')
        },
        success: function(data){
        }
    });
});

$('form .login').click(function (){
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/login',
        data: {
            name: $(this).parent().find('input[name="name"]').val(),
            password: $(this).parent().find('input[name="pass"]').val()
        },
        success: function(data){
            $('.error_box').empty();
            if(data.error) {
                if(data.error.name) {
                    $('.error_box').append('<p>'+data.error.name+'</p>');
                }

                if(data.error.password) {
                    $('.error_box').append('<p>'+data.error.password+'</p>');
                }

                if(data.error.message) {
                    $('.error_box').append('<p>'+data.error.message+'</p>');
                }
            } else {
                $('#myModal').modal('hide')
                $('#regComplete').modal('show')
                setTimeout(reloadPage, 2000);
                function reloadPage() {
                    location.reload('/');
                }

            }
        }
    });

    return false;
});

$('form .reg').click(function (){
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/login/reg',
        data: {
            name: $(this).parent().find('input[name="name"]').val(),
            email: $(this).parent().find('input[name="email"]').val(),
            login: $(this).parent().find('input[name="login"]').val(),
            phone: $(this).parent().find('input[name="phone"]').val(),
            password: $(this).parent().find('input[name="pass"]').val(),
            password2: $(this).parent().find('input[name="pass2"]').val(),
            status: $(this).parent().find('select[name="status"]').val()
        },
        success: function(data){
            $('.error_box_reg').empty();
            if(data.error) {
                if(data.error.login) {
                    $('.error_box_reg').append('<p>'+data.error.login+'</p>');
                }

                 if(data.error.email) {
                    $('.error_box_reg').append('<p>'+data.error.email+'</p>');
                }

                if(data.error.password) {
                    $('.error_box_reg').append('<p>'+data.error.password+'</p>');
                }

                if(data.error.message) {
                    $('.error_box_reg').append('<p>'+data.error.message+'</p>');
                }

                if(data.error.status) {
                    $('.error_box_reg').append('<p>'+data.error.status+'</p>');
                }
            } else {
                $('#myModal').modal('hide')
                $('#regComplete2').modal('show')
                setTimeout(reloadPage, 4000);
                function reloadPage() {
                    location.reload('/');
                }
            }
        }
    });

    return false;
});

$('form .reg_t').click(function (){
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/login/reg_t',
        data: {
            name: $(this).parent().find('input[name="name"]').val(),
            email: $(this).parent().find('input[name="email"]').val(),
            login: $(this).parent().find('input[name="login"]').val(),
            phone: $(this).parent().find('input[name="phone"]').val(),
            password: $(this).parent().find('input[name="pass"]').val(),
            password2: $(this).parent().find('input[name="pass2"]').val()
        },
        success: function(data){
            $('.error_box_reg').empty();
            if(data.error) {
                if(data.error.login) {
                    $('.error_box_reg').append('<p>'+data.error.login+'</p>');
                }

                 if(data.error.email) {
                    $('.error_box_reg').append('<p>'+data.error.email+'</p>');
                }

                if(data.error.password) {
                    $('.error_box_reg').append('<p>'+data.error.password+'</p>');
                }

                if(data.error.message) {
                    $('.error_box_reg').append('<p>'+data.error.message+'</p>');
                }
            } else {
                $('#teachModal').modal('hide');
                $('#regComplete3').modal('show');
                setTimeout(reloadPage, 4000);
                function reloadPage() {
                    location.reload();
                }
            }
        }
    });

    return false;
});

$('form .createThemeModal').click(function (){
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/forum/addTheme',
        data: {
            name: $(this).parent().find('input[name="name"]').val(),
            category: $(this).parent().find('input[name="category"]').val(),
            text: $(this).parent().find('textarea').val()
        },
        success: function(data){
            $('.create_box_theme_err').empty();
            if(data.error) {
                if(data.error.name) {
                    $('.create_box_theme_err').append('<p>'+data.error.name+'</p>');
                }
                if(data.error.text) {
                    $('.create_box_theme_err').append('<p>'+data.error.text+'</p>');
                }
            } else {
                $('#createThemeModal #tab1').html('Тема появится на форуме после модерации');
                setTimeout(reloadPage, 4000);
                function reloadPage() {
                    location.reload();
                }
            }
        }
    });

    return false;
});

/* Удаление изображений */
function imagesRemove(target) {
    $(target).parents('.thumbnail').toggleClass('opacity');
}

$(window).bind('hashchange', function() {
    var hash = window.location.hash.replace('#', '');

    $('.navbar_row.active').removeClass('active');
    $('.sub_s_menu.active').removeClass('active');
    $('[data-link="'+hash+'"]').addClass('active');
    $('[data-menu="'+hash+'"]').addClass('active');
});

$('#myTabs2 li a').click(function() {
    if($(this).hasClass('add_math')) {
        $('input[name="add_math"]').val(1);
    } else {
        $('input[name="add_math"]').val(0);
    }
});

$('.reply_answer').click(function() {
    $('input[name="user_id_to_act"]').val($(this).parent().parent().find('input[name="user_id_m"]').val());
    $('input[name="mess_parent_id"]').val($(this).parent().parent().find('input[name="mess_parent_id_m"]').val());
});

$('.f_messages .right .name a').click(function() {
    CKEDITOR.instances.mess.setData($(this).html() + ', ' );
    //CKEDITOR.instances.mess.insertText( $(this).html() + ', ' );
    $.scrollTo($('.forum_mess_send'), 1000, {offset:-150});
    return false;
});

$(function () {
    $(".datetm").datepicker({
        format: 'DD/MM/YYYY'
    });
});

$('.mess_p_m_act').click(function() {
    if($(this).attr('data-user') != 0) {
        $('.to_u').val($(this).attr('data-user'));
    } else {
        $('.to_u').val(0);
    }
});

$('.sendMessForm').click(function() {
    $('.create_box_m_err').html('');
    if($('#messModal textarea').val() != '') {
        $('form').submit();
    } else {
        $('.create_box_m_err').html('Сообщение не должно быть пустым');
    }
    return false;
});

$(".icons_type a").click(function() {
    $('.icons_type a').removeClass('active');
    $(this).toggleClass('active');
    $('input[name="type_d"]').val($(this).attr('data-type'));
    return false;
});

$("[role='presentation']").click(function() {
    if(!$(this).find('a').hasClass('fa-comment-o')) {
        $('#messModal textarea').val('');
    }
});

$('.profile_form [name="category_id"]').change(function() {
    $.post( "/category/isDoc", {
        'id': $(this).val()
    }, function(data) {
        if(data.result) {
            $('.change_type').fadeIn();
            $('.icons_type').append('<input type="hidden" class="type_d" name="type_d" value="">');
        } else {
            $('.change_type').fadeOut();
            $('.type_d').remove();
        }
    }, 'json');
});

$('#rating_total').rating({
    fx: 'half',
    readOnly: true,
    stars: 6,
    image: '/themes/site/img/stars.png',
    loader: '/themes/site/img/ajax-loader.gif',
    callback: function(responce){
        this.vote_success.fadeOut(2000);
        alert('Спасибо, ваше оценка отправлена на согласование модератору: '+this._data.val);
    }
});


