<!--modal-->
<div tabindex="-1" role="dialog" id="myModal" class="modal fade">
    <!--modal-dialog-->
    <div class="modal-dialog">
        <!--modal-content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
                <ul id="myTabs" role="tablist" class="nav nav-tabs text-center">
                    <li role="presentation" class="active"><a href="#tab0" aria-controls="tab0" role="tab">Авторизация</a></li>
                    <li role="presentation"><a href="#tab1" aria-controls="tab1" role="tab">Регистрация</a></li>
                </ul>
            </div>
            <div class="modal-body text-center">
                <!--Tab panes-->
                <div class="tab-content">
                    <div role="tabpanel" id="tab0" class="tab-pane active fade in">
                        <form method="post">
                            <div class="form-group">
                                <label for="exampleInputEmail11"></label>
                                <input placeholder="Имя" type="text" name="name" id="exampleInputEmail11" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="examplePass"></label>
                                <input placeholder="Пароль" type="password" name="pass" id="examplePass" class="form-control">
                            </div>
                            <div class="error_box"></div>
                            <a href="#" class="btn btn-primary btn sm login">Вход</a>
                            <p><a href="#" class="text-darker text-underline">Забыли пароль?</a></p>
                        </form>
<!--                        <p class="text-darker pull-right">Login with: <a href="#" class="icons fa-facebook text-primary"></a> <a href="#" class="icons fa-twitter text-primary"></a></p>-->
                    </div>
                    <div role="tabpanel" id="tab1" class="tab-pane fade">
                        <form method="post">
                            <div class="form-group">
                                <label for="example1"></label>
                                <input placeholder="ФИО" type="text" name="name" id="example1" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="example2"></label>
                                <input placeholder="EMAIL" type="text" name="email" id="example2" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="example3"></label>
                                <input placeholder="Логин" type="text" name="login" id="example3" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="Pass1"></label>
                                <input placeholder="Телефон" type="text" name="phone" id="Pass1" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="Pass2"></label>
                                <input placeholder="Пароль" type="password" name="pass" id="Pass2" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="Pass3"></label>
                                <input placeholder="Повторите пароль" type="password" name="pass2" id="Pass3" class="form-control">
                            </div>
                            <div class="form-group">
                                <select class=" form-control" id="fid-category_id" name="status" tabindex="3">
                                    <option value="">Выберите статус пользователя</option>
                                    <?php echo HTML::options(Config::$status, 0); ?>
                                </select>
                            </div>
                            <div class="error_box_reg"></div>
                            <a href="#" class="btn btn-primary btn sm reg">Зарегистрироваться</a>
                        </form>
<!--                        <p class="text-darker pull-right">Login with: <a href="#" class="icons fa-facebook text-primary"></a> <a href="#" class="icons fa-twitter text-primary"></a></p>-->
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<div tabindex="-1" role="dialog" id="regComplete" class="modal fade">
    <!--modal-dialog-->
    <div class="modal-dialog">
        <!--modal-content-->
        <div class="modal-content">
            <div class="modal-body text-center">
                <p>Вы вошли в систему</p>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<div tabindex="-1" role="dialog" id="regComplete2" class="modal fade">
    <!--modal-dialog-->
    <div class="modal-dialog">
        <!--modal-content-->
        <div class="modal-content">
            <div class="modal-body text-center">
                <p>Вы зарегестрировались на сайте <?php echo Settings::get('name'); ?></p>
                <p>После подтверждения учетной записи администратором, вы сможете зайти в свой профиль</p>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<div tabindex="-1" role="dialog" id="regComplete3" class="modal fade">
    <!--modal-dialog-->
    <div class="modal-dialog">
        <!--modal-content-->
        <div class="modal-content">
            <div class="modal-body text-center">
                <p>Вы зарегестрировались на сайте <?php echo Settings::get('name'); ?></p>
                <p>После подтверждения учетной записи администратором, вы сможете зайти в свой профиль репетитора</p>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>


