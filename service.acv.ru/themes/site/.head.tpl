<?php  $this->getMata(); ?>
<!--Site Title-->
<meta charset="utf-8">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<link rel="stylesheet" href="/themes/site/css/style.css">
<link rel="stylesheet" href="/themes/site/css/more.css">

<!--[if lt IE 10]>
<script src="/themes/site/js/html5shiv.min.js"></script><![endif]-->

<!--Core Scripts-->
<script src="/themes/site/js/core.min.js"></script>

<script src="/themes/site/js/jquery.rating-2.0.min.js"></script>
<link rel="stylesheet" href="/themes/site/css/jquery.rating.css">
<!--jQuery (necessary for Bootstrap's JavaScript plugins)-->
<!--Include all compiled plugins (below), or include individual files as needed-->
<script src="/themes/site/js/bootstrap.min.js"></script>


<script src="/plugins/ckeditor/ckeditor.js"></script>

<!--<link rel="stylesheet" href="/plugins/tinymce/all.min.css" />-->
