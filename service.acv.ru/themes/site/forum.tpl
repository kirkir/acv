<!DOCTYPE html>
<html lang="ru-RU" class="wide wow-animation">
<head>
    <?php require dirname(__FILE__) . '/.head.tpl'; ?>
</head>
<body>
<?php require dirname(__FILE__) . '/.modals.tpl'; ?>
<div class="wrapper">
    <?php require dirname(__FILE__) . '/.header.tpl'; ?>
    <main class="content">
        <?php echo $this->content; ?>
    </main>
    <?php require dirname(__FILE__) . '/.footer.tpl'; ?>
</div>
</body>
</html>