<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<?php require_once dirname(__FILE__) . '/.head.tpl'; ?>
	<link rel="stylesheet" href="/themes/site/css/frame.css" type="text/css">
</head>
<body>
	<div class="frame_content">
		<?php echo $this->content; ?>	
	</div>
</body>
</html>