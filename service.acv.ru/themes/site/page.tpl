<!DOCTYPE html>
<html lang="ru-RU" class="">
<head>
	<?php require dirname(__FILE__) . '/.head.tpl'; ?>
</head>
<body>
	<div class="wrapper">
		<?php require dirname(__FILE__) . '/.header.tpl'; ?>
		<main class="content">
			<?php echo $this->content; ?>
		</main>
		<?php require dirname(__FILE__) . '/.footer.tpl'; ?>
	</div>
</body>
</html>