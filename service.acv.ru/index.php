<?php
ini_set('upload_max_filesize', 10);
/**
 * Точка входа.
 * @version 5.0
 */
// Инициализация.
require_once dirname(__FILE__) . '/init.php';

// Редирект с index.php
if (@explode('/', $_SERVER['REQUEST_URI'])[1] != Config::$url_admin) {
	redirect('admin_acv');
}

// Редиректы из админки.
$old = trim(Input::getStr($_SERVER['REQUEST_URI']), '/');
if (!empty($old) && $res = DB::getRow("SELECT * FROM `#__redirects` WHERE `old` = ? LIMIT 1", $old)) {
	DB::set("UPDATE `#__redirects` SET `views` = ? WHERE `id` = ?", array($res['views'] + 1, $res['id']));
	echo redirect($res['new']);
}


// Выполнение запрашевоемого модуля.
echo Router::get(@$_SERVER['REQUEST_URI']);
exit();

