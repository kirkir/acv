CKEDITOR.plugins.add('typograf',
{
	init : function(editor) {
		editor.addCommand('typograf', {exec:typofilterProcess});

		editor.ui.addButton( 'typograf', {
			label : 'Оттипографить выделинный текст',
			icon : this.path + 'button.gif',
			command : 'typograf',
			toolbar: 'clipboard'
		});
	}
});

var typofilterProcess = function (e) {
	var selection = e.getSelection();
	if (CKEDITOR.env.ie) {
		selection.unlock(true);
		selection = selection.getNative().createRange().text;
	} else {
		selection = selection.getNative();
	}
	
	var basePath = CKEDITOR.basePath;

	if (selection != '') {
		$.ajax({
			url: CKEDITOR.basePath + '/plugins/typograf/ajax.php',
			type: 'POST',
			data: {text: selection.toString()},
			success: function(data) {
				e.insertHtml(data);
			}
		});
	}
}