<?php

// Локаль.
setlocale(LC_ALL, 'ru_RU');
date_default_timezone_set('Europe/Moscow');
header('Content-type: text/html; charset=utf-8');

if (!empty($_POST['text'])) {
	include_once dirname(__FILE__) . '/typograf.php';
	
	$remoteTypograf = new RemoteTypograf();
	$remoteTypograf->htmlEntities();
	$remoteTypograf->br(false);
	$remoteTypograf->p(true);
	$remoteTypograf->nobr(3);
	$remoteTypograf->quotA('laquo raquo');
	$remoteTypograf->quotB('bdquo ldquo');

	echo $remoteTypograf->processText(stripslashes($_POST['text']));
} else {
	echo '';
}

exit();