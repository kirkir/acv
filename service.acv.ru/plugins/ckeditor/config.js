/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	config.language = 'ru';

	/* Удаление автоматического добовления p. */
	config.autoParagraph = false;
	config.shiftEnterMode = CKEDITOR.ENTER_P;
	config.shiftEnterMode = CKEDITOR.ENTER_BR;

    //config.smiley_path = '/plugins/ckeditor/plugins/smiley/images';

    //config.smiley_images = [
    //    'regular_smile.png','sad_smile.png','wink_smile.png','teeth_smile.png','confused_smile.png','tongue_smile.png',
    //    'embarrassed_smile.png','omg_smile.png','whatchutalkingabout_smile.png','angry_smile.png','angel_smile.png','shades_smile.png',
    //    'devil_smile.png','cry_smile.png','lightbulb.png','thumbs_down.png','thumbs_up.png','heart.png',
    //    'broken_heart.png','kiss.png','envelope.png'
    //];


    config.allowedContent = true;
	config.bodyClass = 'text';
	config.height = 150;
	config.docType = '<!DOCTYPE html>';

	/* CSS */
	config.contentsCss = ['/plugins/normalize.css', '/plugins/ckeditor/contents.css', '/themes/site/css/editor.css'];

	config.extraPlugins = 'smiley,typograf,codemirror,htmlbuttons';
	config.removePlugins = 'elementspath,about,forms,find,pagebreak,save,preview,scayt';
	config.toolbar =
	[
		{ name: 'document',    items : [ 'Source' ] },
		{ name: 'clipboard',   items : [ 'SelectAll','RemoveFormat', 'typograf'] },
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-'] },
		{ name: 'paragraph',   items : [ 'NumberedList','BulletedList' ] },
		{ name: 'paragraph',   items : [ 'Outdent','Indent' ] },
		{ name: 'paragraph',   items : [ 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
		{ name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
		'/',
		{ name: 'styles',      items : [ 'Styles', 'divs', 'Format','Font','FontSize' ] },
		{ name: 'colors',      items : [ 'TextColor','BGColor' ] },
		{ name: 'insert',      items : [ 'Image','Flash','Table','SpecialChar','HorizontalRule', 'CreateDiv', 'Smiley' ] },
		{ name: 'tools',       items : [ 'ShowBlocks', 'Maximize'] }
	];

	/* Подключение ckfinder */
	config.filebrowserBrowseUrl = "/plugins/ckfinder/ckfinder.html";
	config.filebrowserUploadUrl = "/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images";

	/* Дополнительные стили: */
	config.stylesSet = 'my_styles';
};

/* Для HTML 5 */
CKEDITOR.on('instanceReady', function(ev) {
   ev.editor.dataProcessor.writer.selfClosingEnd = '>';
});

/*  */
(function() {
	var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
	if (isChrome) {
		CKEDITOR.on( 'instanceLoaded', function( e ){
			this.addCss('.cke_editable { line-height: normal; }');
		});
	}
})();


//CKEDITOR.stylesSet.add('my_styles',
//[
//	{ name : 'Цитата', element : 'p', attributes : { 'class' : 'blockquote' } },
//	{ name : 'Вступительный текст', element : 'p', attributes : { 'class' : 'introductory' } },
//
//	{ name : 'Буквица', element : 'span', attributes : { 'class' : 'cap' } },
//
//	{ name : '01', element : 'span', attributes : { 'class' : 'n-marker' } },
//	{ name : 'Первая фраза', element : 'span', attributes : { 'class' : 'f-marker' } },
//	{ name : 'Маркеровка текста', element : 'span', attributes : { 'class' : 't-marker' } }
//]);

//CKEDITOR.config.htmlbuttons =	[
//	{
//		name:'divs',
//		icon:'puzzle.png',
//		title:'Элементы верстки',
//		items : [
//			{
//				name: 'nbsp',
//				html: '&nbsp;',
//				title :'Неразрывный пробел'
//			},
//			{
//				name: 'rouble',
//				html: '<span class="rouble">q</span>',
//				title :'Знак рубля'
//			},
//			{
//				name:'col-2',
//				html:'<div class="col-2"><div class="col-2-left"></div><div class="col-2-right"></div></div>',
//				title:'Две колонки'
//			},
//			{
//				name:'btn-more',
//				html: '<a class="btn-more" href="#">подробнее...</a>',
//				title: 'Кнопка «подробнее...»'
//			},
//			{
//				name:'btn_1',
//				html: '<a class="btn_1" href="#">Посчитать на калькуляторе</a>',
//				title: 'Кнопка «Посчитать на калькуляторе»'
//			},
//			{
//				name:'btn_2',
//				html: '<a class="btn_2" href="#">Отправить заказ менеджеру</a>',
//				title: 'Кнопка «Отправить заказ менеджеру»'
//			},
//			{
//				name:'b-caution',
//				html: '<div class="b-caution"><p><span class="f-marker">Внимание!</span> Текст</p></div>',
//				title: 'Блок «Внимание!»'
//			},					{
//				name:'b-idea',
//				html: '<div class="b-idea"><p><span class="f-marker">Идея!</span> Текст</p></div>',
//				title: 'Блок «Идея!»'
//			},
//			{
//				name:'accordion',
//				html: '<div class="accordion"><h3>Заголовок 1</h3><div><p>Текст 1</p></div><h3>Заголовок 2</h3><div><p>Текст 2</p></div><h3>Заголовок 3</h3><div><p>Текст 3</p></div><h3>Заголовок 4</h3><div><p>Текст 4</p></div></div>',
//				title: '«Шторки»'
//			}
//		]
//	}
//];