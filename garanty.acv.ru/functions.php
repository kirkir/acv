<?php
/**
 * Created by PhpStorm.
 * User: kir
 * Date: 19.12.16
 * Time: 23:04
 */

function get_ip() {
    if($_SERVER) {
        if($_SERVER['HTTP_X_FORWARDED_FOR'])
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        elseif($_SERVER['HTTP_CLIENT_IP'])
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        else
            $ip = $_SERVER['REMOTE_ADDR'];
    }
    else {
        if(getenv('HTTP_X_FORWARDED_FOR'))
            $ip = getenv('HTTP_X_FORWARDED_FOR');
        elseif(getenv('HTTP_CLIENT_IP'))
            $ip = getenv('HTTP_CLIENT_IP');
        else
            $ip = getenv('REMOTE_ADDR');
    }

    return $ip;
}

function parse_timestamp( $t = 0 )
{
    $month = floor( $t / 2592000 );
    $day = ( $t / 86400 ) % 30;
    $hour = ( $t / 3600 ) % 24;
    $min = ( $t / 60 ) % 60;
    $sec = $t % 60;

    return array( 'month' => $month, 'day' => $day, 'hour' => $hour, 'min' => $min, 'sec' => $sec );
}

function warranty_end($date_end)
{
    $datetime1 = new DateTime("now");
    $datetime2 = new DateTime(date('d.m.Y', $date_end));
    $interval = $datetime1->diff($datetime2);
    return [
        'year' => $interval->format('%r%y'),
        'month' => $interval->format('%r%M') + $interval->format('%r%y') * 12,
        'day' => $interval->format('%r%D')
    ];
//    echo $interval->format('%r%y лет, %r%M месяцев, %R%D дней, %R%H часов, %R%I минут, %R%S секунд');
}

function setWarrantyTimestamp($months, $import_ship_date)
{
    // забирает текущее время в массив
    $timestamp = $import_ship_date;
    $date_time_array = getdate($timestamp);

    $hours = $date_time_array['hours'];
    $minutes = $date_time_array['minutes'];
    $seconds = $date_time_array['seconds'];
    $month = $date_time_array['mon'];
    $day = $date_time_array['mday'];
    $year = $date_time_array['year'];

    // используйте mktime для обновления UNIX времени
    return $timestamp = mktime($hours,$minutes,$seconds,$month + $months,$day,$year);
}