<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $css = [
        'js/libs/bootstrap/css/bootstrap.min.css',
        'js/libs/bootstrap-select/dist/css/bootstrap-select.css',
        'css/style.css',
    ];
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $js = [
//        'js/script.js',
//        'js/libs/jquery/dist/jquery.min.js',
        'js/libs/bootstrap-select/dist/js/bootstrap-select.min.js',
        'js/libs/bootstrap/js/bootstrap.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
