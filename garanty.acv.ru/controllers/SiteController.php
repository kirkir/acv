<?php

namespace app\controllers;

use app\models\ImportShip;
use app\models\ImportWarranty;
use app\models\Registrylist;
use app\models\Search;
use app\models\Service;
use app\models\Settings;
use DateTime;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\Response;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    CONST ACV = 1;
    CONST RADIJATOR = 2;

    public function getTypes()
    {
        return [
            self::ACV => 'ACV',
            self::RADIJATOR => 'Radijator'
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new Search();

        $items = ImportWarranty::find()->where(['brand' => 'ACV'])->orderBy('name')->all();
        $items = ArrayHelper::map($items,'sku','name');

        return $this->render('index', [
            'brand' => $this->getTypes(),
            'model' => $model,
            'items' => $items
        ]);
    }

    public function actionMakeService()
    {
        $this->layout = 'inner';
        $model = new Service();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            // send mail
            $model->sendEmail(
                'mos@acv-rus.com',
                $model->email,
                "ACV; Ваша заявка в работе",
                "<p>пасибо $model->name, что обратились к нам в компанию. Ваша заявка № "
                . $model->order_name . " принята и назначена в ближайший авторизированный
                 сервисный центр ACV. В ближайшее время с Вами свяжутся.</p>
                 <p>С уважением, коллектив ACV Rus <br>
                  Москва, 8-я ул. Текстильщиков 11, офис 220 <br>
                  Тел. +7 499 272 19 65 <br>
                   www.acv.ru</p>"
                );

            Yii::$app->session->setFlash(
                'success',
                "Спасибо $model->name, Ваша заявка № $model->order_name принята и назначена в ближайший авторизированный сервисный центр ACV. В ближайшее время с Вами свяжется представитель сервисного центра.");
            return $this->redirect(Yii::$app->request->url, 302);
        } else {
            $items = ImportWarranty::find()->orderBy('name')->all();
            $items = ArrayHelper::map($items,'sku','name');

            $regions = Yii::$app->db->createCommand("SELECT * FROM region ORDER BY name")->queryAll();
            $regions = ArrayHelper::map($regions,'id','name');

            $cities = Yii::$app->db->createCommand("SELECT * FROM city WHERE region_id = 0  ORDER BY name")->queryAll();
            $cities = ArrayHelper::map($cities,'id','name');

            return $this->render('make-service', [
                'model' => $model,
                'items' => $items,
                'regions' => $regions,
                'cities' => $cities,
            ]);
        }
    }

    public function actionCallService()
    {
        $this->layout = 'inner';
        $model = new Service();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            // send mail
            $model->sendEmail(
                'mos@acv-rus.com',
                $model->email,
                "ACV; Ваша заявка в работе",
                "<p>пасибо $model->name, что обратились к нам в компанию. Ваша заявка № "
                . $model->order_name . " принята и назначена в ближайший авторизированный
                 сервисный центр ACV. В ближайшее время с Вами свяжутся.</p>
                 <p>С уважением, коллектив ACV Rus <br>
                  Москва, 8-я ул. Текстильщиков 11, офис 220 <br>
                  Тел. +7 499 272 19 65 <br>
                   www.acv.ru</p>"
                );

            Yii::$app->session->setFlash(
                'success',
                "Спасибо $model->name, Ваша заявка № $model->order_name принята и назначена в ближайший авторизированный сервисный центр ACV. В ближайшее время с Вами свяжется представитель сервисного центра.");
            return $this->redirect(Yii::$app->request->url, 302);
        } else {
            $items = ImportWarranty::find()->orderBy('name')->all();
            $items = ArrayHelper::map($items,'sku','name');

            $regions = Yii::$app->db->createCommand("SELECT * FROM region ORDER BY name")->queryAll();
            $regions = ArrayHelper::map($regions,'id','name');

            $cities = Yii::$app->db->createCommand("SELECT * FROM city WHERE region_id = 0  ORDER BY name")->queryAll();
            $cities = ArrayHelper::map($cities,'id','name');

            return $this->render('call-service', [
                'model' => $model,
                'items' => $items,
                'regions' => $regions,
                'cities' => $cities,
            ]);
        }
    }

    public function actionRegistryList()
    {
        $this->layout = 'inner';
        $model = new Registrylist();

        if ($model->load(Yii::$app->request->post())) {
            $model->file1 = UploadedFile::getInstance($model, 'file1');
            $model->file2 = UploadedFile::getInstance($model, 'file2');
            $model->file3 = UploadedFile::getInstance($model, 'file3');

            if ($model->validate() && $model->save(false)) {
                if ($model->id) {
                    $path = realpath(dirname(__FILE__)).'/../web/uploads/registrylist/' . $model->id . '/';
                    if ( !is_dir($path) ) {
                        mkdir( $path );
                    }

                    $model->file1->saveAs( $path . $model->file1);
                    $model->file2->saveAs( $path . $model->file2);
                    if ($model->file3) {
                        $model->file3->saveAs( $path . $model->file3);
                    }

                    $import_ship = ImportShip::find()->where(['serial' => $model->serial])->one();
                    $import_warranty = ImportWarranty::find()->where(['sku' => $model->model])->one();

                    if ($import_ship && $import_warranty) {
    //                    if ($import_warranty->type == 2) {

                        $full = $import_warranty->base + $import_warranty->more;
                        $endWarrantyTimestamp = setWarrantyTimestamp($full, $import_ship->date);

                        $message = "Поздравляем, срок гарантии для Вашего котла продлен на $full месяцев. Обращаем Ваше внимание, что для сохранения гарантии необходимо осуществлять правильную эксплуатацию и своевременное обслуживание оборудования. Подробнее с гарантийными условиями Вы можете ознакомиться по <a href='http://warranty.acv.ru/uploads/Warranty%20condition%2003042017.pdf' target='_blank'>ссылке</a>";

                        // send mail
                        $service = new Service();
                        $service->sendEmail(
                            'mos@acv-rus.com',
                            $model->email,
                            "ACV; регистрация пуска котла:",
                            "<p>$message</p>"
                        );

                        Yii::$app->db->createCommand()->update('z_import_ship', ['start_pusk' => time(), 'end_warranty' => $endWarrantyTimestamp], ['id' => $import_ship->id])->execute();
                        Yii::$app->session->setFlash(
                            'success',
                            $message);
    //                    }
                    } else {
                        Yii::$app->session->setFlash(
                            'warning',
                            "Cерийного номера не существует среди проданного оборудования! 
                    Регистрация выполнена частично, свяжитесь с тех поддержкой ACV!
                    ");
                    }
                }
            }
           return $this->redirect(Yii::$app->request->url, 302);
        } else {
            $items = ImportWarranty::find()->orderBy('name')->all();
            $items = ArrayHelper::map($items,'sku','name');

            $regions = Yii::$app->db->createCommand("SELECT * FROM region ORDER BY name")->queryAll();
            $regions = ArrayHelper::map($regions,'id','name');

            $cities = Yii::$app->db->createCommand("SELECT * FROM city WHERE region_id = 0  ORDER BY name")->queryAll();
            $cities = ArrayHelper::map($cities,'id','name');


            return $this->render('registry-list', [
                'model' => $model,
                'items' => $items,
                'regions' => $regions,
                'cities' => $cities,
            ]);
        }
    }

    public function actionCheckSerial()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $serial = Yii::$app->request->post('Service')['serial'];
        $sku = Yii::$app->request->post('Service')['model'];

        if (Yii::$app->request->isAjax && $serial && $sku) {
            if ($item = ImportShip::find()->where(['serial' => $serial])->andWhere(['sku' => $sku])->with('importWarranty')->one()) {
                $date = '';
                if (isset($item->end_warranty)) {
                    $date = warranty_end($item->end_warranty);
                }
                return ['data' => $item, 'date' => $date];
            } else {
                return ['success' => false];
            }
        } else {
            return false;
        }
    }

    public function actionCheckRegistrySerial()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $serial = Yii::$app->request->post('Registrylist')['serial'];
        $sku = Yii::$app->request->post('Registrylist')['model'];

        if (Yii::$app->request->isAjax && $serial && $sku) {
            if ($item = ImportShip::find()->where(['serial' => $serial])->andWhere(['sku' => $sku])->with('importWarranty')->one()) {
                $date = '';
                if ($item->end_warranty) {
                    $date = warranty_end($item->end_warranty);
                } else {
                    return false;
                }
                return ['data' => $item, 'date' => $date];
            } else {
                return ['success' => false];
            }
        } else {
            return false;
        }
    }

    public function actionLoadCityByRegion($id)
    {
        $cities = Yii::$app->db->createCommand("SELECT id, name FROM city WHERE region_id = :region_id  ORDER BY name")
            ->bindParam(':region_id', $id)
            ->queryAll();

        $option = '';
        foreach ($cities as $city) {
            $option .= '<option value="'.$city['id'].'">'.$city['name'].'</option>';
        }
        return $option;
    }

    public function actionChangeBrandList($id)
    {
        $items = ImportWarranty::find()->where(['brand' => $this->getTypes()[$id]])->all();

        $option = '';
        foreach ($items as $item) {
            $option .= '<option value="'.$item['sku'].'">'.$item['name'].'</option>';
        }
        return $option;
    }

    public function actionSearch()
    {
        $this->layout = 'search';

        $model = new Search();
        $callback = new ContactForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $serial = $model->serial;
            $sku = $model->name;

            $data = ImportShip::find()->where(['serial' => $serial])->andWhere(['sku' => $sku])->with('importWarranty')->one();
            $date_ship = $data['date'];


            if ($date_ship && $data->end_warranty == null) {
                $data->end_warranty = strtotime(date('Y-M-d', $date_ship) . '+' . $data->importWarranty->base . ' month');
                $data->save();
            }

            // check TO
            $checkFalse = false;
            if ($data && $data->importWarranty->type == 2) {
                $checkMaxToDate = strtotime(date('Y-M-d', $date_ship) . '+ 13 month');
                if ($checkMaxToDate <= time()) {
                    if (!$checkItem = $data->checkTo($data['serial'])) {
                        $checkFalse = true;
                    }
                }
            }


            $date = '';
            if (isset($data->end_warranty)) {
                $date = warranty_end($data->end_warranty);
            }

            return $this->render('search', [
                'data' => $data,
                'date' => $date,
                'model' => $model,
                'callback' => $callback,
                'checkFalse' => $checkFalse,
            ]);
        }
        return $this->redirect(Yii::$app->homeUrl, 302);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionCallback()
    {
        if (Yii::$app->request->post()) {

            $form = Yii::$app->request->post('ContactForm');
            $name = $form['name'];
            $email = $form['email'];
            $text = $form['email'];

            $body = "<p>Имя: $name</p>";
            $body .= "<p>Email: $email</p>";
            $body .= "<p>Сообщение: $text</p>";

            $settings = Settings::find()->limit(1)->one();

            $fileMovePath = '';
            if ($_FILES['file']['name']) {
                $file = $_FILES['file']['tmp_name'];
                $fileMovePath = 'uploads/' . $_FILES['file']['name'];
                move_uploaded_file($file, $fileMovePath);
            }

            $mail = Yii::$app->mailer->compose()
                ->setFrom($email)
                ->setTo($settings['email'])
                ->setSubject('Обратная связь ACV - проверка гарантийных сроков')
                ->setHtmlBody($body);

            if ($fileMovePath) {
                $mail->attach($fileMovePath);
            }

            if ($mail->send())
            {
                if ($fileMovePath) {
                    unlink($fileMovePath);
                }
                return $this->goHome();
            }
        }
    }
}

