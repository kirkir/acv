<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "z_import_ship".
 *
 * @property integer $id
 * @property integer $date
 * @property string $serial
 * @property string $sku
 * @property string $contragent
 */
class ImportShip extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'z_import_ship';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'integer'],
            [['serial', 'sku', 'contragent'], 'string', 'max' => 255],
            [['serial'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'serial' => 'Serial',
            'sku' => 'Sku',
            'contragent' => 'Contragent',
        ];
    }

    public function getImportWarranty()
    {
        return $this->hasOne(ImportWarranty::className(), ['sku' => 'sku']);
    }

    public function checkTo($serial)
    {
        return Yii::$app->db->createCommand("SELECT * FROM z_registrylist WHERE serial = :serial", [':serial' => $serial])->queryOne();
    }
}
