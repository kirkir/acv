<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "z_import_warranty".
 *
 * @property integer $id
 * @property string $sku
 * @property string $name
 * @property integer $base
 * @property integer $more
 * @property integer $electric
 */
class ImportWarranty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'z_import_warranty';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['base', 'more', 'electric'], 'integer'],
            [['sku', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sku' => 'Sku',
            'name' => 'Name',
            'base' => 'Base',
            'more' => 'More',
            'electric' => 'Electric',
        ];
    }
}
