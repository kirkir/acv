<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "z_settings".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $email_name
 * @property string $email_signature
 * @property integer $date_edit
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'z_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'email_name', 'email_signature'], 'required'],
            [['email_signature'], 'string'],
            [['date_edit'], 'integer'],
            [['name', 'email', 'email_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'email_name' => 'Email Name',
            'email_signature' => 'Email Signature',
            'date_edit' => 'Date Edit',
        ];
    }
}
