<?php

namespace app\models;


use yii\base\Model;

class Search extends Model
{

    public $serial;
    public $name;
    public $brand;

    public function rules()
    {
        return [
            [['serial', 'brand'], 'required'],
            [['serial', 'name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'serial' => 'Серийный номер',
            'name' => 'Наименование товара',
            'brand' => 'Бренд',
        ];
    }
}