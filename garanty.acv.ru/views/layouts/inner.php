<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <?= $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => '/images/favicon.ico']); ?>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
<body>
<?php $this->beginBody() ?>

<div class="wrapper">
    <div class = 'row'>
        <main id = 'inner'>
            <div class="col-lg-12">
                <br>
                <?php if(Yii::$app->session->hasFlash('success')): ?>
                    <div class="alert alert-success" role="alert">
                        <?= Yii::$app->session->getFlash('success') ?>
                    </div>
                <?php endif; ?>
                <?php if(Yii::$app->session->hasFlash('warning')): ?>
                    <div class="alert alert-warning" role="alert">
                        <?= Yii::$app->session->getFlash('warning') ?>
                    </div>
                <?php endif; ?>
            </div>
            <?= $content; ?>
        </main>

        <footer id = 'footer'>
            <div class = 'col-md-6 col-sm-6 col-xs-7'>
                <section class = 'guarantee'>
                    <a href="/"> <img src="images/term-img.png" alt="term-img"> Назад в поиск</a>
                </section>
            </div>
            <div class = 'col-md-6 col-sm-6 col-xs-5'>
                <section class = 'logo'>
                    <a href="#"><img src="images/logo.png" alt = 'logo'></a>
                </section>
            </div>
        </footer>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
