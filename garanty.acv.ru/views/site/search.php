<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;

$this->title = 'Проверка гарантии';
?>

<h1>Проверка гарантии:</h1>
<section class = 'info-box'>
    <?php if ($checkFalse): ?>
        <h2>Гарантия приостановлена  в связи с отсутствием Технического обслуживания</h2>
    <?php else: ?>
        <h2><?= ($data) ? $data->importWarranty->name : 'Ничего не найдено'; ?></h2>
        <?php if ($date): ?>
            <?php if ($date['month'] < 1 && $date['day'] < 1): ?>
                <div class = 'term'>
                    <h4 style="color: #787878;">Гарантийный срок истек <br><br> <?= date('d.m.Y', $data->end_warranty) ?></h4>
                </div>
            <?php else: ?>
                <?php if ($date['month'] >= 1): ?>
                    <div class = 'term'>
                        <p class = 'number'>  <?php echo $date['month']; ?></p><span class = 'string'>месяцев</span>
                    </div>
                <?php endif; ?>
                <div class = 'term'>
                    <p class = 'number2'>  <?php echo $date['day']; ?></p><span class = 'string'>дней</span>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>

    <a href="<?= \yii\helpers\Url::to(Yii::$app->homeUrl); ?>"> <img src="images/term-img.png" alt = 'term-img'> Назад в поиск</a>
</section>

<?php Modal::begin([
    'header' => 'Обратная связь',
]);

$form = ActiveForm::begin([
    'id' => 'contact-form',
    'action' => 'callback',
    'options' => [
        'enctype' => 'multipart/form-data'
    ],
]);
?>
<p class="title"></p>
<?= $form->field($callback, 'name')->textInput(['autofocus' => true]) ?>

<?= $form->field($callback, 'email') ?>

<?= $form->field($callback, 'body')->textarea(); ?>

<div class="">
    <?= Html::fileInput('file');  ?>
</div>
<br>
<div class="">
    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']);  ?>
</div>

<?php ActiveForm::end() ?>
<?php Modal::end(); ?>

<?php
$script = <<< JS
    $('.rules').on('click', function() {
        $('.modal').modal('show');
        return false;
    })
JS;
echo $this->registerJs($script, yii\web\View::POS_LOAD);
?>
