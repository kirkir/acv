<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = 'Страница не найдена';
?>
<div class="site-error">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        Пожалуйста, попробуйте <a href="<?= \yii\helpers\Url::to('/') ?>">позже</a>.
    </p>
</div>
