<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Проверка гарантии';
?>


<h1>Проверка гарантии:</h1>
<section class = 'form-search'>
        <?php
        $form = ActiveForm::begin([
            'id' => 'search-form',
            'action' => 'search',
            'options' => [
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data'
            ],
        ]);
        ?>

        <div class="control-group">
            <?= $form->field($model, 'brand')->dropDownList($brand, ['class' => 'selectpicker']); ?>
        </div>

        <div class="control-group">
           <?= $form->field($model, 'serial')->textInput(['style' => 'margin: 0 auto;']); ?>
        </div>

        <div class="control-group">
            <?= $form->field($model, 'name')->dropDownList($items, ['class' => 'selectpicker', 'data-live-search' => 'true']); ?>
        </div>
        <div class = 'button-search'>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn">Найти</button>
                </div>
            </div>
        </div>
    <?php ActiveForm::end() ?>
</section>
<section class="links-orders col-md-5 col-sm-6 col-xs-12">
    <div class="link-orders-block">
        <a href="<?= Url::to('/make-service') ?>">Провести сервисное обслуживание <br> оборудования</a>
    </div>
    <div class="link-orders-block">
        <a href="<?= Url::to('/call-service') ?>">Вызов специалиста при поломке <br> оборудования</a>
    </div>
    <div class="link-orders-block">
        <a href="<?= Url::to('/registry-list') ?>">Произвести регистрацию пуска <br> котла</a>
    </div>
</section>

<?php
$script = <<< JS
    $('#search-brand').on('change', function () {
        $.ajax({
            url: '/change-brand-list',
            dataType: 'html',
            type: 'get',
            data: {id: $(this).val()},
            success: function (data) {
            $('#search-name').empty();
            $('#search-name').html(data);
            $('#search-name').selectpicker('refresh');
            }
        });
    }) 
JS;
echo $this->registerJs($script, yii\web\View::POS_LOAD);
?>


