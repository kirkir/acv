<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\MaskedInput;

$this->title = 'Провести сервисное обслуживание оборудования';

$this->params['breadcrumbs'][] = $this->title;
echo Breadcrumbs::widget([
    'homeLink' => ['label' => 'Проверка гарантии', 'url' => '/'],
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]);
?>

<div class="service-form">
    <h1>Провести сервисное обслуживание оборудования:</h1>
    <section class = 'form-search form-full'>
            <?php
            $form = ActiveForm::begin([
                'id' => 'search-form',
                'enableAjaxValidation' => true,
                'action' => 'make-service',
                'validationUrl' => 'check-serial',
                'options' => [
                    'class' => 'form-horizontal',
                    'enctype' => 'multipart/form-data'
                ],
            ]);
            ?>
            <div class="row">
                <div>
                    <strong>Введите персональные данные:</strong>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="control-group">
                        <?= $form->field($model, 'name')->textInput(['style' => 'margin: 0 auto;', 'placeholder' => $model->getAttributeLabel('name')])->label(''); ?>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="control-group">
                        <?= $form->field($model, 'surname')->textInput(['style' => 'margin: 0 auto;', 'placeholder' => $model->getAttributeLabel('surname')])->label(''); ?>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="control-group">
                        <?= $form->field($model, 'last_name')->textInput(['style' => 'margin: 0 auto;', 'placeholder' => $model->getAttributeLabel('last_name')])->label(''); ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="control-group">
                        <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => '+7(999)999-99-99',
                            'options'=>[
                                'placeholder' => $model->getAttributeLabel('phone'),
                                'class'=>'form-control tel_input',
                                'style' => 'margin: 0 auto;'
                            ],
                        ])->label('') ?>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="control-group">
                        <?= $form->field($model, 'email')->textInput(['style' => 'margin: 0 auto;', 'placeholder' => $model->getAttributeLabel('email')])->label(''); ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div><strong>Введите данные об оборудовании:</strong></div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="control-group">
                        <?= $form->field($model, 'model')->dropDownList($items, ['class' => 'selectpicker', 'data-live-search' => 'true'])->label(''); ?>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="control-group">
                        <?= $form->field($model, 'serial', ['enableAjaxValidation' => true])->textInput(['style' => 'margin: 0 auto;', 'placeholder' => 'Введите серийный номер'])->label(''); ?>
                    </div>
                </div>
            </div>

            <div class="row warranty_data" style="display:none;">
                <div><strong>Данные о гарантии:</strong></div>
                <div><strong>До окончания гарантийного срока осталось:</strong></div>
                <section class="info-box">
                    <div class = 'term'>
                        <p class = 'number'></p><span class = 'string'>&nbsp;&nbsp;&nbsp;&nbsp;месяцев</span>
                    </div>
                    <div class = 'term'>
                        <p class = 'number2'></p><span class = 'string'>&nbsp;&nbsp;&nbsp;&nbsp;дней</span>
                    </div>
                </section>
                <br>
                <br>
            </div>

            <div class="row warranty_hidden" style="<?= ($model->serial) ? ' '  : ' display: none' ?>">
                <div><strong>Данные о гарантии:</strong></div>
                <div class="col-xs-12">
                    <div class="control-group">
                        <?= $form->field($model, 'warranty_item')->textInput(['style' => 'margin: 0 auto; width: 80%', 'placeholder' => 'Введите номер гарантийного талона'])->label(''); ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div><strong>Место установки оборудования:</strong></div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="control-group">
                        <?= $form->field($model, 'region_id')->dropDownList($regions, ['class' => 'selectpicker', 'data-live-search' => 'true', 'options' =>[ '0' => ['selected' => true]]])->label(''); ?>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="control-group">
                        <?= $form->field($model, 'city_id')->dropDownList($cities, [
                            'class' => 'selectpicker',
                            'data-live-search' => 'true',
                            'options' =>[ '1' => ['selected' => true]]
                        ])->label(''); ?>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="control-group">
                        <?= $form->field($model, 'street')->textInput(['style' => 'margin: 0 auto;', 'placeholder' => $model->getAttributeLabel('street')])->label(''); ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="control-group">
                        <?= $form->field($model, 'acordul_tc')
                            ->checkbox([
                                'label' => '<a target="_blank" href="/uploads/personal_agree.pdf">Подтверждаю свое согласие на обработку персональных данных</a>',
                                'checked' => false,
                            ]);
                        ?>
                    </div>
                </div>
            </div>

            <div class = 'button-search'>
                <div class="control-group">
                    <div class="controls">
                        <button style="width: 25%" type="submit" class="btn">Отправить</button>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end() ?>
    </section>
</div>


<?php

$this->registerJs("
    $('#search-form').on('ajaxComplete', function (e, attribute, message) {
    console.log(attribute.responseJSON);
       if(attribute.responseJSON.success == false) {
            $('.warranty_hidden').show();
            $('.warranty_data').hide();
        } else if (attribute.responseJSON == false) {
            $('.warranty_data').hide();
        } else {
            $('.warranty_data').show();
            $('.warranty_hidden').hide();
            if (attribute.responseJSON.date) {
//                if (attribute.responseJSON.date.month >=1) {
                    $('.warranty_data .number').html(attribute.responseJSON.date.month);
//                } else {
//                    $('.warranty_data .number').remove();
//                }
                $('.warranty_data .number2').html(attribute.responseJSON.date.day);
            }
        }  
    });
    
    
    // update city list    
    $('#service-region_id').on('change', function () {
        $.ajax({
        url: '/load-city-by-region',
        dataType: 'html',
        type: 'get',
        data: {id: $(this).val()},
        success: function (data) {
            $('#service-city_id').empty();
            $('#service-city_id').html(data);
            $('#service-city_id').selectpicker('refresh');
        }
      });
    })  
    "
);

