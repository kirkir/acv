<?php
/**
 * Created by PhpStorm.
 * User: kir
 * Date: 05.10.17
 * Time: 14:26
 */

namespace app\commands;


use app\models\Service;
use app\models\User;
use yii\console\Controller;
use yii\helpers\Url;

class ServiceController extends Controller
{

    public function actionSendApplications()
    {
        if ($services = Service::find()->where(['sended' => false])->all()) {
            foreach ($services as $service) {
                $services_centr = User::getUsersByRegion($service->region_id, $service->city_id);
                if ($services_centr) {
                    foreach ($services_centr as $item) {
                        $emails = explode(';', $item['email']);
                        foreach ($emails as $email) {
                            if (trim($email)) {
                                $service->sendEmail(
                                    'mos@acv-rus.com',
                                    $email,
                                    "Появилась новая заявка, необходимо связаться с клиентом и совершить выезд",
                                    "<p>
                                        Появилась новая заявка, необходимо связаться с клиентом и совершить выезд  <a href='".Url::to('http://service.acv.ru/admin_acv/service/edit/' . $service->id)."'>Ссылка на заявку</a>
                                    </p>"
                                );
                            }
                        }
                    }
                    $service->sended = 1;
                    $service->save(false);
                }
            }
        }
    }

    public function actionSetLate()
    {
        if ($data = Service::find()->where(['to_user_id' => false])->all()) {
            foreach ($data as $item) {
                $item->late = 1;
                $item->save(false);
            }
            return true;
        }
    }
}