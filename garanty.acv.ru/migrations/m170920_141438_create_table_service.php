<?php

use yii\db\Migration;
use yii\db\Schema;

class m170920_141438_create_table_service extends Migration
{
    public function up()
    {
        $this->createTable('z_service', [
            'id' => Schema::TYPE_PK,
            'order_name' => Schema::TYPE_STRING . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'surname' => Schema::TYPE_STRING . ' NOT NULL',
            'last_name' => Schema::TYPE_STRING . ' NOT NULL',
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'phone' => Schema::TYPE_STRING . ' NOT NULL',
            'model' => Schema::TYPE_STRING . ' NOT NULL',
            'serial' => Schema::TYPE_STRING . ' NOT NULL',
            'warranty_item' => Schema::TYPE_STRING . ' NOT NULL',
            'region_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'city_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'street' => Schema::TYPE_STRING . ' NOT NULL',
            'event' => Schema::TYPE_STRING . ' NOT NULL',
            'content' => Schema::TYPE_TEXT,
            'to_user_id' => Schema::TYPE_INTEGER . ' DEFAULT 0',
            'late' => Schema::TYPE_INTEGER . ' DEFAULT 0',
            'date_add' => Schema::TYPE_INTEGER,
            'date_edit' => Schema::TYPE_INTEGER,
            'process' => Schema::TYPE_INTEGER . ' DEFAULT NULL',
            'sended' => Schema::TYPE_INTEGER . ' DEFAULT 0',
        ]);
    }

    public function down()
    {
        $this->dropTable('z_service');
    }
}
