<?php

use yii\db\Migration;
use yii\db\Schema;

class m171101_104511_alter_table_registrylist extends Migration
{

    public $table = 'z_registrylist';

    public function up()
    {
        $this->addColumn($this->table, 'name_service', Schema::TYPE_STRING);
        $this->addColumn($this->table, 'name_pusk_start', Schema::TYPE_STRING);
        $this->addColumn($this->table, 'phone', Schema::TYPE_STRING);
        $this->addColumn($this->table, 'region_id', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->addColumn($this->table, 'city_id', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->addColumn($this->table, 'street', Schema::TYPE_STRING . ' NOT NULL');

    }

    public function down()
    {
        $this->dropColumn($this->table, 'name_service');
        $this->dropColumn($this->table, 'name_pusk_start');
        $this->dropColumn($this->table, 'phone');
        $this->dropColumn($this->table, 'region_id');
        $this->dropColumn($this->table, 'city_id');
        $this->dropColumn($this->table, 'street');
    }


}
